import base64
import logging

from Crypto import Random
from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes

LOG = logging.getLogger('DataEncrypt')
LOG.setLevel(logging.INFO)


class DataEncryptEAX:
    """
    Encrypt data using the AES and RSA combined
    """

    def __init__(self, en_pu_key=None, st_pu_key: str = None):
        self.aes_key = get_random_bytes(16)
        if en_pu_key:
            decoded = base64.b64decode(en_pu_key[2:].encode('utf-8'))
            try:
                self.pub_key = RSA.importKey(decoded)
            except Exception as e:
                LOG.error("ERROR: error while creating the public key")
                raise e
        if st_pu_key:
            try:
                self.pub_key = RSA.importKey(st_pu_key)
            except Exception as e:
                LOG.error("ERROR: error while creating the public key")
                raise e

    def encrypt_aes(self):
        """
        Encrypts the AES key by the RSA with the given public key
        :return: encoded - encrypted AES key
        """
        cipher_rsa = PKCS1_OAEP.new(self.pub_key)
        return base64.b64encode(cipher_rsa.encrypt(base64.b64encode(self.aes_key)))

    def encrypt_data(self, data):
        """
        Encrypts the given data(text) with the AES in EAX mode
        :param data: text to be encrypted
        :return: encoded - encrypted text and tags to de-cypher the text
        """
        data = data.encode("utf-8")
        cipher_aes = AES.new(self.aes_key, AES.MODE_EAX)
        cipher, tag = cipher_aes.encrypt_and_digest(data)
        return base64.b64encode(cipher_aes.nonce), base64.b64encode(tag), base64.b64encode(cipher)

    def decrypt_data(self, p_k: str, e_aes_key: str, nonce: str, cipher: str, tag: str):
        """
        Decrypts the encrypted data with the given private key and tags
        :param p_k: private key
        :param e_aes_key: encrypted aes key - string of bytes
        :param nonce:
        :param cipher: encrypted data - string of bytes
        :param tag:
        :return: decrypted text - string of bytes
        """
        p_k = RSA.import_key(p_k)
        cipher_rsa = PKCS1_OAEP.new(p_k)
        aes_key = base64.b64decode(cipher_rsa.decrypt(base64.b64decode(e_aes_key)))
        cipher_aes = AES.new(aes_key, AES.MODE_EAX, base64.b64decode(nonce))
        return cipher_aes.decrypt_and_verify(base64.b64decode(cipher), base64.b64decode(tag)).decode("utf-8")

    def encrypt(self, text):
        """
        Encrypts the given text
        :param text: text
        :return: encrypted aes key, encrypted text and the tags
        """
        e_aes_key = self.encrypt_aes()
        nonce, tag, cipher = self.encrypt_data(text)
        return e_aes_key, nonce, tag, cipher


class DataEncryptCBC:

    def __init__(self, pub_key):
        self.pub_key = pub_key

    def pad(self, s):
        return s + b"\0" * (AES.block_size - len(s) % AES.block_size)

    def encrypt_aes(self):
        self.key = get_random_bytes(16)
        cipher_rsa = PKCS1_OAEP.new(self.pub_key)
        return base64.b64encode(cipher_rsa.encrypt(base64.b64encode(self.key)))

    def encrypt_data(self, message):
        message = self.pad(message)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return base64.b64encode(iv), base64.b64encode(cipher.encrypt(message))

    def decrypt_data(self, iv, ciphertext, p_k, e_aes_key):
        cipher_rsa = PKCS1_OAEP.new(p_k)
        session_key = base64.b64decode(cipher_rsa.decrypt(base64.b64decode(e_aes_key)))
        iv = base64.b64decode(iv)
        ciphertext = base64.b64decode(ciphertext)
        cipher = AES.new(session_key, AES.MODE_CBC, iv)
        plaintext = cipher.decrypt(ciphertext)
        return plaintext.rstrip(b"\0").decode("utf-8")


def test_eax_crypt(text):
    key = RSA.generate(2048)

    public_key = RSA.import_key(key.publickey().export_key())
    private_key = RSA.import_key(key.export_key())

    encryptor = DataEncryptEAX(public_key)
    e_aes_key = encryptor.encrypt_aes()
    nonce, tag, ciphertext = encryptor.encrypt_data(text)
    print(str(ciphertext))
    res = encryptor.decrypt_data(private_key, e_aes_key, nonce, ciphertext, tag)
    print(res)


def test_cbc_crypt(text):
    key = RSA.generate(2048)

    public_key = RSA.import_key(key.publickey().export_key())
    private_key = RSA.import_key(key.export_key())

    encryptor = DataEncryptCBC(public_key)
    e_aes_key = encryptor.encrypt_aes()
    iv, crypted = encryptor.encrypt_data(text)
    new_text = encryptor.decrypt_data(iv, crypted, private_key, e_aes_key)
    print(new_text)
