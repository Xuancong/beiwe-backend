#!/usr/bin/env python3

import os, sys, re, uuid, io, traceback, argparse
from datetime import datetime, timedelta, time
from contextlib import redirect_stdout

import boto3, jinja2, psycopg2
import pandas as pd
import firebase_admin
from firebase_admin import credentials, messaging
from flask import Flask, render_template, redirect, request, session, abort
from raven.contrib.flask import Sentry
from werkzeug.middleware.proxy_fix import ProxyFix
from config import load_django
from api import *
from config.constants import CHECKABLE_FILES
from config.settings import *
from libs.admin_authentication import is_logged_in
from libs.security import set_secret_key
from database.models import Researcher, Study, Participant
from api import survey_api

sys.fcm_app = firebase_admin.initialize_app()


# MAIN START
def test2(patient_id, push_immediately):
	obj = { "content":[
				{"display_if": None, "max": "10", "min": "1", "question_id": "1f61ee06-82f0-4134-9e45-3588db4bfab0", "question_text": "What is your energy level?", "question_type": "slider"},
			    {"display_if": {"<": ["1f61ee06-82f0-4134-9e45-3588db4bfab0", 5]}, "play_time_limits": "", "question_id": "d3eeec6c-4ee6-476a-a8c4-2f6d27548101", "question_text": "https://www.mindline.sg/?wysa_tool_id=deep_relaxation", "question_type": "web_survey", "webview_settings": "{}"},
				{"display_if": {"<": ["1f61ee06-82f0-4134-9e45-3588db4bfab0", 5]}, "question_id": "6eecef18-c5c1-446a-9c38-7246f1924c4a", "question_text": "Thank you for visiting the website!", "question_type": "info_text_box"},
				{"display_if": {">=": ["1f61ee06-82f0-4134-9e45-3588db4bfab0", 5]}, "question_id": "4c25831f-c75a-413e-80f4-5f40c0ad0b42", "question_text": "Great, keep it up!", "question_type": "info_text_box"}
			],
			"settings":{ "trigger_on_first_download": False,
						 "survey_notibar_title": "HOPES test push survey",
						 "survey_notibar_desc": "Hello",
						 "allow_notibar_dismiss": True,
						 "allow_navigate_back": True,
						 "randomize": False,
						 "randomize_with_memory": False,
						 "number_of_random_questions": None},
			"expiry": "10000",
			"name": "anything"
		  }
	res, msg = survey_api.send_push_stack_survey(obj, patient_id, push_immediately = push_immediately)
	print(msg, file = sys.stderr)
	return res


def test():
	if False:
		res = survey_api.add_patients_to_survey(['m6lvjzau', 'svw57un9'], '6q56SUIVWzQRGkMMasSkldBK', True)
		res = survey_api.add_patients_to_survey('ubkeormn', '6q56SUIVWzQRGkMMasSkldBK')
	elif False:
		res = survey_api.add_patients_to_survey('m6lvjzau', '6q56SUIVWzQRGkMMasSkldBK')
	elif True:
		res = survey_api.add_patients_to_survey('m6lvjzau', 'tSwUYa6OkqQkn7hiOVjwkiqx', True)
	elif False:
		res = survey_api.del_patients_from_survey('m6lvjzau', 'tSwUYa6OkqQkn7hiOVjwkiqx')
	return res


if __name__ == '__main__':
	parser = argparse.ArgumentParser(usage = '$0 survey_id patient_id\nusage2: $0 patient_id', description = 'push a survey to a patient',
	                                 formatter_class = argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument('survey_id', help = 'survey id')
	parser.add_argument('patient_id', help = 'patient id', nargs = '*')
	parser.add_argument('--immediate', '-i', help = 'send push notification immediately', action = 'store_true')
	# nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=

	opt = parser.parse_args()
	globals().update(vars(opt))

	if not patient_id:
		print(test2(survey_id, immediate))
	else:
		print(survey_api.add_patients_to_survey(patient_id, survey_id, immediate))
