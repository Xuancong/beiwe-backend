"use strict";

const AWS = require("aws-sdk");
const csv = require("csvtojson");
const s3 = new AWS.S3();

module.exports.extract = async event => {
  const Key = decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, " "));
  if (!(Key.includes("identifiers_") && (Key.split("/")).length===3)) return;

  let [, pId, ] = Key.split("/");
  const timestamp = event.Records[0].eventTime;

  const headers = [
    "patientId",
    "registrationTime"
  ];

  const DestinationBucket = process.env.DEST_BUCKET || "nikola-test-prefix-metrics";
  const DestinationKey = `registration_metric.csv`;

  try {
    await s3
      .headObject({ Bucket: DestinationBucket, Key: DestinationKey })
      .promise();
  } catch (e) {
    await s3
      .putObject({
        Bucket: DestinationBucket,
        Key: DestinationKey,
        ACL: "private",
        Body: `${headers.join(",")}\n`
      })
      .promise();
  }

  const todaysFile = await s3
    .getObject({ Bucket: DestinationBucket, Key: DestinationKey })
    .promise();

  const parser = csv({ headers, checkType: true });
  const lines = await parser.fromString(todaysFile.Body.toString());
  const newLine = {};
  headers.map(key => (newLine[key] = ""));
  lines.push({
    ...newLine,
    patientId: `${pId}`,
    registrationTime: `${timestamp}`
  });

  await s3
    .putObject({
      Bucket: DestinationBucket,
      Key: DestinationKey,
      ACL: "private",
      Body: `${headers.join(",")}\n${lines
        .map(line =>
          Object.keys(line)
            .map(f => line[f])
            .join(",")
        )
        .join("\n")}`
    })
    .promise();

  return { message: "registration successfully processed", event };
};
