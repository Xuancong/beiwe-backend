import os, re, smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.message import EmailMessage
from bs4 import BeautifulSoup


is_email_string = lambda s: re.match(r'.+@.+$', s)

def send_email_text(msg_text, subject, addr_from, addr_to, smtp_info):
	msg = EmailMessage()
	msg.set_content(msg_text)

	msg['Subject'] = subject
	msg['From'] = addr_from
	msg['To'] = addr_to

	# Send the message via our own SMTP server.
	smtp = smtplib.SMTP(smtp_info['host'], smtp_info['port'])
	smtp.ehlo()
	smtp.starttls()
	smtp.ehlo()
	smtp.login(smtp_info['un'], smtp_info['pw'])

	smtp.send_message(msg)
	smtp.quit()

def send_email_html(msg_html, subject, addr_from, addr_to, smtp_info, msg_text=None):
	msg = MIMEMultipart('alternative')

	if msg_html is not None:
		msg.attach(MIMEText(msg_html, 'html'))

	if msg_text == '':
		msg.attach(MIMEText(BeautifulSoup(msg_html, features = 'lxml').get_text(), 'text'))
	elif type(msg_text) == str:
		msg.attach(MIMEText(msg_text, 'text'))

	msg['Subject'] = subject
	msg['From'] = addr_from
	msg['To'] = addr_to

	# Send the message via our own SMTP server.
	smtp = smtplib.SMTP(smtp_info['host'], smtp_info['port'])
	smtp.ehlo()
	smtp.starttls()
	smtp.ehlo()
	smtp.login(smtp_info['un'], smtp_info['pw'])

	smtp.sendmail(addr_from, addr_to, msg.as_string())
	smtp.quit()


if __name__ == '__main__':
	smtp_info = {'host': os.getenv('SMTP_HOST'), 'port': os.getenv('SMTP_PORT', 25), 'un': os.getenv('SMTP_UN'), 'pw': os.getenv('SMTP_PW')}
	if True:
		send_email_html(None, 'testing123', 'hopes@mohtgroup.com', 'xuancong.wang@moht.com.sg', smtp_info, 'hello world, testing234')
	elif True:
		send_email_html('<html><H3>hello world, testing456</H3></html>', 'testing123', 'hopes@mohtgroup.com', 'xuancong84@gmail.com',
		                smtp_info, 'hello world, testing789')
	else:
		send_email_text('hello world, testing', 'testing123', 'hopes@mohtgroup.com', 'xuancong.wang@moht.com.sg', smtp_info)
