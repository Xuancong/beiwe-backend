import boto3
import json
from datetime import datetime
from config.settings import \
    SECRETS_MANAGER_ACCESS_CREDENTIALS_USER, \
    SECRETS_MANAGER_ACCESS_CREDENTIALS_KEY, \
    SECRETS_MANAGER_REGION_NAME
from libs.s3 import *
from libs.user_authentication import Optionalizer
from config.constants import ENCRYPT_METHOD

client = Optionalizer(boto3.client('secretsmanager',
                      region_name=SECRETS_MANAGER_REGION_NAME,
                      aws_access_key_id=SECRETS_MANAGER_ACCESS_CREDENTIALS_USER,
                      aws_secret_access_key=SECRETS_MANAGER_ACCESS_CREDENTIALS_KEY))


def create_client_key_pair(patient_id, study_id, unix_time = None):
    """Generate key pairing, push to secrets manager as separate secrets for private and public"""
    public, private = encryption.generate_key_pairing(ENCRYPT_METHOD)
    s3_upload("keys/" + patient_id + "_private", private, study_id)
    s3_upload("keys/" + patient_id + "_public", public, study_id)
    if unix_time:
        s3_upload(f"keys/{patient_id}_private_{unix_time}", private, study_id)
        s3_upload(f"keys/{patient_id}_public_{unix_time}", public, study_id)
    if Optionalizer.MODE != 'skip':
        private_secret_json = json.dumps({'private': str(private)})
        public_secret_json = json.dumps({'public': str(public)})
        private_secret_name = 'private/' + study_id + '/' + patient_id
        public_secret_name = 'public/' + study_id + '/' + patient_id
        timestamp = (datetime.now()).strftime("%d-%b-%Y (%H:%M:%S)")
        description = "secret_name=[private/public]/study_id/patient_id created@ " + timestamp
        client.create_secret(Name=private_secret_name, Description=description, SecretString=private_secret_json)
        client.create_secret(Name=public_secret_name, Description=description, SecretString=public_secret_json)
    return public, private


def get_client_key_pair(key_type, patient_id, study_id):
    """Gets client key pair from secrets manager, return private / public key as json"""
    secret_name = key_type + '/' + study_id + '/' + patient_id
    try:
        response = client.get_secret_value(SecretId=secret_name)
    except Exception as e:
        raise UnknownError("Unknown Error: " + str(e))
    if 'SecretString' in response:
        return json.loads(response['SecretString'])
    else:
        raise SecretStringNotPresentException("This secret %s, does not have secret string specified", secret_name)


def get_client_public_key(patient_id, study_id):
    """Grabs a user's public key from secrets manager"""
    key = get_client_key_pair('public', patient_id, study_id)['public']
    return encryption.import_RSA_key(key)


def get_client_private_key(patient_id, study_id):
    """Grabs a user's private key file from secrets manager"""
    key = get_client_key_pair('private', patient_id, study_id)['private']
    return encryption.import_RSA_key(key)


def get_client_public_key_string(patient_id, study_id):
    """Grabs a user's public key from secrets manager as string"""
    key = get_client_key_pair('public', patient_id, study_id)['public']
    return encryption.prepare_X509_key_for_java(key)


class UnknownError(Exception):
    pass


class SecretStringNotPresentException(Exception):
    pass
