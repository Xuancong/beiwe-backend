import functools, time, hashlib, base64, random, string, os

from flask import request, abort, render_template_string
from werkzeug.datastructures import MultiDict

from database.user_models import Participant
from database.models import OTPtracking
from config.settings import *
from termcolor import colored


# This wrapper is used to make all AWS objects (such as secretsmanager, cloudwatch, acm) fail silently when AWS service is not available,
# so that everything will continue to work without AWS
class Optionalizer:
	# It can work in 3 modes:
	# 1. original: the inherited class will behave exactly the same as the original class
	# 2. skip: the inherited class will skip calling the original method, returning None right away
	# 3. default: the inherited class will try the original method, if it fails then return None
	MODE = os.getenv('OPTIONALIZER_MODE', 'skip')
	def _func(self, s_att, *args, **kwargs):
		try:
			F = getattr(self._wrapped_obj, s_att)
			return F(*args, **kwargs)
		except:
			return None

	def __init__(self, obj):
		if Optionalizer.MODE == 'original':
			self = obj
			return
		self._wrapped_obj = obj
		for func_name in dir(obj):
			if not func_name.startswith('_') and callable(getattr(obj, func_name)):
				def ff(fn):
					if Optionalizer.MODE=='skip':
						return lambda *args, **kwargs: None
					return lambda *args, **kwargs: self._func(fn, *args, **kwargs)
				setattr(self, func_name, ff(func_name))


# avoid confusing characters like O0 Il1
generate_OTP = lambda length=6: ''.join(random.choices('ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789', k=length))
last_prune_OTP = time.time()
def check_update_OTP(study_user_id, otp_expire_in_sec):
	global last_prune_OTP

	# prune OTP database every day
	if time.time()-last_prune_OTP > OTP_PRUNE_INTERVAL:
		OTPtracking.shrink(OTP_PRUNE_EXPIRY)
		last_prune_OTP = time.time()

	# check whether an OTP has already been sent to the email within 24 hours
	return OTPtracking.is_OTP_expired(study_user_id, otp_expire_in_sec)


def authenticate_user_ignore_password(some_function):
	@functools.wraps(some_function)
	def authenticate_and_call(*args, **kwargs):
		correct_for_basic_auth()
		if validate_post_ignore_password():
			return some_function(*args, **kwargs)
		return abort(401 if (kwargs["OS_API"] == Participant.IOS_API) else 403)

	return authenticate_and_call


def validate_post_ignore_password():
	"""Check if user exists, that a password was provided but ignores its validation, and if the
	device id matches. """
	if ("patient_id" not in request.values or "password" not in request.values or "device_id" not in request.values):
		return False

	participant_set = Participant.objects.filter(patient_id = request.values['patient_id'])
	if not participant_set.exists():
		return False
	participant = participant_set.get()
	# Disabled
	# if not participant.validate_password(request.values['password']):
	#     return False
	if not participant.device_id == request.values['device_id']:
		return False
	return True


####################################################################################################


def authenticate_user(some_function):
	"""Decorator for functions (pages) that require a user to provide identification. Returns 403
	(forbidden) or 401 (depending on beiwei-api-version) if the identifying info (usernames,
	passwords device IDs are invalid.

	In any funcion wrapped with this decorator provide a parameter named "patient_id" (with the
	user's id), a parameter named "password" with an SHA256 hashed instance of the user's
	password, a parameter named "device_id" with a unique identifier derived from that device. """

	@functools.wraps(some_function)
	def authenticate_and_call(*args, **kwargs):
		try:
			correct_for_basic_auth()
			if not {"patient_id", "password", "device_id"} <= set(request.values.keys()):
				return render_template_string(''), 401
			participant_set = Participant.objects.filter(patient_id = request.values['patient_id'])
			if not participant_set.exists():
				print(colored(f'Returning 412: {request.values}', 'red', attrs = ['bold']))
				return render_template_string(''), 412   # request phone to self-unregister
			participant = participant_set.get()
			if not participant.device_id == request.values['device_id']:
				return render_template_string(''), 413      # device-id already exist, request phone to re-register
			if not verify_epoch_hash(request.values['password'], participant.password):
				return render_template_string(''), 403
			return some_function(*args, **kwargs)
		except:
			return abort(400)

	return authenticate_and_call


def verify_passcode_hash(input_hash, passcode):
	passcode_hash = hashlib.sha256(passcode.encode('utf-8', 'ignore')).digest()
	if input_hash == base64.urlsafe_b64encode(passcode_hash).decode('utf-8', 'ignore'):
		return True
	return False

def verify_epoch_hash(input_hash, prefix = 'request-OTP'):
	for dt in [0, 1, -1, 2, -2]:
		if verify_passcode_hash(input_hash, '%s@%d'%(prefix, round(time.time() // 100 + dt))):
			return True
	return False


def authenticate_user_registration(some_function):
	""" Decorator for functions (pages) that require a user to provide identification. Returns
	403 (forbidden) or 401 (depending on beiwe-api-version) if the identifying info (username,
	password, device ID) are invalid.

   In any function wrapped with this decorator provide a parameter named "patient_id" (with the
   user's id) and a parameter named "password" with an SHA256 hashed instance of the user's
   password. """

	@functools.wraps(some_function)
	def authenticate_and_call(*args, **kwargs):
		correct_for_basic_auth()
		if validate_registration():
			return some_function(*args, **kwargs)
		return abort(401 if (kwargs["OS_API"] == Participant.IOS_API) else 403)

	return authenticate_and_call


def validate_registration():
	"""Check if user exists, check if the provided passwords match"""
	if "patient_id" not in request.values or \
			"password" not in request.values or \
			"device_id" not in request.values:
		return False
	participant_set = Participant.objects.filter(patient_id = request.values['patient_id'])
	if not participant_set.exists():
		return False
	participant = participant_set.get()
	if not participant.validate_password(request.values['password']):
		return False
	return True


def correct_for_basic_auth():
	"""
	Basic auth is used in IOS.

	If basic authentication exists and is in the correct format, move the patient_id,
	device_id, and password into request.values for processing by the existing user
	authentication functions.

	Flask automatically parses a Basic authentication header into request.authorization

	If this is set, and the username portion is in the form xxxxxx@yyyyyyy, then assume this is
	patient_id@device_id.

	Parse out the patient_id, device_id from username, and then store patient_id, device_id and
	password as if they were passed as parameters (into request.values)

	Note:  Because request.values is immutable in Flask, copy it and replace with a mutable dict
	first.

	Check if user exists, check if the provided passwords match.
	"""
	auth = request.authorization
	if not auth:
		return

	username_parts = auth.username.split('@')
	if len(username_parts) == 2:
		replace_dict = MultiDict(request.values.to_dict())
		if "patient_id" not in replace_dict:
			replace_dict['patient_id'] = username_parts[0]
		if "device_id" not in replace_dict:
			replace_dict['device_id'] = username_parts[1]
		if "password" not in replace_dict:
			replace_dict['password'] = auth.password
		request.values = replace_dict
	return
