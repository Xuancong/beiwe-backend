import json, io, gzip
import traceback
from os import urandom

from Crypto.Cipher import AES, PKCS1_OAEP, PKCS1_v1_5
from Crypto.PublicKey import RSA
from Crypto.Util.Padding import *
from flask import request
from tinyec import registry, ec
import hashlib, secrets

from config.constants import ASYMMETRIC_KEY_LENGTH, RSA_METHOD, ECC_METHOD, ED25519_METHOD, ENCRYPT_METHOD
from config.settings import IS_STAGING
from database.models import Study, DecryptionKeyError, EncryptionErrorMetadata, LineEncryptionError
from libs.logging import log_error
from libs.security import decode_base64, encode_base64
from nacl.public import PrivateKey, Box, SealedBox

class DecryptionKeyInvalidError(Exception): pass


class HandledError(Exception): pass


class InvalidIV(Exception): pass


class InvalidData(Exception): pass


class DefinitelyInvalidFile(Exception): pass

# The private keys are stored server-side (S3), and the public key is sent to the android device.

################################################################################
################################# RSA / ECC ####################################
################################################################################

curve = registry.get_curve('secp256r1')


def generate_key_pairing(key_type=RSA_METHOD):
	"""Generates a public-private key pairing, returns tuple (public, private)"""
	if key_type == ED25519_METHOD:
		key_pair = PrivateKey.generate()
		private_key = bytes(key_pair)
		public_key = bytes(key_pair.public_key)
	elif key_type == ECC_METHOD:
		privKey = secrets.randbelow(curve.field.n)
		private_key = hex(privKey)
		pubKey = privKey * curve.g
		public_key = f"{hex(pubKey.x)},{hex(pubKey.y)}"
	else:
		key_pair = RSA.generate(ASYMMETRIC_KEY_LENGTH)
		private_key = key_pair.exportKey()
		public_key = key_pair.publickey().exportKey()
	return public_key, private_key


def prepare_X509_key_for_java(exported_key):
	# This may actually be a PKCS8 Key specification.
	""" Removes all extraneous config (new lines and labels from a formatted key string,
	because this is how Java likes its key files to be formatted.
	(Y'know, not in accordance with the specification.  Because Java. """
	if type(exported_key) == bytes: exported_key = exported_key.decode()
	return "".join(exported_key.split('\n')[1:-1])


def import_RSA_key(key):
	return RSA.importKey(key)


# This function is only for use in debugging.
# def encrypt_rsa(blob, private_key):
#     return private_key.encrypt("blob of text", "literally anything")
#     """ 'blob of text' can be either a long or a string, we will use strings.
#         The second parameter must be entered... but it is ignored.  Really."""

################################################################################
################################# AES ##########################################
################################################################################

def encrypt_for_server(input_bytes, study_object_id):
	"""
	Encrypts config using the ENCRYPTION_KEY, prepends the generated initialization vector.
	Use this function on an entire file (as a string).
	"""
	encryption_key = Study.objects.get(object_id = study_object_id).encryption_key.encode()  # bytes
	iv = urandom(16)  # bytes
	if type(input_bytes) == str: input_bytes = input_bytes.encode()
	return iv + AES.new(encryption_key, AES.MODE_CFB, segment_size = 8, IV = iv).encrypt(input_bytes)


def decrypt_server(data, study_object_id):
	""" Decrypts config encrypted by the encrypt_for_server function."""
	encryption_key = Study.objects.filter(object_id = study_object_id) \
		.values_list('encryption_key', flat = True).get().encode()
	iv = data[:16]
	data = data[16:]
	return AES.new(encryption_key, AES.MODE_CFB, segment_size = 8, IV = iv).decrypt(data)


def decrypt_server_by_key(data, encryption_key):
	""" Decrypts config encrypted by the encrypt_for_server function."""
	iv = data[:16]
	data = data[16:]
	return AES.new(encryption_key, AES.MODE_CFB, segment_size = 8, IV = iv).decrypt(data)


########################### User/Device Decryption #############################
def decrypt_whole_file_rsa(encrypted_data, decrypt_key, file_name=None, verbose=True):
	try:  # get the decryption key from the file.
		key, iv, encrypted_data = encrypted_data.split(b'\n', 2)
		if len(encrypted_data) == 0:
			return None
		decoded_key = decode_base64(key)
		decoded_iv = decode_base64(iv)
		decryptor = PKCS1_OAEP.new(decrypt_key)
		aes_key = decode_base64(decryptor.decrypt(decoded_key))
		iv = decode_base64(decryptor.decrypt(decoded_iv))
		cipher = AES.new(aes_key, AES.MODE_CBC, iv)
		compressed_data = cipher.decrypt(encrypted_data)
		return unpad(compressed_data, AES.block_size)
	except Exception as e:
		if verbose:
			print(f"Error: decrypt_whole_file_rsa({file_name}) failed: {e}\n{traceback.format_exc()}")
		return None


def decrypt_whole_file_ecc(file_data, decrypt_key, file_name=None, verbose=True):
	try:  # get the decryption key from the file.
		key, iv, encrypted_data = file_data.split(b'\n', 2)
		if len(encrypted_data) == 0:
			return None
		xy_pair = key.decode("utf-8").split(',')
		encrypted_aes_key = ec.Point(curve, int(xy_pair[0], 16), int(xy_pair[1], 16))
		aes_point = decrypt_key * encrypted_aes_key
		aes_key = ecc_point_to_256_bit_key(aes_point)
		iv = decode_base64(iv)
		cipher = AES.new(aes_key, AES.MODE_CBC, iv)
		decrypted_data = cipher.decrypt(encrypted_data)
		return unpad(decrypted_data, AES.block_size) if decrypted_data else decrypted_data
	except Exception as e:
		if verbose:
			print(f"Error: decrypt_whole_file_rsa({file_name}) failed: {e}\n{traceback.format_exc()}")
		return None


def ecc_point_to_256_bit_key(point):
	sha = hashlib.sha256(int.to_bytes(point.x, 32, 'big'))
	sha.update(int.to_bytes(point.y, 32, 'big'))
	return sha.digest()


def decrypt_and_decompress(encrypted_data, decrypt_key, file_name=None, verbose=True) -> None or str:
	if not encrypted_data:
		return ''
	try:
		gzipped_data = (decrypt_whole_file_rsa if ENCRYPT_METHOD == RSA_METHOD else decrypt_whole_file_ecc) \
			(encrypted_data, decrypt_key, file_name=file_name, verbose=verbose)
	except:
		gzipped_data = encrypted_data
	if gzipped_data is None or not gzipped_data:
		return gzipped_data

	try:
		return gzip.decompress(gzipped_data).decode('utf-8', 'ignore')
	except Exception as e:
		print(f"Error: gzip.decompress({file_name}) failed: {e}")
		return None


def decrypt_only(encrypted_data, decrypt_key, file_name=None, verbose=True) -> None or str:
	if not encrypted_data:
		return ''
	try:
		gzipped_data = (decrypt_whole_file_rsa if ENCRYPT_METHOD == RSA_METHOD else decrypt_whole_file_ecc) \
			(encrypted_data, decrypt_key, file_name=file_name, verbose=verbose)
	except:
		gzipped_data = encrypted_data
	if gzipped_data is None:
		return None
	return gzipped_data.decode('utf-8', 'ignore')


def decrypt_device_file(patient_id, original_data, private_key, user):
	""" Runs the line-by-line decryption of a file encrypted by a device.
	This function is a special handler for iOS file uploads. """

	def create_line_error_db_entry(error_type):
		# declaring this inside decrypt device file to access its function-global variables
		# TODO @Eli consider enabling this on prod as well
		if IS_STAGING:
			LineEncryptionError.objects.create(
				type = error_type,
				base64_decryption_key = private_key.decrypt(decoded_key),
				line = line,
				prev_line = file_data[i - 1] if i > 0 else '',
				next_line = file_data[i + 1] if i < len(file_data) - 1 else '',
			)

	bad_lines = []
	error_types = []
	error_count = 0
	return_data = ""
	file_data = [line for line in original_data.split('\n') if line != ""]

	if not file_data:
		raise HandledError("The file had no data in it.  Return 200 to delete file from device.")

	try:  # get the decryption key from the file.
		decoded_key = decode_base64(file_data[0].encode("utf-8"))
		decrypted_key = decode_base64(private_key.decrypt(decoded_key))
	except (TypeError, IndexError) as e:
		DecryptionKeyError.objects.create(
			file_path = request.values['file_name'],
			contents = original_data,
			participant = user,
		)
		raise DecryptionKeyInvalidError("invalid decryption key. %s" % e.message)

	# (we have an inefficiency in this encryption process, this might not need to be doubly
	# encoded in base64.  This is probably never going to be changed.)
	# The following is all error catching code for bugs we encountered (and solved) in development.
	# print "length decrypted key", len(decrypted_key)

	for i, line in enumerate(file_data):
		# we need to skip the first line (the decryption key), but need real index values in i
		if i == 0: continue

		if line is None:
			# this case causes weird behavior inside decrypt_device_line, so we test for it instead.
			error_count += 1
			create_line_error_db_entry(LineEncryptionError.LINE_IS_NONE)
			error_types.append(LineEncryptionError.LINE_IS_NONE)
			bad_lines.append(line)
			print("encountered empty line of data, ignoring.")
			continue

		try:
			decrypted = decrypt_device_line(patient_id, decrypted_key, line)
			if decrypted != '\n':
				return_data += decrypted + "\n"
		except Exception as e:
			error_count += 1

			error_message = "There was an error in user decryption: "
			if isinstance(e, IndexError):
				error_message += "Something is wrong with data padding:\n\tline: %s" % line
				log_error(e, error_message)
				create_line_error_db_entry(LineEncryptionError.PADDING_ERROR)
				error_types.append(LineEncryptionError.PADDING_ERROR)
				bad_lines.append(line)
				continue

			if isinstance(e, TypeError) and decrypted_key is None:
				error_message += "The key was empty:\n\tline: %s" % line
				log_error(e, error_message)
				create_line_error_db_entry(LineEncryptionError.EMPTY_KEY)
				error_types.append(LineEncryptionError.EMPTY_KEY)
				bad_lines.append(line)
				continue

			################### skip these errors ##############################
			if "unpack" in e.message:
				error_message += "malformed line of config, dropping it and continuing."
				log_error(e, error_message)
				create_line_error_db_entry(LineEncryptionError.MALFORMED_CONFIG)
				error_types.append(LineEncryptionError.MALFORMED_CONFIG)
				bad_lines.append(line)
				# the config is not colon separated correctly, this is a single
				# line error, we can just drop it.
				# implies an interrupted write operation (or read)
				continue

			if "Input strings must be a multiple of 16 in length" in e.message:
				error_message += "Line was of incorrect length, dropping it and continuing."
				log_error(e, error_message)
				create_line_error_db_entry(LineEncryptionError.INVALID_LENGTH)
				error_types.append(LineEncryptionError.INVALID_LENGTH)
				bad_lines.append(line)
				continue

			if isinstance(e, InvalidData):
				error_message += "Line contained no data, skipping: " + str(line)
				log_error(e, error_message)
				create_line_error_db_entry(LineEncryptionError.LINE_EMPTY)
				error_types.append(LineEncryptionError.LINE_EMPTY)
				bad_lines.append(line)
				continue

			if isinstance(e, InvalidIV):
				error_message += "Line contained no iv, skipping: " + str(line)
				log_error(e, error_message)
				create_line_error_db_entry(LineEncryptionError.IV_MISSING)
				error_types.append(LineEncryptionError.IV_MISSING)
				bad_lines.append(line)
				continue

			##################### flip out on these errors #####################
			if 'AES key' in e.message:
				error_message += "AES key has bad length."
				create_line_error_db_entry(LineEncryptionError.AES_KEY_BAD_LENGTH)
				error_types.append(LineEncryptionError.AES_KEY_BAD_LENGTH)
				bad_lines.append(line)
			elif 'IV must be' in e.message:
				error_message += "iv has bad length."
				create_line_error_db_entry(LineEncryptionError.IV_BAD_LENGTH)
				error_types.append(LineEncryptionError.IV_BAD_LENGTH)
				bad_lines.append(line)
			elif 'Incorrect padding' in e.message:
				error_message += "base64 padding error, config is truncated."
				create_line_error_db_entry(LineEncryptionError.MP4_PADDING)
				error_types.append(LineEncryptionError.MP4_PADDING)
				bad_lines.append(line)
				# this is only seen in mp4 files. possibilities:
				#  upload during write operation.
				#  broken base64 conversion in the app
				#  some unanticipated error in the file upload
			else:
				raise  # If none of the above errors happened, raise the error.
			raise HandledError(error_message)
			# if any of them did happen, raise a HandledError to cease execution.

	if error_count:
		EncryptionErrorMetadata.objects.create(
			file_name = request.values['file_name'],
			total_lines = len(file_data),
			number_errors = error_count,
			errors_lines = json.dumps(bad_lines),
			error_types = json.dumps(error_types),
		)

	return return_data


def decrypt_device_line(patient_id, key, data):
	""" config is expected to be 3 colon separated values.
		value 1 is the symmetric key, encrypted with the patient's public key.
		value 2 is the initialization vector for the AES CBC cipher.
		value 3 is the config, encrypted using AES CBC, with the provided key and iv. """
	iv, data = data.split(":")
	iv = decode_base64(iv.encode("utf-8"))  # handle non-ascii encoding garbage...
	data = decode_base64(data.encode("utf-8"))
	if not data:
		raise InvalidData()
	if not iv:
		raise InvalidIV
	try:
		decrypted = AES.new(key, mode = AES.MODE_CBC, IV = iv).decrypt(data)
	except Exception:
		if iv is None:
			len_iv = "None"
		else:
			len_iv = len(iv)
		if data is None:
			len_data = "None"
		else:
			len_data = len(data)
		if key is None:
			len_key = "None"
		else:
			len_key = len(key)
		print("length iv: %s, length data: %s, length key: %s" % (len_iv, len_data, len_key))
		print('patient_id: %s' % patient_id)
		raise
	return remove_PKCS5_padding(decrypted)


################################################################################

def remove_PKCS5_padding(data):
	""" Unpacks encrypted config from the device that was encypted using the
		PKCS5 padding scheme (which is the ordinal value of the last byte). """
	# This can raise an indexerror when data is empty, but that is not expected behavior.
	return data[0: -ord(data[-1])]
