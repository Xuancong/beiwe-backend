
path="`dirname $0`"

cmd_1=
if [ "$1" == '-s' ]; then
	shift
	cmd_1=".sort_index()"
fi

if [ $# -gt 0 ]; then
	for fn in "$@"; do
		zcat -f "$fn" \
			| $path/process-csv.py -index \"datetime\" "dt = pd.to_datetime(df['timestamp'], unit=('ms' if df['timestamp'][0]>99999999999 else 's'), origin='unix', utc=True);df['datetime'] = pd.DatetimeIndex(dt).tz_convert(tzlocal()); out=df.set_index('datetime')$cmd_1" | less
	done
else
	$path/process-csv.py -index \"datetime\" "dt = pd.to_datetime(df['timestamp'], unit=('ms' if df['timestamp'][0]>99999999999 else 's'), origin='unix', utc=True);df['datetime'] = pd.DatetimeIndex(dt).tz_convert(tzlocal()); out=df.set_index('datetime')$cmd_1"
fi

