#!/bin/bash

if [ ! "$STUDY_ID" ]; then
	echo "Error: STUDY_ID is not defined"
	exit 1
fi

INPATH=decrypted/$STUDY_ID

# patch powerstate
echo -n "Incrementally patching powerstate files "
find $INPATH -iregex '.*/powerState/.*.csv' | while read file; do
	fo="1.$file"
	if [ -s "$fo" ]; then continue; fi
	mkdir -p "`dirname $fo`"
	if [ `grep -c "^PowerStateListener:" $file` == 0 ]; then
		cat "$file"
	else
		cat "$file" | sed "s/^PowerStateListener: *//g; s/^\([0-9]\+\) /\1,/g"
	fi | cut3.py -d , 0:-2 > "$fo"
	echo -n .
done
echo


for fea in accel accessibilityLog callLog gps light sociabilityCallLog sociabilityLog tapsLog textsLog; do
	echo -n "Incrementally patching $fea files "
	find $INPATH -iregex ".*/$fea/.*.csv" | while read file; do
		fo="1.$file"
		if [ -s "$fo" ]; then continue; fi
		mkdir -p "`dirname $fo`"
		cat "$file" | cut3.py -d , 0:-2 > "$fo"
		echo -n .
	done
	echo
done


# patch heart-rate
echo -n "Incrementally patching heart files "
find $INPATH -iregex '.*/heart/.*.csv' | while read file; do
	fo="1.$file"
	if [ -s "$fo" ]; then continue; fi
	mkdir -p "`dirname $fo`"
	cat "$file" | sed "s:^ID:timestamp:1; s:,UTCTimeStamp::1" | cut3.py -d , 0,4,5 | sed "s:\([0-9]\),:\1000,:1" > "$fo"
	echo -n .
done
echo


# patch steps
echo -n "Incrementally patching steps files "
find $INPATH -iregex '.*/steps/.*.csv' | while read file; do
	fo="1.$file"
	if [ -s "$fo" ]; then continue; fi
	mkdir -p "`dirname $fo`"
	cat "$file" | sed "s:^ID:timestamp:1; s:,UTCTimeStamp::1" | cut3.py -d , 0,4,5 | sed "s:\([0-9]\),:\1000,:1" > "$fo"
	echo -n .
done
echo


# patch sleep
echo -n "Incrementally patching sleep files "
find $INPATH -iregex '.*/sleep/.*.csv' | while read file; do
	fo="1.$file"
	if [ -s "$fo" ]; then continue; fi
	mkdir -p "`dirname $fo`"
	cat "$file" | sed "s:^ID:timestamp:1; s:,UTCTimeStamp::1" | cut3.py -d , 0,4,5,6,7,8 | sed "s:\([0-9]\),:\1000,:1" > "$fo"
	echo -n .
done
echo


## revise gps files
#echo -n "Incrementally revising gps files "
#find $INPATH -iregex ".*/gps/.*.csv" | while read file; do
#	fi="1.$file"
#	fo=`echo "$fi" | sed "s:/gps/:/locTime-gps/:1"`
#	if [ -s "$fo" ]; then continue; fi
#	mkdir -p "`dirname $fo`"
#	cat "$fi" | sed "s:^timestamp:recv-timestamp:g; s:locationTime:timestamp:1" > "$fo"
#	echo -n .
#done
#echo

