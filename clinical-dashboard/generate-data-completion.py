#!/usr/bin/env python3

import os, sys, argparse, matplotlib
import pandas as pd

sys.path.append(os.getcwd())    # work around Python3 stupidity
from libs.email_sender import *

os.main_path = os.getcwd()+'/'
from core import *

matplotlib.rcParams["savefig.format"] = 'png'

def merge_stat(dfs, max_datetime):
	s = pd.DataFrame([df['timestamp'].groupby(pd.Grouper(freq='D')).count() for df in dfs if len(df.index)]).transpose().sum(axis=1)
	return pd.DataFrame({'timestamp': s}) if s.size else pd.DataFrame({'timestamp': []}, index=pd.DatetimeIndex([], tz='tzlocal()'))


def monitor(lastN, out_filename, verbose=1):
	df = pd.DataFrame()
	ret = [pd.Timestamp.today(tz='tzlocal()').floor('D')-pd.to_timedelta('999D')]*2
	for username in dropdown_userlist.options:
		if not os.path.isfile(os.data_path + '/' + username + '/ambientLight.csv.gz'):
			if verbose>0:print('Skipping User %s' % username)
			continue

		if verbose>0:print('Loading data from %s ' % username, end='')
		os.dfs = dfs = [(load_df(username, fn, -1, cache=False, low_memory=False), print('.' if verbose>0 else '', end='', flush=True))[0] for fn in
						['ambientLight.csv.gz', 'accessibilityLog.csv.gz', 'tapsLog.csv.gz',
						 'callLog.csv.gz', 'smsLog.csv.gz', 'sociabilityMsgLog.csv.gz', 'sociabilityCallLog.csv.gz', 'heart.csv.gz', 'sleep.csv.gz',
						 'steps.csv.gz']]

		try:
			max_datetime = max([df.index.max() for df in dfs if len(df.index)]).floor('D')
			ret[0] = max([ret[0]] + [df.index.max() for df in dfs[:-3] if len(df.index)])
			ret[1] = max([ret[1]] + [df.index.max() for df in dfs[-3:] if len(df.index)])
		except:
			if verbose > 0: print('Skip')
			continue

		phone1, phone2, phone3, fitbit1, fitbit2 = [merge_stat([dfs[i] for i in s], max_datetime) for s in [[0], [1, 2], [3, 4, 5, 6], [7], [8]]]

		# Fitbit steps need to sum over every day group
		fitbit0 = (dfs[9] if len(dfs[9].index) else pd.DataFrame({'StepsCount': [0]},
					index=[max_datetime]))[['StepsCount']].groupby(pd.Grouper(freq='D')).sum()

		# Build phone colors
		phoneC = pd.concat([phone1, phone2, phone3], axis=1).fillna(0).sort_index() \
			.apply(lambda x: 3 if min(x[0:3]) else (2 if min(x[0:2]) else (1 if x[0] else (-1 if max(x[0:3]) else 0))), axis=1)

		# Build Fitbit colors
		fitbitC = pd.concat([fitbit0, fitbit1, fitbit2], axis=1).fillna(0).sort_index() \
			.apply(lambda x: 3 if min(x[0:3]) else (2 if min(x[0:2]) else (1 if x[0] else (-1 if max(x[0:3]) else 0))), axis=1)

		df = df.join(pd.DataFrame({username+' phone': phoneC, username + ' fitbit':fitbitC}), how='outer')

		if verbose > 0: print('Done')

	if verbose > 0: print('Plotting dashboard ...')
	os.df = df = df.fillna(0).astype(int)
	os.data = data = df.iloc[-lastN:, :].transpose()

	# draw 2D grid
	Nx, Ny = len(data.columns), len(data.index)
	fig, ax = plt.subplots(figsize=(Nx / 4, Ny))
	ax.title.set_size(30)
	ax.title.set_text('Participant Data Completion Dashboard')
	cmap = colors.ListedColormap(['purple', 'black', 'red', 'orange', 'green'])
	norm = colors.BoundaryNorm([-1.5, -0.5, 0.5, 1.5, 2.5, 3.5], cmap.N)
	ax.grid(False)
	ax.imshow(data, cmap=cmap, norm=norm)

	# x-axis is the date
	ax.set_xticklabels('')
	ax.set_xticks(np.arange(0.5, Nx + 0.5, 1), minor=True)
	ax.set_xticklabels(data.columns, minor=True, rotation=45, ha='right')
	ax.xaxis.grid(True, which='minor', linewidth=0.5)

	# y-axis is the Patient ID
	ax.set_yticks(np.arange(-0.5, Ny + 0.5, 2))
	ax.set_yticklabels([''] + [re.sub('@[^ ]* ', ' ', s) for s in data.index[1::2]], fontdict={'family': 'monospace'}, va='bottom')
	ax.yaxis.grid(True, which='major', linewidth=2)

	ax.set_yticks(np.arange(0.5, Ny + 0.5, 2), minor=True)
	ax.set_yticklabels([re.sub('@[^ ]* ', ' ', s) for s in data.index[0::2]], fontdict={'family': 'monospace'}, minor=True, va='bottom')
	ax.yaxis.grid(True, which='minor', linewidth=0.5)

	# legend
	ax.add_artist(plt.legend(handles=[Patch(color='green', label='sensor+taps+social'), Patch(color='orange', label='sensor+taps'),
									  Patch(color='red', label='sensor only'), Patch(color='purple', label='other'), Patch(color='black', label='no data')], title='Phone',
							 loc='lower center', bbox_to_anchor=(0.1, 1)))
	ax.add_artist(plt.legend(handles=[Patch(color='green', label='steps+heart+sleep'), Patch(color='orange', label='steps+heart'),
									  Patch(color='red', label='steps only'), Patch(color='purple', label='other'), Patch(color='black', label='no data')], title='Fitbit',
							 loc='lower center', bbox_to_anchor=(0.9, 1)))

	if out_filename:
		plt.savefig(out_filename)
	else:
		import time
		plt.show()
		fig.show()
		time.sleep(5)
		plt.close()

	return ret


### MAIN START
if __name__ == '__main__':
	parser = argparse.ArgumentParser(usage='$0 main-path 1>output 2>progress',
									 description='This program generates data completion dashboard image for every patient')
	parser.add_argument('main_path', help='the main directory containing all users')
	parser.add_argument('out_filename', help='output image filename', default='')
	parser.add_argument('--last-n', '-n', help='plot last N days', default=90)
	parser.add_argument('--alert', '-a', help = 'send email alarm if missing last N days of data', type = int, default = 3)
	parser.add_argument('--email', '-e', help = 'email address of the email alarm', default = None)
	parser.add_argument('--verbose', '-v', help='verbosity, default=1', default=1)
	# nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt = parser.parse_args()
	globals().update(vars(opt))

	os.data_path = os.main_path = main_path
	on_change_study({'new':main_path})

	matplotlib.use("Agg")
	last_dates = monitor(last_n, out_filename, verbose=verbose)

	if email:
		phone_tail_days = (pd.Timestamp.now(tz = 'tzlocal()') - last_dates[0]).days
		fitbit_tail_days = (pd.Timestamp.now(tz = 'tzlocal()') - last_dates[1]).days
		if phone_tail_days >= alert and fitbit_tail_days >= alert:
			trigger = 'Both phone and wearable data'
		elif phone_tail_days >= alert:
			trigger = 'Phone data'
		elif fitbit_tail_days >= alert:
			trigger = 'Wearable data'
		else:
			trigger = ''
		if trigger:
			try:
				smtp_info = {'host': os.getenv('SMTP_HOST'), 'port': int(os.getenv('SMTP_PORT', 25)),
				             'un': os.getenv('SMTP_UN'), 'pw': os.getenv('SMTP_PW')}
				send_email_html(f'In one of your digital phenotyping study: {trigger} collection has stopped for {alert} days, please examine!',
				                f'Missing data for consecutive {alert} days', os.getenv('SMTP_FROM'), email, smtp_info, '')
			except:
				pass
