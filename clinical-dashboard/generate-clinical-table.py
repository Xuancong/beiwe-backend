#!/usr/bin/env python3

import os, sys, argparse, json
# Please specify data file path here:
# folder hierarchy: main-path/study-name/user-name/feature-name/[timestamp-sortable].csv or main-path/study-name/user-name/feature-name.csv.gz
# os.main_path = os.getenv('HOME')+''
os.main_path = os.getcwd()

import pandas as pd
import numpy as np
from core import *


def flatten(obj):
	if type(obj) is not list:
		return [obj]
	ret = []
	for o in obj:
		ret += flatten(o)
	return ret


def jsonify(obj, indent_level=0, indent_str=' '):
	if type(obj) is dict:
		ret = indent_str*indent_level+('{\n' if obj else '{')
		for i,k in enumerate(obj):
			ret += indent_str*(indent_level+1) + jsonify(k) + ' : ' + jsonify(obj[k], indent_level+1) + ('\n' if i==len(obj)-1 else ',\n')
		ret += indent_str*indent_level+'}' if obj else '}'
		return ret
	elif type(obj) is str:
		return '"%s"'%obj
	elif type(obj) is list:
		return str(flatten(obj))
	return str(obj)


def group_dates(data, labels, figsize):
	periods = [(s + 'D' if s.isdigit() else s) for s in re.split('[ ,;]+', os.DateGroup)]
	index = data.index if type(data.index) == pd.DatetimeIndex else data.datetime
	max_date = index.max().floor('D') if os.max_date is None else os.max_date
	cut_points = [max_date]
	for period in periods[::-1]:
		max_date -= pd.to_timedelta(period)
		cut_points = [max_date] + cut_points
	dfg = data.groupby(pd.cut(index, cut_points, right=False))
	figsize[0] = len(periods) * 0.4
	if index.nunique() == len(data):
		ret = dfg.mean()
		ret_index = ret.index
	else:
		ret = pd.concat([set_index_to_value(v.reset_index(drop=True), k, 'datetime') for k, v in dfg], ignore_index=True)
		ret_index = [k for k, v in dfg]
	return ret, ret_index, figsize


def calc_exchange(v):
	valid_contacts = set(v[v['type'] == 'received SMS'].contact) & set(v[v['type'] == 'sent SMS'].contact)
	return v[v.contact.isin(valid_contacts) & v.type.str.contains('SMS')]


def computeAll(Username, StartDate, LastDate, DateGroup, last_date):
	global verbose

	# init parameters
	ret = {}
	err = 0
	os.DateGroup = DateGroup
	file_suffix = '.csv.gz'
	xticklabels = ['previous month', 'previous week', 'current week']

	# arrange into horizontal grid
	# fig, axs = plt.subplots(nrows=1, ncols=3, figsize=[12, 4])
	# plt.subplots_adjust(wspace=0.5)

	if last_date is None:
		try:
			dts = load_df(Username, 'ambientLight' + file_suffix, verbose=-1).index
		except:
			dts = load_df(Username, 'light' + file_suffix, verbose=-1).index
		try:
			dts = dts.append(load_df(Username, 'steps' + file_suffix, verbose=-1).index)
			last_date = dts.max().floor('D')
		except:
			last_date = pd.to_datetime(datetime.now())
	os.max_date = last_date
	ret['last_date_exclusive'] = str(last_date)

	## 1. sleep
	try:
		df = filter_by_date(load_df(Username, 'sleep' + file_suffix, verbose=-1), StartDate, LastDate).sort_index()
		df = df[df.Level != 'main']
		df.Seconds /= 3600

		# get sleep efficiency
		sleep_efficiency = df.groupby(pd.Grouper(freq='1D', base=0.5))['Efficiency'].mean()
		SE, SE_index, SE_figsize = group_dates(sleep_efficiency, None, [16, 9])
		SE = SE.to_frame()

		# get sleep duration together with sleep efficiency
		SD = draw(df, None, None, 0.0, False, None, 'sum in each interval', '1D', 0.5, 0, 'time chart (bar)',
				 'Seconds', 'mean', '<entry-count>', False, 'no sort', False, False, False, False, None, verbose=-1, post_processor=group_dates,
				 set_title='Sleep', set_ylabel='Sleep Duration (hours)', set_xticklabels=xticklabels)

		ret['date_ranges'] = str(SE_index.values).split('\n')[0]
		ret['sleep_duration/hours'] = SD.values.tolist()
		ret['sleep_efficiency/%'] = SE.values.tolist()
	except Exception as e:
		if verbose>1:
			print(e+' AT '+Username, file=sys.stderr)
		err += 1


	## 2. sociability
	try:
		# 2.1a whatsapp calls
		df1a = filter_by_date(load_df(Username, 'sociabilityCallLog' + file_suffix, verbose=-1), StartDate, LastDate).sort_index()
		df1a = pd.DataFrame(columns=['app', 'contact', 'call type', 'recordedDuration']) if len(df1a) == 0 \
			else pd.concat([pd.DataFrame({'app': v.app[0], 'contact': v.contact[0],
										  'call type': ('Incoming Call' if v.eventType[0] == 1 else 'Outgoing Call'),
										  'recordedDuration': v.recordedDuration[-1]},
										 index=[v.index[0]]) for k, v in df1a.groupby('sessionId')])
		# 2.1b phone calls
		df1b = filter_by_date(load_df(Username, 'callLog' + file_suffix, verbose=-1), StartDate, LastDate) \
			.rename(columns={'duration in seconds': 'recordedDuration', 'hashed phone number': 'contact'}).sort_index()
		df1b = pd.DataFrame() if len(df1b) == 0 else set_index_to_value(df1b, 'phone', col='app')[
			['app', 'contact', 'call type', 'recordedDuration']]
		# merged DataFrame for both types of calls
		df1 = pd.concat([df1a, df1b]).rename(columns={'call type': 'type'}).sort_index()

		# 2.2a phone SMS
		try:
			df2a = filter_by_date(load_df(Username, 'smsLog' + file_suffix, verbose=-1), StartDate, LastDate).sort_index()
		except:
			df2a = filter_by_date(load_df(Username, 'textsLog' + file_suffix, verbose=-1), StartDate, LastDate).sort_index()
		df2a['app'] = 'phone'
		df2a = pd.DataFrame(columns=['app', 'contact', 'type', 'msgLength']) if len(df2a) == 0 else \
		df2a.rename(columns={'hashed phone number': 'contact', 'sent vs received': 'type', 'message length': 'msgLength'})[
			['app', 'contact', 'type', 'msgLength']]
		# 2.2b WhatsApp message
		try:
			df2b = filter_by_date(load_df(Username, 'sociabilityMsgLog' + file_suffix, verbose=-1), StartDate, LastDate).sort_index()
		except:
			df2b = filter_by_date(load_df(Username, 'sociabilityLog' + file_suffix, verbose=-1), StartDate, LastDate).sort_index()
		if len(df2b) > 0:
			df2b = df2b[['app', 'contact', 'orientation', 'length']].rename(
				columns={'orientation': 'type', 'length': 'msgLength'})
			df2b.type = df2b.type.apply(lambda x: 'sent SMS' if x == 0 else 'received SMS')
		df = pd.concat([df1, df2a, df2b], sort=False).sort_index()
		if len(df) > 0:
			dfg_calls = df.groupby(pd.Grouper(freq='1D'))
			out = [[len(v[v.recordedDuration >= 60]), len(calc_exchange(v).index)/2] for k, v in dfg_calls]
			df = pd.DataFrame(out, index=[k for k, v in dfg_calls], columns=['n_calls>1min', 'n_exchange_msg'])
			sociability = draw(df, None, None, 0.0, False, None, 'pass through all', '1D', 0, 0, 'time chart stacked bar',
						 ['n_exchange_msg', 'n_calls>1min'], 0,
						 '<entry-count>', False, 'no sort', False, False, False, False, None, verbose=-1, post_processor=group_dates,
						 set_title='Sociability', set_xticklabels=xticklabels)
			ret['n_exchange_msg'] = sociability['n_exchange_msg'].values.tolist()
			ret['n_calls>1min'] = sociability['n_calls>1min'].values.tolist()
	except Exception as e:
		if verbose > 1:
			print(e+' AT '+Username, file=sys.stderr)
		err += 1


	## 3. mobility
	try:
		df1 = load_df(Username, 'gps-mobility' + file_suffix, verbose=-1)
		df1 = filter_by_date(df1[['Hometime', 'RoG']], StartDate, LastDate).sort_index()
		df = (24 - df1[['Hometime']] / 60).rename(columns={'Hometime': 'TAFH'})
		# prepare RoG
		RoG, _, _ = group_dates(np.log10(df1[['RoG']].clip(0, np.inf) + 1), None, [16, 9])
		ret['radius_of_gyration'] = [10 ** v for v in RoG.RoG]

		# plot time-away-from-home together with RoG
		mobility = draw(df, None, None, 0.0, False, None, 'mean value in each interval', '1D', 0, 0,
								'time chart (bar)', 'TAFH', 10,
								'<entry-count>', True, 'no sort', False, False, False, False, None, verbose=-1,
								post_processor=group_dates, set_title='Mobility', set_ylabel='Daily Time Away From Home (hours)',
								set_xticklabels=xticklabels)
		ret['time_away_from_home'] = mobility['TAFH'].values.tolist()
	except Exception as e:
		if verbose > 1:
			print(e+' AT '+Username, file=sys.stderr)
		err += 1

	return ret, err


### MAIN START
if __name__ == '__main__':
	parser = argparse.ArgumentParser(usage='$0 main-path 1>output 2>progress',
									 description='This program generates clinician dashboard table for every patient')
	parser.add_argument('main_path', help='the main directory containing all users')
	parser.add_argument('--last-date', help='the last date for evaluation, if absent, it will be derived from ambient-light and steps of that patient', type=str, default=None)
	parser.add_argument('--indent-token', help='indent token, default=" "', default=' ')
	parser.add_argument('--verbose', '-v', help='verbosity, default=1', default=1)
	# nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt = parser.parse_args()
	globals().update(vars(opt))

	if last_date is not None:
		last_date = pd.Timestamp(last_date)

	# ret = computeAll('../3.decrypted/fZ2UoHH1PXiuoPSD1VYDWzh6/moht.dsth.150@moht.com.sg_e3fb5e097f2b', None, None, "30 7 7", last_date)

	ret = {}
	code_map = {
		0: TC.BLG + '3' + TC.END,
		1: TC.BLY + '2' + TC.END,
		2: TC.BLY + '1' + TC.END,
		3: TC.BLR + '0' + TC.END
	}
	for user in os.listdir(main_path):
		if os.path.isdir(main_path+'/'+user) and os.path.isfile(main_path+'/'+user+'/accel.csv.gz'):
			out, err = computeAll(main_path + '/' + user, None, None, "30 7 7", last_date)
			if err<3:
				ret[user] = out
			if verbose:
				print(code_map[err], end='', file=sys.stderr, flush=True)

	if True:
		# The best way to JSON pretty print is to write the code by yourself
		print(jsonify(ret, indent_str=indent_token).replace('nan', 'NaN'))
	else:
		print(json.dumps(ret, sort_keys=True, indent=indent_token))

	if verbose:
		print(file=sys.stderr)


