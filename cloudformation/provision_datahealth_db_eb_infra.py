#!/usr/bin/env python3

import os
import sys
import time

# Check for boto3 (aws client) and then import
try:
    __import__('boto3')
except ImportError:
    os.system('pip install boto3')

import boto3

# The following import statement will run the whole provision_network_infra
from provision_fitbit_datahealth_network_infra import dh_network_stack_name, dh_network_stack_exists, \
    dir_path, stack_list, key_name, aws_access_key_id, \
    aws_secret_access_key, aws_region_name, DISABLE_PROMPTS

# Initialize cloudformation client
cf_client = boto3.client('cloudformation',
                         region_name=aws_region_name,
                         aws_access_key_id=aws_access_key_id,
                         aws_secret_access_key=aws_secret_access_key)

# Initialize elastic beanstalk client
beanstalk_client = boto3.client('elasticbeanstalk',
                                region_name=aws_region_name,
                                aws_access_key_id=aws_access_key_id,
                                aws_secret_access_key=aws_secret_access_key)

'''
DATA HEALTH DATABASE STACK
'''

# Initialize data health database stack name and flag
db_stack_name = ''
db_stack_exists = False

# Check if data health database stack is already exists with parent stack as network stack
if dh_network_stack_exists:
    stack_exists = False
    is_db_stack = False
    for i in range(len(stack_list)):
        data = cf_client.describe_stacks(StackName=stack_list[i]['StackName'])
        try:
            parameters = data['Stacks'][0]['Parameters']
        except:
            continue
        for parameter in parameters:
            if parameter['ParameterKey'] == 'ParentNetworkStack' and \
                    parameter['ParameterValue'] == dh_network_stack_name:
                stack_exists = True
            if parameter['ParameterKey'] == 'DBName':
                is_db_stack = True
            if is_db_stack and stack_exists:
                db_stack_exists = True
                break
        if db_stack_exists:
            db_stack_name = data['Stacks'][0]['StackName']
            db_stack_status = data['Stacks'][0]['StackStatus']
            if '_COMPLETE' not in db_stack_status:
                sys.exit('Error! Database stack already exists with status - {}. '
                         'Try with different vpn stack name'.format(db_stack_status))
            print('DATA HEALTH DATABASE STACK ALREADY EXISTS WITH STATUS - {}, '
                  'SKIPPING DATA HEALTH DATABASE STACK CREATION\n'.format(db_stack_status))
            break

# Create Data Health Database stack only if it does not exist
if not db_stack_exists:

    # Get data health database stack template full path
    db_stack_file = os.path.join(dir_path, 'datahealth-rds-postgres.yml')

    # Get data health database stack name
    print('START DATA HEALTH DATABASE STACK CREATION\n')
    db_stack_name = os.getenv('DATA_HEALTH_DB_STACK_NAME')
    if not DISABLE_PROMPTS:
        db_stack_name = raw_input('Enter DATA_HEALTH_DB_STACK_NAME (default: datahealth-ops-database): ')
        if db_stack_name is None or db_stack_name.strip() == '':
            db_stack_name = 'datahealth-ops-database'

    # Read data health database stack template file and convert to string
    db_stack = open(db_stack_file, 'r')
    db_template = db_stack.read()
    db_stack.close()

    # Get data health database parameters from user
    print('\nGET PARAMETERS FOR DATA HEALTH DATABASE STACK\n')
    db_instance_class = os.getenv('DATA_HEALTH_DB_INSTANCE_CLASS')
    db_port = os.getenv('DATA_HEALTH_DB_PORT')
    db_storage = os.getenv('DATA_HEALTH_DB_STORAGE')
    db_master_username = os.getenv('DATA_HEALTH_DB_MASTER_USERNAME')
    db_name = os.getenv('DATA_HEALTH_DB_NAME')
    db_multi_az = os.getenv('DATA_HEALTH_MULTI_AZ')
    db_engine = os.getenv('DATA_HEALTH_DB_ENGINE')
    db_postgres_engine_version = os.getenv('DATA_HEALTH_DB_POSTGRES_ENGINE_VERSION')
    db_pref_backup_window = os.getenv('DATA_HEALTH_DB_PREFERRED_BACKUP_WINDOW')
    db_pref_maintenance_window = os.getenv('DATA_HEALTH_DB_PREFERRED_MAINTENANCE_WINDOW')
    db_backup_retention_period = os.getenv('DATA_HEALTH_DB_BACKUP_RETENTION_PERIOD')
    db_snapshot_id = os.getenv('DATA_HEALTH_DB_SNAPSHOT_ID')

    if not DISABLE_PROMPTS:
        db_instance_class = raw_input('DBInstanceClass (default: db.t3.micro) = ')
        db_port = raw_input('DBPort (default: 5432) = ')
        db_storage = raw_input('DBStorage (default: 50) = ')
        db_master_username = raw_input('DBMasterUsername (default: OpsDashboard) = ')
        db_name = raw_input('DBName (default: OpsDashboard) = ')
        db_multi_az = raw_input('MultiAZ (default: false) = ')
        db_engine = raw_input('DBEngine (default: postgres) = ')
        db_postgres_engine_version = raw_input('DBPostgresEngineVersion (default: 11.4) = ')
        db_pref_backup_window = raw_input('PreferredBackupWindow (default: 20:34-21:04) = ')
        db_pref_maintenance_window = raw_input('PreferredMaintenanceWindow (default: sun:22:00-sun:23:00) = ')
        db_backup_retention_period = raw_input('DBBackupRetentionPeriod (default: 30) = ')
        db_snapshot_id = raw_input('DBSnapshotIdentifier (optional, default: ) = ')

    # Create data health database parameters list
    db_parameters = [
        {
            'ParameterKey': 'ParentNetworkStack',
            'ParameterValue': dh_network_stack_name
        }
    ]
    if db_instance_class is not None and db_instance_class.strip() != '':
        db_parameters.append({
            'ParameterKey': 'DBInstanceClass',
            'ParameterValue': db_instance_class
        })
    if db_port is not None and db_port.strip() != '':
        db_parameters.append({
            'ParameterKey': 'DBPort',
            'ParameterValue': db_port
        })
    if db_storage is not None and db_storage.strip() != '':
        db_parameters.append({
            'ParameterKey': 'DBStorage',
            'ParameterValue': db_storage
        })
    if db_master_username is not None and db_master_username.strip() != '':
        db_parameters.append({
            'ParameterKey': 'DBMasterUsername',
            'ParameterValue': db_master_username
        })
    if db_name is not None and db_name.strip() != '':
        db_parameters.append({
            'ParameterKey': 'DBName',
            'ParameterValue': db_name
        })
    if db_multi_az is not None and db_multi_az.strip() != '':
        db_parameters.append({
            'ParameterKey': 'MultiAZ',
            'ParameterValue': db_multi_az
        })
    if db_engine is not None and db_engine.strip() != '':
        db_parameters.append({
            'ParameterKey': 'DBEngine',
            'ParameterValue': db_engine
        })
    if db_postgres_engine_version is not None and db_postgres_engine_version.strip() != '':
        db_parameters.append({
            'ParameterKey': 'DBPostgresEngineVersion',
            'ParameterValue': db_postgres_engine_version
        })
    if db_pref_backup_window is not None and db_pref_backup_window.strip() != '':
        db_parameters.append({
            'ParameterKey': 'PreferredBackupWindow',
            'ParameterValue': db_pref_backup_window
        })
    if db_pref_maintenance_window is not None and db_pref_maintenance_window.strip() != '':
        db_parameters.append({
            'ParameterKey': 'PreferredMaintenanceWindow',
            'ParameterValue': db_pref_maintenance_window
        })
    if db_backup_retention_period is not None and db_backup_retention_period.strip() != '':
        db_parameters.append({
            'ParameterKey': 'DBBackupRetentionPeriod',
            'ParameterValue': db_backup_retention_period
        })
    if db_snapshot_id is not None and db_snapshot_id.strip() != '':
        db_parameters.append({
            'ParameterKey': 'DBSnapshotIdentifier',
            'ParameterValue': db_snapshot_id
        })

    # Create data health database stack
    print('\nCREATE DATA HEALTH DATABASE STACK\n')
    cf_client.create_stack(
        StackName=db_stack_name,
        TemplateBody=db_template,
        Parameters=db_parameters
    )

    # Wait for data health database stack creation to complete
    print('WAIT UNTIL DATA HEALTH DATABASE STACK - CREATE COMPLETE\n')
    db_stack_complete = False
    while not db_stack_complete:
        print('Checking status of data health database stack')
        data = cf_client.describe_stacks(StackName=db_stack_name)
        db_stack_status = data['Stacks'][0]['StackStatus']
        print('Stack status: ' + db_stack_status)
        if db_stack_status in ['ROLLBACK_IN_PROGRESS', 'ROLLBACK_COMPLETE']:
            sys.exit('Error in creating data health database stack. '
                     'Delete the data health database stack manually and try again')
        db_stack_complete = db_stack_status == 'CREATE_COMPLETE'
        if not db_stack_complete:
            time.sleep(30)

    print('\nDATABASE DATA HEALTH STACK CREATION COMPLETE\n')

'''
DATA HEALTH ELASTIC BEANSTALK APP STACK
'''

# Get data health eb parameters from user
print('\nGET PARAMETERS FOR DATA HEALTH ELASTIC BEANSTALK APP STACK\n')
eb_stack_name = os.getenv('DATA_HEALTH_EB_APP_STACK_NAME')
eb_app_env_name = os.getenv('DATA_HEALTH_EB_APP_ENV_NAME')
eb_app_name = os.getenv('DATA_HEALTH_EB_APP_NAME')
eb_soln_stack_name = os.getenv('DATA_HEALTH_EB_SOLN_STACK_NAME')
eb_inst_type = os.getenv('DATA_HEALTH_EB_INSTANCE_TYPE')
eb_pf_arn = os.getenv('DATA_HEALTH_EB_PF_ARN')

if not DISABLE_PROMPTS:
    eb_stack_name = raw_input('Enter DATA_HEALTH_EB_APP_STACK_NAME (default: datahealth-ops-eb): ')
    eb_app_env_name = raw_input('EBEnvironmentName (default: OpsDashboard) = ')
    eb_app_name = raw_input('EBApplicationName (default: OpsDashboard) = ')
    eb_soln_stack_name = raw_input(
        'SolutionStackName (default: 64bit Amazon Linux 2018.03 v2.9.3 running Python 3.6) = ')
    eb_inst_type = raw_input('InstanceType (default: t3a.micro) = ')
    eb_pf_arn = raw_input(
        'PlatformArn (default: '
        'arn:aws:elasticbeanstalk:ap-southeast-1::platform/Python 2.7 running on 64bit Amazon Linux/2.9.3) = ')

# Get Data Health Elastic Beanstalk App stack name
print('START DATA HEALTH ELASTIC BEANSTALK APP STACK CREATION\n')
if eb_stack_name is None or eb_stack_name.strip() == '':
    eb_stack_name = 'datahealth-ops-eb'

# Check if data health elastic beanstalk app stack already exists with the same name
eb_stack_exists = True
try:
    data = cf_client.describe_stacks(StackName=eb_stack_name)
    stack_status = data['Stacks'][0]['StackStatus']
    eb_app_parameters = data['Stacks'][0]['Parameters']
    for parameter in eb_app_parameters:
        if parameter['ParameterKey'] == 'EBEnvironmentName':
            eb_app_env_name = parameter['ParameterValue']
    if not stack_status.endswith('_COMPLETE'):
        sys.exit('Error! Data Health Elastic Beanstalk App stack already exists with status - {}. '
                 'Try with different Data Health Elastic Beanstalk App stack name'.format(stack_status))
    print('\nDATA HEALTH ELASTIC BEANSTALK APP STACK ALREADY EXISTS WITH STATUS - {}, '
          'SKIPPING DATA HEALTH ELASTIC BEANSTALK APP STACK CREATION\n'.format(stack_status))
except:
    eb_stack_exists = False

# Create Data Health Elastic Beanstalk App stack only if it does not exist
if not eb_stack_exists:

    # Get data health eb stack template full path
    eb_stack_file = os.path.join(dir_path, 'datahealth-elasticbeanstalk-standalone.yml')

    # Read data health eb stack template file and convert to string
    eb_stack = open(eb_stack_file, 'r')
    eb_template = eb_stack.read()
    eb_stack.close()

    # Create data health eb parameters list
    eb_parameters = [
        {
            'ParameterKey': 'ParentNetworkStack',
            'ParameterValue': dh_network_stack_name
        },
        {
            'ParameterKey': 'ParentDBStack',
            'ParameterValue': db_stack_name
        },
        {
            'ParameterKey': 'EC2KeyName',
            'ParameterValue': key_name
        }
    ]
    if eb_app_env_name is not None and eb_app_env_name.strip() != '':
        eb_parameters.append({
            'ParameterKey': 'EBEnvironmentName',
            'ParameterValue': eb_app_env_name
        })
    if eb_app_name is not None and eb_app_name.strip() != '':
        eb_parameters.append({
            'ParameterKey': 'EBApplicationName',
            'ParameterValue': eb_app_name
        })
    if eb_pf_arn is not None and eb_pf_arn.strip() != '':
        eb_parameters.append({
            'ParameterKey': 'PlatformArn',
            'ParameterValue': eb_pf_arn
        })
    if eb_soln_stack_name is not None and eb_soln_stack_name.strip() != '':
        eb_parameters.append({
            'ParameterKey': 'SolutionStackName',
            'ParameterValue': eb_soln_stack_name
        })
    if eb_inst_type is not None and eb_inst_type.strip() != '':
        eb_parameters.append({
            'ParameterKey': 'InstanceType',
            'ParameterValue': eb_inst_type
        })

    # Create data health eb app stack
    print('\nCREATE DATA HEALTH ELASTIC BEANSTALK APP STACK\n')
    cf_client.create_stack(
        StackName=eb_stack_name,
        TemplateBody=eb_template,
        Parameters=eb_parameters,
        Capabilities=['CAPABILITY_NAMED_IAM']
    )

    # Wait for data health eb app stack creation to complete
    print('WAIT UNTIL DATA HEALTH ELASTIC BEANSTALK APP STACK - CREATE COMPLETE\n')
    eb_stack_complete = False
    while not eb_stack_complete:
        print('Checking status of data health elastic beanstalk app stack')
        data = cf_client.describe_stacks(StackName=eb_stack_name)
        eb_stack_status = data['Stacks'][0]['StackStatus']
        print('Stack status: ' + eb_stack_status)
        if eb_stack_status in ['ROLLBACK_IN_PROGRESS', 'ROLLBACK_COMPLETE']:
            sys.exit('Error in creating data health eb app stack. '
                     'Delete the data health eb app stack manually and try again')
        eb_stack_complete = eb_stack_status == 'CREATE_COMPLETE'
        if not eb_stack_complete:
            time.sleep(30)

    print('\nDATA HEALTH ELASTIC BEANSTALK APP STACK CREATION COMPLETE\n')
