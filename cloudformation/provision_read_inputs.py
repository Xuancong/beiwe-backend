import os
import provision_properties


def set_env():
    os.environ['AWS_REGION_NAME'] = provision_properties.AWS_REGION_NAME

    os.environ['NETWORK_STACK_NAME'] = provision_properties.NETWORK_STACK_NAME
    os.environ['NETWORK_ENV_NAME'] = provision_properties.NETWORK_ENV_NAME
    os.environ['NETWORK_VPC_CIDR'] = provision_properties.NETWORK_VPC_CIDR
    os.environ['NETWORK_PUBLIC_SUBNET1_CIDR'] = provision_properties.NETWORK_PUBLIC_SUBNET1_CIDR
    os.environ['NETWORK_PUBLIC_SUBNET2_CIDR'] = provision_properties.NETWORK_PUBLIC_SUBNET2_CIDR
    os.environ['NETWORK_PUBLIC_SUBNET3_CIDR'] = provision_properties.NETWORK_PUBLIC_SUBNET3_CIDR
    os.environ['NETWORK_PRIVATE_SUBNET1_CIDR'] = provision_properties.NETWORK_PRIVATE_SUBNET1_CIDR
    os.environ['NETWORK_PRIVATE_SUBNET2_CIDR'] = provision_properties.NETWORK_PRIVATE_SUBNET2_CIDR

    os.environ['VPN_STACK_NAME'] = provision_properties.VPN_STACK_NAME
    os.environ['VPN_KEY_NAME'] = provision_properties.VPN_KEY_NAME
    os.environ['VPN_INSTANCE_TYPE'] = provision_properties.VPN_INSTANCE_TYPE
    os.environ['VPN_IMAGE_REGION'] = provision_properties.VPN_IMAGE_REGION

    os.environ['DB_STACK_NAME'] = provision_properties.DB_STACK_NAME
    os.environ['DB_ENGINE'] = provision_properties.DB_ENGINE
    os.environ['DB_SNAPSHOT_ID'] = provision_properties.DB_SNAPSHOT_ID
    os.environ['DB_INSTANCE_CLASS'] = provision_properties.DB_INSTANCE_CLASS
    os.environ['DB_MASTER_USERNAME'] = provision_properties.DB_MASTER_USERNAME
    os.environ['DB_NAME'] = provision_properties.DB_NAME
    os.environ['DB_BACKUP_RETENTION_PERIOD'] = provision_properties.DB_BACKUP_RETENTION_PERIOD
    os.environ['DB_PREFERRED_BACKUP_WINDOW'] = provision_properties.DB_PREFERRED_BACKUP_WINDOW
    os.environ['DB_PREFERRED_MAINTENANCE_WINDOW'] = provision_properties.DB_PREFERRED_MAINTENANCE_WINDOW

    os.environ['RUN_INIT_SQL'] = provision_properties.RUN_INIT_SQL
    os.environ['INSTANCE_AMI_ID'] = provision_properties.INSTANCE_AMI_ID

    os.environ['S3_STACK_NAME'] = provision_properties.S3_STACK_NAME
    os.environ['S3_PREFIX'] = provision_properties.S3_PREFIX

    os.environ['IAM_STACK_NAME'] = provision_properties.IAM_STACK_NAME

    os.environ['EB_APP_STACK_NAME'] = provision_properties.EB_APP_STACK_NAME
    os.environ['EB_APP_ENV_NAME'] = provision_properties.EB_APP_ENV_NAME
    os.environ['EB_APP_NAME'] = provision_properties.EB_APP_NAME
    os.environ['EB_SOLN_STACK_NAME'] = provision_properties.EB_SOLN_STACK_NAME
    os.environ['EB_INSTANCE_TYPE'] = provision_properties.EB_INSTANCE_TYPE

    os.environ['EB_UI_STACK_NAME'] = provision_properties.EB_UI_STACK_NAME
    os.environ['EB_UI_ENV_NAME'] = provision_properties.EB_UI_ENV_NAME
    os.environ['EB_UI_NAME'] = provision_properties.EB_UI_NAME
    os.environ['EB_UI_SOLN_STACK_NAME'] = provision_properties.EB_UI_SOLN_STACK_NAME
    os.environ['EB_UI_INSTANCE_TYPE'] = provision_properties.EB_UI_INSTANCE_TYPE

    os.environ['IAM_UPLOAD_POLICY_NAME'] = provision_properties.IAM_UPLOAD_POLICY_NAME
    os.environ['IAM_UPLOAD_USERNAME'] = provision_properties.IAM_UPLOAD_USERNAME

    os.environ['IAM_SECRETS_POLICY_NAME'] = provision_properties.IAM_SECRETS_POLICY_NAME
    os.environ['IAM_SECRETS_MANAGER_USERNAME'] = provision_properties.IAM_SECRETS_MANAGER_USERNAME

    os.environ['IS_PCA_IN_DIFFERENT_ACCOUNT'] = provision_properties.IS_PCA_IN_DIFFERENT_ACCOUNT
    os.environ['PCA_ACCOUNT_ACCESS_KEY_ID'] = provision_properties.PCA_ACCOUNT_ACCESS_KEY_ID
    os.environ['PCA_ACCOUNT_SECRET_ACCESS_KEY'] = provision_properties.PCA_ACCOUNT_SECRET_ACCESS_KEY
    os.environ['PCA_ACCOUNT_REGION_NAME'] = provision_properties.PCA_ACCOUNT_REGION_NAME

    os.environ['PCA_ROOT_CA'] = provision_properties.PCA_ROOT_CA
    os.environ['PCA_DOMAIN'] = provision_properties.PCA_DOMAIN
    os.environ['PCA_UPLOAD_CLIENT_SUBDOMAIN'] = provision_properties.PCA_UPLOAD_CLIENT_SUBDOMAIN
    os.environ['PCA_UI_SUBDOMAIN'] = provision_properties.PCA_UI_SUBDOMAIN
    os.environ['PCA_UPLOAD_EXTERNAL_SUBDOMAIN'] = provision_properties.PCA_UPLOAD_EXTERNAL_SUBDOMAIN

    os.environ['ASG_STACK_NAME'] = provision_properties.ASG_STACK_NAME
    os.environ['ASG_IMAGE_REGION'] = provision_properties.ASG_IMAGE_REGION
    os.environ['ASG_INSTANCE_TYPE'] = provision_properties.ASG_INSTANCE_TYPE
    os.environ['ASG_SYS_ADMIN_EMAIL'] = provision_properties.ASG_SYS_ADMIN_EMAIL

    os.environ['EB_PF_ARN'] = provision_properties.EB_PF_ARN

    os.environ['EB_SENTRY_ANDROID_DSN'] = provision_properties.EB_SENTRY_ANDROID_DSN
    os.environ['EB_SENTRY_DATA_PROCESSING_DSN'] = provision_properties.EB_SENTRY_DATA_PROCESSING_DSN
    os.environ['EB_SENTRY_ELASTIC_BEANSTALK_DSN'] = provision_properties.EB_SENTRY_ELASTIC_BEANSTALK_DSN
    os.environ['EB_SENTRY_JAVASCRIPT_DSN'] = provision_properties.EB_SENTRY_JAVASCRIPT_DSN
    os.environ['EB_SYSADMIN_EMAILS'] = provision_properties.EB_SYSADMIN_EMAILS
    os.environ['EB_IS_STAGING'] = provision_properties.EB_IS_STAGING

    os.environ['METRICS_LAMBDA_NAME'] = provision_properties.METRICS_LAMBDA_NAME
    os.environ['METRICS_LAMBDA_STAGE'] = provision_properties.METRICS_LAMBDA_STAGE

    os.environ['REG_METRICS_LAMBDA_NAME'] = provision_properties.REG_METRICS_LAMBDA_NAME

    os.environ['FITBIT_NETWORK_STACK_NAME'] = provision_properties.FITBIT_NETWORK_STACK_NAME
    os.environ['FITBIT_NETWORK_ENV_NAME'] = provision_properties.FITBIT_NETWORK_ENV_NAME
    os.environ['FITBIT_NETWORK_VPC_CIDR'] = provision_properties.FITBIT_NETWORK_VPC_CIDR
    os.environ['FITBIT_NETWORK_PUBLIC_SUBNET1_CIDR'] = provision_properties.FITBIT_NETWORK_PUBLIC_SUBNET1_CIDR
    os.environ['FITBIT_NETWORK_PRIVATE_SUBNET1_CIDR'] = provision_properties.FITBIT_NETWORK_PRIVATE_SUBNET1_CIDR
    os.environ['FITBIT_NETWORK_PRIVATE_SUBNET2_CIDR'] = provision_properties.FITBIT_NETWORK_PRIVATE_SUBNET2_CIDR

    os.environ['DATA_HEALTH_NETWORK_STACK_NAME'] = provision_properties.DATA_HEALTH_NETWORK_STACK_NAME
    os.environ['DATA_HEALTH_NETWORK_ENV_NAME'] = provision_properties.DATA_HEALTH_NETWORK_ENV_NAME
    os.environ['DATA_HEALTH_NETWORK_VPC_CIDR'] = provision_properties.DATA_HEALTH_NETWORK_VPC_CIDR
    os.environ['DATA_HEALTH_NETWORK_PUBLIC_SUBNET1_CIDR'] = provision_properties.DATA_HEALTH_NETWORK_PUBLIC_SUBNET1_CIDR
    os.environ['DATA_HEALTH_NETWORK_PRIVATE_SUBNET1_CIDR'] = \
        provision_properties.DATA_HEALTH_NETWORK_PRIVATE_SUBNET1_CIDR
    os.environ['DATA_HEALTH_NETWORK_PRIVATE_SUBNET2_CIDR'] = \
        provision_properties.DATA_HEALTH_NETWORK_PRIVATE_SUBNET2_CIDR

    os.environ['DATA_HEALTH_VPN_STACK_NAME'] = provision_properties.DATA_HEALTH_VPN_STACK_NAME
    os.environ['DATA_HEALTH_VPN_KEY_NAME'] = provision_properties.DATA_HEALTH_VPN_KEY_NAME
    os.environ['DATA_HEALTH_VPN_INSTANCE_TYPE'] = provision_properties.DATA_HEALTH_VPN_INSTANCE_TYPE
    os.environ['DATA_HEALTH_VPN_IMAGE_REGION'] = provision_properties.DATA_HEALTH_VPN_IMAGE_REGION

    os.environ['DATA_HEALTH_DB_STACK_NAME'] = provision_properties.DATA_HEALTH_DB_STACK_NAME
    os.environ['DATA_HEALTH_DB_INSTANCE_CLASS'] = provision_properties.DATA_HEALTH_DB_INSTANCE_CLASS
    os.environ['DATA_HEALTH_DB_PORT'] = provision_properties.DATA_HEALTH_DB_PORT
    os.environ['DATA_HEALTH_DB_STORAGE'] = provision_properties.DATA_HEALTH_DB_STORAGE
    os.environ['DATA_HEALTH_DB_MASTER_USERNAME'] = provision_properties.DATA_HEALTH_DB_MASTER_USERNAME
    os.environ['DATA_HEALTH_DB_NAME'] = provision_properties.DATA_HEALTH_DB_NAME
    os.environ['DATA_HEALTH_MULTI_AZ'] = provision_properties.DATA_HEALTH_MULTI_AZ
    os.environ['DATA_HEALTH_DB_ENGINE'] = provision_properties.DATA_HEALTH_DB_ENGINE
    os.environ['DATA_HEALTH_DB_POSTGRES_ENGINE_VERSION'] = provision_properties.DATA_HEALTH_DB_POSTGRES_ENGINE_VERSION
    os.environ['DATA_HEALTH_DB_PREFERRED_BACKUP_WINDOW'] = provision_properties.DATA_HEALTH_DB_PREFERRED_BACKUP_WINDOW
    os.environ['DATA_HEALTH_DB_PREFERRED_MAINTENANCE_WINDOW'] = \
        provision_properties.DATA_HEALTH_DB_PREFERRED_MAINTENANCE_WINDOW
    os.environ['DATA_HEALTH_DB_BACKUP_RETENTION_PERIOD'] = provision_properties.DATA_HEALTH_DB_BACKUP_RETENTION_PERIOD
    os.environ['DATA_HEALTH_DB_SNAPSHOT_ID'] = provision_properties.DATA_HEALTH_DB_SNAPSHOT_ID

    os.environ['DATA_HEALTH_EB_APP_STACK_NAME'] = provision_properties.DATA_HEALTH_EB_APP_STACK_NAME
    os.environ['DATA_HEALTH_EB_APP_ENV_NAME'] = provision_properties.DATA_HEALTH_EB_APP_ENV_NAME
    os.environ['DATA_HEALTH_EB_APP_NAME'] = provision_properties.DATA_HEALTH_EB_APP_NAME
    os.environ['DATA_HEALTH_EB_SOLN_STACK_NAME'] = provision_properties.DATA_HEALTH_EB_SOLN_STACK_NAME
    os.environ['DATA_HEALTH_EB_INSTANCE_TYPE'] = provision_properties.DATA_HEALTH_EB_INSTANCE_TYPE
    os.environ['DATA_HEALTH_EB_PF_ARN'] = provision_properties.DATA_HEALTH_EB_PF_ARN
