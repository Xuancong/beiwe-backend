#!/usr/bin/env python3

import os
import sys
import time

'''
INITIALIZE
'''

# Check for boto3 (aws client) and then import
try:
    __import__('boto3')
except ImportError:
    os.system('pip install boto3')

import boto3

DISABLE_PROMPTS = os.getenv('DISABLE_PROMPTS', 1)

# If prompts disabled then read from property file
if DISABLE_PROMPTS:
    import provision_read_inputs
    provision_read_inputs.set_env()

if not DISABLE_PROMPTS:
    # Inform pre-requisite to the user
    print('\nPRE-REQUISITES - NETWORK STACK\n')
    print('1. Increase your EIP limits')
    print('2. Create IAM user that has admin rights to provision the stack')
    print('3. Note down the AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY for the created IAM user')
    print('\nPRE-REQUISITES - VPN STACK\n')
    print('1. Generate a Key Pair from AWS Console -> Services -> EC2 -> Key Pairs -> Create Key Pair')
    print('2. Note down the generated Key Pair name')
    print('3. Ensure the VPN AMI has permissions for the account in which the vpn instance is being created')
    print('\nPRE-REQUISITES - REQUESTING PRIVATE CERTIFICATES FROM PRIVATE CA\n')
    print('1. Create a Private CA and note down the ARN for private CA')
    cont = raw_input('\nPress "y" if you have completed above steps and ready to proceed: ')
    while cont != 'y':
        cont = raw_input('Press "y" if you have completed above steps and ready to proceed: ')

    # Update repo
    print('\nINITIALIZE SCRIPT\n')
    print('Executing git pull')
    os.system('git pull')

# Get aws access key id
aws_access_key_id = os.getenv('AWS_ACCESS_KEY_ID') or raw_input('\nEnter AWS_ACCESS_KEY_ID (required): ')

# Get aws secret access key
aws_secret_access_key = os.getenv('AWS_SECRET_ACCESS_KEY') or raw_input('Enter AWS_SECRET_ACCESS_KEY (required): ')

# Get aws region name
aws_region_name = os.getenv('AWS_REGION_NAME')
if not DISABLE_PROMPTS:
    aws_region_name = raw_input('Enter AWS_REGION_NAME (default: ap-southeast-1): ')
    if aws_region_name is None or aws_region_name.strip() == '':
        aws_region_name = 'ap-southeast-1'

# Get directory full path
dir_path = os.path.dirname(os.path.abspath(__file__))

# Initialize cloudformation client
cf_client = boto3.client('cloudformation',
                         region_name=aws_region_name,
                         aws_access_key_id=aws_access_key_id,
                         aws_secret_access_key=aws_secret_access_key)

'''
NETWORK STACK
'''

# Get network stack name
print('\nSTART NETWORK STACK CREATION\n')
network_stack_name = os.getenv('NETWORK_STACK_NAME')
if not DISABLE_PROMPTS:
    network_stack_name = raw_input('Enter NETWORK_STACK_NAME (default: hopes-upload-network): ')
    if network_stack_name is None or network_stack_name.strip() == '':
        network_stack_name = 'hopes-upload-network'

# Check if network stack already exists with the same name
network_stack_exists = True
try:
    data = cf_client.describe_stacks(StackName=network_stack_name)
    stack_status = data['Stacks'][0]['StackStatus']
    if not stack_status.endswith('_COMPLETE'):
        sys.exit('Error! Network stack already exists with status - {}. '
                 'Try with different network stack name'.format(stack_status))
    print('\nNETWORK STACK ALREADY EXISTS WITH STATUS - {}, '
          'SKIPPING NETWORK STACK CREATION\n'.format(stack_status))
except:
    network_stack_exists = False

# Create network stack only if it does not exist
if not network_stack_exists:

    # Get network stack template full path
    nw_stack_file = os.path.join(dir_path, 'network-standalone.yml')

    # Read network stack template file and convert to string
    nw_stack = open(nw_stack_file, 'r')
    network_template = nw_stack.read()
    nw_stack.close()

    print('\nGET PARAMETERS FOR NETWORK STACK\n')
    network_env_name = os.getenv('NETWORK_ENV_NAME')
    vpc_cidr = os.getenv('NETWORK_VPC_CIDR')
    vpc_cidr_ip_split = vpc_cidr.replace('.0.0/16', '')
    pub_subnet1_cidr = os.getenv('NETWORK_PUBLIC_SUBNET1_CIDR')
    pub_subnet2_cidr = os.getenv('NETWORK_PUBLIC_SUBNET2_CIDR')
    pub_subnet3_cidr = os.getenv('NETWORK_PUBLIC_SUBNET3_CIDR')
    pvt_subnet1_cidr = os.getenv('NETWORK_PRIVATE_SUBNET1_CIDR')
    pvt_subnet2_cidr = os.getenv('NETWORK_PRIVATE_SUBNET2_CIDR')

    if not DISABLE_PROMPTS:
        # Get network parameters from user
        network_env_name = raw_input('EnvironmentName (required) = ')
        vpc_cidr = raw_input('VpcCIDR (default: 10.192.0.0/16) = ')
        vpc_cidr_ip_split = vpc_cidr.replace('.0.0/16', '')
        if vpc_cidr_ip_split == '':
            vpc_cidr_ip_split = '10.192'
        pub_subnet1_cidr = raw_input('PublicSubnet1CIDR (default: {}.10.0/24) = '.format(vpc_cidr_ip_split))
        pub_subnet2_cidr = raw_input('PublicSubnet2CIDR (default: {}.11.0/24) = '.format(vpc_cidr_ip_split))
        pub_subnet3_cidr = raw_input('PublicSubnet3CIDR (default: {}.12.0/24) = '.format(vpc_cidr_ip_split))
        pvt_subnet1_cidr = raw_input('PrivateSubnet1CIDR (default: {}.20.0/24) = '.format(vpc_cidr_ip_split))
        pvt_subnet2_cidr = raw_input('PrivateSubnet2CIDR (default: {}.21.0/24) = '.format(vpc_cidr_ip_split))

    # Create network parameters list
    network_parameters = [
        {
            'ParameterKey': 'EnvironmentName',
            'ParameterValue': network_env_name
        }
    ]
    if vpc_cidr is not None and vpc_cidr.strip() != '':
        network_parameters.append({
            'ParameterKey': 'VpcCIDR',
            'ParameterValue': vpc_cidr
        })
    if (pub_subnet1_cidr is not None and pub_subnet1_cidr.strip() != '') or vpc_cidr_ip_split != '10.192':
        if pub_subnet1_cidr is None or pub_subnet1_cidr.strip() == '':
            pub_subnet1_cidr = '{}.10.0/24'.format(vpc_cidr_ip_split)
        network_parameters.append({
            'ParameterKey': 'PublicSubnet1CIDR',
            'ParameterValue': pub_subnet1_cidr
        })
    if (pub_subnet2_cidr is not None and pub_subnet2_cidr.strip() != '') or vpc_cidr_ip_split != '10.192':
        if pub_subnet2_cidr is None or pub_subnet2_cidr.strip() == '':
            pub_subnet2_cidr = '{}.11.0/24'.format(vpc_cidr_ip_split)
        network_parameters.append({
            'ParameterKey': 'PublicSubnet2CIDR',
            'ParameterValue': pub_subnet2_cidr
        })
    if (pub_subnet3_cidr is not None and pub_subnet3_cidr.strip() != '') or vpc_cidr_ip_split != '10.192':
        if pub_subnet3_cidr is None or pub_subnet3_cidr.strip() == '':
            pub_subnet3_cidr = '{}.12.0/24'.format(vpc_cidr_ip_split)
        network_parameters.append({
            'ParameterKey': 'PublicSubnet3CIDR',
            'ParameterValue': pub_subnet3_cidr
        })
    if (pvt_subnet1_cidr is not None and pvt_subnet1_cidr.strip() != '') or vpc_cidr_ip_split != '10.192':
        if pvt_subnet1_cidr is None or pvt_subnet1_cidr.strip() == '':
            pvt_subnet1_cidr = '{}.20.0/24'.format(vpc_cidr_ip_split)
        network_parameters.append({
            'ParameterKey': 'PrivateSubnet1CIDR',
            'ParameterValue': pvt_subnet1_cidr
        })
    if (pvt_subnet2_cidr is not None and pvt_subnet2_cidr.strip() != '') or vpc_cidr_ip_split != '10.192':
        if pvt_subnet2_cidr is None or pvt_subnet2_cidr.strip() == '':
            pvt_subnet2_cidr = '{}.21.0/24'.format(vpc_cidr_ip_split)
        network_parameters.append({
            'ParameterKey': 'PrivateSubnet2CIDR',
            'ParameterValue': pvt_subnet2_cidr
        })

    # Create network stack
    print('\nCREATE NETWORK STACK\n')
    cf_client.create_stack(
        StackName=network_stack_name,
        TemplateBody=network_template,
        Parameters=network_parameters
    )

    # Wait for network stack creation to complete
    print('WAIT UNTIL NETWORK STACK - CREATE COMPLETE\n')
    nw_stack_complete = False
    while not nw_stack_complete:
        print('Checking status of network stack')
        data = cf_client.describe_stacks(StackName=network_stack_name)
        network_stack_status = data['Stacks'][0]['StackStatus']
        print('Stack status: ' + network_stack_status)
        if network_stack_status in ['ROLLBACK_IN_PROGRESS', 'ROLLBACK_COMPLETE']:
            sys.exit('Error in creating network stack. Delete the network stack manually and try again')
        nw_stack_complete = network_stack_status == 'CREATE_COMPLETE'
        if not nw_stack_complete:
            time.sleep(30)

    print('\nNETWORK STACK CREATION COMPLETE\n')

'''
VPN STACK
'''

# Initialize vpn stack name and flag
vpn_stack_name = ''
vpn_stack_exists = False
key_name = ''

# List all stacks
stacks = cf_client.list_stacks(StackStatusFilter=[
        'CREATE_IN_PROGRESS', 'CREATE_FAILED', 'CREATE_COMPLETE', 'ROLLBACK_IN_PROGRESS', 'ROLLBACK_FAILED',
        'ROLLBACK_COMPLETE', 'DELETE_IN_PROGRESS', 'DELETE_FAILED', 'UPDATE_IN_PROGRESS',
        'UPDATE_COMPLETE_CLEANUP_IN_PROGRESS', 'UPDATE_COMPLETE', 'UPDATE_ROLLBACK_IN_PROGRESS',
        'UPDATE_ROLLBACK_FAILED', 'UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS', 'UPDATE_ROLLBACK_COMPLETE'
    ])
stack_list = stacks['StackSummaries']

# Check if vpn stack is already exists with parent stack as network stack
if network_stack_exists:
    stack_exists = False
    is_vpn_stack = False
    is_key_name = False
    for i in range(len(stack_list)):
        data = cf_client.describe_stacks(StackName=stack_list[i]['StackName'])
        try:
            parameters = data['Stacks'][0]['Parameters']
        except:
            continue
        stack_exists = False
        is_vpn_stack = False
        is_key_name = False
        for parameter in parameters:
            if parameter['ParameterKey'] == 'ParentNetworkStack' and parameter['ParameterValue'] == network_stack_name:
                stack_exists = True
            if parameter['ParameterKey'] == 'VpnImageRegion':
                is_vpn_stack = True
            if parameter['ParameterKey'] == 'KeyName':
                key_name = parameter['ParameterValue']
                is_key_name = True
            if is_vpn_stack and stack_exists and is_key_name:
                vpn_stack_exists = True
                break
        if vpn_stack_exists:
            vpn_stack_name = data['Stacks'][0]['StackName']
            vpn_stack_status = data['Stacks'][0]['StackStatus']
            if '_COMPLETE' not in vpn_stack_status:
                sys.exit('Error! VPN stack already exists with status - {}. '
                         'Try with different vpn stack name'.format(vpn_stack_status))
            print('VPN STACK ALREADY EXISTS WITH STATUS - {}, '
                  'SKIPPING VPN STACK CREATION\n'.format(vpn_stack_status))
            break

# Create VPN stack only if it does not exist
if not vpn_stack_exists:

    # Get vpn stack template full path
    vpn_stack_file = os.path.join(dir_path, 'vpn-standalone-beiwe.yml')

    # Get vpn stack name
    print('START VPN STACK CREATION\n')
    vpn_stack_name = os.getenv('VPN_STACK_NAME')
    if not DISABLE_PROMPTS:
        vpn_stack_name = raw_input('Enter VPN_STACK_NAME (default: hopes-prod-vpn): ')
        if vpn_stack_name is None or vpn_stack_name.strip() == '':
            vpn_stack_name = 'hopes-prod-vpn'

    # Read vpn stack template file and convert to string
    vpn_stack = open(vpn_stack_file, 'r')
    vpn_template = vpn_stack.read()
    vpn_stack.close()

    # Get vpn parameters from user
    print('\nGET PARAMETERS FOR VPN STACK\n')
    key_name = os.getenv('VPN_KEY_NAME')
    inst_type = os.getenv('VPN_INSTANCE_TYPE')
    vpn_img_reg = os.getenv('VPN_IMAGE_REGION')

    if not DISABLE_PROMPTS:
        key_name = raw_input('KeyName (required) = ')
        inst_type = raw_input('InstanceType (default: t3a.nano) = ')
        vpn_img_reg = raw_input('VpnImageRegion (Allowed: Singapore/Sydney, Default: Singapore) = ')

    # Create vpn parameters list
    vpn_parameters = [
        {
            'ParameterKey': 'ParentNetworkStack',
            'ParameterValue': network_stack_name
        },
        {
            'ParameterKey': 'KeyName',
            'ParameterValue': key_name
        }
    ]
    if inst_type is not None and inst_type.strip() != '':
        vpn_parameters.append({
            'ParameterKey': 'InstanceType',
            'ParameterValue': inst_type
        })
    if vpn_img_reg is not None and vpn_img_reg.strip() != '':
        vpn_parameters.append({
            'ParameterKey': 'VpnImageRegion',
            'ParameterValue': vpn_img_reg
        })

    # Create vpn stack
    print('\nCREATE VPN STACK\n')
    cf_client.create_stack(
        StackName=vpn_stack_name,
        TemplateBody=vpn_template,
        Parameters=vpn_parameters
    )

    # Wait for vpn stack creation to complete
    print('WAIT UNTIL VPN STACK - CREATE COMPLETE\n')
    vpn_stack_complete = False
    while not vpn_stack_complete:
        print('Checking status of vpn stack')
        data = cf_client.describe_stacks(StackName=vpn_stack_name)
        vpn_stack_status = data['Stacks'][0]['StackStatus']
        print('Stack status: ' + vpn_stack_status)
        if vpn_stack_status in ['ROLLBACK_IN_PROGRESS', 'ROLLBACK_COMPLETE']:
            sys.exit('Error in creating vpn stack. Delete the vpn stack manually and try again')
        vpn_stack_complete = data['Stacks'][0]['StackStatus'] == 'CREATE_COMPLETE'
        if not vpn_stack_complete:
            time.sleep(30)

    print('\nVPN STACK CREATION COMPLETE\n')
