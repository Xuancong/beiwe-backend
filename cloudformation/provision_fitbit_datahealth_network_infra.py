#!/usr/bin/env python3

import os
import sys
import time

'''
INITIALIZE
'''

# Check for boto3 (aws client) and then import
try:
    __import__('boto3')
except ImportError:
    os.system('pip install boto3')

import boto3

DISABLE_PROMPTS = os.getenv('DISABLE_PROMPTS', 1)

# If prompts disabled then read from property file
if DISABLE_PROMPTS:
    import provision_read_inputs
    provision_read_inputs.set_env()

if not DISABLE_PROMPTS:
    # Inform pre-requisite to the user
    print('\nPRE-REQUISITES - FITBIT AND DATA HEALTH NETWORK STACK\n')
    print('1. Increase your EIP limits')
    print('2. Create IAM user that has admin rights to provision the stack')
    print('3. Note down the AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY for the created IAM user')
    print('\nPRE-REQUISITES - FITBIT VPN STACK\n')
    print('1. Generate a Key Pair from AWS Console -> Services -> EC2 -> Key Pairs -> Create Key Pair')
    print('2. Note down the generated Key Pair name')
    print('3. Ensure the VPN AMI has permissions for the account in which the vpn instance is being created')
    cont = raw_input('\nPress "y" if you have completed above steps and ready to proceed: ')
    while cont != 'y':
        cont = raw_input('Press "y" if you have completed above steps and ready to proceed: ')

# Get aws access key id
aws_access_key_id = os.getenv('AWS_ACCESS_KEY_ID') or raw_input('\nEnter AWS_ACCESS_KEY_ID (required): ')

# Get aws secret access key
aws_secret_access_key = os.getenv('AWS_SECRET_ACCESS_KEY') or raw_input('Enter AWS_SECRET_ACCESS_KEY (required): ')

# Get aws region name
aws_region_name = os.getenv('AWS_REGION_NAME')
if not DISABLE_PROMPTS:
    aws_region_name = raw_input('Enter AWS_REGION_NAME (default: ap-southeast-1): ')
    if aws_region_name is None or aws_region_name.strip() == '':
        aws_region_name = 'ap-southeast-1'

# Get directory full path
dir_path = os.path.dirname(os.path.abspath(__file__))

# Initialize cloudformation client
cf_client = boto3.client('cloudformation',
                         region_name=aws_region_name,
                         aws_access_key_id=aws_access_key_id,
                         aws_secret_access_key=aws_secret_access_key)

# Initialize route53 client
route53_client = boto3.client('route53',
                              region_name=aws_region_name,
                              aws_access_key_id=aws_access_key_id,
                              aws_secret_access_key=aws_secret_access_key)

'''
FITBIT NETWORK STACK
'''

# Get network stack name
print('\nGET NETWORK STACK NAME THAT HAS PRIVATE HOSTED ZONE\n')
network_stack_name = os.getenv('NETWORK_STACK_NAME')
if not DISABLE_PROMPTS:
    network_stack_name = raw_input('Enter NETWORK_STACK_NAME (default: hopes-upload-network): ')
    if network_stack_name is None or network_stack_name.strip() == '':
        network_stack_name = 'hopes-upload-network'

# Get hosted zone id for the route 53 association
print('\nGET PRIVATE HOSTED ZONE ID\n')
pvt_hosted_zone_id = ''
data = cf_client.describe_stacks(StackName=network_stack_name)
nw_outputs = data['Stacks'][0]['Outputs']
for output in nw_outputs:
    if output['ExportName'] == network_stack_name + '-PrivateHostedZone':
        pvt_hosted_zone_id = output['OutputValue']

# Get fitbit network stack name
print('\nSTART FITBIT NETWORK STACK CREATION\n')
fitbit_network_stack_name = os.getenv('FITBIT_NETWORK_STACK_NAME')
if not DISABLE_PROMPTS:
    fitbit_network_stack_name = raw_input('Enter FITBIT_NETWORK_STACK_NAME (default: hopes-fitbit-network): ')
    if fitbit_network_stack_name is None or fitbit_network_stack_name.strip() == '':
        fitbit_network_stack_name = 'hopes-fitbit-network'

# Check if fitbit network stack already exists with the same name
fitbit_network_stack_exists = True
try:
    data = cf_client.describe_stacks(StackName=fitbit_network_stack_name)
    stack_status = data['Stacks'][0]['StackStatus']
    if not stack_status.endswith('_COMPLETE'):
        sys.exit('Error! Fitbit Network stack already exists with status - {}. '
                 'Try with different network stack name'.format(stack_status))
    print('\nFITBIT NETWORK STACK ALREADY EXISTS WITH STATUS - {}, '
          'SKIPPING FITBIT NETWORK STACK CREATION\n'.format(stack_status))
except:
    fitbit_network_stack_exists = False

# Create fitbit network stack only if it does not exist
if not fitbit_network_stack_exists:

    # Get fitbit network stack template full path
    fb_nw_stack_file = os.path.join(dir_path, 'network-standalone-fitbit.yml')

    # Read fitbit network stack template file and convert to string
    fb_nw_stack = open(fb_nw_stack_file, 'r')
    fitbit_network_template = fb_nw_stack.read()
    fb_nw_stack.close()

    print('\nGET PARAMETERS FOR FITBIT NETWORK STACK\n')
    fitbit_network_env_name = os.getenv('FITBIT_NETWORK_ENV_NAME')
    fitbit_vpc_cidr = os.getenv('FITBIT_NETWORK_VPC_CIDR')
    fb_vpc_cidr_ip_split = fitbit_vpc_cidr.replace('.0.0/16', '')
    fb_pub_subnet1_cidr = os.getenv('FITBIT_NETWORK_PUBLIC_SUBNET1_CIDR')
    fb_pvt_subnet1_cidr = os.getenv('FITBIT_NETWORK_PRIVATE_SUBNET1_CIDR')
    fb_pvt_subnet2_cidr = os.getenv('FITBIT_NETWORK_PRIVATE_SUBNET2_CIDR')

    if not DISABLE_PROMPTS:
        # Get fitbit network parameters from user
        fitbit_network_env_name = raw_input('EnvironmentName (required) = ')
        fitbit_vpc_cidr = raw_input('VpcCIDR (default: 10.102.0.0/16) = ')
        fb_vpc_cidr_ip_split = fitbit_vpc_cidr.replace('.0.0/16', '')
        if fb_vpc_cidr_ip_split == '':
            fb_vpc_cidr_ip_split = '10.102'
        fb_pub_subnet1_cidr = raw_input('PublicSubnet1CIDR (default: {}.10.0/24) = '.format(fb_vpc_cidr_ip_split))
        fb_pvt_subnet1_cidr = raw_input('PrivateSubnet1CIDR (default: {}.20.0/24) = '.format(fb_vpc_cidr_ip_split))
        fb_pvt_subnet2_cidr = raw_input('PrivateSubnet2CIDR (default: {}.21.0/24) = '.format(fb_vpc_cidr_ip_split))

    # Create fitbit network parameters list
    fitbit_network_parameters = [
        {
            'ParameterKey': 'EnvironmentName',
            'ParameterValue': fitbit_network_env_name
        }
    ]
    if fitbit_vpc_cidr is not None and fitbit_vpc_cidr.strip() != '':
        fitbit_network_parameters.append({
            'ParameterKey': 'VpcCIDR',
            'ParameterValue': fitbit_vpc_cidr
        })
    if (fb_pub_subnet1_cidr is not None and fb_pub_subnet1_cidr.strip() != '') or fb_vpc_cidr_ip_split != '10.102':
        if fb_pub_subnet1_cidr is None or fb_pub_subnet1_cidr.strip() == '':
            fb_pub_subnet1_cidr = '{}.10.0/24'.format(fb_vpc_cidr_ip_split)
        fitbit_network_parameters.append({
            'ParameterKey': 'PublicSubnet1CIDR',
            'ParameterValue': fb_pub_subnet1_cidr
        })
    if (fb_pvt_subnet1_cidr is not None and fb_pvt_subnet1_cidr.strip() != '') or fb_vpc_cidr_ip_split != '10.102':
        if fb_pvt_subnet1_cidr is None or fb_pvt_subnet1_cidr.strip() == '':
            fb_pvt_subnet1_cidr = '{}.20.0/24'.format(fb_vpc_cidr_ip_split)
        fitbit_network_parameters.append({
            'ParameterKey': 'PrivateSubnet1CIDR',
            'ParameterValue': fb_pvt_subnet1_cidr
        })
    if (fb_pvt_subnet2_cidr is not None and fb_pvt_subnet2_cidr.strip() != '') or fb_vpc_cidr_ip_split != '10.102':
        if fb_pvt_subnet2_cidr is None or fb_pvt_subnet2_cidr.strip() == '':
            fb_pvt_subnet2_cidr = '{}.21.0/24'.format(fb_vpc_cidr_ip_split)
        fitbit_network_parameters.append({
            'ParameterKey': 'PrivateSubnet2CIDR',
            'ParameterValue': fb_pvt_subnet2_cidr
        })

    # Create fitbit network stack
    print('\nCREATE FITBIT NETWORK STACK\n')
    cf_client.create_stack(
        StackName=fitbit_network_stack_name,
        TemplateBody=fitbit_network_template,
        Parameters=fitbit_network_parameters
    )

    # Wait for fitbit network stack creation to complete
    print('WAIT UNTIL FITBIT NETWORK STACK - CREATE COMPLETE\n')
    fb_nw_stack_complete = False
    while not fb_nw_stack_complete:
        print('Checking status of fitbit network stack')
        data = cf_client.describe_stacks(StackName=fitbit_network_stack_name)
        fb_network_stack_status = data['Stacks'][0]['StackStatus']
        print('Stack status: ' + fb_network_stack_status)
        if fb_network_stack_status in ['ROLLBACK_IN_PROGRESS', 'ROLLBACK_COMPLETE']:
            sys.exit('Error in creating fitbit network stack. Delete the fitbit network stack manually and try again')
        fb_nw_stack_complete = fb_network_stack_status == 'CREATE_COMPLETE'
        if not fb_nw_stack_complete:
            time.sleep(30)

    print('\nFITBIT NETWORK STACK CREATION COMPLETE\n')

'''
ROUTE 53 VPC ASSOCIATION - FITBIT NETWORK
'''

# Get the vpc id for route 53 association
fb_vpc_id = ''
fb_network_stack = cf_client.describe_stacks(StackName=fitbit_network_stack_name)
for output in fb_network_stack['Stacks'][0]['Outputs']:
    if output['ExportName'] == fitbit_network_stack_name + '-VPCID':
        fb_vpc_id = output['OutputValue']

# Route 53 VPC association
print('\nROUTE 53 VPC ASSOCIATION - FITBIT NETWORK\n')
try:
    route53_client.associate_vpc_with_hosted_zone(
        HostedZoneId=pvt_hosted_zone_id,
        VPC={
            'VPCRegion': aws_region_name,
            'VPCId': fb_vpc_id
        }
    )
except Exception as e:
    if 'ConflictingDomainExists' not in e.message:
        raise
    print('\nROUTE 53 VPC ASSOCIATION ALREADY PRESENT\n')

'''
DATA HEALTH NETWORK STACK
'''

# Get data health network stack name
print('\nSTART DATA HEALTH NETWORK STACK CREATION\n')
dh_network_stack_name = os.getenv('DATA_HEALTH_NETWORK_STACK_NAME')
if not DISABLE_PROMPTS:
    dh_network_stack_name = raw_input('Enter DATA_HEALTH_NETWORK_STACK_NAME (default: hopes-datahealth-network): ')
    if dh_network_stack_name is None or dh_network_stack_name.strip() == '':
        dh_network_stack_name = 'hopes-datahealth-network'

# Check if data health network stack already exists with the same name
dh_network_stack_exists = True
try:
    data = cf_client.describe_stacks(StackName=dh_network_stack_name)
    stack_status = data['Stacks'][0]['StackStatus']
    if not stack_status.endswith('_COMPLETE'):
        sys.exit('Error! Data health Network stack already exists with status - {}. '
                 'Try with different network stack name'.format(stack_status))
    print('\nDATA HEALTH NETWORK STACK ALREADY EXISTS WITH STATUS - {}, '
          'SKIPPING DATA HEALTH NETWORK STACK CREATION\n'.format(stack_status))
except:
    dh_network_stack_exists = False

# Create data health network stack only if it does not exist
if not dh_network_stack_exists:

    # Get data health network stack template full path
    dh_nw_stack_file = os.path.join(dir_path, 'network-standalone-health.yml')

    # Read data health network stack template file and convert to string
    dh_nw_stack = open(dh_nw_stack_file, 'r')
    dh_network_template = dh_nw_stack.read()
    dh_nw_stack.close()

    print('\nGET PARAMETERS FOR DATA HEALTH NETWORK STACK\n')
    dh_network_env_name = os.getenv('DATA_HEALTH_NETWORK_ENV_NAME')
    dh_vpc_cidr = os.getenv('DATA_HEALTH_NETWORK_VPC_CIDR')
    dh_vpc_cidr_ip_split = dh_vpc_cidr.replace('.0.0/16', '')
    dh_pub_subnet1_cidr = os.getenv('DATA_HEALTH_NETWORK_PUBLIC_SUBNET1_CIDR')
    dh_pvt_subnet1_cidr = os.getenv('DATA_HEALTH_NETWORK_PRIVATE_SUBNET1_CIDR')
    dh_pvt_subnet2_cidr = os.getenv('DATA_HEALTH_NETWORK_PRIVATE_SUBNET2_CIDR')

    if not DISABLE_PROMPTS:
        # Get data health network parameters from user
        dh_network_env_name = raw_input('EnvironmentName (required) = ')
        dh_vpc_cidr = raw_input('VpcCIDR (default: 10.152.0.0/16) = ')
        dh_vpc_cidr_ip_split = dh_vpc_cidr.replace('.0.0/16', '')
        if dh_vpc_cidr_ip_split == '':
            dh_vpc_cidr_ip_split = '10.152'
        dh_pub_subnet1_cidr = raw_input('PublicSubnet1CIDR (default: {}.10.0/24) = '.format(dh_vpc_cidr_ip_split))
        dh_pvt_subnet1_cidr = raw_input('PrivateSubnet1CIDR (default: {}.20.0/24) = '.format(dh_vpc_cidr_ip_split))
        dh_pvt_subnet2_cidr = raw_input('PrivateSubnet2CIDR (default: {}.21.0/24) = '.format(dh_vpc_cidr_ip_split))

    # Create data health network parameters list
    dh_network_parameters = [
        {
            'ParameterKey': 'EnvironmentName',
            'ParameterValue': dh_network_env_name
        }
    ]
    if dh_vpc_cidr is not None and dh_vpc_cidr.strip() != '':
        dh_network_parameters.append({
            'ParameterKey': 'VpcCIDR',
            'ParameterValue': dh_vpc_cidr
        })
    if (dh_pub_subnet1_cidr is not None and dh_pub_subnet1_cidr.strip() != '') or dh_vpc_cidr_ip_split != '10.152':
        if dh_pub_subnet1_cidr is None or dh_pub_subnet1_cidr.strip() == '':
            dh_pub_subnet1_cidr = '{}.10.0/24'.format(dh_vpc_cidr_ip_split)
        dh_network_parameters.append({
            'ParameterKey': 'PublicSubnet1CIDR',
            'ParameterValue': dh_pub_subnet1_cidr
        })
    if (dh_pvt_subnet1_cidr is not None and dh_pvt_subnet1_cidr.strip() != '') or dh_vpc_cidr_ip_split != '10.152':
        if dh_pvt_subnet1_cidr is None or dh_pvt_subnet1_cidr.strip() == '':
            dh_pvt_subnet1_cidr = '{}.20.0/24'.format(dh_vpc_cidr_ip_split)
        dh_network_parameters.append({
            'ParameterKey': 'PrivateSubnet1CIDR',
            'ParameterValue': dh_pvt_subnet1_cidr
        })
    if (dh_pvt_subnet2_cidr is not None and dh_pvt_subnet2_cidr.strip() != '') or dh_vpc_cidr_ip_split != '10.152':
        if dh_pvt_subnet2_cidr is None or dh_pvt_subnet2_cidr.strip() == '':
            dh_pvt_subnet2_cidr = '{}.21.0/24'.format(dh_vpc_cidr_ip_split)
        dh_network_parameters.append({
            'ParameterKey': 'PrivateSubnet2CIDR',
            'ParameterValue': dh_pvt_subnet2_cidr
        })

    # Create data health network stack
    print('\nCREATE DATA HEALTH NETWORK STACK\n')
    cf_client.create_stack(
        StackName=dh_network_stack_name,
        TemplateBody=dh_network_template,
        Parameters=dh_network_parameters
    )

    # Wait for data health network stack creation to complete
    print('WAIT UNTIL DATA HEALTH NETWORK STACK - CREATE COMPLETE\n')
    dh_nw_stack_complete = False
    while not dh_nw_stack_complete:
        print('Checking status of data health network stack')
        data = cf_client.describe_stacks(StackName=dh_network_stack_name)
        dh_network_stack_status = data['Stacks'][0]['StackStatus']
        print('Stack status: ' + dh_network_stack_status)
        if dh_network_stack_status in ['ROLLBACK_IN_PROGRESS', 'ROLLBACK_COMPLETE']:
            sys.exit('Error in creating data health network stack. '
                     'Delete the data health network stack manually and try again')
        dh_nw_stack_complete = dh_network_stack_status == 'CREATE_COMPLETE'
        if not dh_nw_stack_complete:
            time.sleep(30)

    print('\nDATA HEALTH NETWORK STACK CREATION COMPLETE\n')

'''
ROUTE 53 VPC ASSOCIATION - DATA HEALTH NETWORK
'''

# Get the vpc id for route 53 association
dh_vpc_id = ''
dh_network_stack = cf_client.describe_stacks(StackName=dh_network_stack_name)
for output in dh_network_stack['Stacks'][0]['Outputs']:
    if output['ExportName'] == dh_network_stack_name + '-VPCID':
        dh_vpc_id = output['OutputValue']

# Route 53 VPC association
print('\nROUTE 53 VPC ASSOCIATION - DATA HEALTH\n')
try:
    route53_client.associate_vpc_with_hosted_zone(
        HostedZoneId=pvt_hosted_zone_id,
        VPC={
            'VPCRegion': aws_region_name,
            'VPCId': dh_vpc_id
        }
    )
except Exception as e:
    if 'ConflictingDomainExists' not in e.message:
        raise
    print('\nROUTE 53 VPC ASSOCIATION ALREADY PRESENT\n')

'''
DATA HEALTH VPN STACK
'''

# Initialize data health vpn stack name and flag
vpn_stack_name = ''
vpn_stack_exists = False
key_name = ''

# List all stacks
stacks = cf_client.list_stacks(StackStatusFilter=[
        'CREATE_IN_PROGRESS', 'CREATE_FAILED', 'CREATE_COMPLETE', 'ROLLBACK_IN_PROGRESS', 'ROLLBACK_FAILED',
        'ROLLBACK_COMPLETE', 'DELETE_IN_PROGRESS', 'DELETE_FAILED', 'UPDATE_IN_PROGRESS',
        'UPDATE_COMPLETE_CLEANUP_IN_PROGRESS', 'UPDATE_COMPLETE', 'UPDATE_ROLLBACK_IN_PROGRESS',
        'UPDATE_ROLLBACK_FAILED', 'UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS', 'UPDATE_ROLLBACK_COMPLETE'
    ])
stack_list = stacks['StackSummaries']

# Check if data health vpn stack is already exists with parent stack as fitbit network stack
if dh_network_stack_exists:
    stack_exists = False
    is_vpn_stack = False
    is_key_name = False
    for i in range(len(stack_list)):
        data = cf_client.describe_stacks(StackName=stack_list[i]['StackName'])
        try:
            parameters = data['Stacks'][0]['Parameters']
        except:
            continue
        for parameter in parameters:
            if parameter['ParameterKey'] == 'ParentNetworkStack' \
                    and parameter['ParameterValue'] == dh_network_stack_name:
                stack_exists = True
            if parameter['ParameterKey'] == 'VpnImageRegion':
                is_vpn_stack = True
            if parameter['ParameterKey'] == 'KeyName':
                key_name = parameter['ParameterValue']
                is_key_name = True
            if is_vpn_stack and stack_exists and is_key_name:
                vpn_stack_exists = True
                break
        if vpn_stack_exists:
            vpn_stack_name = data['Stacks'][0]['StackName']
            vpn_stack_status = data['Stacks'][0]['StackStatus']
            if '_COMPLETE' not in vpn_stack_status:
                sys.exit('Error! Data Health VPN stack already exists with status - {}. '
                         'Try with different data health vpn stack name'.format(vpn_stack_status))
            print('DATA HEALTH VPN STACK ALREADY EXISTS WITH STATUS - {}, '
                  'SKIPPING DATA HEALTH VPN STACK CREATION\n'.format(vpn_stack_status))
            break

# Create data health VPN stack only if it does not exist
if not vpn_stack_exists:

    # Get data health vpn stack template full path
    vpn_stack_file = os.path.join(dir_path, 'vpn-standalone-datahealth.yml')

    # Get data health vpn stack name
    print('START DATA HEALTH VPN STACK CREATION\n')
    vpn_stack_name = os.getenv('DATA_HEALTH_VPN_STACK_NAME')
    if not DISABLE_PROMPTS:
        vpn_stack_name = raw_input('Enter DATA_HEALTH_VPN_STACK_NAME (default: hopes-datahealth-vpn): ')
        if vpn_stack_name is None or vpn_stack_name.strip() == '':
            vpn_stack_name = 'hopes-datahealth-vpn'

    # Read data health vpn stack template file and convert to string
    vpn_stack = open(vpn_stack_file, 'r')
    vpn_template = vpn_stack.read()
    vpn_stack.close()

    # Get data health vpn parameters from user
    print('\nGET PARAMETERS FOR DATA HEALTH VPN STACK\n')
    key_name = os.getenv('DATA_HEALTH_VPN_KEY_NAME')
    inst_type = os.getenv('DATA_HEALTH_VPN_INSTANCE_TYPE')
    vpn_img_reg = os.getenv('DATA_HEALTH_VPN_IMAGE_REGION')

    if not DISABLE_PROMPTS:
        key_name = raw_input('KeyName (required) = ')
        inst_type = raw_input('InstanceType (default: t3a.nano) = ')
        vpn_img_reg = raw_input('VpnImageRegion (Allowed: Singapore/Sydney, Default: Singapore) = ')

    # Create data health vpn parameters list
    vpn_parameters = [
        {
            'ParameterKey': 'ParentNetworkStack',
            'ParameterValue': dh_network_stack_name
        },
        {
            'ParameterKey': 'KeyName',
            'ParameterValue': key_name
        }
    ]
    if inst_type is not None and inst_type.strip() != '':
        vpn_parameters.append({
            'ParameterKey': 'InstanceType',
            'ParameterValue': inst_type
        })
    if vpn_img_reg is not None and vpn_img_reg.strip() != '':
        vpn_parameters.append({
            'ParameterKey': 'VpnImageRegion',
            'ParameterValue': vpn_img_reg
        })

    # Create data health vpn stack
    print('\nCREATE DATA HEALTH VPN STACK\n')
    cf_client.create_stack(
        StackName=vpn_stack_name,
        TemplateBody=vpn_template,
        Parameters=vpn_parameters
    )

    # Wait for data health vpn stack creation to complete
    print('WAIT UNTIL DATA HEALTH VPN STACK - CREATE COMPLETE\n')
    vpn_stack_complete = False
    while not vpn_stack_complete:
        print('Checking status of data health vpn stack')
        data = cf_client.describe_stacks(StackName=vpn_stack_name)
        vpn_stack_status = data['Stacks'][0]['StackStatus']
        print('Stack status: ' + vpn_stack_status)
        if vpn_stack_status in ['ROLLBACK_IN_PROGRESS', 'ROLLBACK_COMPLETE']:
            sys.exit('Error in creating data health vpn stack. '
                     'Delete the data health vpn stack manually and try again')
        vpn_stack_complete = data['Stacks'][0]['StackStatus'] == 'CREATE_COMPLETE'
        if not vpn_stack_complete:
            time.sleep(30)

    print('\nDATA HEALTH VPN STACK CREATION COMPLETE\n')
