AWSTemplateFormatVersion: 2010-09-09

Description: Stack to provision classic load balancer and nginx

Parameters:
  EnvironmentName:
    Description: An environment name that will be prefixed to resource names
    Type: String
  ParentNetworkStack:
    Description: Name of the network stack
    Type: String
  VpnImage:
    Description: "Select Region of the hopes nginx image; Ensure the AMI has permissions for the account in which the vpn instance is being created"
    Type: String
    AllowedValues:
      - Singapore
      - Sydney
    Default: Singapore
  KeyName:
    Description: keypair name
    Type: String
    Default: moht-prod
  InstanceType:
    Description: "Instance type of the SSH bastion host/instance."
    Type: String
    Default: "t2.micro"
  CertArn:
    Description: "Certificate Arn of the public certificate"
    Type: String
  UpstreamUrl:
    Description: "Url of the upstream to which nginx will proxy"
    Type: String

Mappings:
  AMIMap:
    Singapore:
      ImageId: ami-05c859630889c79c8
    Sydney:
      ImageId: ami-07cc15c3ba6f8e287
  # AMIMap:
  #   Singapore:
  #     ImageId: ami-0cc74660ce80ff995
  #     ami-05c859630889c79c8
  #   Sydney:
  #     ImageId: ami-05a596bbb9441a806
Resources:
  CLBSG:
    Type: "AWS::EC2::SecurityGroup"
    Properties:
      GroupDescription: !Sub "${EnvironmentName}-clb-securitygroup"
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: 443
          ToPort: 443
          CidrIp: 0.0.0.0/0
      VpcId: { "Fn::ImportValue": !Sub "${ParentNetworkStack}-VPCID" }
      Tags:
        - Key: Name
          Value: !Sub "${EnvironmentName}-clb-securitygroup"

  nginxSG:
    Type: "AWS::EC2::SecurityGroup"
    Properties:
      GroupDescription: !Sub "${EnvironmentName}-nginx-securitygroup"
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: 443
          ToPort: 443
          SourceSecurityGroupId: !Ref CLBSG
      VpcId: { "Fn::ImportValue": !Sub "${ParentNetworkStack}-VPCID" }
      Tags:
        - Key: Name
          Value: !Sub "${EnvironmentName}-nginx-securitygroup"

  ClassicLoadBalancer:
    Type: AWS::ElasticLoadBalancing::LoadBalancer
    Properties:
      Subnets:
        - {
            "Fn::ImportValue": !Sub "${ParentNetworkStack}-PublicSubnet1-SubnetID",
          }
        - {
            "Fn::ImportValue": !Sub "${ParentNetworkStack}-PublicSubnet2-SubnetID",
          }
      LoadBalancerName: !Sub "${EnvironmentName}-clb"
      Scheme: internet-facing
      SecurityGroups:
        - !Ref CLBSG
      Listeners:
        - LoadBalancerPort: "443"
          InstancePort: "443"
          Protocol: TCP
          InstanceProtocol: TCP
      Tags:
        - Key: Name
          Value: !Sub "${EnvironmentName}-clb"
      # Instances: !Ref Nginx
      HealthCheck:
        Target: TCP:443
        HealthyThreshold: "3"
        UnhealthyThreshold: "5"
        Interval: "30"
        Timeout: "5"
      # DependsOn: Nginx

  # NginxAcmPolicy:
  #   Type: AWS::IAM::Policy
  #   Properties:
  #     PolicyDocument:
  #       Version: '2012-10-17'
  #       Statement:
  #       - Sid: Access Certificate Manager
  #         Effect: Allow
  #         Action:
  #         - acm:ExportCertificate
  #         - acm:DescribeCertificate
  #         - acm:GetCertificate
  #         Resource: !Sub 'arn:aws:acm:*:${PCAAccountID}:certificate/*'
  #       - Sid: Access Certificate Manager
  #         Effect: Allow
  #         Action:
  #         - acm:ListCertificates
  #         - acm:ListTagsForCertificate
  #         Resource: "*"
  #     PolicyName: !Sub '${EnvironmentName}-nginx-acm-policy'

  NginxInstanceProfileRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - ec2.amazonaws.com
            Action:
              - sts:AssumeRole
      Policies:
        - PolicyDocument:
            Version: "2012-10-17"
            Statement:
              - Effect: Allow
                Action:
                  - acm:ExportCertificate
                  - acm:DescribeCertificate
                  - acm:GetCertificate
                Resource: !Ref CertArn
              - Effect: Allow
                Action:
                  - acm:ListCertificates
                  - acm:ListTagsForCertificate
                Resource: "*"
          PolicyName: !Sub "${EnvironmentName}-nginx-acm-policy"
      RoleName: !Sub "${EnvironmentName}-nginx-instance-profile-role"

  NginxInstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Path: /
      Roles:
        - !Ref "NginxInstanceProfileRole"
      InstanceProfileName: !Sub "${EnvironmentName}-nginx-instance-profile"

  Nginx:
    Type: AWS::EC2::Instance
    Metadata:
      AWS::CloudFormation::Init:
        configSets:
          nginx:
            - InstallNginx
            - ConfigureVirtualHost
            - ExportCerts
        InstallNginx:
          packages:
            yum:
              nginx: []
              jq: []
        ConfigureVirtualHost:
          files:
            /etc/nginx/conf.d/study.conf:
              content: !Sub |
                server {
                    listen 443 ssl;

                    server_tokens off;
                    ssl_certificate /etc/nginx/ssl/study.pem;
                    ssl_certificate_key /etc/nginx/ssl/study.key;
                    ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
                    ssl_client_certificate /etc/nginx/ssl/studyca.pem;
                    ssl_verify_depth 2;
                    ssl_verify_client on;

                    client_max_body_size 2M;
                    keepalive_timeout 900;

                    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;

                    location / {
                        if ($ssl_client_verify != SUCCESS) {
                          return 444;
                        }
                        proxy_pass ${UpstreamUrl};
                        proxy_http_version 1.1;
                        proxy_ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
                        proxy_ssl_ciphers   HIGH:!aNULL:!MD5;
                        proxy_set_header Upgrade $http_upgrade;
                        proxy_set_header Connection 'upgrade';
                        proxy_set_header X-Real-IP $remote_addr;
                        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                        proxy_set_header X-Forwarded-Proto $scheme;
                        proxy_set_header Host $host;
                        proxy_cache_bypass $http_upgrade;
                    }
                }
              owner: root
              group: root
          commands:
            01-Createsslfolder:
              command: !Join
                - ""
                - - "sudo mkdir -p /etc/nginx/ssl"
        ExportCerts:
          files:
            /etc/nginx/ssl/passphrase-file:
              content: password
          commands:
            "01-getcertificate":
              command: !Join
                - ""
                - - "aws acm export-certificate --certificate-arn "
                  - !Ref CertArn
                  - " "
                  - --passphrase password --region ap-southeast-1 | jq -r '"\(.Certificate)"' > /tmp/study.pem
            "02-getcertificatechain":
              command: !Join
                - ""
                - - "aws acm export-certificate --certificate-arn "
                  - !Ref CertArn
                  - " "
                  - --passphrase password --region ap-southeast-1 | jq -r '"\(.CertificateChain)"' > /tmp/studyca.pem
            "03-getprivatekeywithpassword":
              command: !Join
                - ""
                - - "aws acm export-certificate --certificate-arn "
                  - !Ref CertArn
                  - " "
                  - --passphrase password --region ap-southeast-1 | jq -r '"\(.PrivateKey)"' > /tmp/studypwd.key
            "04-getprivatekeywithoutpassword":
              command: !Join
                - ""
                - - "sudo openssl rsa -in /tmp/studypwd.key -out /tmp/study.key -passin file:///etc/nginx/ssl/passphrase-file"
            "05-Removeprivatekeywithpwdfromtmp":
              command: !Join
                - ""
                - - "sudo rm /tmp/studypwd.key"
            "06-Movefilestonginx":
              command: !Join
                - ""
                - - "sudo mv /tmp/study* /etc/nginx/ssl"
            "07-Changeownership":
              command: !Join
                - ""
                - - "sudo chown root:root /etc/nginx/ssl/*"

          services:
            sysvinit:
              nginx:
                enabled: true
                ensureRunning: true

    Properties:
      InstanceType: !Ref InstanceType
      KeyName: !Ref KeyName
      SubnetId:
        {
          "Fn::ImportValue": !Sub "${ParentNetworkStack}-PublicSubnet1-SubnetID",
        }
      ImageId: !FindInMap [AMIMap, !Ref VpnImage, ImageId]
      SecurityGroupIds:
        - !Ref nginxSG
      IamInstanceProfile: !Ref NginxInstanceProfile
      Tags:
        - Key: Name
          Value: !Sub "${EnvironmentName}-nginx"
      UserData: !Base64
        "Fn::Join":
          - ""
          - - |
              #!/bin/bash -xe
            - |
              # Install the files and packages from the metadata
            - "/opt/aws/bin/cfn-init -v "
            - "         --stack "
            - !Ref "AWS::StackName"
            - "         --resource Nginx "
            - "         --region "
            - !Ref "AWS::Region"
            - "         --configsets nginx "
            - |+