AWSTemplateFormatVersion: 2010-09-09

Description: Provision RDS Postgres DB in VPC and configured DB Subnet groups from network stack 
Metadata:
  'AWS::CloudFormation::Interface':
    ParameterGroups:
    - Label:
        default: 'Parent Stacks'
      Parameters:
      - ParentNetworkStack
    - Label:
        default: 'RDS Parameters'
      Parameters:
      - DBInstanceClass
      - DBPort
      - DBStorage
      - DBMasterUsername
      - DBName
      - MultiAZ
      - DBEngine
      - DBPostgresEngineVersion
      - PreferredBackupWindow
      - PreferredMaintenanceWindow
      - DBBackupRetentionPeriod
      - DBSnapshotIdentifier

Parameters:
  ParentNetworkStack:
    Description: Name of the network stack
    Type: String
  DBInstanceClass:
    Description: Type of DB Instance to provision
    Type: String
    Default: db.t3.micro
  DBPort:
    Description: Database Port
    Type: String
    Default: '5432'
  DBStorage:
    Description: Database Storage in GB
    Type: String
    Default: '50'
  DBMasterUsername:
    Description: Database Master Username
    Type: String
    AllowedPattern: '[a-zA-Z][a-zA-Z0-9]*'
    ConstraintDescription: 'must begin with a letter and contain only alphanumeric characters'
    Default: OpsDashboard
  DBName:
    Description: Database Name
    Type: String 
    AllowedPattern: '[a-zA-Z0-9]*'
    ConstraintDescription: 'must contain only alphanumeric characters'
    Default: OpsDashboard
  MultiAZ:
    Description: Provision DB in multiple AZ
    Type: String 
    Default: false
    AllowedValues: [true, false]
  DBEngine:
    Description: DB Engine
    Type: String 
    Default: Postgres
  DBPostgresEngineVersion: 
    Description: 'PostgreSQL version.'
    Type: String
    Default: 11.4
  # Do an exclusive list later
  PreferredBackupWindow:
    Description: 'The daily time range in UTC during which you want to create automated backups.'
    Type: String
    Default: '20:34-21:04'
  PreferredMaintenanceWindow:
    Description: The weekly time range (in UTC) during which system maintenance can occur.
    Type: String
    Default: 'sun:22:00-sun:23:00'
  DBBackupRetentionPeriod:
    Description: 'The number of days to keep snapshots of the database.'
    Type: Number
    MinValue: 0
    MaxValue: 35
    Default: 30
  DBSnapshotIdentifier:
    Description: 'Optional name or Amazon Resource Name (ARN) of the DB snapshot from which you want to restore (leave blank to create an empty database).'
    Type: String
    Default: ''

Conditions:
  HasDBSnapshotIdentifier: !Not [ !Equals [ !Ref DBSnapshotIdentifier, '' ] ]


Resources:
  DBEncryptionKey:
    Type: AWS::KMS::Key
    Properties: 
      Description: DB Encryption Key
      Enabled: true
      EnableKeyRotation: False
      KeyPolicy:
        Version: 2012-10-17
        Id: db-encryption-key
        Statement:
          - Sid: Enable IAM User Permissions
            Effect: Allow
            Principal:
              AWS: !Join 
                - ''
                - - 'arn:aws:iam::'
                  - !Ref 'AWS::AccountId'
                  - ':root'
            Action: 'kms:*'
            Resource: '*'
      Tags:
        - Key: Name
          Value: !Join
            - ''
            - - {'Fn::ImportValue': !Sub '${ParentNetworkStack}-EnvironmentName'}
              - '-db-key'

  RDSInstanceSecret:
    Type: AWS::SecretsManager::Secret
    Properties: 
      Description: Dynamically generates rds instance password
      GenerateSecretString:
        SecretStringTemplate: !Join ['',['{"username":','"',!Ref DBMasterUsername,'"','}']]
        GenerateStringKey: password
        PasswordLength: 45
        ExcludePunctuation: true
        RequireEachIncludedType: true
      KmsKeyId: !GetAtt 'DBEncryptionKey.Arn'
      Name: !Sub '${DBName}-credentials'
      Tags: 
        - Key: Name
          Value: !Join
            - ''
            - - {'Fn::ImportValue': !Sub '${ParentNetworkStack}-EnvironmentName'}
              - '-db-master-password'           

  DatabaseSecurityGroup:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      GroupDescription: !Ref 'AWS::StackName'
      VpcId: {'Fn::ImportValue': !Sub '${ParentNetworkStack}-VPCID'}
      SecurityGroupIngress:
         - IpProtocol: tcp
           CidrIp: {'Fn::ImportValue': !Sub '${ParentNetworkStack}-PrivateSubnet1Cidr'}
           FromPort: 5432
           ToPort: 5432
         - IpProtocol: tcp
           CidrIp: {'Fn::ImportValue': !Sub '${ParentNetworkStack}-PrivateSubnet2Cidr'}
           FromPort: 5432
           ToPort: 5432       
      Tags: 
        - Key: Name
          Value: !Join
            - ''
            - - {'Fn::ImportValue': !Sub '${ParentNetworkStack}-EnvironmentName'}
              - '-db-securitygroup'

  DBSubnetGroup:
    Type: AWS::RDS::DBSubnetGroup
    Properties: 
      DBSubnetGroupName: !Sub '${ParentNetworkStack}-db-subnetgroup'
      DBSubnetGroupDescription: !Sub '${ParentNetworkStack}-db-subnetgroup'
      SubnetIds: 
        - Fn::ImportValue: !Sub '${ParentNetworkStack}-PrivateSubnet1-SubnetID'
        - Fn::ImportValue: !Sub '${ParentNetworkStack}-PrivateSubnet2-SubnetID'
      Tags: 
        - Key: Name
          Value: !Sub '${ParentNetworkStack}-db-subnetgroup'

  PostgresRdsDB:
    Type: AWS::RDS::DBInstance
    Properties: 
      DBInstanceClass: !Ref DBInstanceClass
      MultiAZ: !Ref MultiAZ
      PubliclyAccessible: False
      Port: !Ref DBPort
      VPCSecurityGroups: 
        - !Ref DatabaseSecurityGroup
      DBSubnetGroupName: !Ref DBSubnetGroup
      CopyTagsToSnapshot: true
      StorageEncrypted: true
      KmsKeyId: !GetAtt 'DBEncryptionKey.Arn'
      StorageType: gp2
      AllocatedStorage: !Ref DBStorage
      MasterUsername: !If [HasDBSnapshotIdentifier, !Ref 'AWS::NoValue', !Join ['', ['{{resolve:secretsmanager:', !Ref RDSInstanceSecret, ':SecretString:username}}' ]]]
      MasterUserPassword: !If [HasDBSnapshotIdentifier, !Ref 'AWS::NoValue', !Join ['', ['{{resolve:secretsmanager:', !Ref RDSInstanceSecret, ':SecretString:password}}' ]]]
      DBName: !If [HasDBSnapshotIdentifier, !Ref 'AWS::NoValue', !Ref DBName]
      Engine: !Ref DBEngine
      EngineVersion: !If [HasDBSnapshotIdentifier, !Ref 'AWS::NoValue', !Ref DBPostgresEngineVersion]
      PreferredBackupWindow: !Ref PreferredBackupWindow
      PreferredMaintenanceWindow: !Ref PreferredMaintenanceWindow
      AllowMajorVersionUpgrade: false
      AutoMinorVersionUpgrade: true
      BackupRetentionPeriod: !Ref DBBackupRetentionPeriod
      DeleteAutomatedBackups: false
      Tags: 
        - Key: Name
          Value: !Join
            - ''
            - - {'Fn::ImportValue': !Sub '${ParentNetworkStack}-EnvironmentName'}
              - '-db' 
  RDSInstanceSecretAttachment:
    Type: AWS::SecretsManager::SecretTargetAttachment
    Properties:
      SecretId: !Ref RDSInstanceSecret
      TargetId: !Ref PostgresRdsDB
      TargetType: AWS::RDS::DBInstance

Outputs:
  StackName:
    Description: 'Stack name.'
    Value: !Sub '${AWS::StackName}'
  DBSecurityGroup:
    Description: 'Security Group ID of the DB Cluster'
    Value: !GetAtt DatabaseSecurityGroup.GroupId
    Export: 
      Name: !Sub '${AWS::StackName}-DBSecurityGroup'
  RDSInstanceSecret:
    Description: 'Arn of DB Cluster Credentials Secret'
    Value: !Ref RDSInstanceSecret
    Export: 
      Name: !Sub '${AWS::StackName}-RDSInstanceSecret'
