#!/usr/bin/env python3

import os
import sys
import json

# Check for boto3 (aws client) and then import
try:
    __import__('boto3')
except ImportError:
    os.system('pip install boto3')

import boto3

# The following import statement will run the whole provision_network_infra and provision_db_s3_eb_infra
from provision_db_s3_eb_infra import aws_secret_access_key, aws_region_name, aws_access_key_id, \
    s3_bucket, key_name, network_stack_name, eb_app_env_name, eb_ui_env_name, DISABLE_PROMPTS, \
    s3_stack_name, eb_app_name, eb_ui_app_name, s3_prefix, vpn_stack_name

# Get aws account id
sts_client = boto3.client('sts',
                          region_name=aws_region_name,
                          aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)
aws_account_id = sts_client.get_caller_identity()['Account']

# Initialize sqs client
sqs_client = boto3.client('sqs',
                          region_name=aws_region_name,
                          aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

# Initialize ec2 client
ec2_client = boto3.client('ec2',
                          region_name=aws_region_name,
                          aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

# Initialize cloudformation client
cf_client = boto3.client('cloudformation',
                         region_name=aws_region_name,
                         aws_access_key_id=aws_access_key_id,
                         aws_secret_access_key=aws_secret_access_key)

# Initialize s3 client
s3_client = boto3.client('s3',
                         region_name=aws_region_name,
                         aws_access_key_id=aws_access_key_id,
                         aws_secret_access_key=aws_secret_access_key)

# Get directory full path
dir_path = os.path.dirname(os.path.abspath(__file__))

# Read metrics serverless yaml file
metrics_serverless_file = os.path.join(dir_path, 'metrics_serverless.yml')
with open(metrics_serverless_file, 'r') as metrics_serverless:
    metrics_serverless_yaml = metrics_serverless.read()

# Read registration  metrics serverless yaml file
reg_metrics_serverless_file = os.path.join(dir_path, 'registration_metrics_serverless.yml')
with open(reg_metrics_serverless_file, 'r') as reg_metrics_serverless:
    reg_metrics_serverless_yaml = reg_metrics_serverless.read()

# Run npm install serverless
print('\nINSTALL NPM SERVERLESS\n')
install = os.popen('npm install -g serverless')
install_output = install.read()
if 'permission denied' in install_output:
    raise Exception('Permission Denied')

# Run serverless config command
print('\nSET SERVERLESS CONFIG\n')
sl_config = os.popen('serverless config credentials --provider aws --key {0} --secret {1} -o'.format(
    aws_access_key_id, aws_secret_access_key))
sl_config_output = sl_config.read()
if 'command not found' in sl_config_output:
    raise Exception('Serverless not installed')

'''
GET PARAMETERS FOR LAMBDA METRICS
'''

print('\nGET PARAMETERS FOR METRICS LAMBDA\n')
lambda_name = os.getenv('METRICS_LAMBDA_NAME')
lambda_stage = os.getenv('METRICS_LAMBDA_STAGE')
if not DISABLE_PROMPTS:
    lambda_name = raw_input('Enter METRICS_LAMBDA_NAME (required): ')
    lambda_stage = raw_input('Enter METRICS_LAMBDA_STAGE (required): ')

'''
SQS QUEUE CREATION
'''

# Check if Q already present
print('\nCHECK Q PRESENT\n')
sqs_q_arn = "arn:aws:sqs:{0}:{1}:{2}".format(aws_region_name, aws_account_id, lambda_name)
queues = sqs_client.list_queues(QueueNamePrefix=lambda_name)

# Create SQS Q if not present
if 'QueueUrls' not in queues.keys():
    print('\nCREATE SQS Q - {}\n'.format(lambda_name))
    q_response = sqs_client.create_queue(QueueName=lambda_name)
    queue_url = q_response['QueueUrl']
else:
    queue_url = queues['QueueUrls'][0]

'''
ADD POLICY TO SQS
'''

print('\nSET SQS POLICY\n')
sqs_policy = {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": "*",
            "Action": "SQS:*",
            "Resource": sqs_q_arn
        }]}
sqs_client.set_queue_attributes(QueueUrl=queue_url, Attributes={'Policy': json.dumps(sqs_policy)})

'''
UPDATE SERVERLESS YAML AND DEPLOY - METRICS
'''

metrics_bucket = s3_prefix + '-metrics'

# Get private subnet ids from network stack outputs
subnet_id1 = ''
subnet_id2 = ''
vpc_id = ''
network_stack = cf_client.describe_stacks(StackName=network_stack_name)
for output in network_stack['Stacks'][0]['Outputs']:
    if output['ExportName'] == network_stack_name + '-PrivateSubnet1-SubnetID':
        subnet_id1 = output['OutputValue']
    if output['ExportName'] == network_stack_name + '-PrivateSubnet2-SubnetID':
        subnet_id2 = output['OutputValue']
    if output['ExportName'] == network_stack_name + '-VPCID':
        vpc_id = output['OutputValue']

# Get default vpc security group id
response = ec2_client.describe_security_groups(
    Filters=[
        {
            'Name': 'vpc-id',
            'Values': [
                vpc_id,
            ]
        },
        {
            'Name': 'description',
            'Values': [
                'default VPC security group',
            ]
        },
    ],
)
vpn_sg_id = response['SecurityGroups'][0]['GroupId']

# Update metrics yaml file with values
metrics_yaml = metrics_serverless_yaml.format(
    lambda_name,
    lambda_stage,
    aws_region_name,
    s3_bucket,
    subnet_id1,
    subnet_id2,
    metrics_bucket,
    aws_account_id,
    vpn_sg_id
)

# Update serverless yaml file inside export-fn for metrics lambda
print('\nUPDATE SERVERLESS YAML FILE - METRICS\n')
serverless_file = os.path.join(dir_path, '..', 'export-fn', 'serverless.yml')
with open(serverless_file, 'r') as serverless:
    default_serverless = serverless.read()
with open(serverless_file, 'w') as serverless:
    serverless.write(metrics_yaml)

# Run serverless deploy command
print('SERVERLESS DEPLOY - METRICS\n')
serverless_deploy = os.popen('cd .. && cd export-fn && serverless deploy -v')
serverless_deploy_output = serverless_deploy.read()

# Check for successful deployment
print(serverless_deploy_output)
if 'Serverless Error' in serverless_deploy_output:
    # Update the file back to original
    with open(serverless_file, 'w') as serverless:
        serverless.write(default_serverless)
    sys.exit('SERVERLESS DEPLOYMENT FAILED - METRICS')

print('\nSERVERLESS DEPLOYMENT COMPLETE - METRICS\n')

# Update the file back to original
with open(serverless_file, 'w') as serverless:
    serverless.write(default_serverless)

'''
ADD S3 TRIGGER FOR SQS
'''

# Get bucket configuration
bucket_configuration = s3_client.get_bucket_notification_configuration(Bucket=s3_bucket)

print('\nCHECK IF S3 BUCKET NOTIFICATION IS SET ALREADY\n')
q_config_present = 0
if 'QueueConfigurations' in bucket_configuration.keys():
    q_configs = bucket_configuration['QueueConfigurations']
    for q_config in q_configs:
        if q_config['QueueArn'] == sqs_q_arn and \
                'Put' in q_config['Events'][0] and \
                q_config['Filter']['Key']['FilterRules'][0]['Value'] == '.raw':
            q_config_present = 1
            break

# Add q config only if not present
if not q_config_present:
    queue_configuration = {
        'QueueConfigurations': [
            {
                'QueueArn': sqs_q_arn,
                'Events': [
                    's3:ObjectCreated:Put'
                ],
                'Filter': {
                    'Key': {
                        'FilterRules': [
                            {
                                'Name': 'suffix',
                                'Value': '.raw'
                            }
                        ]
                    }
                }
            }
        ]
    }

    # Update S3 bucket config
    print('\nADD S3 BUCKET NOTIFICATION FOR SQS\n')
    response = s3_client.put_bucket_notification_configuration(
        Bucket=s3_bucket,
        NotificationConfiguration=queue_configuration)
else:
    print('\nS3 BUCKET NOTIFICATION FOR SQS ALREADY SET\n')

'''
UPDATE SERVERLESS YAML AND DEPLOY - REGISTRATION METRICS
'''

# Get Parameters for registration metrics
print('\nGET PARAMETERS FOR REGISTRATION METRICS LAMBDA\n')
reg_lambda_name = os.getenv('REG_METRICS_LAMBDA_NAME')
if not DISABLE_PROMPTS:
    reg_lambda_name = raw_input('Enter REG_METRICS_LAMBDA_NAME (required): ')

# Update metrics yaml file with values
reg_metrics_yaml = reg_metrics_serverless_yaml.format(
    reg_lambda_name,
    lambda_stage,
    aws_region_name,
    s3_bucket,
    subnet_id1,
    subnet_id2,
    metrics_bucket,
    aws_account_id,
    vpn_sg_id
)

# Update serverless yaml file inside export-fn for metrics lambda
print('\nUPDATE SERVERLESS YAML FILE - REGISTRATION METRICS\n')
reg_serverless_file = os.path.join(dir_path, '..', 'registration-export-fn', 'serverless.yml')
with open(reg_serverless_file, 'r') as reg_serverless:
    default_reg_serverless = reg_serverless.read()
with open(reg_serverless_file, 'w') as reg_serverless:
    reg_serverless.write(reg_metrics_yaml)

# Run serverless deploy command
print('SERVERLESS DEPLOY - REGISTRATION METRICS\n')
reg_serverless_deploy = os.popen('cd .. && cd registration-export-fn && serverless deploy -v')
reg_serverless_deploy_output = reg_serverless_deploy.read()

# Check for successful deployment
print(reg_serverless_deploy_output)
if 'Serverless Error' in reg_serverless_deploy_output:
    # Update the file back to original
    with open(reg_serverless_file, 'w') as reg_serverless:
        reg_serverless.write(default_reg_serverless)
    sys.exit('SERVERLESS DEPLOYMENT FAILED - REGISTRATION METRICS')

print('\nSERVERLESS DEPLOYMENT COMPLETE - REGISTRATION METRICS\n')

# Update the file back to original
with open(reg_serverless_file, 'w') as reg_serverless:
    reg_serverless.write(default_reg_serverless)
