AWSTemplateFormatVersion: 2010-09-09

Description: Provision Elastic Beanstalk Environment to deploy the application
Metadata:
  'AWS::CloudFormation::Interface':
    ParameterGroups:
    - Label:
        default: 'Parent Stacks'
      Parameters:
      - ParentNetworkStack
      - ParentDBStack
      - ParentS3Stack
    - Label:
        default: 'EB Parameters'
      Parameters:
      - EBEnvironmentName
      - EBApplicationName

Parameters:
  ParentNetworkStack:
    Description: Name of the network stack
    Type: String
  ParentDBStack:
    Description: Name of the db stack
    Type: String
  ParentS3Stack:
    Description: Name of the S3 bucket stack
    Type: String
  ParentIAMStack:
    Description: Name of the S3 bucket stack
    Type: String
  EBEnvironmentName:
    Description: Name of the elastic beanstalk environment
    Type: String
    Default: hopes-app
  EBApplicationName:
    Description: Name of the elastic beanstalk application name
    Type: String
    Default: hopes-app
  # PlatformArn:
  #   Description: Application platform
  #   Type: String
  #   Default: arn:aws:elasticbeanstalk:ap-southeast-1::platform/Python 2.7 running on 64bit Amazon Linux/2.9.3
  SolutionStackName:
    Description: Application stack
    Type: String
    Default: "64bit Amazon Linux 2018.03 v2.9.4 running Python 2.7"
  EC2KeyName:
    Type: String
  InstanceType:
    Type: String
    Default: t3a.micro

Resources:
  InstanceSecurityGroup:
    Type: "AWS::EC2::SecurityGroup"
    Properties:
      GroupDescription: !Sub "${EBEnvironmentName}-InstanceSecurityGroup"
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: 80
          ToPort: 80
          SourceSecurityGroupId: !Ref LBSecurityGroup
      VpcId: { "Fn::ImportValue": !Sub "${ParentNetworkStack}-VPCID" }
      GroupName: !Sub "${EBEnvironmentName}-InstanceSecurityGroup"
      Tags:
        - Key: Name
          Value: !Sub "${EBEnvironmentName}-InstanceSecurityGroup"

  LBSecurityGroup:
    Type: "AWS::EC2::SecurityGroup"
    Properties:
      GroupDescription: !Sub "${EBEnvironmentName}-LBSecurityGroup"
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: 443
          ToPort: 443
          CidrIp: 0.0.0.0/0
      VpcId: { "Fn::ImportValue": !Sub "${ParentNetworkStack}-VPCID" }
      GroupName: !Sub "${EBEnvironmentName}-LBSecurityGroup"
      Tags:
        - Key: Name
          Value: !Sub "${EBEnvironmentName}-LBSecurityGroup"       

  EBEnvironment:
    Type: AWS::ElasticBeanstalk::Environment
    Properties: 
      ApplicationName: !Ref EBApplicationName
      Description: Elastic beanstalk application cluster
      EnvironmentName: !Ref EBEnvironmentName
      OptionSettings: 
        #Environment Settings

        - Namespace: aws:elasticbeanstalk:application:environment
          OptionName: RDS_HOSTNAME
          Value: !Join
            - ''
            - - '{{resolve:secretsmanager:'
              - { 'Fn::ImportValue': !Sub '${ParentDBStack}-RDSInstanceSecret' }
              - ':SecretString:host}}'

        - Namespace: aws:elasticbeanstalk:application:environment
          OptionName: RDS_DB_NAME  
          Value: !Join
            - ''
            - - '{{resolve:secretsmanager:'
              - { 'Fn::ImportValue': !Sub '${ParentDBStack}-RDSInstanceSecret' }
              - ':SecretString:dbname}}'

        - Namespace: aws:elasticbeanstalk:application:environment
          OptionName: RDS_PASSWORD
          Value: !Join
            - ''
            - - '{{resolve:secretsmanager:'
              - { 'Fn::ImportValue': !Sub '${ParentDBStack}-RDSInstanceSecret' }
              - ':SecretString:password}}'
          
        - Namespace: aws:elasticbeanstalk:application:environment
          OptionName: RDS_USERNAME
          Value: !Join
            - ''
            - - '{{resolve:secretsmanager:'
              - { 'Fn::ImportValue': !Sub '${ParentDBStack}-RDSInstanceSecret' }
              - ':SecretString:username}}'

        - Namespace: aws:elasticbeanstalk:application:environment
          OptionName: S3_BUCKET
          Value: { 'Fn::ImportValue': !Sub '${ParentS3Stack}-BeiweBucket' } 
          
        - Namespace: aws:elasticbeanstalk:application:environment
          OptionName: S3_ENDPOINT_URL
          Value: { 'Fn::ImportValue': !Sub '${ParentS3Stack}-BeiweBucket-Endpoint' } 

        # Instance Configuration

        - Namespace: aws:autoscaling:launchconfiguration
          OptionName: IamInstanceProfile
          Value: { 'Fn::ImportValue': !Sub '${ParentIAMStack}-InstanceProfile' } 

        - Namespace: aws:elasticbeanstalk:environment
          OptionName: ServiceRole
          Value: { 'Fn::ImportValue': !Sub '${ParentIAMStack}-ServiceRole' } 

        - Namespace: aws:autoscaling:launchconfiguration
          OptionName: EC2KeyName
          Value: !Ref EC2KeyName

        - Namespace: aws:autoscaling:launchconfiguration
          OptionName: InstanceType
          Value: !Ref InstanceType

        - Namespace: aws:autoscaling:launchconfiguration
          OptionName: SecurityGroups
          Value: !Ref InstanceSecurityGroup

        # Monitoring Settings
        - Namespace: aws:elasticbeanstalk:healthreporting:system
          OptionName: SystemType
          Value: enhanced
        

        # autoscaling settings
        
        - Namespace: aws:autoscaling:asg
          OptionName: Availability Zones
          Value: Any
        
        - Namespace: aws:autoscaling:asg
          OptionName: Cooldown
          Value: 360
        
        - Namespace: aws:autoscaling:asg
          OptionName: MaxSize
          Value: 8        
        
        - Namespace: aws:autoscaling:asg
          OptionName: MinSize
          Value: 2
        
        - Namespace: aws:autoscaling:trigger
          OptionName: LowerBreachScaleIncrement
          Value: -1
        
        - Namespace: aws:autoscaling:trigger
          OptionName: LowerThreshold
          Value: 20
        
        - Namespace: aws:autoscaling:trigger
          OptionName: MeasureName
          Value: CPUUtilization
        
        - Namespace: aws:autoscaling:trigger
          OptionName: Period
          Value: 1
        
        - Namespace: aws:autoscaling:trigger
          OptionName: Statistic
          Value: Maximum
        
        - Namespace: aws:autoscaling:trigger
          OptionName: Unit
          Value: Percent
        
        - Namespace: aws:autoscaling:trigger
          OptionName: UpperBreachScaleIncrement
          Value: 1
        
        - Namespace: aws:autoscaling:trigger
          OptionName: UpperThreshold
          Value: 85
        
        - Namespace: aws:autoscaling:updatepolicy:rollingupdate
          OptionName: MaxBatchSize
          Value: 1
        
        - Namespace: aws:autoscaling:updatepolicy:rollingupdate
          OptionName: MinInstancesInService
          Value: 1
        
        - Namespace: aws:autoscaling:updatepolicy:rollingupdate
          OptionName: RollingUpdateEnabled
          Value: true
        
        - Namespace: aws:autoscaling:updatepolicy:rollingupdate
          OptionName: RollingUpdateType
          Value: Immutable
        
        - Namespace: aws:autoscaling:updatepolicy:rollingupdate
          OptionName: Timeout
          Value: PT30M
        
        
        # VPC Configuration

        - Namespace: aws:ec2:vpc
          OptionName: VPCId
          Value: 
            Fn::ImportValue: !Sub '${ParentNetworkStack}-VPCID'

        - Namespace: aws:ec2:vpc
          OptionName: ELBSubnets
          Value: 
            Fn::ImportValue: !Sub '${ParentNetworkStack}-PrivateSubnetsList'

        - Namespace: aws:ec2:vpc
          OptionName: Subnets
          Value: 
            Fn::ImportValue: !Sub '${ParentNetworkStack}-PrivateSubnetsList'

        - Namespace: aws:ec2:vpc
          OptionName: ELBScheme
          Value: internal

        ##   Elastic Load Balancer configuration

        - Namespace: aws:elasticbeanstalk:environment
          OptionName: EnvironmentType
          Value: LoadBalanced

        - Namespace: aws:elasticbeanstalk:environment
          OptionName: LoadBalancerType
          Value: application

        - Namespace: aws:elb:loadbalancer
          OptionName: SecurityGroups
          Value: !Ref LBSecurityGroup

        - Namespace: aws:elbv2:loadbalancer
          OptionName: AccessLogsS3Bucket
          Value: { 'Fn::ImportValue': !Sub '${ParentS3Stack}-LBAccessLogsBucket' } 

        - Namespace: aws:elbv2:loadbalancer
          OptionName: AccessLogsS3Prefix
          Value: !Ref EBEnvironmentName

        - Namespace: aws:elbv2:loadbalancer
          OptionName: AccessLogsS3Enabled
          Value: true

      # PlatformArn: !Ref PlatformArn
      SolutionStackName: !Ref SolutionStackName

  EBApplication:
    Type: AWS::ElasticBeanstalk::Application
    Properties:
      ApplicationName: !Ref EBApplicationName
      Description: !Ref EBApplicationName

  DBSecGrpIngress:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      GroupId: { 'Fn::ImportValue': !Sub '${ParentDBStack}-DBClusterSecurityGroup' }
      IpProtocol: tcp
      FromPort: 5432
      ToPort: 5432
      SourceSecurityGroupId: !Ref InstanceSecurityGroup 

Outputs:
  URL:
    Description: URL of the AWS Elastic Beanstalk Environment
    Value: !Join [ '', [ 'http://', !GetAtt 'EBEnvironment.EndpointURL' ]]
