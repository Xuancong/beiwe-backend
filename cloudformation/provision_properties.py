# AWS properties
AWS_REGION_NAME = 'ap-southeast-1'

# Network stack properties
NETWORK_STACK_NAME = 'test-nw'
NETWORK_ENV_NAME = 'test-nw'
NETWORK_VPC_CIDR = '10.200.0.0/16'
NETWORK_PUBLIC_SUBNET1_CIDR = '10.200.10.0/24'
NETWORK_PUBLIC_SUBNET2_CIDR = '10.200.11.0/24'
NETWORK_PUBLIC_SUBNET3_CIDR = '10.200.12.0/24'
NETWORK_PRIVATE_SUBNET1_CIDR = '10.200.20.0/24'
NETWORK_PRIVATE_SUBNET2_CIDR = '10.200.21.0/24'

# VPN stack properties
VPN_STACK_NAME = 'test-vpn'
VPN_KEY_NAME = 'test-key'
VPN_INSTANCE_TYPE = ''
VPN_IMAGE_REGION = ''

# DB stack properties
DB_STACK_NAME = 'test-database'
DB_ENGINE = ''
DB_SNAPSHOT_ID = ''
DB_INSTANCE_CLASS = ''
DB_BACKUP_RETENTION_PERIOD = ''
DB_MASTER_USERNAME = 'testdb'
DB_NAME = 'testdb'
DB_PREFERRED_BACKUP_WINDOW = ''
DB_PREFERRED_MAINTENANCE_WINDOW = ''

# INIT SQL PROPERTIES
RUN_INIT_SQL = '1'
INSTANCE_AMI_ID = 'ami-07539a31f72d244e7'

# S3 stack properties
S3_STACK_NAME = 'test-s3'
S3_PREFIX = 'test-prefix'

# IAM stack properties
IAM_STACK_NAME = 'test-iam'

# EB APP stack properties
EB_APP_STACK_NAME = 'test-app'
EB_APP_ENV_NAME = 'test-app'
EB_APP_NAME = 'test-app'
EB_SOLN_STACK_NAME = '64bit Amazon Linux 2018.03 v2.9.4 running Python 2.7'
EB_INSTANCE_TYPE = ''

# EB UI stack properties
EB_UI_STACK_NAME = 'test-ui'
EB_UI_ENV_NAME = 'test-ui'
EB_UI_NAME = 'test-ui'
EB_UI_SOLN_STACK_NAME = '64bit Amazon Linux 2018.03 v2.9.4 running Python 2.7'
EB_UI_INSTANCE_TYPE = ''

# IAM Upload User creation
IAM_UPLOAD_POLICY_NAME = 'test-upload-policy'
IAM_UPLOAD_USERNAME = 'test-upload-user'

# IAM Secrets Manager User creation
IAM_SECRETS_POLICY_NAME = 'test-secretsmanager-policy'
IAM_SECRETS_MANAGER_USERNAME = 'test-secretsmanager-user'

# If PCA is in different account then provide pca account access details
IS_PCA_IN_DIFFERENT_ACCOUNT = '1'
PCA_ACCOUNT_ACCESS_KEY_ID = ''
PCA_ACCOUNT_SECRET_ACCESS_KEY = ''
PCA_ACCOUNT_REGION_NAME = 'ap-southeast-1'

# PRE-CREATED ACM properties - Request Private Certs
PCA_ROOT_CA = ''
PCA_DOMAIN = '.study.mohtgroup.com'
PCA_UPLOAD_CLIENT_SUBDOMAIN = 'test-client'
PCA_UI_SUBDOMAIN = 'test-ui'
PCA_UPLOAD_EXTERNAL_SUBDOMAIN = 'test'

# ASG stack properties
ASG_STACK_NAME = 'test-httpd-asg'
ASG_IMAGE_REGION = ''
ASG_INSTANCE_TYPE = ''
ASG_SYS_ADMIN_EMAIL = 'moht.tech.aws@moht.com.sg'

# EB Platform ARN
EB_PF_ARN = 'arn:aws:elasticbeanstalk:ap-southeast-1::platform/Python 2.7 running on 64bit Amazon Linux/2.9.4'

# EB ENVIRONMENT PROPERTIES
EB_SENTRY_ANDROID_DSN = 'https://foo:foo@sentry.io/foo'
EB_SENTRY_ELASTIC_BEANSTALK_DSN = 'https://foo:foo@sentry.io/foo'
EB_SENTRY_DATA_PROCESSING_DSN = 'https://foo:foo@sentry.io/foo'
EB_SENTRY_JAVASCRIPT_DSN = 'https://foo:foo@sentry.io/foo'
EB_SYSADMIN_EMAILS = 'moht.tech.aws@moht.com.sg'
EB_IS_STAGING = 'true'

# METRICS LAMBDA PROPERTIES
METRICS_LAMBDA_NAME = 'test-metrics'
METRICS_LAMBDA_STAGE = 'prod'

# REGISTRATION METRICS LAMBDA PROPERTIES
REG_METRICS_LAMBDA_NAME = 'test-registration-metrics'

# Fitbit Network stack properties
FITBIT_NETWORK_STACK_NAME = 'test-fitbit-nw'
FITBIT_NETWORK_ENV_NAME = 'test-fitbit-nw'
FITBIT_NETWORK_VPC_CIDR = '10.102.0.0/16'
FITBIT_NETWORK_PUBLIC_SUBNET1_CIDR = '10.102.10.0/24'
FITBIT_NETWORK_PRIVATE_SUBNET1_CIDR = '10.102.20.0/24'
FITBIT_NETWORK_PRIVATE_SUBNET2_CIDR = '10.102.21.0/24'

# Data Health Network stack properties
DATA_HEALTH_NETWORK_STACK_NAME = 'test-datahealth-nw'
DATA_HEALTH_NETWORK_ENV_NAME = 'test-datahealth-nw'
DATA_HEALTH_NETWORK_VPC_CIDR = '10.152.0.0/16'
DATA_HEALTH_NETWORK_PUBLIC_SUBNET1_CIDR = '10.152.10.0/24'
DATA_HEALTH_NETWORK_PRIVATE_SUBNET1_CIDR = '10.152.20.0/24'
DATA_HEALTH_NETWORK_PRIVATE_SUBNET2_CIDR = '10.152.21.0/24'

# Data Health VPN stack properties
DATA_HEALTH_VPN_STACK_NAME = 'test-datahealth-vpn'
DATA_HEALTH_VPN_KEY_NAME = 'test-key'
DATA_HEALTH_VPN_INSTANCE_TYPE = ''
DATA_HEALTH_VPN_IMAGE_REGION = ''

# Data Health DB stack properties
DATA_HEALTH_DB_STACK_NAME = 'test-datahealth-ops-database'
DATA_HEALTH_DB_INSTANCE_CLASS = ''
DATA_HEALTH_DB_PORT = ''
DATA_HEALTH_DB_STORAGE = ''
DATA_HEALTH_DB_MASTER_USERNAME = 'testdatahealthopsdb'
DATA_HEALTH_DB_NAME = 'testdatahealthopsdb'
DATA_HEALTH_MULTI_AZ = ''
DATA_HEALTH_DB_ENGINE = ''
DATA_HEALTH_DB_POSTGRES_ENGINE_VERSION = ''
DATA_HEALTH_DB_PREFERRED_BACKUP_WINDOW = ''
DATA_HEALTH_DB_PREFERRED_MAINTENANCE_WINDOW = ''
DATA_HEALTH_DB_BACKUP_RETENTION_PERIOD = ''
DATA_HEALTH_DB_SNAPSHOT_ID = ''

# Data Health Elastic Beanstalk stack properties
DATA_HEALTH_EB_APP_STACK_NAME = 'test-datahealth-ops-eb'
DATA_HEALTH_EB_APP_ENV_NAME = 'test-datahealth-ops-app'
DATA_HEALTH_EB_APP_NAME = 'test-datahealth-ops-app'
DATA_HEALTH_EB_SOLN_STACK_NAME = '64bit Amazon Linux 2018.03 v2.9.4 running Python 3.6'
DATA_HEALTH_EB_INSTANCE_TYPE = ''
DATA_HEALTH_EB_PF_ARN = \
    'arn:aws:elasticbeanstalk:ap-southeast-1::platform/Python 2.7 running on 64bit Amazon Linux/2.9.4'
