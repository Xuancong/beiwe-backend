AWSTemplateFormatVersion: 2010-09-09

Description:  
  'Network Sub Stack to provision a VPC, with a pair of public and private subnets spread
  across two Availability Zones. It deploys an Internet Gateway, with a default
  route on the public subnets. It deploy a NAT Gateway in public subnet,
  and default routes for them in the private subnets
  It can only be run with cfn-master.yml'

Parameters:
  ApplicationName:
    Description: Name of the project or study which will be appended to elements of this infrastructure
    Type: String
    ConstraintDescription: must be all lowercase
    AllowedPattern: '[a-z]*'
    MaxLength: 25
    MinLength: 1

  # VpcCIDR:
  #   Description: Please enter the IP range (CIDR notation) for this VPC
  #   Type: String
  #   Default: 10.192.0.0/16

  # Subnet1AZ:
  #   Description: Please enter the availability zone for first subnet
  #   Type: String
  #   Default: ap-southeast-1a

  # Subnet2AZ:
  #   Description: Please enter the availability zone for second subnet
  #   Type: String
  #   Default: ap-southeast-1b

  # PublicSubnet1CIDR:
  #   Description: Please enter the IP range (CIDR notation) for the public subnet in the first Availability Zone
  #   Type: String
  #   Default: 10.192.10.0/24

  # PublicSubnet2CIDR:
  #   Description: Please enter the IP range (CIDR notation) for the public subnet in the second Availability Zone
  #   Type: String
  #   Default: 10.192.11.0/24

  # PrivateSubnet1CIDR:
  #   Description: Please enter the IP range (CIDR notation) for the private subnet in the first Availability Zone
  #   Type: String
  #   Default: 10.192.20.0/24

  # PrivateSubnet2CIDR:
  #   Description: Please enter the IP range (CIDR notation) for the private subnet in the second Availability Zone
  #   Type: String
  #   Default: 10.192.21.0/24

  KeyName:
    Description: 'key pair of the ec2-user to establish a SSH connection to the SSH bastion host/instance.'
    Type: String
    Default: moht-prod
  InstanceType:
    Description: 'Instance type of the SSH bastion host/instance.'
    Type: String
    Default: 't3a.nano'
  SSHFrom:
    Description: 'Lockdown SSH access to the bastion host (default can be accessed from anywhere)'
    Type: String
    MinLength: 9
    MaxLength: 18
    Default: 0.0.0.0/0
    AllowedPattern: "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})"
    ConstraintDescription: must be a valid CIDR range of the form x.x.x.x/x.

Mappings:
  SubnetConfig:
    VPC:
      CIDR: 10.172.0.0/16
    Public1:
      CIDR: 10.172.10.0/24
    Public2:
      CIDR: 10.172.11.0/24
    Private1:
      CIDR: 10.172.20.0/24
    Private2:
      CIDR: 10.172.21.0/24

Resources:
  VPC:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: !FindInMap
        - SubnetConfig
        - VPC
        - CIDR
      EnableDnsSupport: true
      EnableDnsHostnames: true
      Tags:
        - Key: Name
          Value: !Sub '${ApplicationName}-vpc'
          # Value: !Join 
          #   - ''
          #   - - !Select 
          #       - 1
          #       - - !Split 
          #           - '-'
          #           - !Ref 'AWS::StackName'
          #     - '-vpc'

  InternetGateway:
    Type: AWS::EC2::InternetGateway
    Properties:
      Tags:
        - Key: Name
          Value: !Sub '${ApplicationName}-igw'
          # Value: !Join 
          #   - ''
          #   - - !Select 
          #       - 1
          #       - - !Split 
          #           - '-'
          #           - !Ref 'AWS::StackName'
          #     - '-igw' 

  InternetGatewayAttachment:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
      InternetGatewayId: !Ref InternetGateway
      VpcId: !Ref VPC

  PublicSubnet1:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select [ 0, !GetAZs '' ]
      CidrBlock: !FindInMap
        - SubnetConfig
        - Public1
        - CIDR
      MapPublicIpOnLaunch: true
      Tags:
        - Key: Name
          Value: !Sub '${ApplicationName}-PublicSubnet-AZ1'
          # Value: !Join 
          #   - ''
          #   - - !Select 
          #       - 1
          #       - - !Split 
          #           - '-'
          #           - !Ref 'AWS::StackName'
          #     - '-PublicSubnet-AZ1'

  PublicSubnet2:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select [ 1, !GetAZs  '' ]
      CidrBlock: !FindInMap
        - SubnetConfig
        - Public2
        - CIDR
      MapPublicIpOnLaunch: true
      Tags:
        - Key: Name
          Value: !Sub '${ApplicationName}-PublicSubnet-AZ2'
          # Value: !Join 
          #   - ''
          #   - - !Select 
          #       - 1
          #       - - !Split 
          #           - '-'
          #           - !Ref 'AWS::StackName'
          #     - '-PublicSubnet-AZ2'

  PrivateSubnet1:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select [ 0, !GetAZs  '' ]
      CidrBlock: !FindInMap
        - SubnetConfig
        - Private1
        - CIDR
      MapPublicIpOnLaunch: false
      Tags:
        - Key: Name
          Value: !Sub '${ApplicationName}-PrivateSubnet-AZ1'
          # Value: !Join 
          #   - ''
          #   - - !Select 
          #       - 1
          #       - - !Split 
          #           - '-'
          #           - !Ref 'AWS::StackName'
          #     - '-PrivateSubnet-AZ1'

  PrivateSubnet2:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select [ 1, !GetAZs  '' ]
      CidrBlock: !FindInMap
        - SubnetConfig
        - Private2
        - CIDR
      MapPublicIpOnLaunch: false
      Tags:
        - Key: Name
          Value: !Sub '${ApplicationName}-PrivateSubnet-AZ2'
          # Value: !Join 
          #   - ''
          #   - - !Select 
          #       - 1
          #       - - !Split 
          #           - '-'
          #           - !Ref 'AWS::StackName'
          #     - '-PrivateSubnet-AZ2'

  NatGatewayEIP:
    Type: AWS::EC2::EIP
    DependsOn: InternetGatewayAttachment
    Properties:
      Domain: vpc

  NatGateway:
    Type: AWS::EC2::NatGateway
    Properties:
      AllocationId: !GetAtt NatGatewayEIP.AllocationId
      SubnetId: !Ref PublicSubnet1

  PublicRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: Name
          Value: !Sub '${ApplicationName}-PublicRT'
          # Value: !Join 
          #   - ''
          #   - - !Select 
          #       - 1
          #       - - !Split 
          #           - '-'
          #           - !Ref 'AWS::StackName'
          #     - '-PublicRT'

  DefaultPublicRoute:
    Type: AWS::EC2::Route
    DependsOn: InternetGatewayAttachment
    Properties:
      RouteTableId: !Ref PublicRouteTable
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref InternetGateway

  PublicSubnet1RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref PublicSubnet1

  PublicSubnet2RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref PublicSubnet2


  PrivateRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: Name
          Value: !Sub '${ApplicationName}-PrivateRT'
          # Value: !Join 
          #   - ''
          #   - - !Select 
          #       - 1
          #       - - !Split 
          #           - '-'
          #           - !Ref 'AWS::StackName'
          #     - '-PrivateRT'

  DefaultPrivateRoute:
    Type: AWS::EC2::Route
    Properties:
      RouteTableId: !Ref PrivateRouteTable
      DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: !Ref NatGateway

  PrivateSubnet1RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PrivateRouteTable
      SubnetId: !Ref PrivateSubnet1

  PrivateSubnet2RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PrivateRouteTable
      SubnetId: !Ref PrivateSubnet2

  NoIngressSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupName: "no-ingress-sg"
      GroupDescription: "Security group with no ingress rule"
      VpcId: !Ref VPC

  # DBSubnetGroup:
  #   Type: AWS::RDS::DBSubnetGroup
  #   Properties: 
  #     DBSubnetGroupName: !Sub '${AWS::StackName}-db-subnetgroup'
  #     DBSubnetGroupDescription: !Sub '${AWS::StackName}-db-subnetgroup'
  #     SubnetIds: 
  #       - !Ref PrivateSubnet1
  #       - !Ref PrivateSubnet2
  #     Tags: 
  #       - Key: Name
  #         Value: !Sub '${AWS::StackName}-db-subnetgroup'
          # Value: !Join 
          #   - ''
          #   - - !Select 
          #       - 1
          #       - - !Split 
          #           - '-'
          #           - !Ref 'AWS::StackName'
          #     - '-db-subnetgroup'
  # BastionIPAddress:
  #   Type: 'AWS::EC2::EIP'
  #   DependsOn: InternetGatewayAttachment
  #   Properties:
  #     Domain: vpc
  #     InstanceId: !Ref 'SSHBastion'
  SSHBastionSG:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      GroupDescription: 'Security Group for SSH Bastion Host'
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: 22
          ToPort: 22
          CidrIp: !Ref SSHFrom
      SecurityGroupEgress:
        - IpProtocol: tcp
          FromPort: '22'
          ToPort: '22'
          CidrIp: !FindInMap
            - SubnetConfig
            - Private1
            - CIDR
        - IpProtocol: tcp
          FromPort: '22'
          ToPort: '22'
          CidrIp: !FindInMap
            - SubnetConfig
            - Private2
            - CIDR
      VpcId: !Ref VPC
      Tags: 
        - Key: Name
          Value: !Sub '${ApplicationName}-bastion-securitygroup'
          # Value: !Join 
          #   - ''
          #   - - !Select 
          #       - 1
          #       - - !Split 
          #           - '-'
          #           - !Ref 'AWS::StackName'
          #     - '-bastion-securitygroup'   
  SSHBastion:
    Type: AWS::EC2::Instance
    Properties: 
      InstanceType: !Ref InstanceType
      KeyName: !Ref KeyName
      SubnetId: !Ref PublicSubnet1
      ImageId: ami-0aef1be4fdffaf8bc
      SecurityGroupIds:
        - !Ref SSHBastionSG
      Tags: 
        - Key: Name
          Value: !Sub '${ApplicationName}-bastion' 
          # Value: !Join 
          #   - ''
          #   - - !Select 
          #       - 1
          #       - - !Split 
          #           - '-'
          #           - !Ref 'AWS::StackName'
          #     - '-bastion'   

Outputs:
  VPC:
    Description: A reference to the created VPC
    Value: !Ref VPC

  PublicSubnets:
    Description: A list of the public subnets
    Value: !Join [ ",", [ !Ref PublicSubnet1, !Ref PublicSubnet2 ]]

  PrivateSubnets:
    Description: A list of the private subnets
    Value: !Join [ ",", [ !Ref PrivateSubnet1, !Ref PrivateSubnet2 ]]

  PublicSubnet1:
    Description: A reference to the public subnet in the 1st Availability Zone
    Value: !Ref PublicSubnet1

  PublicSubnet2:
    Description: A reference to the public subnet in the 2nd Availability Zone
    Value: !Ref PublicSubnet2

  PrivateSubnet1:
    Description: A reference to the private subnet in the 1st Availability Zone
    Value: !Ref PrivateSubnet1

  PrivateSubnet2:
    Description: A reference to the private subnet in the 2nd Availability Zone
    Value: !Ref PrivateSubnet2

  NoIngressSecurityGroup:
    Description: Security group with no ingress rule
    Value: !Ref NoIngressSecurityGroup

  # DBSubnetGroup:
  #   Description: DB Subnet group for RDS
  #   Value: !Ref DBSubnetGroup
   