AWSTemplateFormatVersion: 2010-09-09

Description: Provision Elastic Beanstalk Environment to deploy the application
Metadata:
  'AWS::CloudFormation::Interface':
    ParameterGroups:
    - Label:
        default: 'Parent Stacks'
      Parameters:
      - ParentNetworkStack
      - ParentDBStack
    - Label:
        default: 'EB Parameters'
      Parameters:
      - EBEnvironmetName
      - EBApplicationName

Parameters:
  ParentNetworkStack:
    Description: Name of the network stack
    Type: String
  EBEnvironmentName:
    Description: Name of the elastic beanstalk environment
    Type: String
  EBApplicationName:
    Description: Name of the elastic beanstalk application name
    Type: String
  PlatformArn:
    Description: Application platform
    Type: String

  FlaskSecretKey:
    Type: String
    Default: ''
  S3Bucket:
    Type: String
    Default: ''
  S3awsSecretAccessKey:
    Type: String
    Default: ''
  S3awsAccessKeyID:
    Type: String
    Default: ''
  S3EndpointUrl:
    Type: String
    Default: ''
  EC2KeyName:
    Type: String
  EBInstanceType:
    Type: String
  DatabaseName:
    Type: String
  DatabaseUser:
    Type: String
  DatabasePassword:
    Type: String
  DatabaseHostname:
    Type: String
  PublicSubnetsList:
    Type: String  
  PrivateSubnetsList:
    Type: String 

Resources:
  EBEnvironment:
    Type: AWS::ElasticBeanstalk::Environment
    Properties: 
      ApplicationName: !Ref EBApplicationName
      Description: !Sub 'Elastic beanstalk environment for ${EBEnvironmentName}'
      EnvironmentName: !Ref EBEnvironmentName
      OptionSettings: 
        #Environment Settings

        - Namespace: aws:elasticbeanstalk:application:environment
          OptionName: RDS_HOSTNAME
          Value: {{resolve:secretsmanager:HopesDB-credentials:SecretString:host}}             

        - Namespace: aws:elasticbeanstalk:application:environment
          OptionName: RDS_DB_NAME  
          Value: {{resolve:secretsmanager:HopesDB-credentials:SecretString:dbname}}
             
        - Namespace: aws:elasticbeanstalk:application:environment
          OptionName: RDS_PASSWORD
          Value: {{resolve:secretsmanager:HopesDB-credentials:SecretString:password}}             
          
        - Namespace: aws:elasticbeanstalk:application:environment
          OptionName: RDS_USERNAME
          Value: {{resolve:secretsmanager:HopesDB-credentials:SecretString:username}}
  
        - Namespace: aws:elasticbeanstalk:application:environment
          OptionName: FLASK_SECRET_KEY
          Value: !Ref FlaskSecretKey

        - Namespace: aws:elasticbeanstalk:application:environment
          OptionName: S3_BUCKET
          Value: !Ref S3Bucket
          
        - Namespace: aws:elasticbeanstalk:application:environment
          OptionName: S3_ACCESS_CREDENTIALS_USER
          Value: !Ref S3awsAccessKeyID

        - Namespace: aws:elasticbeanstalk:application:environment
          OptionName: S3_ACCESS_CREDENTIALS_KEY
          Value: !Ref S3awsSecretAccessKey

        - Namespace: aws:elasticbeanstalk:application:environment
          OptionName: S3_ENDPOINT_URL
          Value: !Ref S3EndpointUrl

        # Instance Configuration

        - Namespace: aws:autoscaling:launchconfiguration
          OptionName: IamInstanceProfile
          Value: !Ref InstanceProfile

        - Namespace: aws:elasticbeanstalk:environment
          OptionName: ServiceRole
          Value: !Ref ServiceRole

        - Namespace: aws:autoscaling:launchconfiguration
          OptionName: EC2KeyName
          Value: !Ref EC2KeyName

        - Namespace: aws:autoscaling:launchconfiguration
          OptionName: InstanceType
          Value: !Ref EBInstanceType

        # - Namespace: aws:autoscaling:launchconfiguration
        #   OptionName: SecurityGroups
        #   Value: !Ref InstanceSecurityGroup

        # Monitoring Settings
        - Namespace: aws:elasticbeanstalk:healthreporting:system
          OptionName: SystemType
          Value: enhanced
        

        # autoscaling settings
        
        - Namespace: aws:autoscaling:asg
          OptionName: Availability Zones
          Value: Any
        
        - Namespace: aws:autoscaling:asg
          OptionName: Cooldown
          Value: 360
        
        - Namespace: aws:autoscaling:asg
          OptionName: MaxSize
          Value: 2        
        
        - Namespace: aws:autoscaling:asg
          OptionName: MinSize
          Value: 1
        
        - Namespace: aws:autoscaling:trigger
          OptionName: LowerBreachScaleIncrement
          Value: -1
        
        - Namespace: aws:autoscaling:trigger
          OptionName: LowerThreshold
          Value: 20
        
        - Namespace: aws:autoscaling:trigger
          OptionName: MeasureName
          Value: CPUUtilization
        
        - Namespace: aws:autoscaling:trigger
          OptionName: Period
          Value: 1
        
        - Namespace: aws:autoscaling:trigger
          OptionName: Statistic
          Value: Maximum
        
        - Namespace: aws:autoscaling:trigger
          OptionName: Unit
          Value: Percent
        
        - Namespace: aws:autoscaling:trigger
          OptionName: UpperBreachScaleIncrement
          Value: 1
        
        - Namespace: aws:autoscaling:trigger
          OptionName: UpperThreshold
          Value: 85
        
        - Namespace: aws:autoscaling:updatepolicy:rollingupdate
          OptionName: MaxBatchSize
          Value: 1
        
        - Namespace: aws:autoscaling:updatepolicy:rollingupdate
          OptionName: MinInstancesInService
          Value: 1
        
        - Namespace: aws:autoscaling:updatepolicy:rollingupdate
          OptionName: RollingUpdateEnabled
          Value: true
        
        - Namespace: aws:autoscaling:updatepolicy:rollingupdate
          OptionName: RollingUpdateType
          Value: Immutable
        
        - Namespace: aws:autoscaling:updatepolicy:rollingupdate
          OptionName: Timeout
          Value: PT30M
        
        
        # VPC Configuration

        - Namespace: aws:ec2:vpc
          OptionName: VPCId
          Value: !Ref ParentNetworkStack

        - Namespace: aws:ec2:vpc
          OptionName: ELBSubnets
          Value: !Ref PublicSubnetsList

        - Namespace: aws:ec2:vpc
          OptionName: Subnets
          Value: !Ref PrivateSubnetsList

        ##   Elastic Load Balancer configuration

        - Namespace: aws:elasticbeanstalk:environment
          OptionName: EnvironmentType
          Value: LoadBalanced

        - Namespace: aws:elasticbeanstalk:environment
          OptionName: LoadBalancerType
          Value: application

        # - Namespace: aws:elb:loadbalancer
          # OptionName: SecurityGroups
          # Value: !Ref LBSecurityGroup


        # Notification Endpoint
   
      PlatformArn: !Ref PlatformArn
      # Tags: 
      #   - Tag
      # TemplateName: String
      # Tier: 
      #   Tier
      # VersionLabel: String
  EBApplication:
    Type: AWS::ElasticBeanstalk::Application
    Properties:
      ApplicationName: !Ref EBApplicationName
      Description: !Ref EBApplicationName

  ServiceRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement: 
          - Effect: Allow
            Principal: 
              Service: 
                - elasticbeanstalk.amazonaws.com
            Action: 
              - sts:AssumeRole
            Condition:
              StringEquals: 
                sts:ExternalId: elasticbeanstalk
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkService
        - arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkEnhancedHealth
      RoleName: !Sub '${EBEnvironmentName}-elasticbeanstalk-service-role'

  InstanceProfileRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement: 
          - Effect: Allow
            Principal: 
              Service: 
                - ec2.amazonaws.com
            Action: 
              -  sts:AssumeRole
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier
        - arn:aws:iam::aws:policy/AWSElasticBeanstalkMulticontainerDocker
        - arn:aws:iam::aws:policy/AWSElasticBeanstalkWorkerTier
      RoleName: !Sub '${EBEnvironmentName}-elasticbeanstalk-instance-profile-role'

  InstanceProfile:
    Type: "AWS::IAM::InstanceProfile"
    Properties:
      Path: /
      Roles:
        - !Ref 'InstanceProfileRole'
      InstanceProfileName: !Sub '${EBEnvironmentName}-elasticbeanstalk-instance-profile'
 
Outputs:
  URL:
    Description: URL of the AWS Elastic Beanstalk Environment
    Value: !Join [ '', [ 'http://', !GetAtt 'EBEnvironment.EndpointURL' ]]
  # InstanceSecurityGroup:
  #   Description: EB instance security group
  #   Value: !GetAtt InstanceSecurityGroup.GroupId
  #   Export:
  #     Name: !Sub '${AWS::StackName}-eb-instance-securitygroup'