AWSTemplateFormatVersion: 2010-09-09

Description: Provision Elastic Beanstalk Environment to deploy the application
Metadata:
  'AWS::CloudFormation::Interface':
    ParameterGroups:
    - Label:
        default: 'Parent Stacks'
      Parameters:
      - ParentNetworkStack
      - ParentDBStack
    - Label:
        default: 'EB Parameters'
      Parameters:
      - EBEnvironmetName
      - EBApplicationName

Parameters:
  ParentNetworkStack:
    Description: Name of the network stack
    Type: String
  ParentDBStack:
    Description: Name of the db stack
    Type: String
  EBEnvironmentName:
    Description: Name of the elastic beanstalk environment
    Type: String
    Default: OpsDashboard
  EBApplicationName:
    Description: Name of the elastic beanstalk application name
    Type: String
    Default: OpsDashboard
  PlatformArn:
    Description: Application platform
    Type: String
    Default: arn:aws:elasticbeanstalk:ap-southeast-1::platform/Python 2.7 running on 64bit Amazon Linux/2.9.3
  SolutionStackName:
    Description: Application stack
    Type: String
    Default: "64bit Amazon Linux 2018.03 v2.9.3 running Python 3.6"
  EC2KeyName:
    Type: String
  InstanceType:
    Type: String
    Default: t3a.micro

Resources:
  InstanceSecurityGroup:
    Type: "AWS::EC2::SecurityGroup"
    Properties:
      GroupDescription: !Sub "${EBEnvironmentName}-securitygroup"
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: 80
          ToPort: 80
          SourceSecurityGroupId: !Ref LBSecurityGroup
      VpcId: { "Fn::ImportValue": !Sub "${ParentNetworkStack}-VPCID" }
      Tags:
        - Key: Name
          Value: !Sub "${EBEnvironmentName}-securitygroup"

  LBSecurityGroup:
    Type: "AWS::EC2::SecurityGroup"
    Properties:
      GroupDescription: !Sub "${EBEnvironmentName}-lb-securitygroup"
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: 443
          ToPort: 443
          CidrIp: 0.0.0.0/0
      VpcId: { "Fn::ImportValue": !Sub "${ParentNetworkStack}-VPCID" }
      Tags:
        - Key: Name
          Value: !Sub "${EBEnvironmentName}-lb-securitygroup"       

  EBEnvironment:
    Type: AWS::ElasticBeanstalk::Environment
    Properties: 
      ApplicationName: !Ref EBApplicationName
      Description: Elastic beanstalk application cluster
      EnvironmentName: !Ref EBEnvironmentName
      OptionSettings: 
        #Environment Settings

        - Namespace: aws:elasticbeanstalk:application:environment
          OptionName: RDS_HOSTNAME
          Value: !Join
            - ''
            - - '{{resolve:secretsmanager:'
              - { 'Fn::ImportValue': !Sub '${ParentDBStack}-RDSInstanceSecret' }
              - ':SecretString:host}}'

        - Namespace: aws:elasticbeanstalk:application:environment
          OptionName: RDS_DB_NAME  
          Value: !Join
            - ''
            - - '{{resolve:secretsmanager:'
              - { 'Fn::ImportValue': !Sub '${ParentDBStack}-RDSInstanceSecret' }
              - ':SecretString:dbname}}'

        - Namespace: aws:elasticbeanstalk:application:environment
          OptionName: RDS_PASSWORD
          Value: !Join
            - ''
            - - '{{resolve:secretsmanager:'
              - { 'Fn::ImportValue': !Sub '${ParentDBStack}-RDSInstanceSecret' }
              - ':SecretString:password}}'
          
        - Namespace: aws:elasticbeanstalk:application:environment
          OptionName: RDS_USERNAME
          Value: !Join
            - ''
            - - '{{resolve:secretsmanager:'
              - { 'Fn::ImportValue': !Sub '${ParentDBStack}-RDSInstanceSecret' }
              - ':SecretString:username}}'

        # Instance Configuration

        - Namespace: aws:autoscaling:launchconfiguration
          OptionName: IamInstanceProfile
          Value: !Ref InstanceProfile

        - Namespace: aws:elasticbeanstalk:environment
          OptionName: ServiceRole
          Value: !Ref ServiceRole

        - Namespace: aws:autoscaling:launchconfiguration
          OptionName: EC2KeyName
          Value: !Ref EC2KeyName

        - Namespace: aws:autoscaling:launchconfiguration
          OptionName: InstanceType
          Value: !Ref InstanceType

        # - Namespace: aws:autoscaling:launchconfiguration
        #   OptionName: SecurityGroups
        #   Value: !Ref InstanceSecurityGroup

        # Monitoring Settings
        - Namespace: aws:elasticbeanstalk:healthreporting:system
          OptionName: SystemType
          Value: enhanced
        

        # autoscaling settings
        
        - Namespace: aws:autoscaling:asg
          OptionName: Availability Zones
          Value: Any
        
        - Namespace: aws:autoscaling:asg
          OptionName: Cooldown
          Value: 360
        
        - Namespace: aws:autoscaling:asg
          OptionName: MaxSize
          Value: 4        
        
        - Namespace: aws:autoscaling:asg
          OptionName: MinSize
          Value: 2
        
        - Namespace: aws:autoscaling:trigger
          OptionName: LowerBreachScaleIncrement
          Value: -1
        
        - Namespace: aws:autoscaling:trigger
          OptionName: LowerThreshold
          Value: 20
        
        - Namespace: aws:autoscaling:trigger
          OptionName: MeasureName
          Value: CPUUtilization
        
        - Namespace: aws:autoscaling:trigger
          OptionName: Period
          Value: 1
        
        - Namespace: aws:autoscaling:trigger
          OptionName: Statistic
          Value: Maximum
        
        - Namespace: aws:autoscaling:trigger
          OptionName: Unit
          Value: Percent
        
        - Namespace: aws:autoscaling:trigger
          OptionName: UpperBreachScaleIncrement
          Value: 1
        
        - Namespace: aws:autoscaling:trigger
          OptionName: UpperThreshold
          Value: 85
        
        - Namespace: aws:autoscaling:updatepolicy:rollingupdate
          OptionName: MaxBatchSize
          Value: 1
        
        - Namespace: aws:autoscaling:updatepolicy:rollingupdate
          OptionName: MinInstancesInService
          Value: 1
        
        - Namespace: aws:autoscaling:updatepolicy:rollingupdate
          OptionName: RollingUpdateEnabled
          Value: true
        
        - Namespace: aws:autoscaling:updatepolicy:rollingupdate
          OptionName: RollingUpdateType
          Value: Immutable
        
        - Namespace: aws:autoscaling:updatepolicy:rollingupdate
          OptionName: Timeout
          Value: PT30M

        - Namespace: aws:autoscaling:launchconfiguration
          OptionName: SecurityGroups
          Value: !Ref InstanceSecurityGroup
        
        # VPC Configuration

        - Namespace: aws:ec2:vpc
          OptionName: VPCId
          Value: 
            Fn::ImportValue: !Sub '${ParentNetworkStack}-VPCID'

        - Namespace: aws:ec2:vpc
          OptionName: ELBSubnets
          Value: 
            Fn::ImportValue: !Sub '${ParentNetworkStack}-PrivateSubnetsList'

        - Namespace: aws:ec2:vpc
          OptionName: Subnets
          Value: 
            Fn::ImportValue: !Sub '${ParentNetworkStack}-PrivateSubnetsList'

        - Namespace: aws:ec2:vpc
          OptionName: ELBScheme
          Value: internal

        ##   Elastic Load Balancer configuration

        - Namespace: aws:elasticbeanstalk:environment
          OptionName: EnvironmentType
          Value: LoadBalanced

        - Namespace: aws:elasticbeanstalk:environment
          OptionName: LoadBalancerType
          Value: application

        - Namespace: aws:elb:loadbalancer
          OptionName: SecurityGroups
          Value: !Ref LBSecurityGroup


        # Notification Endpoint
   
      # PlatformArn: !Ref PlatformArn
      SolutionStackName: !Ref SolutionStackName
      # Tags: 
      #   - Tag
      # TemplateName: String
      # Tier: 
      #   Tier
      # VersionLabel: String
  EBApplication:
    Type: AWS::ElasticBeanstalk::Application
    Properties:
      ApplicationName: !Ref EBApplicationName
      Description: !Ref EBApplicationName

  ServiceRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement: 
          - Effect: Allow
            Principal: 
              Service: 
                - elasticbeanstalk.amazonaws.com
            Action: 
              - sts:AssumeRole
            Condition:
              StringEquals: 
                sts:ExternalId: elasticbeanstalk
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkService
        - arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkEnhancedHealth
      RoleName: !Sub '${EBEnvironmentName}-elasticbeanstalk-service-role'

  InstanceProfileRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement: 
          - Effect: Allow
            Principal: 
              Service: 
                - ec2.amazonaws.com
            Action: 
              -  sts:AssumeRole
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier
        - arn:aws:iam::aws:policy/AWSElasticBeanstalkMulticontainerDocker
        - arn:aws:iam::aws:policy/AWSElasticBeanstalkWorkerTier
      RoleName: !Sub '${EBEnvironmentName}-elasticbeanstalk-instance-profile-role'

  InstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Path: /
      Roles:
        - !Ref 'InstanceProfileRole'
      InstanceProfileName: !Sub '${EBEnvironmentName}-elasticbeanstalk-instance-profile'

  DBSecGrpIngress:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      GroupId: { 'Fn::ImportValue': !Sub '${ParentDBStack}-DBSecurityGroup' }
      IpProtocol: tcp
      FromPort: 5432
      ToPort: 5432
      SourceSecurityGroupId: !Ref InstanceSecurityGroup
 
Outputs:
  URL:
    Description: URL of the AWS Elastic Beanstalk Environment
    Value: !Join [ '', [ 'http://', !GetAtt 'EBEnvironment.EndpointURL' ]]
