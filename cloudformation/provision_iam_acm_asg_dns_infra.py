#!/usr/bin/env python3

import os
import sys
import time
import json
import random
import string

from Cryptodome.PublicKey import RSA

# Check for boto3 (aws client) and then import
try:
    __import__('boto3')
except ImportError:
    os.system('pip install boto3')

import boto3

# The following import statement will run the whole provision_network_infra and provision_db_s3_eb_infra
from provision_lambda_deploy_infra import aws_secret_access_key, aws_region_name, aws_access_key_id, \
    s3_bucket, key_name, network_stack_name, eb_app_env_name, eb_ui_env_name, DISABLE_PROMPTS, \
    s3_stack_name, eb_app_name, eb_ui_app_name

# Get aws account id
sts_client = boto3.client('sts',
                          region_name=aws_region_name,
                          aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)
aws_account_id = sts_client.get_caller_identity()['Account']

# Initialize iam client
iam_client = boto3.client('iam',
                          region_name=aws_region_name,
                          aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

# Initialize acm client
acm_client = boto3.client('acm',
                          region_name=aws_region_name,
                          aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

# Initialize cloudformation client
cf_client = boto3.client('cloudformation',
                         region_name=aws_region_name,
                         aws_access_key_id=aws_access_key_id,
                         aws_secret_access_key=aws_secret_access_key)

# Initialize elastic beanstalk client
eb_client = boto3.client('elasticbeanstalk',
                         region_name=aws_region_name,
                         aws_access_key_id=aws_access_key_id,
                         aws_secret_access_key=aws_secret_access_key)

# Initialize elbv2 client
elbv2_client = boto3.client('elbv2',
                            region_name=aws_region_name,
                            aws_access_key_id=aws_access_key_id,
                            aws_secret_access_key=aws_secret_access_key)

# Initialize route53 client
route53_client = boto3.client('route53',
                              region_name=aws_region_name,
                              aws_access_key_id=aws_access_key_id,
                              aws_secret_access_key=aws_secret_access_key)

# Get directory full path
dir_path = os.path.dirname(os.path.abspath(__file__))

'''
IAM UPLOAD USER CREATION
'''

# Initialize upload policy arn
upload_policy_arn = ''

# Get iam upload policy name
print('START IAM UPLOAD POLICY CREATION\n')
iam_upload_policy = os.getenv('IAM_UPLOAD_POLICY_NAME')
if not DISABLE_PROMPTS:
    iam_upload_policy = raw_input('Enter IAM_UPLOAD_POLICY_NAME (default: hopes-upload-policy): ')
    if iam_upload_policy is None or iam_upload_policy.strip() == '':
        iam_upload_policy = 'hopes-upload-policy'

# Check if iam upload policy name already exists
iam_upload_policy_exists = True
try:
    upload_policy_arn = 'arn:aws:iam::{0}:policy/{1}'.format(aws_account_id, iam_upload_policy)
    iam_client.get_policy(PolicyArn=upload_policy_arn)
    print('\nIAM UPLOAD POLICY ALREADY EXISTS, SKIPPING IAM UPLOAD POLICY CREATION\n')
except:
    iam_upload_policy_exists = False

# Create upload policy only it does not exist
if not iam_upload_policy_exists:

    # Upload Policy document
    upload_policy_document = {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Action": "s3:PutObject",
                    "Resource": "arn:aws:s3:::{}/*".format(s3_bucket)
                },
                {
                    "Effect": "Allow",
                    "Action": "cloudwatch:PutMetricData",
                    "Resource": "*"
                }
            ]
        }

    # Create upload policy
    iam_client.create_policy(
        PolicyName=iam_upload_policy,
        PolicyDocument=json.dumps(upload_policy_document)
    )
    print('\nIAM UPLOAD POLICY CREATION COMPLETE\n')

# Get iam upload user name
print('START IAM UPLOAD USER CREATION\n')
iam_upload_username = os.getenv('IAM_UPLOAD_USERNAME')
if not DISABLE_PROMPTS:
    iam_upload_username = raw_input('Enter IAM_UPLOAD_USERNAME (default: hopes-upload-user): ')
    if iam_upload_username is None or iam_upload_username.strip() == '':
        iam_upload_username = 'hopes-upload-user'

# Check if iam upload user name already exists
iam_upload_user_exists = True
try:
    iam_client.get_user(UserName=iam_upload_username)
    print('\nIAM UPLOAD USER ALREADY EXISTS, SKIPPING IAM UPLOAD USER CREATION\n')

    # Delete all the previous access keys if the user exists
    try:
        for access_key in iam_client.list_access_keys(UserName=iam_upload_username)['AccessKeyMetadata']:
            print('Deleting access key {0}.'.format(access_key['AccessKeyId']))
            response = iam_client.delete_access_key(
                UserName=iam_upload_username,
                AccessKeyId=access_key['AccessKeyId']
            )
    except:
        print('\nError in deleting old access keys\n')
except:
    iam_upload_user_exists = False

# Create iam upload user only if user does not exist
if not iam_upload_user_exists:

    # Create iam upload user
    iam_client.create_user(UserName=iam_upload_username)
    print('\nIAM UPLOAD USER CREATION COMPLETE\n')

# Attach the policy to user only if either policy or user has been created new
if not iam_upload_user_exists and not iam_upload_policy_exists:
    print('ATTACH IAM UPLOAD POLICY TO IAM UPLOAD USER\n')

    # Attach upload iam policy to iam user
    iam_client.attach_user_policy(
        UserName=iam_upload_username,
        PolicyArn=upload_policy_arn
    )
    print('IAM UPLOAD POLICY ATTACHED TO IAM UPLOAD USER\n')

# Create access key and get secret access and set it to environment variable
print('\nIAM UPLOAD USER ACCESS KEY CREATION\n')
try:
    response = iam_client.create_access_key(UserName=iam_upload_username)
    os.environ['S3_ACCESS_CREDENTIALS_USER'] = response['AccessKey']['AccessKeyId']
    os.environ['S3_ACCESS_CREDENTIALS_KEY'] = response['AccessKey']['SecretAccessKey']
except:
    print('\nException occurred while creating access key\n')
    sys.exit('Exception occurred while creating access key')

'''
IAM SECRETS MANAGER USER CREATION
'''

# Initialize secrets policy arn
secrets_policy_arn = ''

# Get iam secrets policy name
print('START IAM SECRETS MANAGER POLICY CREATION\n')
iam_secrets_policy = os.getenv('IAM_SECRETS_POLICY_NAME')
if not DISABLE_PROMPTS:
    iam_secrets_policy = raw_input('Enter IAM_SECRETS_POLICY_NAME (default: hopes-secretsmanager-policy): ')
    if iam_secrets_policy is None or iam_secrets_policy.strip() == '':
        iam_secrets_policy = 'hopes-secretsmanager-policy'

# Check if iam secrets policy name already exists
iam_secrets_policy_exists = True
try:
    secrets_policy_arn = 'arn:aws:iam::{0}:policy/{1}'.format(aws_account_id, iam_secrets_policy)
    iam_client.get_policy(PolicyArn=secrets_policy_arn)
    print('\nIAM SECRETS MANAGER POLICY ALREADY EXISTS, SKIPPING IAM SECRETS MANAGER POLICY CREATION\n')
except:
    iam_secrets_policy_exists = False

# Create secrets policy only it does not exist
if not iam_secrets_policy_exists:
    aws_secrets_resource_statement = "arn:aws:secretsmanager:{0}:{1}:secret:"\
        .format(aws_region_name, aws_account_id)

    # Secrets Policy document
    secrets_policy_document = {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Action": "secretsmanager:GetSecretValue",
                    "Resource": [
                        aws_secrets_resource_statement + "public/*",
                        aws_secrets_resource_statement + "private/*"
                    ]
                },
                {
                    "Effect": "Allow",
                    "Action": "secretsmanager:CreateSecret",
                    "Resource": "*"
                }
            ]
        }

    # Create secrets policy
    iam_client.create_policy(
        PolicyName=iam_secrets_policy,
        PolicyDocument=json.dumps(secrets_policy_document)
    )
    print('\nIAM SECRETS MANAGER POLICY CREATION COMPLETE\n')

# Get iam secrets user name
print('START IAM SECRETS MANAGER USER CREATION\n')
iam_secrets_username = os.getenv('IAM_SECRETS_MANAGER_USERNAME')
if not DISABLE_PROMPTS:
    iam_secrets_username = raw_input('Enter IAM_SECRETS_MANAGER_USERNAME (default: hopes-secretsmanager-user): ')
    if iam_secrets_username is None or iam_secrets_username.strip() == '':
        iam_secrets_username = 'hopes-secretsmanager-user'

# Check if iam secrets manager user name already exists
iam_secrets_user_exists = True
try:
    iam_client.get_user(UserName=iam_secrets_username)
    print('\nIAM SECRETS MANAGER USER ALREADY EXISTS, SKIPPING IAM SECRETS MANAGER USER CREATION\n')

    # Delete all the previous access keys if the user exists
    try:
        for access_key in iam_client.list_access_keys(UserName=iam_upload_username)['AccessKeyMetadata']:
            print('Deleting access key {0}.'.format(access_key['AccessKeyId']))
            response = iam_client.delete_access_key(
                UserName=iam_upload_username,
                AccessKeyId=access_key['AccessKeyId']
            )
    except:
        print('\nError in deleting old access keys\n')
except:
    iam_secrets_user_exists = False

# Create iam secrets manager user only if user does not exist
if not iam_secrets_user_exists:

    # Create iam secrets manager user
    iam_client.create_user(UserName=iam_secrets_username)
    print('\nIAM SECRETS MANAGER USER CREATION COMPLETE\n')

# Attach the policy to user only if either policy or user has been created new
if not iam_secrets_user_exists and not iam_secrets_policy_exists:
    print('ATTACH IAM SECRETS MANAGER POLICY TO IAM SECRETS MANAGER USER\n')

    # Attach secrets manager iam policy to secrets manager iam user
    iam_client.attach_user_policy(
        UserName=iam_secrets_username,
        PolicyArn=secrets_policy_arn
    )
    print('IAM SECRETS MANAGER POLICY ATTACHED TO IAM SECRETS MANAGER USER\n')

# Create access key and get secret access and set it to environment variable
print('\nIAM SECRETS MANAGER USER ACCESS KEY CREATION\n')
try:
    response = iam_client.create_access_key(UserName=iam_upload_username)
    os.environ['SECRETS_MANAGER_ACCESS_CREDENTIALS_USER'] = response['AccessKey']['AccessKeyId']
    os.environ['SECRETS_MANAGER_ACCESS_CREDENTIALS_KEY'] = response['AccessKey']['SecretAccessKey']
except:
    print('\nException occurred while creating access key\n')
    sys.exit('Exception occurred while creating access key')

'''
REQUESTING PRIVATE CERTIFICATES
'''

# Get the private ca arn and domain and sub domains
print('GET THE PRIVATE ROOT CA, PCA DOMAIN AND SUB DOMAINS\n')
is_pca_diff_account = os.getenv('IS_PCA_IN_DIFFERENT_ACCOUNT')
pca_account_access_key_id = os.getenv('PCA_ACCOUNT_ACCESS_KEY_ID')
pca_account_secret_access_key = os.getenv('PCA_ACCOUNT_SECRET_ACCESS_KEY')
pca_account_region_name = os.getenv('PCA_ACCOUNT_REGION_NAME')
private_ca_arn = os.getenv('PCA_ROOT_CA')
pca_domain = os.getenv('PCA_DOMAIN')
pca_ui_subdomain = os.getenv('PCA_UI_SUBDOMAIN')
pca_upload_client_subdomain = os.getenv('PCA_UPLOAD_CLIENT_SUBDOMAIN')
pca_upload_external_subdomain = os.getenv('PCA_UPLOAD_EXTERNAL_SUBDOMAIN')
if not DISABLE_PROMPTS:
    is_pca_diff_account = raw_input('Enter IS_PCA_IN_DIFFERENT_ACCOUNT (required, values: 0 or 1): ')
    pca_account_access_key_id = raw_input('Enter PCA_ACCOUNT_ACCESS_KEY_ID '
                                          '(required if IS_PCA_IN_DIFFERENT_ACCOUNT = 1): ')
    pca_account_secret_access_key = raw_input('Enter PCA_ACCOUNT_SECRET_ACCESS_KEY '
                                              '(required if IS_PCA_IN_DIFFERENT_ACCOUNT = 1): ')
    pca_account_region_name = raw_input('Enter PCA_ACCOUNT_REGION_NAME '
                                        '(required if IS_PCA_IN_DIFFERENT_ACCOUNT = 1): ')
    private_ca_arn = raw_input('Enter PCA_ROOT_CA (required if IS_PCA_IN_DIFFERENT_ACCOUNT = 0): ')
    pca_domain = raw_input('Enter PCA_DOMAIN (required): ')
    pca_ui_subdomain = raw_input('Enter PCA_UI_SUBDOMAIN (required): ')
    pca_upload_client_subdomain = raw_input('Enter PCA_UPLOAD_CLIENT_SUBDOMAIN (required): ')
    pca_upload_external_subdomain = raw_input('Enter PCA_UPLOAD_EXTERNAL_SUBDOMAIN (required): ')

# If PCA is in different account initialize acm pca client to get the certificate
private_ca_response = ''
if is_pca_diff_account == '1':
    # Initialize acm client for the pca account
    acm_client = boto3.client('acm',
                              region_name=pca_account_region_name,
                              aws_access_key_id=pca_account_access_key_id,
                              aws_secret_access_key=pca_account_secret_access_key)

# Get admin app domain name from the user
print('\nSTART REQUEST CERTIFICATE FOR EXTERNAL UPLOAD APP\n')
external_admin_app_domain_name = pca_upload_external_subdomain + pca_domain

# Delete old certificates if present for the domain
print('\nDELETE OLD CERTIFICATES IF GENERATED ALREADY FOR THE EXTERNAL UPLOAD APP\n')
old_certificates = acm_client.list_certificates()
for old_cert in old_certificates['CertificateSummaryList']:
    if old_cert['DomainName'] == external_admin_app_domain_name:
        acm_client.delete_certificate(CertificateArn=old_cert['CertificateArn'])

print('\nREQUEST CERTIFICATES FOR EXTERNAL UPLOAD APP\n')
admin_app_cert_arn = acm_client.request_certificate(
    DomainName=external_admin_app_domain_name,
    CertificateAuthorityArn=private_ca_arn
)

# Get admin client app domain name from the user
print('START REQUEST CERTIFICATE FOR UPLOAD CLIENT APP\n')
admin_app_client_domain_name = pca_upload_client_subdomain + pca_domain

# Delete old certificates if present for the domain
print('\nDELETE OLD CERTIFICATES IF GENERATED ALREADY FOR THE UPLOAD CLIENT APP\n')
old_certificates = acm_client.list_certificates()
for old_cert in old_certificates['CertificateSummaryList']:
    if old_cert['DomainName'] == admin_app_client_domain_name:
        acm_client.delete_certificate(CertificateArn=old_cert['CertificateArn'])

print('\nREQUEST CERTIFICATES FOR UPLOAD CLIENT APP\n')
admin_app_client_cert_arn = acm_client.request_certificate(
    DomainName=admin_app_client_domain_name,
    CertificateAuthorityArn=private_ca_arn
)

# Get ui app domain name from the user
print('START REQUEST CERTIFICATE FOR UI APP\n')
ui_app_domain_name = pca_ui_subdomain + pca_domain

# Delete old certificates if present for the domain
print('\nDELETE OLD CERTIFICATES IF GENERATED ALREADY FOR THE UI APP\n')
old_certificates = acm_client.list_certificates()
for old_cert in old_certificates['CertificateSummaryList']:
    if old_cert['DomainName'] == ui_app_domain_name:
        acm_client.delete_certificate(CertificateArn=old_cert['CertificateArn'])

print('\nREQUEST CERTIFICATES FOR UI APP\n')
ui_cert_arn = acm_client.request_certificate(
    DomainName=ui_app_domain_name,
    CertificateAuthorityArn=private_ca_arn
)

# Generate passphrase for the certs if needed to be exported
ui_cert_passphrase = ''.join(random.SystemRandom().choice(
    string.ascii_uppercase + string.ascii_lowercase + string.digits
) for _ in range(16))
admin_app_cert_passphrase = ''.join(random.SystemRandom().choice(
    string.ascii_uppercase + string.ascii_lowercase + string.digits
) for _ in range(16))
admin_app_client_cert_passphrase = ''.join(random.SystemRandom().choice(
    string.ascii_uppercase + string.ascii_lowercase + string.digits
) for _ in range(16))

# If PCA is different account export certs and import certs to target account
if is_pca_diff_account == '1':
    # Wait for max 2 minutes for the ready state on requested certificates
    print('REQUESTED CERTIFICATES - STATUS CHECK')
    ready = 0
    status = ''
    for x in range(1, 12):
        ui_cert_response = acm_client.describe_certificate(
            CertificateArn=ui_cert_arn['CertificateArn'])
        ui_cert_creation_status = ui_cert_response['Certificate']['Status']
        admin_app_cert_response = acm_client.describe_certificate(
            CertificateArn=admin_app_cert_arn['CertificateArn'])
        admin_app_cert_creation_status = admin_app_cert_response['Certificate']['Status']
        admin_app_client_cert_response = acm_client.describe_certificate(
            CertificateArn=admin_app_client_cert_arn['CertificateArn'])
        admin_app_client_cert_creation_status = admin_app_client_cert_response['Certificate']['Status']

        greenAndReady = 0
        if ui_cert_creation_status.lower().find('issued') > -1 \
                and admin_app_cert_creation_status.lower().find('issued') > -1 \
                and admin_app_client_cert_creation_status.lower().find('issued') > -1:
            greenAndReady = 1

        if greenAndReady:
            ready = 1
            break
        else:
            print('Sleep and try again')
            time.sleep(10)

    if not ready:
        sys.exit('REQUESTED CERTIFICATES - Status not issued')

    # Export the Ui cert
    print('\nEXPORT REQUESTED CERTIFICATE - UI APP CERT\n')
    ui_cert_export = acm_client.export_certificate(
        CertificateArn=ui_cert_arn['CertificateArn'],
        Passphrase=ui_cert_passphrase
    )
    ui_app_pvt_key = RSA.import_key(ui_cert_export['PrivateKey'], ui_cert_passphrase)
    ui_pvt_key = ui_app_pvt_key.exportKey(format='PEM', pkcs=1)

    # Export the admin app cert
    print('\nEXPORT REQUESTED CERTIFICATE - EXTERNAL ADMIN APP CERT\n')
    admin_app_cert_export = acm_client.export_certificate(
        CertificateArn=admin_app_cert_arn['CertificateArn'],
        Passphrase=admin_app_cert_passphrase
    )
    admin_app_pvt_key = RSA.import_key(admin_app_cert_export['PrivateKey'], admin_app_cert_passphrase)
    admin_pvt_key = admin_app_pvt_key.exportKey(format='PEM', pkcs=1)

    # Export the admin app client cert
    print('\nEXPORT REQUESTED CERTIFICATE - ADMIN CLIENT APP CERT\n')
    admin_app_client_export = acm_client.export_certificate(
        CertificateArn=admin_app_client_cert_arn['CertificateArn'],
        Passphrase=admin_app_client_cert_passphrase
    )
    admin_client_app_pvt_key = RSA.import_key(admin_app_client_export['PrivateKey'], admin_app_client_cert_passphrase)
    admin_client_pvt_key = admin_client_app_pvt_key.exportKey(format='PEM', pkcs=1)

    # Initialize acm client to the target account
    acm_client = boto3.client('acm',
                              region_name=aws_region_name,
                              aws_access_key_id=aws_access_key_id,
                              aws_secret_access_key=aws_secret_access_key)

    # Delete old certificates if present for the domain
    print('\nDELETE OLD CERTIFICATES IF GENERATED ALREADY FOR THE UI APP\n')
    old_certificates = acm_client.list_certificates()
    for old_cert in old_certificates['CertificateSummaryList']:
        if old_cert['DomainName'] == ui_app_domain_name:
            acm_client.delete_certificate(CertificateArn=old_cert['CertificateArn'])

    # Import the certificate to the target account
    print('\nIMPORT EXPORTED CERTIFICATE - UI CERT\n')
    ui_cert_arn = acm_client.import_certificate(
        Certificate=ui_cert_export['Certificate'],
        PrivateKey=ui_pvt_key,
        CertificateChain=ui_cert_export['CertificateChain']
    )

    # Delete old certificates if present for the domain
    print('\nDELETE OLD CERTIFICATES IF GENERATED ALREADY FOR THE EXTERNAL ADMIN APP\n')
    old_certificates = acm_client.list_certificates()
    for old_cert in old_certificates['CertificateSummaryList']:
        if old_cert['DomainName'] == external_admin_app_domain_name:
            acm_client.delete_certificate(CertificateArn=old_cert['CertificateArn'])

    # Import the certificate to the target account
    print('\nIMPORT EXPORTED CERTIFICATE - EXTERNAL ADMIN APP CERT\n')
    admin_app_cert_arn = acm_client.import_certificate(
        Certificate=admin_app_cert_export['Certificate'],
        PrivateKey=admin_pvt_key,
        CertificateChain=admin_app_cert_export['CertificateChain']
    )

    # Delete old certificates if present for the domain
    print('\nDELETE OLD CERTIFICATES IF GENERATED ALREADY FOR THE ADMIN CLIENT APP\n')
    old_certificates = acm_client.list_certificates()
    for old_cert in old_certificates['CertificateSummaryList']:
        if old_cert['DomainName'] == admin_app_client_domain_name:
            acm_client.delete_certificate(CertificateArn=old_cert['CertificateArn'])

    # Import the certificate to the target account
    print('\nIMPORT EXPORTED CERTIFICATE - ADMIN CLIENT CERT\n')
    admin_app_client_cert_arn = acm_client.import_certificate(
        Certificate=admin_app_client_export['Certificate'],
        PrivateKey=admin_client_pvt_key,
        CertificateChain=admin_app_client_export['CertificateChain']
    )

    # Wait for max 2 minutes for the ready state on requested certificates
    print('IMPORTED CERTIFICATES - STATUS CHECK')
    ready = 0
    status = ''
    for x in range(1, 12):
        ui_cert_response = acm_client.describe_certificate(
            CertificateArn=ui_cert_arn['CertificateArn'])
        ui_cert_creation_status = ui_cert_response['Certificate']['Status']
        admin_app_cert_response = acm_client.describe_certificate(
            CertificateArn=admin_app_cert_arn['CertificateArn'])
        admin_app_cert_creation_status = admin_app_cert_response['Certificate']['Status']
        admin_app_client_cert_response = acm_client.describe_certificate(
            CertificateArn=admin_app_client_cert_arn['CertificateArn'])
        admin_app_client_cert_creation_status = admin_app_client_cert_response['Certificate']['Status']

        greenAndReady = 0
        if ui_cert_creation_status.lower().find('issued') > -1 \
                and admin_app_cert_creation_status.lower().find('issued') > -1 \
                and admin_app_client_cert_creation_status.lower().find('issued') > -1:
            greenAndReady = 1

        if greenAndReady:
            ready = 1
            break
        else:
            print('Sleep and try again')
            time.sleep(10)

    if not ready:
        sys.exit('IMPORTED CERTIFICATES - Status not issued')

'''
HTTPD ASG STACK
'''

# Get ASG stack name
print('\nSTART ASG STACK CREATION\n')
asg_stack_name = os.getenv('ASG_STACK_NAME')
if not DISABLE_PROMPTS:
    asg_stack_name = raw_input('Enter ASG_STACK_NAME (default: hopes-httpd-asg-log-stream): ')
    if asg_stack_name is None or asg_stack_name.strip() == '':
        asg_stack_name = 'hopes-httpd-asg-log-stream'

# Check if asg stack already exists with the same name
asg_stack_exists = True
try:
    data = cf_client.describe_stacks(StackName=asg_stack_name)
    stack_status = data['Stacks'][0]['StackStatus']
    if not stack_status.endswith('_COMPLETE'):
        sys.exit('Error! ASG stack already exists with status - {}. '
                 'Try with different ASG stack name'.format(stack_status))
    print('\nASG STACK ALREADY EXISTS WITH STATUS - {}, '
          'SKIPPING ASG STACK CREATION\n'.format(stack_status))
except:
    asg_stack_exists = False

# Create ASG stack only if it does not exist
if not asg_stack_exists:

    # Get asg stack template full path
    asg_stack_file = os.path.join(dir_path, 'httpd-asg-logstream.yml')

    # Read asg stack template file and convert to string
    asg_stack = open(asg_stack_file, 'r')
    asg_template = asg_stack.read()
    asg_stack.close()

    # Get asg parameters from user
    print('\nGET PARAMETERS FOR ASG STACK\n')
    asg_img_reg = os.getenv('ASG_IMAGE_REGION')
    inst_type = os.getenv('ASG_INSTANCE_TYPE')
    sys_admin_email = os.getenv('ASG_SYS_ADMIN_EMAIL')
    if not DISABLE_PROMPTS:
        asg_img_reg = raw_input('ImageRegion (Allowed: Singapore/Sydney, Default: Singapore) = ')
        inst_type = raw_input('InstanceType (default: t3a.micro) = ')
        sys_admin_email = raw_input('SysadminEmail (required) = ')

    # Create asg parameters list
    asg_parameters = [
        {
            'ParameterKey': 'ParentNetworkStack',
            'ParameterValue': network_stack_name
        },
        {
            'ParameterKey': 'ParentS3Stack',
            'ParameterValue': s3_stack_name
        },
        {
            'ParameterKey': 'PublicDnsEndpoint',
            'ParameterValue': external_admin_app_domain_name
        },
        {
            'ParameterKey': 'CertArn',
            'ParameterValue': admin_app_cert_arn['CertificateArn']
        },
        {
            'ParameterKey': 'UpstreamUrl',
            'ParameterValue': admin_app_client_domain_name
        },
        {
            'ParameterKey': 'KeyName',
            'ParameterValue': key_name
        }
    ]
    if asg_img_reg is not None and asg_img_reg.strip() != '':
        asg_parameters.append({
            'ParameterKey': 'ImageRegion',
            'ParameterValue': asg_img_reg
        })
    if inst_type is not None and inst_type.strip() != '':
        asg_parameters.append({
            'ParameterKey': 'InstanceType',
            'ParameterValue': inst_type
        })
    if sys_admin_email is not None and sys_admin_email.strip() != '':
        asg_parameters.append({
            'ParameterKey': 'SysadminEmail',
            'ParameterValue': sys_admin_email
        })

    # Create ASG stack
    print('\nCREATE ASG STACK\n')
    cf_client.create_stack(
        StackName=asg_stack_name,
        TemplateBody=asg_template,
        Parameters=asg_parameters,
        Capabilities=['CAPABILITY_NAMED_IAM'],
        OnFailure='DO_NOTHING'
    )

    # Wait for httpd stack creation to complete
    print('WAIT UNTIL ASG STACK - CREATE COMPLETE\n')
    asg_stack_complete = False
    while not asg_stack_complete:
        print('Checking status of ASG stack')
        data = cf_client.describe_stacks(StackName=asg_stack_name)
        asg_stack_status = data['Stacks'][0]['StackStatus']
        print('Stack status: ' + asg_stack_status)
        if asg_stack_status in ['ROLLBACK_IN_PROGRESS', 'ROLLBACK_COMPLETE', 'CREATE_FAILED']:
            sys.exit('Error in creating asg stack. Delete the asg stack manually and try again')
        asg_stack_complete = data['Stacks'][0]['StackStatus'] == 'CREATE_COMPLETE'
        if not asg_stack_complete:
            time.sleep(30)

    print('\nASG STACK CREATION COMPLETE\n')

'''
ROUTE 53 - DNS SETUP FOR ADMIN APP AND UPLOAD API
'''

# Initialize private hosted zone id
pvt_hosted_zone_id = ''

# Get private hosted zone id from network stack
print('ROUTE 53 DNS SETUP - GET PRIVATE HOSTED ZONE ID\n')
data = cf_client.describe_stacks(StackName=network_stack_name)
nw_outputs = data['Stacks'][0]['Outputs']
for output in nw_outputs:
    if output['ExportName'] == network_stack_name + '-PrivateHostedZone':
        pvt_hosted_zone_id = output['OutputValue']

# Get app client load balancer arn from eb
app_eb_response = eb_client.describe_environment_resources(EnvironmentName=eb_app_env_name)
app_load_balancer = app_eb_response['EnvironmentResources']['LoadBalancers'][0]['Name']

# Get app client hosted zone id and dns name from load balancer
app_lb_response = elbv2_client.describe_load_balancers(LoadBalancerArns=[app_load_balancer])
app_hosted_zone_id = app_lb_response['LoadBalancers'][0]['CanonicalHostedZoneId']
app_dns_name = app_lb_response['LoadBalancers'][0]['DNSName']

print('\nCREATE RECORD SET FOR ADMIN CLIENT APP\n')
# Create an alias record set in route53
try:
    route53_client.change_resource_record_sets(
        HostedZoneId=pvt_hosted_zone_id,
        ChangeBatch={
            'Changes': [
                {
                    'Action': 'CREATE',
                    'ResourceRecordSet': {
                        'Name': admin_app_client_domain_name,
                        'Type': 'A',
                        'AliasTarget': {
                            'HostedZoneId': app_hosted_zone_id,
                            'DNSName': app_dns_name,
                            'EvaluateTargetHealth': False
                        }
                    }
                },
            ]
        })
except Exception as e:
    if 'already exists' not in e.message:
        raise

# Get ui load balancer arn from eb
ui_eb_response = eb_client.describe_environment_resources(EnvironmentName=eb_ui_env_name)
ui_load_balancer = ui_eb_response['EnvironmentResources']['LoadBalancers'][0]['Name']

# Get ui hosted zone id and dns name from load balancer
ui_lb_response = elbv2_client.describe_load_balancers(LoadBalancerArns=[ui_load_balancer])
ui_hosted_zone_id = ui_lb_response['LoadBalancers'][0]['CanonicalHostedZoneId']
ui_dns_name = ui_lb_response['LoadBalancers'][0]['DNSName']

print('\nCREATE RECORD SET FOR UI APP\n')
# Create an alias record set in route53
try:
    route53_client.change_resource_record_sets(
        HostedZoneId=pvt_hosted_zone_id,
        ChangeBatch={
            'Changes': [
                {
                    'Action': 'CREATE',
                    'ResourceRecordSet': {
                        'Name': ui_app_domain_name,
                        'Type': 'A',
                        'AliasTarget': {
                            'HostedZoneId': ui_hosted_zone_id,
                            'DNSName': ui_dns_name,
                            'EvaluateTargetHealth': False
                        }
                    }
                },
            ]
        })
except Exception as e:
    if 'already exists' not in e.message:
        raise
