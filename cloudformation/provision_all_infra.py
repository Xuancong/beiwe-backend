"""
A. PRE-REQUISITES - SOFTWARE:
-----------------------------
1. Python 2.7.x and pip installed
2. Node 10.x and npm installed

B. PRE-REQUISITES - AWS:
------------------------
1. Increase your EIP limits through AWS support
2. Create IAM user that has admin rights to provision the stack
3. Note down the AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY for the created IAM user
4. Generate a Key Pair from AWS Console -> Services -> EC2 -> Key Pairs -> Create Key Pair
5. Note down the generated Key Pair name (EC2 key name)
6. Ensure the VPN AMI has permissions for the account in which the vpn instance is being created
7. Make sure you are connected to the VPN before running the script
8. Create a Private CA and note down the ARN for private CA
9. If PCA is in different account, create a IAM user that has admin access in the PCA account
10. Note down the PCA account's AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY for the created IAM user
11. If needed, increase the limit for import certificates on ACM through AWS support

C. PRE-REQUISITES - PYTHON SCRIPT:
----------------------------------
1. Set environment variable DISABLE_PROMPTS = 1 if we wanted to run the script without prompts
2. If DISABLE_PROMPTS = 1, update provision_properties.py with appropriate values
3. Set environment variables AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY (from B.3) for the account we are provisioning

D. RUN COMMAND:
---------------
1. Run 'python provision_all_infra.py' from the cloudformation directory

E. HOPES INFRA - SCRIPT DESCRIPTION:
------------------------------------
1. Sequence of execution = network -> db_s3_eb -> lambda_deploy -> iam_acm_asg_dns -> eb_deploy
2. provision_network_infra.py -> provision network and vpn stacks through cloudformation
                              -> From document: Step 7, 8, 9
3. provision_db_s3_eb_infra.py -> provision database, s3, iam and eb stacks through cloudformation
                               -> From document: Step 11, 12, 14, 16, 17, 18
4. provision_lambda_deploy_infra.py -> provision lambda through serverless and setup sqs q
                                    -> From document: Step 30
5. provision_iam_acm_asg_dns_infra.py -> Create Iam policies and users, request certificates from PCA
                                      -> provision auto scaling group, create record sets on route 53
                                      -> From document: Step 19, 21, 22, 23
6. provision_eb_deploy_infra.py -> Use eb cli to update environment variables, ssl settings and deploy code to beanstalk
                                -> From document: Step 24, 25, 26, 29

F. FITBIT AND DATA HEALTH INFRA - SCRIPT DESCRIPTION:
-----------------------------------------------------
1. Sequence of execution = fitbit_datahealth_network -> datahealth_db_eb
2. provision_fitbit_datahealth_network_infra.py
            -> provision fitbit, datahealth network and vpn stacks through cloudformation
            -> provision route 53 vpc association for both network stacks
            -> From document: Fitbit Step 1, 2 & Data Health Step 1, 2, 3, 4
3. provision_datahealth_db_eb_infra.py
            -> provision database and eb stacks through cloudformation
            -> From document: Data Health Step 5, 6
"""

# This line provisions the required infra for hopes app
import provision_eb_deploy_infra

# This line provisions the required fitbit and data health dashboard infra
import provision_datahealth_db_eb_infra
