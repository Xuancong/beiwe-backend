#!/usr/bin/env python3

import os
import sys
import time
import json

# Check for boto3 (aws client) and then import
try:
    __import__('boto3')
except ImportError:
    os.system('pip install boto3')

import boto3

# The following import statement will run the whole provision_network_infra
from provision_network_infra import network_stack_name, network_stack_exists, \
    dir_path, stack_list, key_name, aws_access_key_id, \
    aws_secret_access_key, aws_region_name, DISABLE_PROMPTS, vpn_stack_name

# Initialize cloudformation client
cf_client = boto3.client('cloudformation',
                         region_name=aws_region_name,
                         aws_access_key_id=aws_access_key_id,
                         aws_secret_access_key=aws_secret_access_key)

# Initialize route53 client
r53_client = boto3.client('route53',
                          region_name=aws_region_name,
                          aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

# Initialize elastic beanstalk client
beanstalk_client = boto3.client('elasticbeanstalk',
                                region_name=aws_region_name,
                                aws_access_key_id=aws_access_key_id,
                                aws_secret_access_key=aws_secret_access_key)

# Initialize elastic load balancer client
elb_client = boto3.client('elb',
                          region_name=aws_region_name,
                          aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

# Initialize ec2 client
ec2_client = boto3.client('ec2',
                          region_name=aws_region_name,
                          aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

# Initialize secret manager client
sm_client = boto3.client('secretsmanager',
                         region_name=aws_region_name,
                         aws_access_key_id=aws_access_key_id,
                         aws_secret_access_key=aws_secret_access_key)

'''
DATABASE STACK
'''

# Initialize database stack name and flag
db_stack_name = ''
db_stack_exists = False

# Check if database stack is already exists with parent stack as network stack
if network_stack_exists:
    stack_exists = False
    is_db_stack = False
    for i in range(len(stack_list)):
        data = cf_client.describe_stacks(StackName=stack_list[i]['StackName'])
        try:
            parameters = data['Stacks'][0]['Parameters']
        except:
            continue
        stack_exists = False
        is_db_stack = False
        db_stack_exists = False
        for parameter in parameters:
            if parameter['ParameterKey'] == 'ParentNetworkStack' and parameter['ParameterValue'] == network_stack_name:
                stack_exists = True
            if parameter['ParameterKey'] == 'DBName':
                is_db_stack = True
            if is_db_stack and stack_exists:
                db_stack_exists = True
                break
        if db_stack_exists:
            db_stack_name = data['Stacks'][0]['StackName']
            db_stack_status = data['Stacks'][0]['StackStatus']
            if '_COMPLETE' not in db_stack_status:
                sys.exit('Error! Database stack already exists with status - {}. '
                         'Try with different vpn stack name'.format(db_stack_status))
            print('DATABASE STACK ALREADY EXISTS WITH STATUS - {}, '
                  'SKIPPING DATABASE STACK CREATION\n'.format(db_stack_status))
            break

# Create Database stack only if it does not exist
if not db_stack_exists:

    # Get database stack template full path
    db_stack_file = os.path.join(dir_path, 'rds-aurora-standalone.yml')

    # Get database stack name
    print('START DATABASE STACK CREATION\n')
    db_stack_name = os.getenv('DB_STACK_NAME')
    if not DISABLE_PROMPTS:
        db_stack_name = raw_input('Enter DATABASE_STACK_NAME (default: hopes-database): ')
        if db_stack_name is None or db_stack_name.strip() == '':
            db_stack_name = 'hopes-database'

    # Read database stack template file and convert to string
    db_stack = open(db_stack_file, 'r')
    db_template = db_stack.read()
    db_stack.close()

    # Get database parameters from user
    print('\nGET PARAMETERS FOR DATABASE STACK\n')
    db_engine = os.getenv('DB_ENGINE')
    db_snapshot_id = os.getenv('DB_SNAPSHOT_ID')
    db_instance_class = os.getenv('DB_INSTANCE_CLASS')
    db_backup_retention_period = os.getenv('DB_BACKUP_RETENTION_PERIOD')
    db_master_username = os.getenv('DB_MASTER_USERNAME')
    db_name = os.getenv('DB_NAME')
    db_pref_backup_window = os.getenv('DB_PREFERRED_BACKUP_WINDOW')
    db_pref_maintenance_window = os.getenv('DB_PREFERRED_MAINTENANCE_WINDOW')

    if not DISABLE_PROMPTS:
        db_engine = raw_input('Engine (default: aurora-postgresql-10.7) = ')
        db_snapshot_id = raw_input('DBSnapshotIdentifier (optional, default: ) = ')
        db_instance_class = raw_input('DBInstanceClass (default: db.t3.medium) = ')
        db_backup_retention_period = raw_input('DBBackupRetentionPeriod (default: 30) = ')
        db_master_username = raw_input('DBMasterUsername (default: mohtproddb) = ')
        db_name = raw_input('DBName (default: mohtproddb) = ')
        db_pref_backup_window = raw_input('PreferredBackupWindow (default: 04:34-05:04) = ')
        db_pref_maintenance_window = raw_input('PreferredMaintenanceWindow (default: sat:08:00-sat:09:00) = ')

    # Create database parameters list
    db_parameters = [
        {
            'ParameterKey': 'ParentNetworkStack',
            'ParameterValue': network_stack_name
        }
    ]
    if db_engine is not None and db_engine.strip() != '':
        db_parameters.append({
            'ParameterKey': 'Engine',
            'ParameterValue': db_engine
        })
    if db_snapshot_id is not None and db_snapshot_id.strip() != '':
        db_parameters.append({
            'ParameterKey': 'DBSnapshotIdentifier',
            'ParameterValue': db_snapshot_id
        })
    if db_instance_class is not None and db_instance_class.strip() != '':
        db_parameters.append({
            'ParameterKey': 'DBInstanceClass',
            'ParameterValue': db_instance_class
        })
    if db_backup_retention_period is not None and db_backup_retention_period.strip() != '':
        db_parameters.append({
            'ParameterKey': 'DBBackupRetentionPeriod',
            'ParameterValue': db_backup_retention_period
        })
    if db_master_username is not None and db_master_username.strip() != '':
        db_parameters.append({
            'ParameterKey': 'DBMasterUsername',
            'ParameterValue': db_master_username
        })
    if db_name is not None and db_name.strip() != '':
        db_parameters.append({
            'ParameterKey': 'DBName',
            'ParameterValue': db_name
        })
    if db_pref_backup_window is not None and db_pref_backup_window.strip() != '':
        db_parameters.append({
            'ParameterKey': 'PreferredBackupWindow',
            'ParameterValue': db_pref_backup_window
        })
    if db_pref_maintenance_window is not None and db_pref_maintenance_window.strip() != '':
        db_parameters.append({
            'ParameterKey': 'PreferredMaintenanceWindow',
            'ParameterValue': db_pref_maintenance_window
        })

    # Create database stack
    print('\nCREATE DATABASE STACK\n')
    cf_client.create_stack(
        StackName=db_stack_name,
        TemplateBody=db_template,
        Parameters=db_parameters
    )

    # Wait for database stack creation to complete
    print('WAIT UNTIL DATABASE STACK - CREATE COMPLETE\n')
    db_stack_complete = False
    while not db_stack_complete:
        print('Checking status of database stack')
        data = cf_client.describe_stacks(StackName=db_stack_name)
        db_stack_status = data['Stacks'][0]['StackStatus']
        print('Stack status: ' + db_stack_status)
        if db_stack_status in ['ROLLBACK_IN_PROGRESS', 'ROLLBACK_COMPLETE']:
            sys.exit('Error in creating database stack. Delete the database stack manually and try again')
        db_stack_complete = db_stack_status == 'CREATE_COMPLETE'
        if not db_stack_complete:
            time.sleep(30)

    print('\nDATABASE STACK CREATION COMPLETE\n')

'''
CREATE EC2 INSTANCE AND RUN INIT SQL
'''

print('\nGET PARAMETERS FOR INIT SQL EXECUTION\n')
# Get required parameters for init sql instance
run_init_sql = os.getenv('RUN_INIT_SQL')
instance_ami_id = os.getenv('INSTANCE_AMI_ID')
if not DISABLE_PROMPTS:
    run_init_sql = raw_input('Enter RUN_INIT_SQL (default: 1): ')
    instance_ami_id = raw_input('Enter INSTANCE_AMI_ID (default: ami-07539a31f72d244e7): ')

# Create key pair, instance, and run init sql only when needed
if run_init_sql == '1':
    print('\nCREATE KEY PAIR\n')
    init_sql_key_file = os.path.join(dir_path, 'init_sql.pem')
    init_sql_key_name = 'init-sql'

    # Delete key pair and create one if already present
    try:
        ec2_client.delete_key_pair(KeyName=init_sql_key_name)
    except:
        pass

    # Create key pair
    with open(init_sql_key_file, 'w') as init_sql:
        key_pair = ec2_client.create_key_pair(KeyName=init_sql_key_name)
        init_sql.write(key_pair['KeyMaterial'])

    print('\nCHECK IF IMAGE ALREADY PRESENT\n')
    image_id = ''
    response = ec2_client.describe_images(
        Filters=[
            {
                'Name': 'name',
                'Values': [init_sql_key_name]
            }
        ]
    )
    if len(response['Images']) == 1:
        image_id = response['Images'][0]['ImageId']

    print(image_id)
    if image_id == '':
        # Create a new instance
        response = ec2_client.run_instances(
            ImageId=instance_ami_id,
            MinCount=1,
            MaxCount=1,
            InstanceType='t2.micro',
            KeyName=init_sql_key_name
        )
        instance_id = response['Instances'][0]['InstanceId']

        # Wait until instance is in running state and get public dns name
        print('\nWAIT FOR INSTANCE TO BE RUNNING TO GET PUBLIC DNS NAME\n')
        public_dns_name = ''
        sec_grp_id = ''
        instance_running = False
        while not instance_running:
            response = ec2_client.describe_instances(InstanceIds=[instance_id])
            public_dns_name = response['Reservations'][0]['Instances'][0]['PublicDnsName']
            status = response['Reservations'][0]['Instances'][0]['State']['Name']
            if public_dns_name != '':
                instance_running = True
                sec_grp_id = response['Reservations'][0]['Instances'][0]['SecurityGroups'][0]['GroupId']
                break
            print('Sleep and try again - State: {}'.format(status))
            time.sleep(10)
        print('\nINSTANCE ID - {}'.format(instance_id))

        # Add inbound rule to security group to accept ssh
        print('\nADD INBOUND RULE TO ACCEPT SSH\n')
        try:
            ec2_client.authorize_security_group_ingress(
                GroupId=sec_grp_id,
                IpProtocol="tcp",
                CidrIp="0.0.0.0/0",
                FromPort=22,
                ToPort=22
            )
        except Exception as e:
            if 'InvalidPermission.Duplicate' not in e.message:
                raise

        print('\nWAIT FOR 60 SECONDS FOR THE SECURITY GROUP TO GET UPDATED\n')
        time.sleep(60)

        print('\nDISABLE STRICT HOST KEY CHECK\n')
        os.system('ssh -o StrictHostKeyChecking=no {}'.format(public_dns_name))

        # Copy init-postgres.sql file into instance
        print('\nCOPY ACTUAL SQL FILE TO INSTANCE\n')
        os.system('scp -i {0} ../scripts/init-postgres.sql ec2-user@{1}:/home/ec2-user'.format(init_sql_key_file, public_dns_name))

        # Connect to the instance using ssh client
        print('\nSSH INTO INSTANCE AND INSTALL PSQL\n')
        os.system('ssh -o "StrictHostKeyChecking=no" -i {0} ec2-user@{1} "{2}"'.format(
            init_sql_key_file,
            public_dns_name,
            'sudo amazon-linux-extras install postgresql10 vim epel -y; '
            'sudo yum install -y postgresql-server postgresql-devel; '
            'sudo /usr/bin/postgresql-setup --initdb; '
            'sudo systemctl enable postgresql; '
            'sudo systemctl start postgresql; '
            'sudo systemctl start postgresql'
        ))

        # Attach the ec2 instance to the VPC
        print('\nCREATE IMAGE FROM THE INSTANCE\n')
        image_output = ec2_client.create_image(InstanceId=instance_id, Name=init_sql_key_name)
        print(image_output)
        image_id = image_output['ImageId ']

        print('\nWAIT FOR IMAGE TO BE AVAILABLE\n')
        image_available = False
        while not image_available:
            response = ec2_client.describe_images(ImageIds=[image_id])
            status = response['Images'][0]['State']
            if status == 'available':
                image_available = True
                break
            print('Sleep and try again - State: {}'.format(status))
            time.sleep(10)

        ec2_client.terminate_instances(InstanceIds=[instance_id])

    print('\nIMAGE ID - {}'.format(image_id))

    # Get the db secrets from secrets manager
    print('\nGET DB SECRETS FROM SECRETS MANAGER\n')
    db_secret_arn = ''
    db_stack = cf_client.describe_stacks(StackName=db_stack_name)
    for output in db_stack['Stacks'][0]['Outputs']:
        if 'ExportName' in output.keys() and output['ExportName'] == db_stack_name + '-RDSInstanceSecret':
            db_secret_arn = output['OutputValue']
    db_secrets = sm_client.get_secret_value(SecretId=db_secret_arn)
    db_secrets = json.loads(db_secrets['SecretString'])

    print('\nCREATE NEW INSTANCE FROM THE IMAGE\n')
    subnet_id1 = ''
    network_stack = cf_client.describe_stacks(StackName=network_stack_name)
    for output in network_stack['Stacks'][0]['Outputs']:
        if output['ExportName'] == network_stack_name + '-PublicSubnet1-SubnetID':
            subnet_id1 = output['OutputValue']

    vpc_sg_id = ''
    vpn_stack = cf_client.describe_stacks(StackName=vpn_stack_name)
    for output in vpn_stack['Stacks'][0]['Outputs']:
        if output['ExportName'] == vpn_stack_name + '-Vpn-SG':
            vpc_sg_id = output['OutputValue']

    # Create a new instance in the vpc
    response = ec2_client.run_instances(
        ImageId=image_id,
        MinCount=1,
        MaxCount=1,
        InstanceType='t2.micro',
        KeyName=init_sql_key_name,
        NetworkInterfaces=[{
            'SubnetId': subnet_id1,
            'DeviceIndex': 0,
            'AssociatePublicIpAddress': True,
            'Groups': [vpc_sg_id]
        }],
    )
    new_instance_id = response['Instances'][0]['InstanceId']
    print('\nINSTANCE ID - {}'.format(new_instance_id))

    # Wait until new instance is in running state and get public dns name
    print('\nWAIT FOR INSTANCE TO BE RUNNING TO GET PUBLIC DNS NAME\n')
    public_dns_name = ''
    instance_running = False
    while not instance_running:
        response = ec2_client.describe_instances(InstanceIds=[new_instance_id])
        public_dns_name = response['Reservations'][0]['Instances'][0]['PublicDnsName']
        status = response['Reservations'][0]['Instances'][0]['State']['Name']
        if public_dns_name != '':
            instance_running = True
            break
        print('Sleep and try again - State: {}'.format(status))
        time.sleep(10)

    print('\nWAIT FOR 60 SECONDS\n')
    time.sleep(60)

    # TODO: Can not to the instance through ssh - Debug needed
    # Run the psql command
    print('\nSSH TO NEW INSTANCE AND RUN PSQL COMMAND\n')
    os.system('ssh -o "StrictHostKeyChecking=no" -i {0} ec2-user@{1} "{2}"'.format(
        init_sql_key_file,
        public_dns_name,
        'psql postgres://{0}:{1}@{2}/{3} < scripts/init-postgres.sql'.format(
            db_secrets['username'],
            db_secrets['password'],
            db_secrets['host'],
            db_secrets['dbname']
        )
    ))

    # Delete the instance and key pair after completion
    print('\nTERMINATE THE INSTANCE AND DELETE KEY PAIR\n')
    # ec2_client.terminate_instances(InstanceIds=[new_instance_id])
    # ec2_client.delete_key_pair(KeyName=init_sql_key_name)
    # os.system('rm {}'.format(init_sql_key_file))

'''
S3 STACK
'''

# Initialize bucket name
s3_bucket = ''

print('\nGET PARAMETERS FOR S3 STACK\n')
s3_stack_name = os.getenv('S3_STACK_NAME')
s3_prefix = os.getenv('S3_PREFIX')

if not DISABLE_PROMPTS:
    s3_stack_name = raw_input('Enter S3_STACK_NAME (default: hopes-s3): ')
    s3_prefix = raw_input('Prefix (required) = ')

# Get s3 stack name
print('START S3 STACK CREATION\n')
if s3_stack_name is None or s3_stack_name.strip() == '':
    s3_stack_name = 'hopes-s3'

# Check if s3 stack already exists with the same name
s3_stack_exists = True
try:
    data = cf_client.describe_stacks(StackName=s3_stack_name)
    stack_status = data['Stacks'][0]['StackStatus']
    if not stack_status.endswith('_COMPLETE'):
        sys.exit('Error! S3 stack already exists with status - {}. '
                 'Try with different S3 stack name'.format(stack_status))
    print('\nS3 STACK ALREADY EXISTS WITH STATUS - {}, '
          'SKIPPING S3 STACK CREATION\n'.format(stack_status))
    s3_prefix = data['Stacks'][0]['Parameters'][0]['ParameterValue']
    s3_bucket = s3_prefix + '-beiwe'
    print('S3 BUCKET NAME - {}\n'.format(s3_bucket))
except:
    s3_stack_exists = False

# Create s3 stack only if it does not exist
if not s3_stack_exists:

    # Get s3 stack template full path
    s3_stack_file = os.path.join(dir_path, 's3bucket-standalone.yml')

    # Read s3 stack template file and convert to string
    s3_stack = open(s3_stack_file, 'r')
    s3_template = s3_stack.read()
    s3_stack.close()

    # Get s3 parameters from user
    print('\nGET PARAMETERS FOR S3 STACK\n')
    s3_bucket = s3_prefix + '-beiwe'

    # Create database parameters list
    s3_parameters = [
        {
            'ParameterKey': 'Prefix',
            'ParameterValue': s3_prefix
        }
    ]

    # Create s3 stack
    print('\nCREATE S3 STACK\n')
    cf_client.create_stack(
        StackName=s3_stack_name,
        TemplateBody=s3_template,
        Parameters=s3_parameters
    )

    # Wait for s3 stack creation to complete
    print('WAIT UNTIL S3 STACK - CREATE COMPLETE\n')
    s3_stack_complete = False
    while not s3_stack_complete:
        print('Checking status of database stack')
        data = cf_client.describe_stacks(StackName=s3_stack_name)
        s3_stack_status = data['Stacks'][0]['StackStatus']
        print('Stack status: ' + s3_stack_status)
        if s3_stack_status in ['ROLLBACK_IN_PROGRESS', 'ROLLBACK_COMPLETE']:
            sys.exit('Error in creating s3 stack. Delete the s3 stack manually and try again')
        s3_stack_complete = s3_stack_status == 'CREATE_COMPLETE'
        if not s3_stack_complete:
            time.sleep(30)

    print('\nS3 STACK CREATION COMPLETE\n')

'''
IAM STACK
'''

print('\nGET PARAMETERS FOR IAM STACK\n')
iam_stack_name = os.getenv('IAM_STACK_NAME')

if not DISABLE_PROMPTS:
    iam_stack_name = raw_input('Enter IAM_STACK_NAME (default: hopes-prod-iam): ')

# Get IAM stack name
print('START IAM STACK CREATION\n')
if iam_stack_name is None or iam_stack_name.strip() == '':
    iam_stack_name = 'hopes-prod-iam'

# Check if iam stack already exists with the same name
iam_stack_exists = True
try:
    data = cf_client.describe_stacks(StackName=iam_stack_name)
    stack_status = data['Stacks'][0]['StackStatus']
    if not stack_status.endswith('_COMPLETE'):
        sys.exit('Error! IAM stack already exists with status - {}. '
                 'Try with different IAM stack name'.format(stack_status))
    print('\nIAM STACK ALREADY EXISTS WITH STATUS - {}, '
          'SKIPPING IAM STACK CREATION\n'.format(stack_status))
except:
    iam_stack_exists = False

# Create IAM stack only if it does not exist
if not iam_stack_exists:

    # Get iam stack template full path
    iam_stack_file = os.path.join(dir_path, 'iam-standalone.yml')

    # Read iam stack template file and convert to string
    iam_stack = open(iam_stack_file, 'r')
    iam_template = iam_stack.read()
    iam_stack.close()

    # Create iam parameters list
    iam_parameters = [
        {
            'ParameterKey': 'ParentNetworkStack',
            'ParameterValue': network_stack_name
        },
        {
            'ParameterKey': 'ParentS3Stack',
            'ParameterValue': s3_stack_name
        }
    ]

    # Create iam app stack
    print('\nCREATE IAM STACK\n')
    cf_client.create_stack(
        StackName=iam_stack_name,
        TemplateBody=iam_template,
        Parameters=iam_parameters,
        Capabilities=['CAPABILITY_NAMED_IAM']
    )

    # Wait for iam app stack creation to complete
    print('WAIT UNTIL IAM STACK - CREATE COMPLETE\n')
    iam_stack_complete = False
    while not iam_stack_complete:
        print('Checking status of iam stack')
        data = cf_client.describe_stacks(StackName=iam_stack_name)
        iam_stack_status = data['Stacks'][0]['StackStatus']
        print('Stack status: ' + iam_stack_status)
        if iam_stack_status in ['ROLLBACK_IN_PROGRESS', 'ROLLBACK_COMPLETE']:
            sys.exit('Error in creating iam stack. Delete the iam stack manually and try again')
        iam_stack_complete = iam_stack_status == 'CREATE_COMPLETE'
        if not iam_stack_complete:
            time.sleep(30)

    print('\nIAM STACK CREATION COMPLETE\n')

'''
ELASTIC BEANSTALK APP STACK
'''

# Get eb parameters from user
print('\nGET PARAMETERS FOR ELASTIC BEANSTALK APP STACK\n')
eb_stack_name = os.getenv('EB_APP_STACK_NAME')
eb_app_env_name = os.getenv('EB_APP_ENV_NAME')
eb_app_name = os.getenv('EB_APP_NAME')
eb_soln_stack_name = os.getenv('EB_SOLN_STACK_NAME')
eb_inst_type = os.getenv('EB_INSTANCE_TYPE')

if not DISABLE_PROMPTS:
    eb_stack_name = raw_input('Enter EB_APP_STACK_NAME (default: hopes-prod-app): ')
    eb_app_env_name = raw_input('EBEnvironmentName (default: hopes-app) = ')
    eb_app_name = raw_input('EBApplicationName (default: hopes-app) = ')
    eb_soln_stack_name = raw_input(
        'SolutionStackName (default: 64bit Amazon Linux 2018.03 v2.9.3 running Python 2.7) = ')
    eb_inst_type = raw_input('InstanceType (default: t3a.micro) = ')

# Get Elastic Beanstalk App stack name
print('START ELASTIC BEANSTALK APP STACK CREATION\n')
if eb_stack_name is None or eb_stack_name.strip() == '':
    eb_stack_name = 'hopes-prod-app'

# Check if elastic beanstalk app stack already exists with the same name
eb_stack_exists = True
try:
    data = cf_client.describe_stacks(StackName=eb_stack_name)
    stack_status = data['Stacks'][0]['StackStatus']
    eb_app_parameters = data['Stacks'][0]['Parameters']
    for parameter in eb_app_parameters:
        if parameter['ParameterKey'] == 'EBEnvironmentName':
            eb_app_env_name = parameter['ParameterValue']
    if not stack_status.endswith('_COMPLETE'):
        sys.exit('Error! Elastic Beanstalk App stack already exists with status - {}. '
                 'Try with different Elastic Beanstalk App stack name'.format(stack_status))
    print('\nELASTIC BEANSTALK APP STACK ALREADY EXISTS WITH STATUS - {}, '
          'SKIPPING ELASTIC BEANSTALK APP STACK CREATION\n'.format(stack_status))
except:
    eb_stack_exists = False

# Create Elastic Beanstalk App stack only if it does not exist
if not eb_stack_exists:

    # Get eb stack template full path
    eb_stack_file = os.path.join(dir_path, 'elasticbeanstalk-standalone.yml')

    # Read eb stack template file and convert to string
    eb_stack = open(eb_stack_file, 'r')
    eb_template = eb_stack.read()
    eb_stack.close()

    # Create eb parameters list
    eb_parameters = [
        {
            'ParameterKey': 'ParentNetworkStack',
            'ParameterValue': network_stack_name
        },
        {
            'ParameterKey': 'ParentDBStack',
            'ParameterValue': db_stack_name
        },
        {
            'ParameterKey': 'ParentS3Stack',
            'ParameterValue': s3_stack_name
        },
        {
            'ParameterKey': 'ParentIAMStack',
            'ParameterValue': iam_stack_name
        },
        {
            'ParameterKey': 'EC2KeyName',
            'ParameterValue': key_name
        }
    ]
    if eb_app_env_name is not None and eb_app_env_name.strip() != '':
        eb_parameters.append({
            'ParameterKey': 'EBEnvironmentName',
            'ParameterValue': eb_app_env_name
        })
    if eb_app_name is not None and eb_app_name.strip() != '':
        eb_parameters.append({
            'ParameterKey': 'EBApplicationName',
            'ParameterValue': eb_app_name
        })
    if eb_soln_stack_name is not None and eb_soln_stack_name.strip() != '':
        eb_parameters.append({
            'ParameterKey': 'SolutionStackName',
            'ParameterValue': eb_soln_stack_name
        })
    if eb_inst_type is not None and eb_inst_type.strip() != '':
        eb_parameters.append({
            'ParameterKey': 'InstanceType',
            'ParameterValue': eb_inst_type
        })

    # Create eb app stack
    print('\nCREATE ELASTIC BEANSTALK APP STACK\n')
    cf_client.create_stack(
        StackName=eb_stack_name,
        TemplateBody=eb_template,
        Parameters=eb_parameters,
        Capabilities=['CAPABILITY_NAMED_IAM']
    )

    # Wait for eb app stack creation to complete
    print('WAIT UNTIL ELASTIC BEANSTALK APP STACK - CREATE COMPLETE\n')
    eb_stack_complete = False
    while not eb_stack_complete:
        print('Checking status of elastic beanstalk app stack')
        data = cf_client.describe_stacks(StackName=eb_stack_name)
        eb_stack_status = data['Stacks'][0]['StackStatus']
        print('Stack status: ' + eb_stack_status)
        if eb_stack_status in ['ROLLBACK_IN_PROGRESS', 'ROLLBACK_COMPLETE']:
            sys.exit('Error in creating eb app stack. Delete the eb app stack manually and try again')
        eb_stack_complete = eb_stack_status == 'CREATE_COMPLETE'
        if not eb_stack_complete:
            time.sleep(30)

    print('\nELASTIC BEANSTALK APP STACK CREATION COMPLETE\n')

'''
ELASTIC BEANSTALK UI STACK
'''

# Get eb ui parameters from user
print('\nGET PARAMETERS FOR ELASTIC BEANSTALK UI STACK\n')
eb_ui_stack_name = os.getenv('EB_UI_STACK_NAME')
eb_ui_env_name = os.getenv('EB_UI_ENV_NAME')
eb_ui_app_name = os.getenv('EB_UI_NAME')
eb_ui_soln_stack_name = os.getenv('EB_UI_SOLN_STACK_NAME')
eb_ui_inst_type = os.getenv('EB_UI_INSTANCE_TYPE')

if not DISABLE_PROMPTS:
    eb_ui_stack_name = raw_input('Enter EB_UI_STACK_NAME (default: hopes-prod-app-ui): ')
    eb_ui_env_name = raw_input('EBEnvironmentName (default: hopes-ui) = ')
    eb_ui_app_name = raw_input('EBApplicationName (default: hopes-ui) = ')
    eb_ui_soln_stack_name = raw_input(
        'SolutionStackName (default: 64bit Amazon Linux 2018.03 v2.9.3 running Python 2.7) = ')
    eb_ui_inst_type = raw_input('InstanceType (default: t3a.micro) = ')

# Get Elastic Beanstalk UI stack name
print('START ELASTIC BEANSTALK UI STACK CREATION\n')
if eb_ui_stack_name is None or eb_ui_stack_name.strip() == '':
    eb_ui_stack_name = 'hopes-prod-app-ui'

# Check if elastic beanstalk ui stack already exists with the same name
eb_ui_stack_exists = True
try:
    data = cf_client.describe_stacks(StackName=eb_ui_stack_name)
    stack_status = data['Stacks'][0]['StackStatus']
    eb_ui_parameters = data['Stacks'][0]['Parameters']
    for parameter in eb_ui_parameters:
        if parameter['ParameterKey'] == 'EBEnvironmentName':
            eb_ui_env_name = parameter['ParameterValue']
    if not stack_status.endswith('_COMPLETE'):
        sys.exit('Error! Elastic Beanstalk UI stack already exists with status - {}. '
                 'Try with different Elastic Beanstalk UI stack name'.format(stack_status))
    print('\nELASTIC BEANSTALK UI STACK ALREADY EXISTS WITH STATUS - {}, '
          'SKIPPING ELASTIC BEANSTALK UI STACK CREATION\n'.format(stack_status))
except:
    eb_ui_stack_exists = False

# Create Elastic Beanstalk UI stack only if it does not exist
if not eb_ui_stack_exists:

    # Get eb ui stack template full path
    eb_ui_stack_file = os.path.join(dir_path, 'elasticbeanstalk-standalone-ui.yml')

    # Read eb ui stack template file and convert to string
    eb_ui_stack = open(eb_ui_stack_file, 'r')
    eb_ui_template = eb_ui_stack.read()
    eb_ui_stack.close()

    # Create eb ui parameters list
    eb_ui_parameters = [
        {
            'ParameterKey': 'EC2KeyName',
            'ParameterValue': key_name
        },
        {
            'ParameterKey': 'ParentNetworkStack',
            'ParameterValue': network_stack_name
        },
        {
            'ParameterKey': 'ParentDBStack',
            'ParameterValue': db_stack_name
        },
        {
            'ParameterKey': 'ParentS3Stack',
            'ParameterValue': s3_stack_name
        },
        {
            'ParameterKey': 'ParentIAMStack',
            'ParameterValue': iam_stack_name
        }
    ]
    if eb_ui_app_name is not None and eb_ui_app_name.strip() != '':
        eb_ui_parameters.append({
            'ParameterKey': 'EBApplicationName',
            'ParameterValue': eb_ui_app_name
        })
    if eb_ui_env_name is not None and eb_ui_env_name.strip() != '':
        eb_ui_parameters.append({
            'ParameterKey': 'EBEnvironmentName',
            'ParameterValue': eb_ui_env_name
        })
    if eb_ui_inst_type is not None and eb_ui_inst_type.strip() != '':
        eb_ui_parameters.append({
            'ParameterKey': 'InstanceType',
            'ParameterValue': eb_ui_inst_type
        })
    if eb_ui_soln_stack_name is not None and eb_ui_soln_stack_name.strip() != '':
        eb_ui_parameters.append({
            'ParameterKey': 'SolutionStackName',
            'ParameterValue': eb_ui_soln_stack_name
        })

    # Create eb ui stack
    print('\nCREATE ELASTIC BEANSTALK UI STACK\n')
    cf_client.create_stack(
        StackName=eb_ui_stack_name,
        TemplateBody=eb_ui_template,
        Parameters=eb_ui_parameters,
        Capabilities=['CAPABILITY_NAMED_IAM']
    )

    # Wait for eb ui stack creation to complete
    print('WAIT UNTIL ELASTIC BEANSTALK UI STACK - CREATE COMPLETE\n')
    eb_ui_stack_complete = False
    while not eb_ui_stack_complete:
        print('Checking status of elastic beanstalk ui stack')
        data = cf_client.describe_stacks(StackName=eb_ui_stack_name)
        eb_ui_stack_status = data['Stacks'][0]['StackStatus']
        print('Stack status: ' + eb_ui_stack_status)
        if eb_ui_stack_status in ['ROLLBACK_IN_PROGRESS', 'ROLLBACK_COMPLETE']:
            sys.exit('Error in creating eb ui stack. Delete the eb ui stack manually and try again')
        eb_ui_stack_complete = eb_ui_stack_status == 'CREATE_COMPLETE'
        if not eb_ui_stack_complete:
            time.sleep(30)

    print('\nELASTIC BEANSTALK UI STACK CREATION COMPLETE\n')
