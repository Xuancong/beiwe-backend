AWSTemplateFormatVersion: 2010-09-09

Description: Provision Elastic Beanstalk Environment to deploy the application
Metadata:
  'AWS::CloudFormation::Interface':
    ParameterGroups:
    - Label:
        default: 'Parent Stacks'
      Parameters:
      - ParentNetworkStack
      - ParentS3Stack
Parameters:
  ParentNetworkStack:
    Description: Name of the vpc network stack
    Type: String
  ParentS3Stack:
    Description: Name of the S3 bucket stack
    Type: String

Resources:
  UploadPolicy:
    Type: AWS::IAM::Policy
    Properties:
      PolicyDocument:
        Version: '2012-10-17'
        Statement:
        - Sid: VisualEditor0
          Effect: Allow
          Action: s3:PutObject
          Resource: !Join
            - ''
            - - 'arn:aws:s3:::'
              - {'Fn::ImportValue': !Sub '${ParentS3Stack}-BeiweBucket'}
              - '/*'
        - Sid: VisualEditor1
          Effect: Allow
          Action: cloudwatch:PutMetricData
          Resource: "*"
      PolicyName: !Join
            - ''
            - - {'Fn::ImportValue': !Sub '${ParentNetworkStack}-EnvironmentName'}
              - '-uploadpolicy'
      Roles: [!Ref InstanceProfileRole]

  SecretsManagerPolicy:
    Type: AWS::IAM::Policy
    Properties:
      PolicyDocument:
        Version: '2012-10-17'
        Statement:
        - Sid: VisualEditor0
          Effect: Allow
          Action: secretsmanager:GetSecretValue
          Resource:
          - !Sub 'arn:aws:secretsmanager:${AWS::Region}:${AWS::AccountId}:secret:public/*'
          - !Sub 'arn:aws:secretsmanager:${AWS::Region}:${AWS::AccountId}:secret:private/*'
        - Sid: VisualEditor1
          Effect: Allow
          Action: secretsmanager:CreateSecret
          Resource: "*"
      PolicyName: !Join
            - ''
            - - {'Fn::ImportValue': !Sub '${ParentNetworkStack}-EnvironmentName'}
              - '-secretsmanagerpolicy'
      Roles: [!Ref InstanceProfileRole]

  ServiceRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement: 
          - Effect: Allow
            Principal: 
              Service: 
                - elasticbeanstalk.amazonaws.com
            Action: 
              - sts:AssumeRole
            Condition:
              StringEquals: 
                sts:ExternalId: elasticbeanstalk
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkService
        - arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkEnhancedHealth
      RoleName:  !Join
            - ''
            - - {'Fn::ImportValue': !Sub '${ParentNetworkStack}-EnvironmentName'}
              - '-elasticbeanstalk-service-role'

  InstanceProfileRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement: 
          - Effect: Allow
            Principal: 
              Service: 
                - ec2.amazonaws.com
            Action: 
              -  sts:AssumeRole
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier
        - arn:aws:iam::aws:policy/AWSElasticBeanstalkMulticontainerDocker
        - arn:aws:iam::aws:policy/AWSElasticBeanstalkWorkerTier
      RoleName:  !Join
            - ''
            - - {'Fn::ImportValue': !Sub '${ParentNetworkStack}-EnvironmentName'}
              - '-elasticbeanstalk-instance-profile-role'

  InstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Path: /
      Roles:
        - !Ref 'InstanceProfileRole'
      InstanceProfileName: !Join
            - ''
            - - {'Fn::ImportValue': !Sub '${ParentNetworkStack}-EnvironmentName'}
              - '-elasticbeanstalk-instance-profile'
Outputs:
  ServiceRole:
    Description: A reference to service role that elastic beanstalk can use.
    Value: !Ref ServiceRole
    Export: 
      Name: !Sub '${AWS::StackName}-ServiceRole'

  InstanceProfile:
    Description: A reference to instance profile that elastic beanstalk or ec2 instances can use .
    Value: !Ref InstanceProfile
    Export: 
      Name: !Sub '${AWS::StackName}-InstanceProfile'  

  InstanceProfileRole:
    Description: A reference to instance profile role that elastic beanstalk or ec2 instances can use .
    Value: !Ref InstanceProfileRole
    Export: 
      Name: !Sub '${AWS::StackName}-InstanceProfileRole'  
