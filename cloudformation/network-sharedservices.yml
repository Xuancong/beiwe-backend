AWSTemplateFormatVersion: 2010-09-09

Description:  This template deploys a VPC, with 2 public subnets, 2 private subnets with internet access, 2 private subnets with no internet access spread
  across two Availability Zones, an Internet Gateway, a NAT Gateway, Public Private & PrivateNoNAT Route tables and its corresponding association with subnets.

Parameters:
  EnvironmentName:
    Description: An environment name that will be prefixed to resource names
    Type: String

  VpcCIDR:
    Description: Please enter the IP range (CIDR notation) for this VPC
    Type: String
    Default: 10.0.0.0/16
    AllowedPattern: "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})"
    ConstraintDescription: must be a valid CIDR range of the form x.x.x.x/x.

  PublicSubnet1CIDR:
    Description: Please enter the IP range (CIDR notation) for the public subnet in the 1st Availability Zone
    Type: String
    Default: 10.0.10.0/24
    AllowedPattern: "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})"
    ConstraintDescription: must be a valid CIDR range of the form x.x.x.x/x and a subset of the VpcCIDR.

  PublicSubnet2CIDR:
    Description: Please enter the IP range (CIDR notation) for the public subnet in the 2nd Availability Zone
    Type: String
    Default: 10.0.11.0/24
    AllowedPattern: "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})"
    ConstraintDescription: must be a valid CIDR range of the form x.x.x.x/x and a subset of the VpcCIDR.

  PrivateSubnet1CIDR:
    Description: Please enter the IP range (CIDR notation) for the private subnet with internet access in the first Availability Zone
    Type: String
    Default: 10.0.20.0/24
    AllowedPattern: "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})"
    ConstraintDescription: must be a valid CIDR range of the form x.x.x.x/x and a subset of the VpcCIDR.

  PrivateSubnet2CIDR:
    Description: Please enter the IP range (CIDR notation) for the private subnet with internet access in the second Availability Zone
    Type: String
    Default: 10.0.21.0/24
    AllowedPattern: "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})"
    ConstraintDescription: must be a valid CIDR range of the form x.x.x.x/x and a subset of the VpcCIDR.

  PrivateSubnet3NoNATCIDR:
    Description: Please enter the IP range (CIDR notation) for the private subnet without internet access in the first Availability Zone
    Type: String
    Default: 10.0.22.0/24
    AllowedPattern: "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})"
    ConstraintDescription: must be a valid CIDR range of the form x.x.x.x/x and a subset of the VpcCIDR.

  PrivateSubnet4NoNATCIDR:
    Description: Please enter the IP range (CIDR notation) for the private subnet without internet access in the second Availability Zone
    Type: String
    Default: 10.0.23.0/24
    AllowedPattern: "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})"
    ConstraintDescription: must be a valid CIDR range of the form x.x.x.x/x and a subset of the VpcCIDR.

Resources:
  VPC:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: !Ref VpcCIDR
      EnableDnsSupport: true
      EnableDnsHostnames: true
      Tags:
        - Key: Name
          Value: !Join [ '', [ !Ref EnvironmentName,'-vpc' ] ]

  PublicSubnet1:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select [ 0, !GetAZs '' ]
      CidrBlock: !Ref PublicSubnet1CIDR
      MapPublicIpOnLaunch: true
      Tags:
        - Key: Name
          Value: !Sub '${EnvironmentName}-PublicSubnet-AZ1'

  PublicSubnet2:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select [ 1, !GetAZs '' ]
      CidrBlock: !Ref PublicSubnet2CIDR
      MapPublicIpOnLaunch: true
      Tags:
        - Key: Name
          Value: !Sub '${EnvironmentName}-PublicSubnet-AZ2'

  PrivateSubnet1:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select [ 0, !GetAZs  '' ]
      CidrBlock: !Ref PrivateSubnet1CIDR
      MapPublicIpOnLaunch: false
      Tags:
        - Key: Name
          Value: !Sub '${EnvironmentName}-PrivateSubnet-AZ1'

  PrivateSubnet2:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select [ 1, !GetAZs  '' ]
      CidrBlock: !Ref PrivateSubnet2CIDR
      MapPublicIpOnLaunch: false
      Tags:
        - Key: Name
          Value: !Sub '${EnvironmentName}-PrivateSubnet-AZ2'

  PrivateSubnet3NoNAT:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select [ 0, !GetAZs  '' ]
      CidrBlock: !Ref PrivateSubnet3NoNATCIDR
      MapPublicIpOnLaunch: false
      Tags:
        - Key: Name
          Value: !Sub '${EnvironmentName}-PrivateSubnet-AZ3-NoNAT'

  PrivateSubnet4NoNAT:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select [ 1, !GetAZs  '' ]
      CidrBlock: !Ref PrivateSubnet4NoNATCIDR
      MapPublicIpOnLaunch: false
      Tags:
        - Key: Name
          Value: !Sub '${EnvironmentName}-PrivateSubnet-AZ4-NoNAT'

  InternetGateway:
    Type: AWS::EC2::InternetGateway
    Properties:
      Tags:
        - Key: Name
          Value: !Join [ '', [ !Ref EnvironmentName,'-igw' ] ]

  InternetGatewayAttachment:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
      InternetGatewayId: !Ref InternetGateway
      VpcId: !Ref VPC

  NatGatewayEIP:
    Type: AWS::EC2::EIP
    DependsOn: InternetGatewayAttachment
    Properties:
      Domain: vpc

  NatGateway:
    Type: AWS::EC2::NatGateway
    Properties:
      AllocationId: !GetAtt NatGatewayEIP.AllocationId
      SubnetId: !Ref PublicSubnet1

  PublicRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: Name
          Value: !Sub '${EnvironmentName}-PublicRT'

  DefaultPublicRoute:
    Type: AWS::EC2::Route
    DependsOn: InternetGatewayAttachment
    Properties:
      RouteTableId: !Ref PublicRouteTable
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref InternetGateway

  PublicSubnet1RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref PublicSubnet1

  PublicSubnet2RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref PublicSubnet2

  PrivateRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: Name
          Value: !Sub '${EnvironmentName}-PrivateRT'

  PrivateNoNATRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: Name
          Value: !Sub '${EnvironmentName}-PrivateRT-NoNAT'

  DefaultPrivateRoute:
    Type: AWS::EC2::Route
    Properties:
      RouteTableId: !Ref PrivateRouteTable
      DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: !Ref NatGateway

  PrivateSubnet1RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PrivateRouteTable
      SubnetId: !Ref PrivateSubnet1

  PrivateSubnet2RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PrivateRouteTable
      SubnetId: !Ref PrivateSubnet2

  PrivateSubnet3NoNATRouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PrivateNoNATRouteTable
      SubnetId: !Ref PrivateSubnet3NoNAT

  PrivateSubnet4NoNATRouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PrivateNoNATRouteTable
      SubnetId: !Ref PrivateSubnet4NoNAT 

Outputs:
  VPC:
    Description: A reference to the created VPC
    Value: !Ref VPC
    Export: 
      Name: !Sub '${AWS::StackName}-VPCID'

  VPCCidr:
    Description: Vpc cidr
    Value: !GetAtt VPC.CidrBlock
    Export: 
      Name: !Sub '${AWS::StackName}-VPCCidr'

  PublicSubnet1Cidr:
    Description: public subnet1 cidr
    Value: !Ref PublicSubnet1CIDR
    Export: 
      Name: !Sub '${AWS::StackName}-PublicSubnet1Cidr'

  PublicSubnet2Cidr:
    Description: public subnet2 cidr
    Value: !Ref PublicSubnet2CIDR
    Export: 
      Name: !Sub '${AWS::StackName}-PublicSubnet2Cidr'

  PrivateSubnet1Cidr:
    Description: private subnet1 cidr
    Value: !Ref PrivateSubnet1CIDR
    Export: 
      Name: !Sub '${AWS::StackName}-PrivateSubnet1Cidr'

  PrivateSubnet2Cidr:
    Description: private subnet2 cidr
    Value: !Ref PrivateSubnet2CIDR
    Export: 
      Name: !Sub '${AWS::StackName}-PrivateSubnet2Cidr'

  PrivateNoNATSubnet3Cidr:
    Description: private NoNAT subnet3 
    Value: !Ref PrivateSubnet3NoNATCIDR
    Export: 
      Name: !Sub '${AWS::StackName}-PrivateNoNATSubnet3Cidr'

  PrivateNoNATSubnet4Cidr:
    Description: private NoNAT subnet4
    Value: !Ref PrivateSubnet4NoNATCIDR
    Export: 
      Name: !Sub '${AWS::StackName}-PrivateNoNATSubnet4Cidr'

  PublicSubnet1:
    Description: A reference to the public subnet id in the 1st Availability Zone
    Value: !Ref PublicSubnet1
    Export: 
      Name: !Sub '${AWS::StackName}-PublicSubnet1-SubnetID'

  PublicSubnet2:
    Description: A reference to the public subnet id in the 2nd Availability Zone
    Value: !Ref PublicSubnet2
    Export: 
      Name: !Sub '${AWS::StackName}-PublicSubnet2-SubnetID'

  PrivateSubnet1:
    Description: A reference to the private subnet id with interent access through NAT Gateway in the 1st Availability Zone
    Value: !Ref PrivateSubnet1
    Export: 
      Name: !Sub '${AWS::StackName}-PrivateSubnet1-SubnetID'

  PrivateSubnet2:
    Description: A reference to the private subnet id with interent access through NAT Gateway in the 2nd Availability Zone
    Value: !Ref PrivateSubnet2
    Export: 
      Name: !Sub '${AWS::StackName}-PrivateSubnet2-SubnetID'

  PrivateSubnet3NoNAT:
    Description: A reference to the private subnet id with no internet access in the 1st Availability Zone
    Value: !Ref PrivateSubnet3NoNAT
    Export: 
      Name: !Sub '${AWS::StackName}-PrivateSubnet3NoNAT-SubnetID'

  PrivateSubnet4NoNAT:
    Description: A reference to the private subnet id with no interent access in the 2nd Availability Zone
    Value: !Ref PrivateSubnet4NoNAT
    Export: 
      Name: !Sub '${AWS::StackName}-PrivateSubnet4NoNAT-SubnetID'

  EnvironmentName:
    Description: A reference to environment name that will be prefixed to resource names.
    Value: !Ref EnvironmentName
    Export: 
      Name: !Sub '${AWS::StackName}-EnvironmentName'


   