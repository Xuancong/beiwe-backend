#!/usr/bin/env python3

import os
import sys
import time
import random
import string

# Check for boto3 (aws client) and then import
try:
    __import__('boto3')
except ImportError:
    os.system('pip install boto3')

import boto3

# The following import statement will run the whole provision_network_infra and provision_db_s3_eb_infra
from provision_iam_acm_asg_dns_infra import aws_secret_access_key, aws_region_name, aws_access_key_id, \
    DISABLE_PROMPTS, eb_app_name, eb_ui_app_name, eb_app_env_name, eb_ui_env_name, \
    external_admin_app_domain_name, admin_app_client_cert_arn, ui_cert_arn

# Initialize elastic beanstalk client
eb_client = boto3.client('elasticbeanstalk',
                         region_name=aws_region_name,
                         aws_access_key_id=aws_access_key_id,
                         aws_secret_access_key=aws_secret_access_key)

# Initialize elastic load balancer client
elbv2_client = boto3.client('elbv2',
                            region_name=aws_region_name,
                            aws_access_key_id=aws_access_key_id,
                            aws_secret_access_key=aws_secret_access_key)

# Initialize acm client
acm_client = boto3.client('acm',
                          region_name=aws_region_name,
                          aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key)

# Get full directory path
dir_path = os.path.dirname(os.path.abspath(__file__))

# https option settings to add
ssl_option_settings = "  - namespace: aws:elbv2:listener:443\n\
    option_name: DefaultProcess\n\
    value: https\n\
  - namespace: aws:elbv2:listener:443\n\
    option_name: ListenerEnabled\n\
    value: true\n\
  - namespace: aws:elbv2:listener:443\n\
    option_name: Protocol\n\
    value: HTTPS\n\
  - namespace: aws:elbv2:listener:443\n\
    option_name: SSLCertificateArns\n\
    value: {0}\n\
  - namespace: aws:elbv2:listener:443\n\
    option_name: ListenerEnabled\n\
    value: true\n\
  - namespace: aws:elasticbeanstalk:environment:process:https\n\
    option_name: Port\n\
    value: 443\n\
  - namespace: aws:elasticbeanstalk:environment:process:https\n\
    option_name: Protocol\n\
    value: HTTPS\n"

# Install EB CLI
print('INSTALL AWS EB CLI\n')
os.system('pip install awsebcli')

# Get eb platform arn
print('\nGET EB PLATFORM ARN\n')
eb_platform_arn = os.getenv('EB_PF_ARN')
if not DISABLE_PROMPTS:
    eb_platform_arn = raw_input('Enter EB_PLATFORM_ARN (default: '
                                'arn:aws:elasticbeanstalk:ap-southeast-1::platform/Python 2.7 running on 64bit Amazon '
                                'Linux/2.9.4): ')
    if eb_platform_arn is None or eb_platform_arn.strip() == '':
        eb_platform_arn = 'arn:aws:elasticbeanstalk:ap-southeast-1::platform/Python 2.7 running on 64bit Amazon ' \
                          'Linux/2.9.4 '

'''
EB UPDATE ENV PROPERTIES AND DEPLOY - APP
'''

print('EB UPDATE ENV PROPERTIES - APP')
# Create EB APP env properties dictionary
flask_key = ''.join(random.SystemRandom().choice(
    string.ascii_uppercase + string.ascii_lowercase + string.digits
) for _ in range(80))
eb_app_env_properties = {
    'SENTRY_ANDROID_DSN': os.getenv('EB_SENTRY_ANDROID_DSN'),
    'SENTRY_ELASTIC_BEANSTALK_DSN': os.getenv('EB_SENTRY_ELASTIC_BEANSTALK_DSN'),
    'SENTRY_DATA_PROCESSING_DSN': os.getenv('EB_SENTRY_DATA_PROCESSING_DSN'),
    'SENTRY_JAVASCRIPT_DSN': os.getenv('EB_SENTRY_JAVASCRIPT_DSN'),
    'SYSADMIN_EMAILS': os.getenv('EB_SYSADMIN_EMAILS'),
    'DOMAIN_NAME': external_admin_app_domain_name,
    'IS_STAGING': os.getenv('EB_IS_STAGING'),
    'FLASK_SECRET_KEY': flask_key,
    'S3_ACCESS_CREDENTIALS_USER': os.getenv('S3_ACCESS_CREDENTIALS_USER'),
    'S3_ACCESS_CREDENTIALS_KEY': os.getenv('S3_ACCESS_CREDENTIALS_KEY'),
    'SECRETS_MANAGER_ACCESS_CREDENTIALS_KEY': os.getenv('SECRETS_MANAGER_ACCESS_CREDENTIALS_KEY'),
    'SECRETS_MANAGER_ACCESS_CREDENTIALS_USER': os.getenv('SECRETS_MANAGER_ACCESS_CREDENTIALS_USER'),
    'PCA_ARN': os.getenv('PCA_ROOT_CA'),
    'USE_HTTP': '0',
    'HealthStreamingEnabled': 'true',
    'DeleteOnTerminate': 'false',
    'RetentionInDays': '7'
}

# Read options config file
options_config_file = os.path.join(dir_path, '..', '.ebextensions', 'options.config')
with open(options_config_file, 'r') as options_config:
    default_options = options_config.readlines()

# Change default options to new options
new_options = []
option_name = ''
for option_line in default_options:
    if 'option_name:' in option_line:
        option_name = (option_line.split(':')[1]).strip()
    if 'value:' in option_line:
        replace_string = option_line.split('value: ')[1]
        option_line = option_line.replace(replace_string, eb_app_env_properties[option_name])
        if option_line[-1] != '\n':
            option_line = option_line + '\n'
    new_options.append(option_line)
    new_options.append(ssl_option_settings.format(admin_app_client_cert_arn['CertificateArn']))

# Write new options to config file
with open(options_config_file, 'w') as options_config:
    options_config.writelines(new_options)

# Wait for max 20 minutes for the ready state on EB app stack
print('EB CHECK STATUS - APP')
ready = 0
status = ''
for x in range(1, 10):
    eb_response = eb_client.describe_environments(
        ApplicationName=eb_app_name,
        EnvironmentNames=[eb_app_env_name]
    )

    greenAndReady = 0
    environments = eb_response['Environments']
    for env in environments:
        if env['Status'].lower().find('ready') > -1:
            greenAndReady = 1
            status = env['Status']

    if greenAndReady:
        ready = 1
        break
    else:
        print('Sleep and try again')
        time.sleep(20)

if not ready:
    sys.exit('EB APP - Environment not starting up in allotted time or environment is not healthy')

print('\nEB APP STATUS - {0}\n'.format(status))

# Run eb init command for the app
print('EB INIT - APP\n')
eb_init_command = 'cd .. && eb init --region {0} --platform="{1}" {2}'\
    .format(aws_region_name, eb_platform_arn, eb_app_name)
os.system(eb_init_command)

# Run eb use command for the app
print('EB USE - APP\n')
eb_use_command = 'cd .. && eb use {0}'.format(eb_app_env_name)
os.system(eb_use_command)

# Run eb deploy for app
print('EB DEPLOY - APP\n')
eb_deploy = os.popen('cd .. && eb deploy --timeout 60')
eb_deploy_output = eb_deploy.read()

print(eb_deploy_output)
if 'Environment update completed successfully' not in eb_deploy_output:
    print('\nEB DEPLOYMENT - APP ERROR\n')
    # Write default options back to config file
    with open(options_config_file, 'w') as options_config:
        options_config.writelines(default_options)
    sys.exit(0)

print('\nEB DEPLOYMENT - APP COMPLETED\n')

# Write default options back to config file
with open(options_config_file, 'w') as options_config:
    options_config.writelines(default_options)

'''
EB UPDATE ENV PROPERTIES AND DEPLOY - UI
'''

print('EB UPDATE ENV PROPERTIES - UI')
# Create EB UI env properties dictionary
flask_key = ''.join(random.SystemRandom().choice(
    string.ascii_uppercase + string.ascii_lowercase + string.digits
) for _ in range(80))
eb_app_env_properties = {
    'SENTRY_ANDROID_DSN': os.getenv('EB_SENTRY_ANDROID_DSN'),
    'SENTRY_ELASTIC_BEANSTALK_DSN': os.getenv('EB_SENTRY_ELASTIC_BEANSTALK_DSN'),
    'SENTRY_DATA_PROCESSING_DSN': os.getenv('EB_SENTRY_DATA_PROCESSING_DSN'),
    'SENTRY_JAVASCRIPT_DSN': os.getenv('EB_SENTRY_JAVASCRIPT_DSN'),
    'SYSADMIN_EMAILS': os.getenv('EB_SYSADMIN_EMAILS'),
    'DOMAIN_NAME': external_admin_app_domain_name,
    'IS_STAGING': os.getenv('EB_IS_STAGING'),
    'FLASK_SECRET_KEY': flask_key,
    'S3_ACCESS_CREDENTIALS_USER': os.getenv('S3_ACCESS_CREDENTIALS_USER'),
    'S3_ACCESS_CREDENTIALS_KEY': os.getenv('S3_ACCESS_CREDENTIALS_KEY'),
    'SECRETS_MANAGER_ACCESS_CREDENTIALS_KEY': os.getenv('SECRETS_MANAGER_ACCESS_CREDENTIALS_KEY'),
    'SECRETS_MANAGER_ACCESS_CREDENTIALS_USER': os.getenv('SECRETS_MANAGER_ACCESS_CREDENTIALS_USER'),
    'PCA_ARN': os.getenv('PCA_ROOT_CA'),
    'USE_HTTP': '0',
    'HealthStreamingEnabled': 'true',
    'DeleteOnTerminate': 'false',
    'RetentionInDays': '7'
}

# Read options config file
options_config_file = os.path.join(dir_path, '..', '.ebextensions', 'options.config')
with open(options_config_file, 'r') as options_config:
    default_options = options_config.readlines()

# Change default options to new options
new_options = []
option_name = ''
for option_line in default_options:
    if 'option_name:' in option_line:
        option_name = (option_line.split(':')[1]).strip()
    if 'value:' in option_line:
        replace_string = option_line.split('value: ')[1]
        option_line = option_line.replace(replace_string, eb_app_env_properties[option_name])
        if option_line[-1] != '\n':
            option_line = option_line + '\n'
    new_options.append(option_line)
    new_options.append(ssl_option_settings.format(ui_cert_arn['CertificateArn']))

# Write new options to config file
with open(options_config_file, 'w') as options_config:
    options_config.writelines(new_options)

# Wait for max 20 minutes for the ready state on EB UI stack
print('EB CHECK STATUS - UI')
ready = 0
status = ''
for x in range(1, 10):
    eb_response = eb_client.describe_environments(
        ApplicationName=eb_ui_app_name,
        EnvironmentNames=[eb_ui_env_name]
    )

    greenAndReady = 0
    environments = eb_response['Environments']
    for env in environments:
        if env['Status'].lower().find('ready') > -1:
            greenAndReady = 1
            status = env['Status']

    if greenAndReady:
        ready = 1
        break
    else:
        print('Sleep and try again')
        time.sleep(20)

if not ready:
    sys.exit('EB UI - Environment not starting up in allotted time or environment is not healthy')

print('\nEB UI STATUS - {0}\n'.format(status))

# Run eb init command for the ui
print('\nEB INIT - UI\n')
eb_ui_init_command = 'cd .. && eb init --region {0} --platform="{1}" {2}'\
    .format(aws_region_name, eb_platform_arn, eb_ui_app_name)
os.system(eb_ui_init_command)

# Run eb use command for the ui
print('\nEB USE - UI\n')
eb_ui_use_command = 'cd .. && eb use {0}'.format(eb_ui_env_name)
os.system(eb_ui_use_command)

# Run eb deploy for ui
print('EB DEPLOY - UI\n')
eb_ui_deploy = os.popen('cd .. && eb deploy --timeout 60')
eb_ui_deploy_output = eb_ui_deploy.read()

print(eb_ui_deploy_output)
if 'Environment update completed successfully' not in eb_ui_deploy_output:
    print('\nEB DEPLOYMENT - UI ERROR\n')
    # Write default options back to config file
    with open(options_config_file, 'w') as options_config:
        options_config.writelines(default_options)
    sys.exit(0)

print('\nEB DEPLOYMENT - UI COMPLETED\n')

# Write default options back to config file
with open(options_config_file, 'w') as options_config:
    options_config.writelines(default_options)
