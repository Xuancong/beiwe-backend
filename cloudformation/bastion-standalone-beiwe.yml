AWSTemplateFormatVersion: '2010-09-09'
Description: 'This template provisions a Bastion instance with an elastic IP and a security group. The security group restricts access on port 22 & 443 to vpc cidr range and allows access on udp port 4500, 500 for l2tp connections'
Metadata:
  'AWS::CloudFormation::Interface':
    ParameterGroups:
    - Label:
        default: 'Parent Stacks'
      Parameters:
      - ParentNetworkStack
    - Label:
        default: 'EC2 Parameters'
      Parameters:
      - KeyName
      - InstanceType
      - BastionImageRegion

Parameters:
  ParentNetworkStack:
    Description: 'Stack name of parent VPC stack based'
    Type: String
  KeyName:
    Description: 'key pair of the ec2-user to establish a SSH connection to the bastion instance.'
    Type: String
  InstanceType:
    Description: 'Instance type of the SSH bastion host/instance.'
    Type: String
    Default: 't3a.nano'
  BastionImageRegion:
    Description: 'Select Region of the hopes Bastion image; Ensure the AMI has permissions for the account in which the Bastion instance is being created'
    Type: String
    AllowedValues:
      - Singapore
      - Sydney
    Default: Singapore
  
Mappings:
  AMIMap:
    Singapore: 
      ImageId: ami-0d3912dd78b48a7e1
    Sydney: 
      ImageId: ami-07e9917b952abe192


Resources:
  BastionIPAddress:
    Type: 'AWS::EC2::EIP'
    Properties:
      Domain: vpc
      InstanceId: !Ref 'Bastion'
  BastionSG:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      GroupDescription: !Join
            - ''
            - - {'Fn::ImportValue': !Sub '${ParentNetworkStack}-EnvironmentName'}
              - '-Bastion-securitygroup'
      GroupName: !Join
            - ''
            - - {'Fn::ImportValue': !Sub '${ParentNetworkStack}-EnvironmentName'}
              - '-Bastion-securitygroup'
      SecurityGroupIngress:
      - IpProtocol: tcp
        FromPort: 22
        ToPort: 22
        CidrIp: {'Fn::ImportValue': !Sub '${ParentNetworkStack}-VPCCidr'}
        Description: ssh              
      - IpProtocol: udp
        FromPort: 4500
        ToPort: 4500
        CidrIp: 0.0.0.0/0
        Description: l2tp
      - IpProtocol: udp
        FromPort: 500
        ToPort: 500
        CidrIp: 0.0.0.0/0
        Description: l2tp
      - IpProtocol: tcp
        FromPort: 443
        ToPort: 443
        CidrIp: {'Fn::ImportValue': !Sub '${ParentNetworkStack}-VPCCidr'}
        Description: Management 
      Tags:
        - Key: Name
          Value: !Join
            - ''
            - - {'Fn::ImportValue': !Sub '${ParentNetworkStack}-EnvironmentName'}
              - '-Bastion-securitygroup'
      VpcId: {'Fn::ImportValue': !Sub '${ParentNetworkStack}-VPCID'}
  Bastion:
    Type: AWS::EC2::Instance
    Properties: 
      InstanceType: !Ref InstanceType
      KeyName: !Ref KeyName
      SubnetId: {'Fn::ImportValue': !Sub '${ParentNetworkStack}-PublicSubnet1-SubnetID'}
      ImageId: !FindInMap [AMIMap, !Ref BastionImageRegion, ImageId ]
      SecurityGroupIds:
        - !Ref BastionSG
      Tags:
        - Key: Name
          Value: !Join
            - ''
            - - {'Fn::ImportValue': !Sub '${ParentNetworkStack}-EnvironmentName'}
              - '-Bastion'
Outputs:
  BastionSG:
    Description: 'Security Group ID of the Bastion Instance'
    Value: !GetAtt BastionSG.GroupId
    Export: 
      Name: !Sub '${AWS::StackName}-Bastion-SG'

  BastionPublicIP:
    Description: 'Public IP of the Bastion Instance'
    Value: !GetAtt Bastion.PublicIp
    Export: 
      Name: !Sub '${AWS::StackName}-Bastion-PublicIP' 
  