#!/bin/bash

if [ ! "$STUDY_ID" ]; then
    echo "Error: STUDY_ID is not defined"
    exit 1
fi

mkdir -p 4.decrypted/$STUDY_ID
./feature-processor/process-all.sh 3.decrypted/$STUDY_ID 4.decrypted/$STUDY_ID


