#!/usr/bin/env python3

import os, sys, re, argparse
import base64
import boto3
import json
from Crypto.Cipher import AES
from Crypto.PublicKey import RSA
from collections import defaultdict

class TC:
	HEADER = '\033[95m'
	OKBLUE = '\033[94m'
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'
	END = '\033[0m'
	BLR = '\033[1m\033[91m'
	BLG = '\033[1m\033[92m'
	BLY = '\033[1m\033[93m'
	LR = '\033[91m'
	LG = '\033[92m'
	LY = '\033[93m'

def get_private_key(secret_name):
	response = sm.get_secret_value(SecretId=secret_name)
	return json.loads(response['SecretString'])['private']

def remove_PKCS5_padding(data):
	return data[0: -ord(data[-1])]

def cleanup_slash(L):
	L1 = re.sub(r'/+', '/', L)
	return re.sub(r'/+$', '', L1)


sm = boto3.client('secretsmanager', region_name="ap-southeast-1", aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'), aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'))
smCache = defaultdict(lambda: None)


def proc(ipath, opath):
	if os.path.isdir(ipath):
		if [1 for pat in exclude if pat in ipath]:
			return
		for path in os.listdir(ipath):
			proc(ipath+'/'+path, opath+'/'+path)
	elif ipath.endswith(extension):
		Ipath = cleanup_slash(ipath)
		if [1 for pat in exclude if pat in Ipath]:
			return
		Opath = cleanup_slash(opath)
		if os.path.isdir(Opath):
			Opath += '/'+os.path.basename(Ipath)
		if Opath.endswith(extension):
			Opath = Opath[:-len(extension)]
		if not force and os.path.isfile(Opath) and os.path.getsize(Opath):
			return

		try:
			os.makedirs(os.path.dirname(Opath))
		except:
			pass
		writeHere = open(Opath, "w")

		# decrypt the file
		print(sys.stderr, Opath, '                                    \r',)

		# skip empty files instead of throwing out decryption error
		txt = open(Ipath).read()
		if len(txt) == 0:
			writeHere.close()
			return

		try:
			Study, pId = Ipath.split('/')[1:3]
			if smCache[pId] is None:
				smCache[pId] = get_private_key("private/" + Study + "/" + pId)
			private_key = RSA.importKey(smCache[pId])

			file_data = [line for line in txt.splitlines() if line]

			decoded_key = base64.urlsafe_b64decode(file_data[0].encode("utf-8"))
			decrypted_key = base64.urlsafe_b64decode(private_key.decrypt(decoded_key))

			for data in file_data[1:]:
				iv, data = data.split(":")
				iv = base64.urlsafe_b64decode(iv.encode("utf-8"))
				data = base64.urlsafe_b64decode(data.encode("utf-8"))
				decrypted = AES.new(decrypted_key, mode=AES.MODE_CBC, IV=iv).decrypt(data)
				returning = remove_PKCS5_padding(decrypted)
				if returning == '':
					continue
				if not returning.endswith('\n'):
					returning += '\n'
				writeHere.write(returning)
			writeHere.close()

		except:
			print("%sError: decryption failed on file %s%s%s"%(TC.BLR, TC.BLY, Ipath, TC.END), file=sys.stderr)


if __name__=='__main__':
	parser = argparse.ArgumentParser(usage='$0 [source_path] [target_path] [options] 2>progress', description='decrypt files locally')
	parser.add_argument('ipath', default='encrypted', help='positional argument', nargs='?')
	parser.add_argument('opath', default='decrypted', help='positional argument', nargs='?')
	parser.add_argument('--extension', '-e', default='.raw', help='valid source file extension')
	parser.add_argument('--exclude', '-x', default=[], type=str, help='exclude files if fullpath string contains', action='append')
	parser.add_argument('--force', '-f', help='force decrypt and overwrite existing files', action='store_true')
	#nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt=parser.parse_args()
	globals().update(vars(opt))

	proc(ipath, opath)
	print(sys.stderr)

