#!/usr/bin/env bash

mkdir -p encrypted

if [ "$SECRET" ]; then
	source $SECRET
fi

arg="$1"

if [ ! "$arg" ] || [[ "$arg" =~ 1 ]]; then
	mkdir -p encrypted
	rm encrypted/$STUDY_ID/*

	echo "Syncing HOPES-Beiwe files ..."
	aws s3 sync s3://$BUCKET_PREFIX-beiwe/$STUDY_ID encrypted/$STUDY_ID
fi

if [ ! "$arg" ] || [[ "$arg" =~ 2 ]]; then
	mkdir -p encrypted-fitbit
	echo "Syncing HOPES-Fitbit files ..."
	aws s3 sync s3://$BUCKET_PREFIX-fitbit/$STUDY_ID encrypted-fitbit/$STUDY_ID
fi

if [ ! "$arg" ] || [[ "$arg" =~ 3 ]]; then
	echo "Decrypting HOPES-Beiwe files ..."
	~/anaconda3/bin/python ./decryptor-local.py -x bluetoothState encrypted/$STUDY_ID decrypted/$STUDY_ID
fi

if [ ! "$arg" ] || [[ "$arg" =~ 4 ]]; then
	echo "Decrypting HOPES-Fitbit files ..."
	~/anaconda3/bin/python ./decryptor-fitbit-local.py encrypted-fitbit/$STUDY_ID decrypted/$STUDY_ID
fi

echo "All done!"

