import json
import os, base64, gzip, random, secrets, time, re, tempfile, tarfile
from csv import writer
from io import BytesIO
from re import sub
from zipfile import ZipFile

import boto3, qrcode
import pandas as pd
import numpy as np
from Crypto.PublicKey import RSA
from flask import *
from jinja2 import Template

from config.constants import *
from config.settings import *
from database.study_models import Study
from database.user_models import Participant
from libs.admin_authentication import *
from libs.s3 import s3_upload
from libs.secrets_manager import create_client_key_pair, get_client_key_pair
from libs.streaming_bytes_io import StreamingBytesIO

participant_administration = Blueprint('participant_administration', __name__)

tm = Template(open('./participant-form/with-keys.html', 'r').read())


@participant_administration.route('/reset_participant_password', methods = ["POST"])
@authenticate_admin_study_access
def reset_participant_password():
	""" Takes a patient ID and resets its password. Returns the new random password."""
	patient_id = request.values['patient_id']
	study_id = request.values['study_id']
	participant_set = Participant.objects.filter(patient_id = patient_id)
	if participant_set.exists() and str(participant_set.values_list('study', flat = True).get()) == study_id:
		participant = participant_set.get()
		participant.reset_password()
		new_password = participant.password
		flash('Patient {:s}\'s password has been reset to {:s}.'.format(patient_id, new_password), 'success')
	else:
		flash('Sorry, something went wrong when trying to reset the patient\'s password.', 'danger')

	if not isPatientRegistered(study_id, patient_id):
		return make_QR(study_id, patient_id, new_password, session["timezone"])

	return redirect('/view_study/{:s}'.format(study_id))


@participant_administration.route('/reset_device', methods = ["POST"])
@authenticate_admin_study_access
def reset_device():
	"""
	Resets a participant's device. The participant will not be able to connect until they register a new device.
	"""
	patient_id = request.values['patient_id']
	study_id = request.values['study_id']
	participant_set = Participant.objects.filter(patient_id = patient_id)
	if participant_set.exists() and str(participant_set.values_list('study', flat = True).get()) == study_id:
		participant = participant_set.get()
		participant.clear_device()
		flash('For patient {:s}, device was reset; password is untouched.'.format(patient_id), 'success')
	else:
		flash('Sorry, something went wrong when trying to reset the patient\'s device.', 'danger')

	return redirect('/view_study/{:s}'.format(study_id))


@participant_administration.route('/set_remarks', methods = ["POST"])
@authenticate_admin_study_access
def set_remarks():
	""" Set remarks for a patient. """
	study_id = request.values['study_id']
	patient_id = request.values['patient_id']
	remarks = request.values['remarks']
	participant_set = Participant.objects.filter(patient_id = patient_id)
	if participant_set.exists() and str(participant_set.values_list('study', flat = True).get()) == study_id:
		participant = participant_set.get()
		participant.set_remarks(remarks)
		flash('The remarks on Patient %s is set successfully!' % patient_id, 'success')
	else:
		flash('Internal error: failed to set remarks for Patient %s' % patient_id, 'danger')

	return redirect('/view_study/{:s}'.format(study_id))


@participant_administration.route('/get_completion_report/<string:study_id>/<string:patient_id>', methods = ["GET"])
@authenticate_admin_study_access
def get_completion_report(study_id = None, patient_id = None):
	# Do something with this last update timestamp to avoid sending duplicates
	participant_set = Participant.objects.filter(patient_id = patient_id)
	if not participant_set.exists() or str(participant_set.values_list('study', flat = True).get()) != study_id:
		Response('Error: failed to get upload details for Patient %s' % patient_id, mimetype = 'text/plain')
	user = participant_set.get()
	html = generate_data_completion_report(user, user.study)
	return html

@participant_administration.route('/check_upload_details/<string:study_id>/<string:patient_id>', methods = ["GET"])
@authenticate_admin_study_access
def check_upload_details(study_id = None, patient_id = None):
	""" Get patient data upload details """
	participant_set = Participant.objects.filter(patient_id = patient_id)
	if not participant_set.exists() or str(participant_set.values_list('study', flat = True).get()) != study_id:
		Response('Error: failed to get upload details for Patient %s' % patient_id, mimetype = 'text/plain')
	user = participant_set.get()
	df = user.get_upload_info()

	return render_template_string('{{ df.to_html()|safe }}', **locals(), patient = user)


@participant_administration.route('/get_state_var/<string:study_id>/<string:patient_id>', methods = ["GET"])
@authenticate_admin_study_access
def get_state_var(study_id = None, patient_id = None):
	""" Get patient data upload details """
	participant_set = Participant.objects.filter(patient_id = patient_id)
	if not participant_set.exists() or str(participant_set.values_list('study', flat = True).get()) != study_id:
		Response('Error: failed to get upload details for Patient %s' % patient_id, mimetype = 'text/plain')
	user = participant_set.get()

	return user.state_variable or '{}'


@participant_administration.route('/create_defined_patient', methods = ["POST"])
@authenticate_admin_study_access
def create_defined_patient():
	study_id = request.values['study_id']
	patient_id, password, participant = Participant.create_with_password(study_id = study_id)
	participant.set_patientid(request.values['participant_id'])
	participant.set_password(request.values['set_password'])

	study_object_id = Study.objects.filter(pk = study_id).values_list('object_id', flat = True).get()
	s3_upload(request.values['participant_id'], "", study_object_id)
	create_client_key_pair(request.values['participant_id'], study_object_id)
	return request.values['participant_id']


@participant_administration.route('/create_new_patient', methods = ["POST"])
@authenticate_admin_study_access
def create_new_patient():
	"""
	Creates a new user, generates a password and keys, pushes data to s3 and user database, adds user to
	the study they are supposed to be attached to and returns a string containing password and patient id.
	"""

	study_pk = request.values['study_id']
	patient_id, password, user = Participant.create_with_password(patient_name = request.values['email_pattern'], study_id = study_pk)

	# Create an empty file on S3 indicating that this user exists
	study_object_id = Study.objects.get(pk = study_pk).object_id
	s3_upload(patient_id, "", study_object_id)
	# create_client_key_pair(patient_id, study_object_id)

	flash('Created User `%s` with userID: %s, password: %s' % (user.name, patient_id, user.password), 'success')
	return make_QR(study_pk, patient_id, user.password, timezone = session["timezone"])


@participant_administration.route('/check_new_patient/<string:study_id>/<string:patient_id>', methods = ["GET"])
@authenticate_admin_study_access
def check_new_patient(study_id = None, patient_id = None):
	return Response('yes' if isPatientRegistered(study_id, patient_id) else 'no', mimetype = 'text/plain')


def create_archive(tar_fp, data):
	TMPDIR = tempfile.TemporaryDirectory(dir=tempfile.gettempdir())

	# write the .csv file containing all participants registration info
	csv_fn = f'all_{len(data)}_users.csv'
	fp = open(TMPDIR.name+'/'+csv_fn, 'w', encoding='utf-8')
	filewriter = writer(fp)
	filewriter.writerow(['Patient ID', 'Registration password'])
	for patient_id, password, user in data:
		filewriter.writerow([patient_id, password])
	fp.close()

	# write individual QR codes
	with tarfile.open(fileobj = tar_fp, mode='w:gz') as tar:
		tar.add(TMPDIR.name+'/'+csv_fn, arcname = csv_fn)

		for ii, (patient_id, password, user) in enumerate(data):
			bn = f'{user.name}.png'
			image = qrcode.make('{"url":"%s", "uid":"%s", "utp":"%s"}' % (SERVER_URL, patient_id, password))
			image.save(TMPDIR.name+'/'+bn, format = "PNG")
			tar.add(TMPDIR.name+'/'+bn, arcname = bn)

	# delete the temp directory
	TMPDIR.cleanup()


@participant_administration.route('/create_many_patients/<string:study_id>', methods = ["POST"])
@authenticate_admin_study_access
def create_many_patients(study_id = None):
	""" Creates a number of new users at once for a study.  Generates a password and keys for
	each one, pushes data to S3 and the user database, adds users to the study they're supposed
	to be attached to, and returns a CSV file for download with a mapping of Patient IDs and passwords. """

	number_of_new_patients = int(request.form.get('number_of_new_patients', 0))
	patient_name_template = request.form.get('patient_name_template', '')
	use_random = not bool(patient_name_template)
	if '#' not in patient_name_template:
		patient_name_template += ('_'+'#'*len(str(number_of_new_patients-1)))
	match = re.search('#+', patient_name_template).group()
	name_template = re.sub(match, f'%0{len(match)}d', patient_name_template)

	# create N patients
	study_object_id = Study.objects.get(pk = study_id).object_id
	data = []
	for i in range(number_of_new_patients):
		patient_id, password, user = Participant.create_with_password(study_id = study_id)\
			if use_random else Participant.create_with_password(patient_name = name_template % i, study_id = study_id)

		# Create an empty file on S3 indicating that this user exists
		s3_upload(patient_id, "", study_object_id)
		data += [(patient_id, user.password, user)]

	tar_data = BytesIO()
	create_archive(tar_data, data)
	tar_data.flush()
	tar_data.seek(0)

	return send_file(tar_data, attachment_filename=f'new_patients_{number_of_new_patients}.tar.gz', mimetype = 'application/x-gzip', as_attachment=True)


def user_generator(study_id, participant):
	study = Study.objects.get(pk = study_id)
	study_object_id, study_name = study.object_id, study.name
	# generate fitbit and additional metadata
	fitbit_id = participant.patient_id.split('_')[0]
	password_fitbit = (secrets.token_hex(8)[:16]).upper()

	last_three = fitbit_id.split('@')[0].split('.')
	if len(last_three) < 3:
		last_three = (secrets.token_hex(2)[:3]).upper()
	else:
		last_three = last_three[2]

	public_key = get_client_key_pair('public', participant.patient_id, study_object_id)

	password_new = (secrets.token_hex(6)[:12]).upper()
	password_next = (secrets.token_hex(6)[:12]).upper()
	password_final = (secrets.token_hex(6)[:12]).upper()

	participant.set_password(password_new)

	hashKey = base64.b64encode(os.urandom(64))
	hashIteration = random.randint(40000, 60000)
	latitudeOffset = random.random() * 1000 - 500
	longitudeOffset = random.random() * 1000 - 500

	# Get AWS clients
	sm = boto3.client('secretsmanager',
	                  region_name = SECRETS_MANAGER_REGION_NAME,
	                  aws_access_key_id = SECRETS_MANAGER_ACCESS_CREDENTIALS_USER,
	                  aws_secret_access_key = SECRETS_MANAGER_ACCESS_CREDENTIALS_KEY)

	acm = boto3.client('acm',
	                   region_name = SECRETS_MANAGER_REGION_NAME,
	                   aws_access_key_id = SECRETS_MANAGER_ACCESS_CREDENTIALS_USER,
	                   aws_secret_access_key = SECRETS_MANAGER_ACCESS_CREDENTIALS_KEY)

	# Update fitbit
	public_key['fitbitId'] = fitbit_id
	public_key['fitbitPassword'] = password_fitbit
	sm.update_secret(
		SecretId = 'public/' + study_object_id + '/' + participant.patient_id,
		SecretString = json.dumps(public_key)
	)

	# create qr 1
	qr1 = qrcode.make(
		'{"url":"%s", "uid":"%s", "utp":"%s", "unp":"%s", "step": %i, "longitudeOffset": %f, "latitudeOffset": %f, "hashKey": "%s", "hashIteration": %i}' %
		(DOMAIN_NAME, participant.patient_id, password_new, password_next, 0, longitudeOffset, latitudeOffset, hashKey,
		 hashIteration))
	buffered = BytesIO()
	qr1.save(buffered, format = "PNG")
	qr1_str = base64.b64encode(buffered.getvalue())
	qr1 = "data:image/jpeg;base64," + qr1_str.decode()

	# create qr 2
	qr2 = qrcode.make(
		'{"url":"%s", "uid":"%s", "utp":"%s", "unp":"%s", "step": %i, "longitudeOffset": %f, "latitudeOffset": %f, "hashKey": "%s", "hashIteration": %i}' %
		(
		DOMAIN_NAME, participant.patient_id, password_next, password_final, 1, longitudeOffset, latitudeOffset, hashKey,
		hashIteration))
	buffered = BytesIO()
	qr2.save(buffered, format = "PNG")
	qr2_str = base64.b64encode(buffered.getvalue())
	qr2 = "data:image/jpeg;base64," + qr2_str.decode()

	'''
	Previously the certificates and items are separated into individual QR codes,
	resulting in some QR codes containing much more data than others.
	It then became too hard to scan QR with high density of data.
	Thus, the new solution proposes to concatenate all Step 2 to Step 5 together
	before running a compression algorithm, gzip over it, before subdividing into 5 QR codes
	to reduce the density of each QR code. Also the first QR code in Step is bigger, hence,
	I am putting more data into it instead.

	The current solution recombines all the new QR code for the mutual certificate part, before
	uncompress it, and then run all the processing on the respective data

	The naming convention of the QR code in the HTML is not changed much, so that it can be changed
	back easily when there is a need to. And an additional QR code (step 6) is added to the mix.

	Previous Steps and their Respective Data:
	Step 2: Registration - Study Root of Trust
	Step 3: Registration - Participant Identifier
	Step 4: Registration - Participant Login key
	Step 5: Registration - Participant Encryption Key
	'''

	# Perform the CA requisition
	domain_name = participant.patient_id.split('_')[1] + '.' + study_object_id + '.study.mohtgroup.com';
	ca = acm.request_certificate(DomainName = domain_name, CertificateAuthorityArn = PCA_ARN)
	time.sleep(7)
	ca = acm.export_certificate(CertificateArn = ca['CertificateArn'],
	                            Passphrase = bytes(participant.patient_id, 'utf8'))
	key = RSA.import_key(ca['PrivateKey'], participant.patient_id)

	# Concatenating the different steps data together
	different_steps_data = [
		open('participant-form/external.pem', 'r').read(),
		ca['Certificate'],
		key.exportKey(format = 'PEM', pkcs = 1).decode(),
		public_key['public']
	]
	uncompressed_str = '|'.join(different_steps_data)
	replace_items = ['-----BEGIN CERTIFICATE-----\n', '\n-----END CERTIFICATE-----',
	                 '-----BEGIN RSA PRIVATE KEY-----\n', '\n-----END RSA PRIVATE KEY-----',
	                 '-----BEGIN PUBLIC KEY-----\n', '\n-----END PUBLIC KEY-----']
	for replace_item in replace_items:
		uncompressed_str = uncompressed_str.replace(replace_item, '')

	# Run a gzip compression over it
	compressed_str = (base64.b64encode(gzip.compress(uncompressed_str.encode()))).decode('utf-8')

	# Calculate the data distribution amount across the given QR codes available
	DIVIDE_AMOUNT = 5
	TOTAL_AMOUNT = len(compressed_str)
	DATA_DISTRIBUTION = [1] * DIVIDE_AMOUNT
	DATA_DISTRIBUTION[0] *= 1.3  # First QR code is slightly larger, and should hold more data
	SUM_DATA = sum(DATA_DISTRIBUTION)
	DATA_DISTRIBUTION = [int(x / SUM_DATA * TOTAL_AMOUNT) for x in DATA_DISTRIBUTION]
	for x in range(1, DIVIDE_AMOUNT):
		DATA_DISTRIBUTION[x] += DATA_DISTRIBUTION[x - 1]
	DATA_DISTRIBUTION.insert(0, 0)
	DATA_DISTRIBUTION[-1] = TOTAL_AMOUNT

	# Generate the compressed QR codes
	compressed_data_qr_codes = []
	for i in range(DIVIDE_AMOUNT):
		compressed_qr = qrcode.make('{"step":"%i", "body": "%s"}' %
		                            (i + 2, compressed_str[DATA_DISTRIBUTION[i]:DATA_DISTRIBUTION[i + 1]]))
		buffered = BytesIO()
		compressed_qr.save(buffered, format = "PNG")
		compressed_qr_str = base64.b64encode(buffered.getvalue())
		compressed_qr = "data:image/jpeg;base64," + compressed_qr_str.decode()
		compressed_data_qr_codes.append(compressed_qr)

	msg = tm.render(studyName = study_name, studyId = study_object_id, hopesURL = DOMAIN_NAME, fitbitId = fitbit_id,
	                fitbitPassword = password_fitbit, hopesPassword = password_new, lastThree = last_three,
	                hopesId = participant.patient_id, qrCode = qr1, qrReregister = qr2,
	                chain = compressed_data_qr_codes[0], cert = compressed_data_qr_codes[1],
	                key = compressed_data_qr_codes[2], public = compressed_data_qr_codes[3],
	                step6 = compressed_data_qr_codes[4])

	f = open(participant.patient_id + ".html", 'w+')
	f.write(msg)
	f.close()
	f = open(participant.patient_id + ".html", 'r')
	ret = f.read()
	f.close()
	os.remove(participant.patient_id + ".html")
	return ret


def make_QR(study_pk, patient_id, password, timezone = 0):
	image = qrcode.make('{"url":"%s", "uid":"%s", "utp":"%s"}' % (SERVER_URL, patient_id, password))
	buffered = BytesIO()
	image.save(buffered, format = "PNG")
	img_str = base64.b64encode(buffered.getvalue()).decode('utf-8', 'ignore')
	img_base64 = "data:image/jpeg;base64," + img_str

	study = Study.objects.get(pk = study_pk)
	tracking_survey_ids = study.get_survey_basic_info('tracking_survey')
	audio_survey_ids = study.get_survey_basic_info('audio_survey')
	participants = study.participants.all()

	return render_template(
		'view_study.html',
		study = study,
		TZ = timezone,
		qr_image = img_base64,
		check_id = patient_id,
		patients = participants,
		audio_survey_ids = audio_survey_ids,
		tracking_survey_ids = tracking_survey_ids,
		allowed_studies = get_admins_allowed_studies(),
		system_admin = admin_is_system_admin()
	)


def isPatientRegistered(study_id, patient_id):
	"""
	Check whether a user with patient_id in study_id has registered, returns yes or no.
	"""
	try:
		participant = Participant.objects.get(patient_id = patient_id)
		isReg = (participant.device_id and str(participant.study_id) == study_id)
	except:
		isReg = False
	return isReg


def generate_data_completion_report(user: Participant, study: Study):
	# Do something with this last update timestamp to avoid sending duplicates
	def print_dataframe(df: pd.DataFrame):
		if type(df.index) == pd.DatetimeIndex:
			df = df.set_index(df.index.map(lambda x: str(x.date())))
		if not df.index.name:
			return df.to_html()
		return df.to_html(index_names = False).replace('<th></th>', f'<th>{df.index.name}</th>', 1)

	def month2list(s: pd.Series):
		ret = pd.Series(['NaN']*s.index[0].days_in_month)
		ret[s.index[0].day-1 : s.index[-1].day] = s
		return ret.to_list()

	def calc_daily_chart(df: pd.Series):
		ret = df.groupby(pd.Grouper(freq = 'M')).apply(month2list)
		return {str(k.date())[:-3]: v for k, v in ret.items()}

	settings = study.get_study_and_device_settings()
	df_raw = user.get_upload_info()
	df = df_raw.groupby(pd.Grouper(freq='D')).mean().fillna(0).astype(int)
	df.columns = map(lambda x:SHORT_NAME_INV_MAPPING[x], df_raw.columns)
	df = df[[c for c in df.columns if settings.get(c, False)]]

	fml = settings['data_completion_eval_formulae']
	exec(DEFAULT_DATA_COMPLETION_EVALUATION if fml == 'default' else fml)

	report_template = DEFAULT_DATA_COMPLETION_REPORT if study.data_completion_report_template == 'default' else study.data_completion_report_template

	daily_chart = calc_daily_chart(locals().get('df_report')['Average'])

	return render_template_string(report_template, **locals())

