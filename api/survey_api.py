import hashlib
import json
import os

from flask import *
from libs.admin_authentication import *
from libs.json_logic import do_validate_survey
from config.settings import DOMAIN_NAME
from database.models import Survey, Participant
from firebase_admin import messaging

survey_api = Blueprint('survey_api', __name__)


################################################################################
############################## Creation/Deletion ###############################
################################################################################
@survey_api.route('/create_survey/<string:study_id>/<string:survey_type>', methods = ['GET', 'POST'])
@authenticate_admin_study_access
def create_survey(study_id = None, survey_type = 'tracking_survey'):
	new_survey = Survey.create_with_settings(study_id = study_id, survey_type = survey_type)
	return redirect('/edit_survey/{:d}'.format(new_survey.id))


@survey_api.route('/delete_survey/<string:survey_id>', methods = ['GET', 'POST'])
@authenticate_admin_study_access
def delete_survey(survey_id = None):
	try:
		survey = Survey.objects.get(pk = survey_id)
	except Survey.DoesNotExist:
		return abort(404)

	study_id = survey.study_id
	survey.mark_deleted()
	return redirect('/view_study/{:d}'.format(study_id))


################################################################################
############################# Setters and Editors ##############################
################################################################################
@survey_api.route('/update_survey/<string:survey_id>', methods = ['GET', 'POST'])
@authenticate_admin_study_access
def update_survey(survey_id = None):
	try:
		survey = Survey.objects.get(pk = survey_id)
	except Survey.DoesNotExist:
		return abort(404)

	content = json.loads(request.values['content'])
	content = justify_question_json(content)

	if survey.survey_type == Survey.TRACKING_SURVEY:
		errors = do_validate_survey(content)
		if len(errors) > 1:
			return make_response(json.dumps(errors), 400)

	# These three all stay JSON when added to survey
	content = json.dumps(content)  # type: str
	timings = request.values['timings']
	expiry = request.values['expiry']
	settings = request.values['settings']
	target_users = request.values['target_users'].strip()
	name = request.values['name']
	if target_users.startswith('@'):    # push-stack surveys must be of push-type as well
		timings = '[[],[],[],[],[],[],[]]'
		settings = json.loads(settings)
		settings['trigger_on_first_download'] = False
		settings = json.dumps(settings)
	checksum = hashlib.md5(f'{content}{timings}{expiry}{settings}{name}'.encode('utf-8')).hexdigest()
	survey.update(content = content, checksum = checksum, timings = timings, settings = settings, expiry = expiry, target_users = target_users, name = name)

	return make_response("", 201)


def justify_question_json(json_content):
	""" Remove inactive options for shorter JSON
	Turns min/max int values into strings, because the iOS app expects strings. This is for
	backwards compatibility; when all the iOS apps involved in studies can handle ints,
	we can remove this function. """
	for question in json_content:
		if 'display_if' in question and question['display_if'] is None:
			question.pop('display_if')
		if 'save_state_variable' in question and not question['save_state_variable']:
			question.pop('save_state_variable')
		if 'compulsory' in question and not question['compulsory']:
			question.pop('compulsory')
		if 'webview_settings' in question and question['webview_settings'] in ['{}', '']:
			question.pop('webview_settings')
		if 'max' in question:
			question['max'] = str(question['max'])
		if 'min' in question:
			question['min'] = str(question['min'])
	return json_content


@survey_api.route('/edit_survey/<string:survey_id>')
@authenticate_admin_study_access
def render_edit_survey(survey_id = None):
	try:
		survey = Survey.objects.get(pk = survey_id)
	except Survey.DoesNotExist:
		return abort(404)

	study = survey.study
	return render_template(
		'edit_survey.html',
		survey = survey.as_native_python(),
		study = study,
		allowed_studies = get_admins_allowed_studies(),
		system_admin = admin_is_system_admin(),
		domain_name = DOMAIN_NAME,  # used in a Javascript alert, see survey-editor.js
	)


@survey_api.route('/send_fcm', methods = ["POST"])
@authenticate_admin_study_access
def send_fcm():
	try:
		patient_id = request.values['patient_id']
		study_id = request.values['study_id']
		participant_set = Participant.objects.filter(patient_id = patient_id)
		if not participant_set.exists() or str(participant_set.values_list('study', flat = True).get()) != study_id:
			Response('Error: failed to get upload details for Patient %s' % patient_id, mimetype = 'text/plain')
		user = participant_set.get()

		if request.values['type'] == "add-to-push-stack":
			res, msg = send_push_stack_survey(request.values['data1'], patient_id)
			return render_template_string('Success' if res==1 else str(msg))

		message = messaging.Message(
			data = {k: request.values[k] for k in ['type', 'dismissable', 'expiry', 'title', 'content', 'data'] if k in request.values},
			token = user.fcm_token,
			android = messaging.AndroidConfig(priority = "high", ttl = 0),
			apns = messaging.APNSConfig(headers = {"apns-priority": "10"}),
			webpush = messaging.WebpushConfig(headers = {"Urgency": "high"}),
		)
		messaging.send(message)
	except Exception as e:
		return render_template_string(f'Fail: {str(e)}')

	return render_template_string('Success')


def send_fcm_trigger_update(patient_id: str or list or tuple or set):
	if type(patient_id) in [list, set, tuple]:
		return sum([send_fcm_trigger_update(patient_id1)[0] for patient_id1 in patient_id])

	try:
		participant_set = Participant.objects.filter(patient_id = patient_id)
		if not participant_set.exists():
			Response('Error: failed to get upload details for Patient %s' % patient_id, mimetype = 'text/plain')
		user = participant_set.get()

		message = messaging.Message(
			data = {'type': 'update-survey'},
			token = user.fcm_token,
			android = messaging.AndroidConfig(priority = "high", ttl = 0),
			apns = messaging.APNSConfig(headers = {"apns-priority": "10"}),
			webpush = messaging.WebpushConfig(headers = {"Urgency": "high"}),
		)
		res = messaging.send(message)
		return 1, res
	except Exception as e:
		return 0, e


def add_patients_to_survey(patient_id: str or list or tuple or set, survey_id: str, push_immediately = False):
	if type(patient_id) in (list, set, tuple):
		return sum([add_patients_to_survey(pid, survey_id, push_immediately) for pid in patient_id])

	try:
		survey = Survey.objects.get(object_id = survey_id)
		patient = Participant.objects.get(patient_id = patient_id)
	except Exception as e:
		print(str(e), file = sys.stderr)
		return 0

	if not (survey.target_users in ['*', None] or patient_id in survey.target_users):
		survey.target_users += ' ' + patient_id
		survey.save()

	if push_immediately:
		res2 = send_fcm_trigger_update(patient_id)

	return 1


def del_patients_from_survey(patient_id: str or list or tuple or set, survey_id: str):
	if type(patient_id) in (list, set, tuple):
		return sum([del_patients_from_survey(pid, survey_id) for pid in patient_id])

	try:
		survey = Survey.objects.get(object_id = survey_id)
		patient = Participant.objects.get(patient_id = patient_id)
	except Exception as e:
		return 0

	if survey.target_users in ['*', None, 'None']:
		survey.target_users = ' '.join([p.patient_id for p in survey.study.participants.all()])

	if patient_id not in survey.target_users:
		return 0

	survey.target_users = survey.target_users.replace(patient_id, '').replace('  ', ' ').strip()
	survey.save()

	return 1


def send_push_stack_survey(survey_json, patient_id, survey_type = 'tracking_survey', push_immediately = False):
	try:
		patient = Participant.objects.get(patient_id=patient_id)
		obj = json.loads(survey_json) if type(survey_json) is str else survey_json
		new_survey = Survey.create_with_settings(study_id = patient.study_id, survey_type = survey_type)

		target_users = '@'+patient_id
		content = obj['content'] if type(obj['content']) is str else json.dumps(obj['content'])
		timings = '[[],[],[],[],[],[],[]]'
		expiry = obj.get('expiry', '')
		name = obj.get('name', '')
		settings = obj['settings'] if type(obj['settings']) is str else json.dumps(obj['settings'])
		settings = json.loads(settings)
		settings['trigger_on_first_download'] = False
		settings = json.dumps(settings)

		checksum = hashlib.md5(f'{content}{timings}{expiry}{settings}{name}'.encode('utf-8')).hexdigest()
		new_survey.update(content = content, checksum = checksum, timings = timings, settings = settings,
		                  expiry = expiry, target_users = target_users, name = name)
		return send_fcm_trigger_update(patient_id) if push_immediately else (1, 'not pushed through Firebase')
	except Exception as e:
		return 0, str(e)
