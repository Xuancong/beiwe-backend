import os, sys, calendar, time, json
from collections import *
from datetime import datetime, timedelta

import boto3, zlib, builtins
import pandas as pd
from flask import *
from werkzeug.datastructures import FileStorage

from config.constants import *
from api.participant_administration import *
from database.models import Researcher, Study, Participant, Survey
from libs.android_error_reporting import send_android_error_report
from libs.http_utils import determine_os_api
from libs.logging import log_error
from libs.s3 import *
from libs.sentry import make_sentry_client
from libs.user_authentication import *
from libs.email_sender import *
from libs.secrets_manager import create_client_key_pair, get_client_key_pair
from libs.encryption import *
from api import survey_api

############################# GLOBALS... #######################################
mobile_api = Blueprint('mobile_api', __name__)

client = Optionalizer(  boto3.client('cloudwatch',
						aws_access_key_id = os.environ['S3_ACCESS_CREDENTIALS_USER'],
						aws_secret_access_key = os.environ['S3_ACCESS_CREDENTIALS_KEY'],
						region_name = 'ap-southeast-1'))


################################################################################
################################ UPLOADS #######################################
################################################################################
def update_feature_stats(data: str, user: Participant, file_name=None):
	try:
		lines = [its for L in data.splitlines() for its in [L.split(',', 3)] if len(L)>5 and L[:3].islower() and L[3]==',' and its[1].isdigit()]
		if lines:
			fea_list, stmp_list = zip(*[line[:2] for line in lines])
			df = pd.DataFrame({'feature': fea_list}, index = pd.to_datetime(stmp_list, unit='ms', utc=True).tz_convert('tzlocal()'))
			df1 = df.groupby(pd.Grouper(freq='D')).feature.value_counts().unstack()
			dfA_old = user.get_upload_info()
			dfA_new = dfA_old.add(df1[[c for c in dfA_old.columns if c in df1.columns]], fill_value=0)
			user.set_upload_info(dfA_new)
			new_dct = {line[3].split(',', 1)[0]:line[3].split(',', 1)[1] for line in lines if len(line)==4 and line[0]=='svy' and line[2]=='save-state-variable' and ',' in line[3]}
			if new_dct:
				user.update_state_variable(new_dct)
	except Exception as e:
		print(f"Error: update_feature_stats({file_name}) failed: {e}\n{traceback.format_exc()}", file = sys.stderr, flush = True)


@mobile_api.route('/update_fcm', methods = ['POST'])
@determine_os_api
@authenticate_user
def update_fcm(OS_API = ""):
	try:
		patient_id = request.values['patient_id']
		fcm_token = request.values['fcm_token']
		if fcm_token:
			user = Participant.objects.get(patient_id = patient_id)
			user.set_fcm_token(fcm_token)
		return "", 200
	except Exception as e:
		return "", 500


@mobile_api.route('/upload', methods = ['POST'])
@mobile_api.route('/upload/ios/', methods = ['GET', 'POST'])
@determine_os_api
@authenticate_user
# @authenticate_user_ignore_password
def upload(OS_API = ""):
	""" Entry point to upload GPS, Accelerometer, Audio, PowerState, Calls Log, Texts Log,
	Survey Response, and debugging files to s3.

	Behavior:
	The Beiwe app is supposed to delete the uploaded file if it receives an html 200 response.
	The API returns a 200 response when the file has A) been successfully handled, B) the file it
	has been sent is empty, C) the file did not decrypt properly.  We encountered problems in
	production with incorrectly encrypted files (as well as Android generating "rList" files
	under unknown circumstances) and the app then uploads them.  The source of encryption errors
	is not well understood and could not be tracked down.  In order to salvage partial data the
	server decrypts files to the best of its ability and uploads it to S3.  In order to delete
	these files we still send a 200 response.

	(The above about encryption is awful, in a theoretical version 2.0 the 200 response would be
	replaced with a difference response code to allow for better debugging and less/fewer ... hax.)

	A 400 error means there is something is wrong with the uploaded file or its parameters,
	administrators will be emailed regarding this upload, the event will be logged to the apache
	log.  The app should not delete the file, it should try to upload it again at some point.

	If a 500 error occurs that means there is something wrong server side, administrators will be
	emailed and the event will be logged. The app should not delete the file, it should try to
	upload it again at some point.

	Request format:
	send an http post request to [domain name]/upload, remember to include security
	parameters (see user_authentication for documentation). Provide the contents of the file,
	encrypted (see encryption specification) and properly converted to Base64 encoded text,
	as a request parameter entitled "file".
	Provide the file name in a request parameter entitled "file_name". """
	patient_id = request.values['patient_id']
	user = Participant.objects.get(patient_id = patient_id)
	study = user.study

	# Slightly different values for iOS vs Android behavior.
	# Android sends the file data as standard form post parameter (request.values)
	# iOS sends the file as a multipart upload (so ends up in request.files)
	# if neither is found, consider the "body" of the post the file
	# ("body" post is not currently used by any client, only here for completeness)
	if "file" in request.files:
		uploaded_data = request.files['file']
	elif "file" in request.values:
		uploaded_data = request.values['file']
	else:
		uploaded_data = request.data

	if isinstance(uploaded_data, FileStorage):
		uploaded_data = uploaded_data.read()

	if not isinstance(uploaded_data, bytes):
		uploaded_data = uploaded_data.encode('utf-8', 'ignore')

	file_name = patient_id + '/' + request.values['file_name'].replace('_', '/')

	client.put_metric_data(
		Namespace = 'HOPES',
		MetricData = [
			{
				'MetricName': 'UploadReceived',
				'Dimensions': [
					{
						'Name': 'PatientId',
						'Value': patient_id
					},
					{
						'Name': 'File',
						'Value': file_name.split("/")[1]
					},
				],
				'Timestamp': time.time(),
				'Value': 1,
				'Unit': 'Count',
				'StorageResolution': 60
			},
			{
				'MetricName': 'FileUploaded',
				'Dimensions': [
					{
						'Name': 'PatientId',
						'Value': patient_id
					},
					{
						'Name': 'File',
						'Value': file_name.split("/")[1]
					},
				],
				'Timestamp': time.time(),
				'Value': len(uploaded_data),
				'Unit': 'Bytes',
				'StorageResolution': 60
			},
		]
	)

	# print "uploaded file name:", file_name, len(uploaded_file)
	if "crashlog" in file_name.lower():
		send_android_error_report(patient_id, uploaded_data)
		return render_template_string(''), 200

	if file_name[:6] == "rList-":
		return render_template_string(''), 200

	canUpload = s3_upload(file_name, uploaded_data, study.object_id, encrypt = False)

	client.put_metric_data(
		Namespace = 'HOPES',
		MetricData = [
			{
				'MetricName': 'UploadSucceeded',
				'Dimensions': [
					{
						'Name': 'PatientId',
						'Value': patient_id
					},
					{
						'Name': 'File',
						'Value': file_name.split("/")[1]
					},
				],
				'Timestamp': time.time(),
				'Value': 1,
				'Unit': 'Count',
				'StorageResolution': 60
			},
		]
	)

	return_obj = {'filesize': len(uploaded_data), 'checksum': zlib.crc32(uploaded_data)}

	try:
		data = decrypt(study.object_id, patient_id, uploaded_data, file_name.lower().endswith('.gz'))
		if data:
			update_feature_stats(data, user, file_name)
			user.set_upload_time()
	except Exception as e:
		print(f"Error: upload({file_name}) failed: {e}\n{traceback.format_exc()}")

	if int(request.values.get('is_last', 0)):
		return_obj['report_html'] = generate_data_completion_report(user, study)

	return json.dumps(return_obj), 200 if canUpload else 500


def human_readable_timedelta(td_sec):
	t = timedelta(seconds = td_sec)
	hr = int(t.seconds/3600)
	mn = int((t.seconds%3600)/60)
	sec = t.seconds%60
	return ('%d days'%t.days if t.days else '') + ('%d hour'%hr if hr else '')\
	       + ('%d minutes'%mn if mn else '') + ('%d seconds'%sec if sec else '')


def send_email_OTP(email_addr, email_title, email_content):
	smtp_info = {'host': os.getenv('SMTP_HOST'), 'port': int(os.getenv('SMTP_PORT', 25)),
	             'un': os.getenv('SMTP_UN'), 'pw': os.getenv('SMTP_PW')}
	send_email_html(email_content, email_title, os.getenv('SMTP_FROM'), email_addr, smtp_info, '')


### Step-1 self-registration will send an email user_id and an epoch-based OTP
@mobile_api.route('/request_otp', methods = ['POST'])
def request_otp():
	try:
		study_id, patient_id, passhash = [request.values[v] for v in ['study_id', 'patient_id', 'password']]

		# check whether the study_name is valid and the username is in that study's registration_allow_list
		study = Study.objects.filter(name = study_id).get()
		found = False
		for L in study.registration_allow_list.splitlines():
			its = L.split()
			if len(its)==1 and re.match(its[0], patient_id):
				found = True
				break
		if not found:
			return render_template_string(''), 406

		# check whether epoch hash is correct
		if not verify_epoch_hash(passhash):
			return render_template_string(''), 401

		# if patient_id is an email address, check whether the OTP has been issued within 24 hours
		if is_email_string(patient_id):
			if not check_update_OTP(study_id+' '+patient_id, study.otp_expire_in_sec):
				return render_template_string(''), 425

		try:
			OTP = generate_OTP()
			# send_email_OTP(patient_id, study.otp_email_title, study.otp_email_content%(OTP, human_readable_timedelta(study.otp_expire_in_sec)))
			send_email_OTP(patient_id, study.otp_email_title,
			               study.otp_email_content.replace('<OTP>', OTP).replace('<DURATION>',
			                human_readable_timedelta(study.otp_expire_in_sec)))
		except Exception as e:
			print('Error in request_otp: '+e, file=sys.stderr, flush=True)
			return render_template_string(''), 417

		OTPtracking.set_OTP(study_id+' '+patient_id, OTP)
		return render_template_string(''), 200
	except:
		return abort(400)


### Step-2 self-registration will send both username and OTP
@mobile_api.route('/self_register', methods = ['GET', 'POST'])
@mobile_api.route('/self_register/ios/', methods = ['GET', 'POST'])
@determine_os_api
def self_register(OS_API = ""):
	try:
		user_name = request.values['patient_id']
		study_name = request.values['study_id']
		passhash = request.values['password']
		phone_number = request.values['phone_number']
		device_id = request.values['device_id']
		device_os = request.values.get('device_os', 'none')
		os_version = request.values.get('os_version', 'none')
		product = request.values.get("product", 'none')
		brand = request.values.get("brand", 'none')
		hardware_id = request.values.get("hardware_id", 'none')
		fcm_token = request.values.get("fcm_token", '')
		manufacturer = request.values.get("manufacturer", 'none')
		model = request.values.get("model", 'none')
		beiwe_version = request.values.get("beiwe_version", 'none')
		mac_address = request.values.get('bluetooth_id', 'none')

		registration_method = 0
		if study_name == '*':
			# 1. registration by QR scan or raw-patient_id-and-password-pair
			user = Participant.objects.get(patient_id = user_name)
			if not verify_epoch_hash(passhash, user.password) and not user.validate_password(passhash):
				return render_template_string(''), 401
			user_name = user.name
			study = user.study
			study_name = study.name
			registration_method = 1

		else:
			study = Study.objects.get(name=study_name)

			# verify OTP
			userpass = study.reg_allow_list_get_userpass(user_name)
			if userpass:
				# 2. registration by username-password pair in allow-list
				if not verify_epoch_hash(passhash, userpass):
					return render_template_string(''), 406
				registration_method = 2
			else:
				# 3. registration by username-OTP pair with username in allow-list
				if OTPtracking.is_OTP_expired(study_name+' '+user_name, study.otp_expire_in_sec):
					return render_template_string(''), 408
				elif not verify_epoch_hash(passhash, OTPtracking.get_OTP(study_name+' '+user_name)):
					return render_template_string(''), 406
				registration_method = 3

			# if the user exists, re-generate key pair, otherwise create new user with new key pair
			user = Participant.objects.filter(name = user_name, study_id = study.id)
			if not user.exists():
				user = Participant.create_with_password(patient_name=user_name, study_id=study.pk)[2]
			else:
				user = user.get()

		if user.device_id and user.device_id != request.values['device_id']:
			# CASE: if a user is registering the same account using a different device,
			# - for Email OTP-based registration, allow it to pass through directly
			# - for QR registration or fixed username-password-pair registration, admin must click the de-register button on the admin console
			#   unless in the study settings allow_same_qr_diff_phone_rereg is enabled
			if registration_method != 3 and not study.allow_same_qr_diff_phone_rereg:
				return render_template_string(''), 405

		if user.os_type and user.os_type != OS_API:
			# CASE: this patient has registered, but the user was previously registered with a
			# different device type. To keep the CSV munging code sane and data consistent (don't
			# cross the iOS and Android data streams!) we disallow it.
			return render_template_string(''), 409

		# At this point the device has been checked for validity and will be registered successfully.
		# Any errors after this point will be server errors and return 500 codes. the final return
		# will be the encryption key associated with this user.
		unix_time = str(calendar.timegm(time.gmtime()))
		public_key, private_key = create_client_key_pair(user.patient_id, study.object_id, unix_time)
		study_id = user.study.object_id

		# Upload the user's various identifiers.
		file_name = user.patient_id + '/identifiers_' + unix_time + ".csv"

		# Construct a manual csv of the device attributes
		file_contents = (DEVICE_IDENTIFIERS_HEADER + "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" %
		                 (user.patient_id, study_name, mac_address, phone_number, device_id, device_os,
		                  os_version, product, brand, hardware_id, manufacturer, model, beiwe_version))
		s3_upload(file_name, file_contents, study_id)
		# FileToProcess.append_file_for_processing(file_name, user.study.object_id, participant = user)

		# set up device.
		user.set_register_time()
		if fcm_token:
			user.set_fcm_token(fcm_token)
		user.set_device(device_id)
		user.set_os_type(OS_API)
		user.set_os_desc(OS_API + ' ' + os_version + ' ' + manufacturer + ' ' + model)
		device_settings = study.device_settings.as_native_python()

		try:
			device_settings['mainpage_title'] = eval(device_settings['mainpage_title'], builtins.globals(), locals())
		except:
			pass

		return_obj = {  'user_id': user.patient_id,     # for upload login identifier
						'password': user.password,      # for upload login verification
		                'created_on': round(user.created_on.timestamp()*1000000),   # to generate random GPS secrets
		                'device_settings': device_settings,
						'encrypt_key': prepare_X509_key_for_java(public_key) if ENCRYPT_METHOD == RSA_METHOD else public_key
						}

		if registration_method == 3:
			OTPtracking.delete(study_name+' '+user_name)

		if registration_method == 2 and not study.allow_same_qr_same_phone_rereg:
			study.reg_allow_list_set_userpass(user_name, generate_OTP(8))

		return json.dumps(return_obj), 200

	except Exception as e:
		return abort(400)


@mobile_api.route('/register_user', methods = ['GET', 'POST'])
@mobile_api.route('/register_user/ios/', methods = ['GET', 'POST'])
@determine_os_api
def register_user(OS_API = ""):
	""" Checks that the patient id has been granted, and that there is no device registered with
	that id.  If the patient id has no device registered it registers this device and logs the
	bluetooth mac address.
	Check the documentation in user_authentication to ensure you have provided the proper credentials.
	Returns the encryption key for this patient/user. """

	# CASE: If the id and password combination do not match, the decorator returns a 403 error.
	# the following parameter values are required.
	try:
		patient_id = request.values['patient_id']
		study_name = request.values['study_id']
		phone_number = request.values['phone_number']
		device_id = request.values['device_id']
		device_os = request.values.get('device_os', 'none')
		os_version = request.values.get('os_version', 'none')
		product = request.values.get("product", 'none')
		brand = request.values.get("brand", 'none')
		hardware_id = request.values.get("hardware_id", 'none')
		manufacturer = request.values.get("manufacturer", 'none')
		model = request.values.get("model", 'none')
		beiwe_version = request.values.get("beiwe_version", 'none')
		mac_address = request.values.get('bluetooth_id', 'none')
	except:
		abort(400)

	user = Participant.objects.get(patient_id = patient_id)
	study_id = user.study.object_id

	if user.device_id and user.device_id != request.values['device_id']:
		# CASE: this patient has a registered a device already and it does not match this device.
		#   They need to contact the study and unregister their their other device.  The device
		#   will receive a 405 error and should alert the user accordingly.
		# Provided a user does not completely reset their device (which resets the device's
		# unique identifier) they user CAN reregister an existing device, the unlock key they
		# need to enter to at registration is their old password.
		# KG: 405 is good for IOS and Android, no need to check OS_API
		return abort(405)

	if user.os_type and user.os_type != OS_API:
		# CASE: this patient has registered, but the user was previously registered with a
		# different device type. To keep the CSV munging code sane and data consistent (don't
		# cross the iOS and Android data streams!) we disallow it.
		return abort(400)

	# At this point the device has been checked for validity and will be registered successfully.
	# Any errors after this point will be server errors and return 500 codes. the final return
	# will be the encryption key associated with this user.

	# Upload the user's various identifiers.
	unix_time = str(calendar.timegm(time.gmtime()))
	file_name = patient_id + '/identifiers_' + unix_time + ".csv"

	# Construct a manual csv of the device attributes
	file_contents = (DEVICE_IDENTIFIERS_HEADER + "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" %
	                 (patient_id, study_name, mac_address, phone_number, device_id, device_os,
	                  os_version, product, brand, hardware_id, manufacturer, model, beiwe_version))
	# print(file_contents + "\n")
	s3_upload(file_name, file_contents, study_id)
	# FileToProcess.append_file_for_processing(file_name, user.study.object_id, participant = user)

	# set up device.
	user.set_register_time()
	user.set_device(device_id)
	user.set_os_type(OS_API)
	user.set_os_desc(OS_API + ' ' + os_version + ' ' + manufacturer + ' ' + model)
	device_settings = user.study.device_settings.as_native_python()
	device_settings.pop('_id', None)
	return_obj = {'client_public_key': get_client_public_key_string(patient_id, study_id), 'device_settings': device_settings}

	client.put_metric_data(
		Namespace = 'HOPES',
		MetricData = [
			{
				'MetricName': 'RegistrationSucceeded',
				'Dimensions': [
					{
						'Name': 'PatientId',
						'Value': patient_id
					},
					{
						'Name': 'DeviceId',
						'Value': device_id
					},
					{
						'Name': 'DeviceOS',
						'Value': device_os
					},
					{
						'Name': 'OSVersion',
						'Value': os_version
					},
					{
						'Name': 'Model',
						'Value': model
					},
					{
						'Name': 'Manufacturer',
						'Value': manufacturer
					},
				],
				'Timestamp': time.time(),
				'Value': 1,
				'Unit': 'Count',
				'StorageResolution': 60
			},
		]
	)

	return json.dumps(return_obj), 200


################################################################################
############################### USER FUNCTIONS #################################
################################################################################

@mobile_api.route('/set_password', methods = ['GET', 'POST'])
@mobile_api.route('/set_password/ios/', methods = ['GET', 'POST'])
@determine_os_api
@authenticate_user
def set_password(OS_API = ""):
	""" After authenticating a user, sets the new password and returns 200.
	Provide the new password in a parameter named "new_password"."""
	participant = Participant.objects.get(patient_id = request.values['patient_id'])
	participant.set_password(request.values["new_password"])
	return render_template_string(''), 200


################################################################################
########################## FILE NAME FUNCTIONALITY #############################
################################################################################


def grab_file_extension(file_name):
	""" grabs the chunk of text after the final period. """
	return file_name.rsplit('.', 1)[1]


def contains_valid_extension(file_name):
	""" Checks if string has a recognized file extension, this is not necessarily limited to 4 characters. """
	return '.' in file_name and grab_file_extension(file_name) in ALLOWED_EXTENSIONS


################################################################################
################################# Download #####################################
################################################################################

orig_str_encode = json.encoder.encode_basestring_ascii
def new_str_encode(obj):
	if len(obj) > 1024:
		return "'%d %s'" % (len(obj), obj)
	return orig_str_encode(obj)

def GS_JSON_encode(obj):
	json.encoder.encode_basestring_ascii = new_str_encode
	ret = json.dumps(obj)
	json.encoder.encode_basestring_ascii = orig_str_encode
	return ret

def is_push_survey(survey):
	return survey['settings']['trigger_on_first_download']==False and not [j for i in survey['timings'] for j in i]

@mobile_api.route('/download_surveys', methods = ['GET', 'POST'])
@mobile_api.route('/download_surveys/ios/', methods = ['GET', 'POST'])
@determine_os_api
@authenticate_user
def get_latest_surveys(OS_API = ""):
	try:
		checksums = json.loads(request.values.get('checksums'))
	except:
		checksums = {}
	participant = Participant.objects.get(patient_id=request.values['patient_id'])
	study = participant.study
	surveys = study.get_surveys_for_study()
	ret_surveys = []
	push_surveys = []
	push_stacks = []
	for survey in surveys:
		if survey['target_users'] == '@'+participant.patient_id:
			push_stacks += [survey['_id']]
		else:
			if not (survey['target_users'] in ['*', None] or participant.patient_id in survey['target_users']):
				continue
			if is_push_survey(survey):
				push_surveys += [survey['_id']]
		checksum = survey['checksum']
		if checksum in checksums.values():
			survey_id = survey["_id"]
			ret_surveys.append({
				"_id": survey_id,
				"up_to_date": True,
			})
		else:
			ret_surveys.append(survey)
	# ret = json.dumps(study.get_surveys_for_study())
	ret = GS_JSON_encode(ret_surveys)
	for sid in push_surveys:
		survey_api.del_patients_from_survey(request.values['patient_id'], sid)
	for sid in push_stacks:
		Survey.objects.get(object_id = sid).mark_deleted()
	return ret
