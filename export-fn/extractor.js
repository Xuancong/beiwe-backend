const AWS = require("aws-sdk");
const fs = require("fs");
const path = require("path");
const csv = require("csvtojson");
const s3 = new AWS.S3();

const Bucket = "hopes-palo-staging";

async function GetObjects(ContinuationToken) {
  let objects = [];
  const firstResponse = await s3
    .listObjectsV2({
      Bucket,
      MaxKeys: 1000,
      Prefix: "a8SL2JEVYzbta1rHiQKfCZZr",
      ContinuationToken
    })
    .promise();

  objects = [...firstResponse.Contents];

  const NextContinuationToken = firstResponse.NextContinuationToken;
  if (NextContinuationToken) {
    const response = await GetObjects(NextContinuationToken);
    objects = [...objects, ...response];
  }

  return objects;
}

(async event => {
  let objects = await GetObjects();

  for (let object of objects) {
    if (object.Key.includes(".raw")) {
      let [, pId, feature, timestamp] = object.Key.split("/");
      timestamp = timestamp.split(".")[0];
      const data = await s3
        .getObject({ Bucket, Key: object.Key.replace(`${Bucket}/`, "") })
        .promise();
      var length = data.Body.toString()
        .split("\n")
        .filter(r => r !== "").length;
      if (isNaN(length)) {
        continue;
      }

      if (length <= 3) {
        length = null;
      } else {
        length -= 2;
      }

      if (length === 0) {
        length = null;
      }

      const headers = [
        "hour",
        "accel",
        "accessibilityLog",
        "sociabilityLog",
        "sociabilityCallLog",
        "callLog",
        "gyro",
        "gps",
        "light",
        "powerState",
        "tapsLog",
        "textsLog",
        "usage",
        "logFile",
        "wifiLog",
        "bluetoothLog",
        "devicemotion",
        "magnetometer",
        "reachability",
        "proximity"
      ];

      const today = new Date(Number(timestamp));
      const filename = `./extracted/${pId}/${today.getUTCFullYear()}-${today.getUTCMonth() +
        1}-${today.getUTCDate()}.csv`;

      if (!fs.existsSync(path.dirname(filename))) {
        fs.mkdirSync(path.dirname(filename));
      }

      if (!fs.existsSync(filename)) {
        fs.writeFileSync(
          filename,
          `${headers.join(",")}\n0:00,${new Array(headers.length - 1)
            .fill(null)
            .join(",")}`
        );
      }

      const file = fs.readFileSync(filename);
      const parser = csv({ headers, checkType: true });
      const lines = await parser.fromString(file.toString());

      let hasHour = false;
      lines.map((line, index) => {
        if (line.hour === `${today.getUTCHours()}:00`) {
          line[feature] = Number(line[feature]) + length;
          hasHour = true;
        }
      });

      if (hasHour === false) {
        const newLine = {};
        headers.map(key => (newLine[key] = ""));
        lines.push({
          ...newLine,
          hour: `${today.getUTCHours()}:00`,
          [`${feature}`]: length
        });
      }

      fs.writeFileSync(
        filename,
        `${headers.join(",")}\n${lines
          .map(line =>
            Object.keys(line)
              .map(f => line[f])
              .join(",")
          )
          .join("\n")}`
      );
    }
  }
  return;
})();
