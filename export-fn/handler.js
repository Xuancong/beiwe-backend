"use strict";

const AWS = require("aws-sdk");
const csv = require("csvtojson");
const s3 = new AWS.S3();

module.exports.extract = async event => {
  event = JSON.parse(event.Records[0].body);
  const Bucket = event.Records[0].s3.bucket.name;
  const Key = decodeURIComponent(
    event.Records[0].s3.object.key.replace(/\+/g, " ")
  );
  let [, pId, feature, timestamp] = Key.split("/");
  timestamp = timestamp.split(".")[0];

  const object = await s3.getObject({ Bucket, Key }).promise();

  var length = object.Body.toString()
    .split("\n")
    .filter(r => r !== "").length;
  if (isNaN(length)) {
    return { message: "upload skipped", event };
  }

  if (length <= 3) {
    length = null;
  } else {
    length -= 2;
  }

  const headers = [
    "hour",
    "accel",
    "accessibilityLog",
    "sociabilityLog",
    "sociabilityCallLog",
    "callLog",
    "gyro",
    "gps",
    "light",
    "powerState",
    "tapsLog",
    "textsLog",
    "usage",
    "logFile",
    "wifiLog",
    "bluetoothLog",
    "devicemotion",
    "magnetometer",
    "reachability",
    "proximity"
  ];

  const today = new Date(Number(timestamp));
  const DestinationBucket = process.env.DEST_BUCKET || "hopes-2019-metrics";
  const DestinationKey = `beiwe/${pId}/${today.getUTCFullYear()}-${today.getUTCMonth() +
    1}-${today.getUTCDate()}.csv`;

  // write the latest upload date
  await s3
    .putObject({
      Bucket: DestinationBucket,
      Key: `uploads/${pId}/${today.getUTCFullYear()}-${today.getUTCMonth() +
        1}-${today.getUTCDate()} ${today.getUTCHours()}:${today.getUTCMinutes()}:${today.getUTCSeconds()}`,
      ACL: "private",
      Body: ""
    })
    .promise();

  try {
    await s3
      .headObject({ Bucket: DestinationBucket, Key: DestinationKey })
      .promise();
  } catch (e) {
    await s3
      .putObject({
        Bucket: DestinationBucket,
        Key: DestinationKey,
        ACL: "private",
        Body: `${headers.join(",")}\n0:00,${new Array(headers.length - 1)
          .fill(0)
          .join(",")}`
      })
      .promise();
  }

  const todaysFile = await s3
    .getObject({ Bucket: DestinationBucket, Key: DestinationKey })
    .promise();

  const parser = csv({ headers, checkType: true });

  const lines = await parser.fromString(todaysFile.Body.toString());

  let hasHour = false;
  lines.map((line, index) => {
    if (line.hour === `${today.getUTCHours()}:00`) {
      line[feature] = Number(line[feature]) + length;
      hasHour = true;
    }
  });

  if (hasHour === false) {
    const newLine = {};
    headers.map(key => (newLine[key] = ""));
    lines.push({
      ...newLine,
      hour: `${today.getUTCHours()}:00`,
      [`${feature}`]: length
    });
  }

  await s3
    .putObject({
      Bucket: DestinationBucket,
      Key: DestinationKey,
      ACL: "private",
      Body: `${headers.join(",")}\n${lines
        .map(line =>
          Object.keys(line)
            .map(f => line[f])
            .join(",")
        )
        .join("\n")}`
    })
    .promise();

  return { message: "upload successfully processed", event };
};
