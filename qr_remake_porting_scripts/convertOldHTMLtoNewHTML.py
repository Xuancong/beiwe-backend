from PIL import Image
from pyzbar.pyzbar import decode
import base64
import gzip
import qrcode	
import os
import urllib
import bs4
import json
import shutil


qrcode_imgs = [str(x) for x in [1,3,5,6,7]]
qrcode_compress = [str(x) for x in [3,5,6,7]]
replace_items = ['-----BEGIN CERTIFICATE-----\n', '\n-----END CERTIFICATE-----', '-----BEGIN RSA PRIVATE KEY-----\n', '\n-----END RSA PRIVATE KEY-----', '-----BEGIN PUBLIC KEY-----\n', '\n-----END PUBLIC KEY-----']

def getqrcode_str(dir_name, swift=0):
	total_str = '['

	for idx, qrcode_img in enumerate(qrcode_imgs):
		data = decode(Image.open(dir_name + '/' + qrcode_img + '.jpg'))
		# print(data)
		decoded = data[0][0].decode()
		#print("Step " + str(idx + 1))
		#print()
		#print(decoded)
		useful_data = decoded
		if swift:
			useful_data = decoded.replace('"', '\\\"').replace('\n', '\\\\n')
		#print(useful_data)
		if idx != 0:
			total_str += ',\n'
		total_str +=  f'{idx + 1}: \"{useful_data}\"'
		#print()

	total_str += ']'
	# print(total_str)
	return total_str

def getqrcode_compressed(dir_name):
	total_str = ''

	for idx, qrcode_img in enumerate(qrcode_compress):
		data = decode(Image.open(dir_name + '/' + qrcode_img + '.jpg'))
		decoded = data[0][0].decode()
		# print(decoded)
		decoded = json.loads(decoded,strict=False)
		decoded = decoded['body']
		for replace_item in replace_items:
			decoded = decoded.replace(replace_item, '')
		if idx != 0:
			total_str += '|'
		total_str += decoded

	# print(total_str)
	return total_str

def read_one_qr(filename):
	data = decode(Image.open(filename))
	decoded = data[0][0].decode()
	return decoded

def compressQRcodes(folder_name=''):
	if folder_name:
		with open(f'{folder_name}/compressqr_str.txt', 'r') as f:
			c = str.encode(f.read())
	compressed = (base64.b64encode(gzip.compress(c))).decode('utf-8')
	# print(f"Length: {len(compressed)}")
	# print(f"Original Length: {len(c)}")
	# print(compressed)
	DIVIDE_AMOUNT = 5
	TOTAL_AMOUNT = len(compressed) 

	# First QR code is slightly larger, and should hold more data
	DATA_DISTRIBUTION = [1] * DIVIDE_AMOUNT
	DATA_DISTRIBUTION[0] *= 1.3
	SUM_DATA = sum(DATA_DISTRIBUTION)
	DATA_DISTRIBUTION = [int(x/SUM_DATA * TOTAL_AMOUNT) for x in DATA_DISTRIBUTION]
	for x in range(1, DIVIDE_AMOUNT):
		DATA_DISTRIBUTION[x] += DATA_DISTRIBUTION[x-1]
	DATA_DISTRIBUTION.insert(0, 0)
	DATA_DISTRIBUTION[-1] = TOTAL_AMOUNT
	# print(DATA_DISTRIBUTION)

	for i in range(DIVIDE_AMOUNT):

		qr = qrcode.QRCode(	
		    error_correction=qrcode.constants.ERROR_CORRECT_L,
		    box_size=10,
		    border=4,
		)

		parsable_format = '{"step":"' + str(2+i) + '", "body":"' + compressed[DATA_DISTRIBUTION[i]:DATA_DISTRIBUTION[i+1]] + '"}'	
		# print(parsable_format)
		qr.add_data(parsable_format)

		qr.make(fit=True)

		img = qr.make_image(fill_color="black", back_color="white")

		img.save(f"{folder_name}/qr_gen_{i}.jpg","JPEG")
		with open(f"{folder_name}/qr_gen_{i}.jpg", "rb") as img_file:
		    base64_imgbytes = base64.b64encode(img_file.read())
		    with open(f"{folder_name}/base64_{i}.txt", "w") as base64f:
		    	base64f.write(base64_imgbytes.decode('utf-8'))


def add_img_to_template(folderName, studyNameElement, studyId_hopesId, fitbit_data, saved_filename='exportedtest.html', saved_folder_name='.'):
	if saved_folder_name == '.':
		saved_folder_name = folderName

	i = 0
	with open("qr_redesign/test.html", "r") as f:
		soup = bs4.BeautifulSoup(f, "lxml")
		for idx, img in enumerate(soup('img')):
			imgurl = img['src']
			imgpath = urllib.parse.urlparse(imgurl).path
			if imgpath[:10] == 'image/jpeg':
				i += 1
				if 1 <= i < 3:
					with open(folderName + '/' + f'{i}' + '.jpg', "rb") as image_file:
						data = base64.b64encode(image_file.read())
						newimgpath = 'data:' + imgpath[:18] + data.decode('utf-8')
						img['src'] = newimgpath


				elif i >= 3:
					with open(f'{folderName}/base64_{i-3}.txt', 'r') as base64f:
						newimgpath = 'data:' + imgpath[:18] + base64f.read()
						img['src'] = newimgpath

		soup.find("div", {"style" : "width:50%"}).find("p").string.replace_with(studyNameElement.string)
		for idx, id_element in enumerate(soup('strong')):
			id_element.string.replace_with(studyId_hopesId[idx].string)
		for idx, id_element in enumerate(soup.find("div", {"style" : "font-weight:bold;"}).findAll("div")):
			id_element.string.replace_with(fitbit_data[idx].string)

		with open(f'{saved_folder_name}/{saved_filename}', 'w') as exportedTemplate:
			exportedTemplate.write(str(soup))


def scrapeImg(filename, folder_for_html_file='', generated_html_folder='.'):
	folder_name = ''
	with open(f'{folder_for_html_file}/{filename}', "r") as f:
		soup = bs4.BeautifulSoup(f, "lxml")
		# Get images
		for idx, img in enumerate(soup('img')):
			imgurl = img['src']
			imgpath = urllib.parse.urlparse(imgurl).path
			if len(imgpath.split('base64')) >= 2:
				base64data = imgpath.split('base64')[1].strip(',')
				imgpath = str(idx) + '.jpg'
				folder_name = f"{generated_html_folder}/{filename.split('_')[1]}"
				if not os.path.isdir(folder_name):
					os.makedirs(folder_name)
				with open(f'{folder_name}/{imgpath}', "wb") as f:
					f.write(base64.b64decode(base64data))

		# Get the information in the HTML
		studyNameElement = soup.find("div", {"style" : "width:50%"}).find("p")
		studyName = studyNameElement.text
		studyId_hopesId = soup.findAll("strong")
		studyId = studyId_hopesId[0].text
		hopesId = studyId_hopesId[1].text
		fitbit_data = soup.find("div", {"style" : "font-weight:bold;"}).findAll("div")
		fitbitId = fitbit_data[0].text
		fitbitPassword = fitbit_data[1].text
		print(f"Study Name: {studyName}")
		print(f"Study ID: {studyId}")
		print(f"HOPES ID: {hopesId}")
		print(f"Fitbit ID: {fitbitId}")
		print(f"Fitbit Password: {fitbitPassword}")
		print(f"Converting {hopesId} HTML file...")



	# # Move the html file into the folder
	# if folder_name:
	# 	os.rename(file_name, f'{folder_name}/{file_name}')
	# else:
	# 	print("Invalid folder name")


	# Generate the QR into parsable form for quick injection
	if folder_name:
		qr_str = getqrcode_str(folder_name, swift=1)
		with open(f'{folder_name}/qr_str_swift.txt', "w") as f:
			f.write(qr_str)
		qr_str = getqrcode_str(folder_name, swift=0)
		with open(f'{folder_name}/qr_str.txt', "w") as f:
			f.write(qr_str)
		compressqr_str = getqrcode_compressed(folder_name)
		with open(f'{folder_name}/compressqr_str.txt', "w") as f:
			f.write(compressqr_str)

		compressQRcodes(folder_name)
		add_img_to_template(folder_name, studyNameElement, studyId_hopesId, fitbit_data, saved_filename=filename ,saved_folder_name=generated_html_folder)
		try:
		    shutil.rmtree(folder_name)
		except OSError as e:
		    print ("Error: %s - %s." % (e.filename, e.strerror))
	else:
		print("Invalid folder name")

	print(f"Conversion done: {hopesId} HTML file")



if __name__ == '__main__':
	oldHtmlFolder = 'placeOldHtmlHere'
	generated_html_folder = 'newHtmlGeneratedHere'
	for file_name in os.listdir(oldHtmlFolder):
		if '.html' in file_name:
			scrapeImg(file_name, oldHtmlFolder, generated_html_folder=generated_html_folder)