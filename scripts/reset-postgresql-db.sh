#!/usr/bin/env bash

set -e -o pipefail

# set default environment variables
if [ ! "$PORT" ]; then
	PORT=5432
fi

# Detect PostgreSQL binaries
FAIL=
for bin in postgres psql initdb createdb pg_isready; do
	if [ ! "`which $bin`" ]; then
		FAIL=1
	fi
done

if [ "$FAIL" ]; then
	PSQL_VER=`ls /usr/lib/postgresql | sort -n | tail -1`
	if [ ! "$PSQL_VER" ]; then
		echo "Error: PostgreSQL is not installed" >&2
		exit 1
	fi
	export PATH=/usr/lib/postgresql/$PSQL_VER/bin:$PATH
fi

if [ ! "$PSQL_ROOT" ]; then
	PSQL_ROOT=./db.psql
fi

echo "Using PORT=$PORT PSQL_ROOT=$PSQL_ROOT , `which postgres`"



### START

rm -rf $PSQL_ROOT

initdb -D $PSQL_ROOT

if [ "`netstat -tlnp 2>/dev/null | grep \":$PORT \"`" ]; then
	echo "Error: PostgreSQL Port $PORT is already bound" >&2
	exit 1
fi

postgres -k . -D $PSQL_ROOT &
PID=$!

while ! [[ "`pg_isready -h 127.0.0.1`" =~ .*accepting\ connections$ ]]; do
	echo "Waiting for PostgreSQL to be ready ..." >&2
	sleep 2
done

createdb -h 127.0.0.1 beiwe

psql -h 127.0.0.1 beiwe <./scripts/init-postgres.sql

sleep 5

kill $PID

while [ "`netstat -tlnp 2>/dev/null | grep \":$PORT \"`" ]; do
	echo "Waiting for PostgreSQL to shutdown ..." >&2
	sleep 2
done
