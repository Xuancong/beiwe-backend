

CREATE TABLE database_decryptionkeyerror (
    id BIGSERIAL PRIMARY KEY,
    deleted boolean NOT NULL,
    file_path character varying(256) NOT NULL,
    contents text NOT NULL,
    participant_id integer NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_updated timestamp with time zone NOT NULL
);


CREATE TABLE database_devicesettings (
    id BIGSERIAL PRIMARY KEY,
    deleted boolean NOT NULL,
    accelerometer boolean NOT NULL,
    gps boolean NOT NULL,
    calls boolean NOT NULL,
    sms boolean NOT NULL,
    wifi boolean NOT NULL,
    bluetooth boolean NOT NULL,
    bluetooth_state boolean NOT NULL,
    power_state boolean NOT NULL,
    proximity boolean NOT NULL,
    gyro boolean NOT NULL,
    magnetometer boolean NOT NULL,
    pedometer boolean NOT NULL,
    devicemotion boolean NOT NULL,
    reachability boolean NOT NULL,
    allow_upload_over_cellular_data boolean NOT NULL,
    accelerometer_off_duration_seconds integer NOT NULL,
    accelerometer_on_duration_seconds integer NOT NULL,
    bluetooth_on_duration_seconds integer NOT NULL,
    bluetooth_total_duration_seconds integer NOT NULL,
    bluetooth_global_offset_seconds integer NOT NULL,
    check_for_new_surveys_frequency_seconds integer NOT NULL,
    create_new_data_files_frequency_seconds integer NOT NULL,
    gps_off_duration_seconds integer NOT NULL,
    gps_on_duration_seconds integer NOT NULL,
    seconds_before_auto_logout integer NOT NULL,
    upload_data_files_frequency_seconds integer NOT NULL,
    voice_recording_max_time_length_seconds integer NOT NULL,
    wifi_log_frequency_seconds integer NOT NULL,
    gyro_off_duration_seconds integer NOT NULL,
    gyro_on_duration_seconds integer NOT NULL,
    magnetometer_off_duration_seconds integer NOT NULL,
    magnetometer_on_duration_seconds integer NOT NULL,
    pedometer_off_duration_seconds integer NOT NULL,
    pedometer_on_duration_seconds integer NOT NULL,
    devicemotion_off_duration_seconds integer NOT NULL,
    devicemotion_on_duration_seconds integer NOT NULL,
    about_page_text text NOT NULL,
    mainpage_title text,
    consent_form_text text,
    survey_submit_success_toast_text text NOT NULL,
    consent_sections text NOT NULL,
    study_id integer NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_updated timestamp with time zone NOT NULL,
    ambientlight boolean DEFAULT false,
    ambientlight_interval_seconds integer DEFAULT 60,
    taps boolean DEFAULT false,
    sociability_call boolean DEFAULT false,
    sociability_msg boolean DEFAULT false,
    accessibility boolean DEFAULT false,
    usage boolean DEFAULT false,
    usage_update_interval_seconds integer DEFAULT 3600,
    use_anonymized_hashing boolean DEFAULT false,
    phone_number_length integer DEFAULT 8,
    primary_care text,
    use_gps_fuzzing boolean DEFAULT false,
    write_buffer_size integer DEFAULT 0,
    use_compression boolean DEFAULT false,
    CONSTRAINT database_devicesettings_accelerometer_off_duration_second_check CHECK ((accelerometer_off_duration_seconds >= 0)),
    CONSTRAINT database_devicesettings_accelerometer_on_duration_seconds_check CHECK ((accelerometer_on_duration_seconds >= 0)),
    CONSTRAINT database_devicesettings_bluetooth_global_offset_seconds_check CHECK ((bluetooth_global_offset_seconds >= 0)),
    CONSTRAINT database_devicesettings_bluetooth_on_duration_seconds_check CHECK ((bluetooth_on_duration_seconds >= 0)),
    CONSTRAINT database_devicesettings_bluetooth_total_duration_seconds_check CHECK ((bluetooth_total_duration_seconds >= 0)),
    CONSTRAINT database_devicesettings_check_for_new_surveys_frequency_s_check CHECK ((check_for_new_surveys_frequency_seconds >= 0)),
    CONSTRAINT database_devicesettings_create_new_data_files_frequency_s_check CHECK ((create_new_data_files_frequency_seconds >= 0)),
    CONSTRAINT database_devicesettings_devicemotion_off_duration_seconds_check CHECK ((devicemotion_off_duration_seconds >= 0)),
    CONSTRAINT database_devicesettings_devicemotion_on_duration_seconds_check CHECK ((devicemotion_on_duration_seconds >= 0)),
    CONSTRAINT database_devicesettings_gps_off_duration_seconds_check CHECK ((gps_off_duration_seconds >= 0)),
    CONSTRAINT database_devicesettings_gps_on_duration_seconds_check CHECK ((gps_on_duration_seconds >= 0)),
    CONSTRAINT database_devicesettings_gyro_off_duration_seconds_check CHECK ((gyro_off_duration_seconds >= 0)),
    CONSTRAINT database_devicesettings_gyro_on_duration_seconds_check CHECK ((gyro_on_duration_seconds >= 0)),
    CONSTRAINT database_devicesettings_magnetometer_off_duration_seconds_check CHECK ((magnetometer_off_duration_seconds >= 0)),
    CONSTRAINT database_devicesettings_magnetometer_on_duration_seconds_check CHECK ((magnetometer_on_duration_seconds >= 0)),
    CONSTRAINT database_devicesettings_pedometer_off_duration_seconds_check CHECK ((pedometer_off_duration_seconds >= 0)),
    CONSTRAINT database_devicesettings_pedometer_on_duration_seconds_check CHECK ((pedometer_on_duration_seconds >= 0)),
    CONSTRAINT database_devicesettings_seconds_before_auto_logout_check CHECK ((seconds_before_auto_logout >= 0)),
    CONSTRAINT database_devicesettings_upload_data_files_frequency_secon_check CHECK ((upload_data_files_frequency_seconds >= 0)),
    CONSTRAINT database_devicesettings_voice_recording_max_time_length_s_check CHECK ((voice_recording_max_time_length_seconds >= 0)),
    CONSTRAINT database_devicesettings_wifi_log_frequency_seconds_check CHECK ((wifi_log_frequency_seconds >= 0))
);


CREATE TABLE database_encryptionerrormetadata (
    id BIGSERIAL PRIMARY KEY,
    deleted boolean NOT NULL,
    file_name character varying(256) NOT NULL,
    total_lines integer NOT NULL,
    number_errors integer NOT NULL,
    errors_lines text NOT NULL,
    error_types text NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_updated timestamp with time zone NOT NULL,
    CONSTRAINT database_encryptionerrormetadata_number_errors_check CHECK ((number_errors >= 0)),
    CONSTRAINT database_encryptionerrormetadata_total_lines_check CHECK ((total_lines >= 0))
);


CREATE TABLE database_fileprocesslock (
    id BIGSERIAL PRIMARY KEY,
    deleted boolean NOT NULL,
    lock_time timestamp with time zone,
    created_on timestamp with time zone NOT NULL,
    last_updated timestamp with time zone NOT NULL
);


CREATE TABLE database_filetoprocess (
    id BIGSERIAL PRIMARY KEY,
    deleted boolean NOT NULL,
    s3_file_path character varying(256) NOT NULL,
    participant_id integer NOT NULL,
    study_id integer NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_updated timestamp with time zone NOT NULL
);


CREATE TABLE database_lineencryptionerror (
    id BIGSERIAL PRIMARY KEY,
    deleted boolean NOT NULL,
    type character varying(32) NOT NULL,
    line text NOT NULL,
    base64_decryption_key character varying(256) NOT NULL,
    prev_line text NOT NULL,
    next_line text NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_updated timestamp with time zone NOT NULL
);


CREATE TABLE database_participant (
    id BIGSERIAL PRIMARY KEY,
	name character varying(256),
    deleted boolean NOT NULL,
    password character varying(44) NOT NULL,
    salt character varying(24) NOT NULL,
    patient_id character varying(256) NOT NULL,
    device_id character varying(256),
    os_type character varying(16) NOT NULL,
    study_id integer NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_updated timestamp with time zone NOT NULL,
    os_desc character varying(64),
    fcm_token character varying(256),
    upload_info bytea,
    upload_info_text text,
    last_upload_time timestamp without time zone,
    remarks text,
    state_variable text,
    ext_dashboards_json jsonb
);


CREATE TABLE database_pipelineupload (
    id BIGSERIAL PRIMARY KEY,
    deleted boolean NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_updated timestamp with time zone NOT NULL,
    object_id character varying(24) NOT NULL,
    file_name text NOT NULL,
    s3_path text NOT NULL,
    file_hash character varying(128) NOT NULL,
    study_id integer NOT NULL
);

CREATE TABLE database_pipelineuploadtags (
    id BIGSERIAL PRIMARY KEY,
    deleted boolean NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_updated timestamp with time zone NOT NULL,
    tag text NOT NULL,
    pipeline_upload_id integer NOT NULL
);

CREATE TABLE database_researcher (
    id BIGSERIAL PRIMARY KEY,
    deleted boolean NOT NULL,
    password character varying(44) NOT NULL,
    salt character varying(24) NOT NULL,
    username character varying(32) NOT NULL,
    admin boolean NOT NULL,
    access_key_id character varying(64),
    access_key_secret character varying(44) NOT NULL,
    access_key_secret_salt character varying(24) NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_updated timestamp with time zone NOT NULL
);


CREATE TABLE database_researcher_studies (
    id BIGSERIAL PRIMARY KEY,
    researcher_id integer NOT NULL,
    study_id integer NOT NULL
);


CREATE TABLE database_study (
    id BIGSERIAL PRIMARY KEY,
    deleted boolean NOT NULL,
    name text NOT NULL,
    encryption_key character varying(32) NOT NULL,
    object_id character varying(24) NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_updated timestamp with time zone NOT NULL,
	study_cycle_days integer DEFAULT 30,
	daily_check_formula text,
	date_elapse_color text,
    external_dashboards text,
	registration_allow_list text,
	otp_email_title text,
	otp_email_content text,
	otp_expire_in_sec integer DEFAULT 86400,
    is_test boolean NOT NULL,
	allow_same_qr_same_phone_rereg boolean NOT NULL,
	allow_same_qr_diff_phone_rereg boolean NOT NULL,
	data_completion_eval_formulae text,
	data_completion_report_template text
);


CREATE TABLE database_survey (
    id BIGSERIAL PRIMARY KEY,
    deleted boolean NOT NULL,
	name text,
    content text NOT NULL,
    survey_type character varying(16) NOT NULL,
    settings text NOT NULL,
    timings text NOT NULL,
	target_users text,
	expiry text,
    object_id character varying(24) NOT NULL,
    study_id integer NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_updated timestamp with time zone NOT NULL,
    checksum character varying(32) NOT NULL
);


CREATE TABLE database_surveyarchive (
    id BIGSERIAL PRIMARY KEY,
    deleted boolean NOT NULL,
	name text,
    content text NOT NULL,
    survey_type character varying(16) NOT NULL,
    settings text NOT NULL,
    timings text NOT NULL,
	target_users text,
	expiry text,
    archive_start timestamp with time zone NOT NULL,
    archive_end timestamp with time zone NOT NULL,
    study_id integer NOT NULL,
    survey_id integer NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_updated timestamp with time zone NOT NULL,
    checksum character varying(32) NOT NULL
);


CREATE TABLE database_uploadtracking (
    id BIGSERIAL PRIMARY KEY,
    deleted boolean NOT NULL,
	name text,
    file_path character varying(256) NOT NULL,
    file_size integer NOT NULL,
    "timestamp" timestamp with time zone NOT NULL,
    participant_id integer NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_updated timestamp with time zone NOT NULL,
    CONSTRAINT database_uploadtracking_file_size_check CHECK ((file_size >= 0))
);


CREATE TABLE database_OTPtracking (
    id BIGSERIAL PRIMARY KEY,
	study_user_id character varying(256) NOT NULL,
	otp_value character varying (8) NOT NULL,
	create_on timestamp with time zone NOT NULL
);


CREATE TABLE django_migrations (
    id BIGSERIAL PRIMARY KEY,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


CREATE TABLE database_chunkregistry (
    id BIGSERIAL PRIMARY KEY,
    deleted boolean NOT NULL,
    is_chunkable boolean NOT NULL,
    chunk_path character varying(256) NOT NULL,
    chunk_hash character varying(25) NOT NULL,
    data_type character varying(32) NOT NULL,
    time_bin timestamp with time zone NOT NULL,
    participant_id integer NOT NULL,
    study_id integer NOT NULL,
    survey_id integer,
    created_on timestamp with time zone NOT NULL,
    last_updated timestamp with time zone NOT NULL
);

ALTER TABLE ONLY public.database_devicesettings
    ADD CONSTRAINT database_devicesettings_study_id_key UNIQUE (study_id);

ALTER TABLE ONLY public.database_participant
    ADD CONSTRAINT database_participant_patient_id_key UNIQUE (patient_id);

ALTER TABLE ONLY public.database_pipelineupload
    ADD CONSTRAINT database_pipelineupload_object_id_key UNIQUE (object_id);

ALTER TABLE ONLY public.database_researcher
    ADD CONSTRAINT database_researcher_access_key_id_key UNIQUE (access_key_id);

ALTER TABLE ONLY public.database_researcher_studies
    ADD CONSTRAINT database_researcher_stud_researcher_id_study_id_8b2bd313_uniq UNIQUE (researcher_id, study_id);

ALTER TABLE ONLY public.database_researcher
    ADD CONSTRAINT database_researcher_username_key UNIQUE (username);

ALTER TABLE ONLY public.database_study
    ADD CONSTRAINT database_study_name_key UNIQUE (name);

ALTER TABLE ONLY public.database_study
    ADD CONSTRAINT database_study_object_id_key UNIQUE (object_id);

ALTER TABLE ONLY public.database_survey
    ADD CONSTRAINT database_survey_object_id_key UNIQUE (object_id);

ALTER TABLE ONLY public.database_OTPtracking
    ADD CONSTRAINT database_OTPtracking_study_user_id UNIQUE (study_user_id);



CREATE INDEX database_chunkregistry_participant_id_86ed17cf ON database_chunkregistry USING btree (participant_id);
CREATE INDEX database_chunkregistry_study_id_02c441c3 ON database_chunkregistry USING btree (study_id);
CREATE INDEX database_chunkregistry_survey_id_6d3ed776 ON database_chunkregistry USING btree (survey_id);
CREATE INDEX database_decryptionkeyerror_participant_id_820ee4b8 ON database_decryptionkeyerror USING btree (participant_id);
CREATE INDEX database_filetoprocess_participant_id_58eb91cf ON database_filetoprocess USING btree (participant_id);
CREATE INDEX database_filetoprocess_study_id_bde5df71 ON database_filetoprocess USING btree (study_id);
CREATE INDEX database_participant_patient_id_6acaeab5_like ON database_participant USING btree (patient_id varchar_pattern_ops);
CREATE INDEX database_participant_study_id_8a79528b ON database_participant USING btree (study_id);
CREATE INDEX database_pipelineupload_object_id_c49fb769_like ON database_pipelineupload USING btree (object_id varchar_pattern_ops);
CREATE INDEX database_pipelineupload_study_id_03446ed9 ON database_pipelineupload USING btree (study_id);
CREATE INDEX database_pipelineuploadtags_pipeline_upload_id_930237e0 ON database_pipelineuploadtags USING btree (pipeline_upload_id);
CREATE INDEX database_researcher_access_key_id_85b1e779_like ON database_researcher USING btree (access_key_id varchar_pattern_ops);
CREATE INDEX database_researcher_studies_researcher_id_d50f7b28 ON database_researcher_studies USING btree (researcher_id);
CREATE INDEX database_researcher_studies_study_id_c9b031c0 ON database_researcher_studies USING btree (study_id);
CREATE INDEX database_researcher_username_b31dab38_like ON database_researcher USING btree (username varchar_pattern_ops);
CREATE INDEX database_study_name_155b14db_like ON database_study USING btree (name text_pattern_ops);
CREATE INDEX database_study_object_id_be2f81fa_like ON database_study USING btree (object_id varchar_pattern_ops);
CREATE INDEX database_survey_object_id_ad7ce4cb_like ON database_survey USING btree (object_id varchar_pattern_ops);
CREATE INDEX database_survey_study_id_bc7ed043 ON database_survey USING btree (study_id);
CREATE INDEX database_surveyarchive_study_id_960cf592 ON database_surveyarchive USING btree (study_id);
CREATE INDEX database_surveyarchive_survey_id_9b38709e ON database_surveyarchive USING btree (survey_id);
CREATE INDEX database_uploadtracking_participant_id_f6783799 ON database_uploadtracking USING btree (participant_id);
CREATE INDEX database_OTPtracking_study_user_id_f6783799 ON database_OTPtracking USING btree (study_user_id);


ALTER TABLE ONLY database_chunkregistry
    ADD CONSTRAINT database_chunkregist_participant_id_86ed17cf_fk_database_ FOREIGN KEY (participant_id) REFERENCES database_participant(id) DEFERRABLE INITIALLY DEFERRED;


ALTER TABLE ONLY database_chunkregistry
    ADD CONSTRAINT database_chunkregistry_study_id_02c441c3_fk_database_study_id FOREIGN KEY (study_id) REFERENCES database_study(id) DEFERRABLE INITIALLY DEFERRED;


ALTER TABLE ONLY database_chunkregistry
    ADD CONSTRAINT database_chunkregistry_survey_id_6d3ed776_fk_database_survey_id FOREIGN KEY (survey_id) REFERENCES database_survey(id) DEFERRABLE INITIALLY DEFERRED;


ALTER TABLE ONLY database_decryptionkeyerror
    ADD CONSTRAINT database_decryptionk_participant_id_820ee4b8_fk_database_ FOREIGN KEY (participant_id) REFERENCES database_participant(id) DEFERRABLE INITIALLY DEFERRED;


ALTER TABLE ONLY database_devicesettings
    ADD CONSTRAINT database_devicesettings_study_id_5967a5af_fk_database_study_id FOREIGN KEY (study_id) REFERENCES database_study(id) DEFERRABLE INITIALLY DEFERRED;


ALTER TABLE ONLY database_filetoprocess
    ADD CONSTRAINT database_filetoproce_participant_id_58eb91cf_fk_database_ FOREIGN KEY (participant_id) REFERENCES database_participant(id) DEFERRABLE INITIALLY DEFERRED;


ALTER TABLE ONLY database_filetoprocess
    ADD CONSTRAINT database_filetoprocess_study_id_bde5df71_fk_database_study_id FOREIGN KEY (study_id) REFERENCES database_study(id) DEFERRABLE INITIALLY DEFERRED;


ALTER TABLE ONLY database_participant
    ADD CONSTRAINT database_participant_study_id_8a79528b_fk_database_study_id FOREIGN KEY (study_id) REFERENCES database_study(id) DEFERRABLE INITIALLY DEFERRED;


ALTER TABLE ONLY database_pipelineuploadtags
    ADD CONSTRAINT database_pipelineupl_pipeline_upload_id_930237e0_fk_database_ FOREIGN KEY (pipeline_upload_id) REFERENCES database_pipelineupload(id) DEFERRABLE INITIALLY DEFERRED;


ALTER TABLE ONLY database_pipelineupload
    ADD CONSTRAINT database_pipelineupload_study_id_03446ed9_fk_database_study_id FOREIGN KEY (study_id) REFERENCES database_study(id) DEFERRABLE INITIALLY DEFERRED;


ALTER TABLE ONLY database_researcher_studies
    ADD CONSTRAINT database_researcher__researcher_id_d50f7b28_fk_database_ FOREIGN KEY (researcher_id) REFERENCES database_researcher(id) DEFERRABLE INITIALLY DEFERRED;


ALTER TABLE ONLY database_researcher_studies
    ADD CONSTRAINT database_researcher__study_id_c9b031c0_fk_database_ FOREIGN KEY (study_id) REFERENCES database_study(id) DEFERRABLE INITIALLY DEFERRED;


ALTER TABLE ONLY database_survey
    ADD CONSTRAINT database_survey_study_id_bc7ed043_fk_database_study_id FOREIGN KEY (study_id) REFERENCES database_study(id) DEFERRABLE INITIALLY DEFERRED;


ALTER TABLE ONLY database_uploadtracking
    ADD CONSTRAINT database_uploadtrack_participant_id_f6783799_fk_database_ FOREIGN KEY (participant_id) REFERENCES database_participant(id) DEFERRABLE INITIALLY DEFERRED;

INSERT INTO "database_researcher"("id","deleted","password","salt","username","admin","access_key_id","access_key_secret","access_key_secret_salt","created_on","last_updated")
VALUES
(1,FALSE,E'6U8DdHB-XJvsW_QAPe1s4-t4qqaHTgVOhGhVXdeUuDs=',E'zupiGXyScPa9wR_a-1IlVw==',E'default_admin',TRUE,E'se3nMx1LtolPNBFNFHG5h+6oZ6MYb/dXfNouS8G2Mk+VwArv/f9zkaXqzo45lnbP',E'cK0l36e6foxiMHW22bJnzHPZgzqxQ_pEPQZusM1HQAc=',E'ApgbGWLYp74W1SODl2oqaA==',E'2019-09-06 04:01:23.161301+00',E'2019-09-06 04:01:23.161324+00');
