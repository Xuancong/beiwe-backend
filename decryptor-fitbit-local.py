#!/usr/bin/env python3

import os, sys, re, argparse
import base64
import boto3
import json
from Crypto.Cipher import AES
from Crypto.PublicKey import RSA
from collections import defaultdict
from data_encryptor import DataEncryptEAX

class TC:
	HEADER = '\033[95m'
	OKBLUE = '\033[94m'
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'
	END = '\033[0m'
	BLR = '\033[1m\033[91m'
	BLG = '\033[1m\033[92m'
	BLY = '\033[1m\033[93m'
	LR = '\033[91m'
	LG = '\033[92m'
	LY = '\033[93m'

def get_private_key(secret_name):
	response = sm.get_secret_value(SecretId=secret_name)
	return json.loads(response['SecretString'])['private']

def remove_PKCS5_padding(data):
	return data[0: -ord( data[-1] ) ]

def cleanup_slash(L):
	L1 = re.sub(r'/+', '/', L)
	return re.sub(r'/+$', '', L1)

sm = boto3.client('secretsmanager', region_name="ap-southeast-1", aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'), aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'))
smCache = defaultdict(lambda: None)
e = DataEncryptEAX()

def proc(ipath, opath):
	if os.path.isdir(ipath):
		for path in os.listdir(ipath):
			proc(ipath+'/'+path, opath+'/'+path)
	elif ipath.endswith(extension):
		Ipath = cleanup_slash(ipath)
		Opath = cleanup_slash(opath)
		if os.path.isdir(Opath):
			Opath += '/'+os.path.basename(Ipath)
		if Opath.endswith(extension):
			Opath = Opath[:-len(extension)]
		if not force and os.path.isfile(Opath) and os.path.getsize(Opath):
			return

		os.makedirs(os.path.dirname(Opath), exist_ok=True)
		writeHere = open(Opath, "w")

		# decrypt the file
		print(Opath, '                                    \r', file=sys.stderr, flush=True, end='')

		try:
			Study, pId = Ipath.split('/')[1:3]
			if smCache[pId] is None:
				smCache[pId] = get_private_key("private/" + Study + "/" + pId)

			with open(Ipath, "r+b") as reader:
				out = reader.read()
			k,n,t,cy = out.decode("utf-8").split("\n")[:4]
			text = e.decrypt_data(smCache[pId], k, n, cy, t)
			writeHere.write(text)
			writeHere.close()

		except:
			print("%sError: decryption failed on file %s%s%s" % (TC.BLR, TC.BLY, Ipath, TC.END), file=sys.stderr)


if __name__=='__main__':
	parser = argparse.ArgumentParser(usage='$0 [source_path] [target_path] [options] 2>progress', description='decrypt files locally')
	parser.add_argument('ipath', default='encrypted-fitbit', help='positional argument', nargs='?')
	parser.add_argument('opath', default='decrypted', help='positional argument', nargs='?')
	parser.add_argument('--extension', '-e', default='.raw', help='valid source file extension')
	parser.add_argument('--force', '-f', help='force decrypt and overwrite existing files', action='store_true')
	#nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt=parser.parse_args()
	globals().update(vars(opt))

	proc(ipath, opath)
	print(file=sys.stderr)

