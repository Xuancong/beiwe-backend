#!/usr/bin/env bash

set -e -o pipefail

# set default environment variables
if [ ! "$PORT" ]; then
	PORT=5432
fi

# Detect PostgreSQL binaries
check_sql(){
	for bin in postgres psql initdb createdb pg_isready; do
		if [ ! "`which $bin`" ]; then
			echo 0
			return
		fi
		$bin --help 1>/dev/null 2>/dev/null
		if [ $? != 0 ]; then
			echo 0
			return
		fi
	done
	echo 1
}

if [ `check_sql` == 0 ]; then
	PSQL_VER=`ls -d /usr/lib/postgresql/*/ | sort -n | tail -1`
	if [ "$PSQL_VER" ]; then
		export PATH=$PSQL_VER/bin:$PATH
	fi
	if [ `check_sql` == 0 ]; then
		echo "Warning: PostgreSQL is not installed, trying to use built-in PostgreSQL binary" >&2
		export PATH=$PWD/third-party/postgreSQL:$PATH
		if [ `check_sql` == 0 ]; then
			echo "Error: not possible to launch PostgreSQL"
			exit 1
		fi
	fi
fi


if [ ! "$PSQL_ROOT" ]; then
	PSQL_ROOT=./db.psql
fi

echo "Using PORT=$PORT PSQL_ROOT=$PSQL_ROOT , `which postgres`"


### START

postgres -k . -D $PSQL_ROOT -p $PORT

