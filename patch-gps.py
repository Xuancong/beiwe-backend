#!/usr/bin/env python3
# coding=utf-8

import os, sys, argparse, re, gzip
import pandas as pd


# rebase into range: latitude=[-90, 90], longitude=[-180, 180]
def proc(df, base):
	if len(df):
		# determine offset
		latC, lonC = base
		lat0, lon0 = df.loc[0, ['latitude', 'longitude']]
		lat_offset = latC-lat0
		lon_offset = lonC-lon0

		# transform latitude
		df.latitude += lat_offset

		# transform longitude
		df.longitude = (df.longitude+lon_offset+540)%360-180

	return df


def Open(fn, mode='r'):
	if fn == '-':
		return sys.stdin if mode.startswith('r') else sys.stdout
	return gzip.open(fn, mode) if fn.lower().endswith('.gz') else open(fn, mode)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(usage='$0 input-file output-file 1>progress 2>warning_msg',
									 description='This program rebases gps coordinates for mobility feature extraction')
	parser.add_argument('input_fn', help='positional argument')
	parser.add_argument('output_fn', help='positional argument')
	parser.add_argument('--coords', '-c', help='rebase GPS, default is in Singapore (1.3521, 103.8198)', nargs=2, default=[1.3521, 103.8198])
	# nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt = parser.parse_args()
	globals().update(vars(opt))

	df = proc(pd.read_csv(Open(input_fn)), coords)
	Open(output_fn, 'w').write(df.to_csv(index=False)) if output_fn == '-' else df.to_csv(output_fn, index=False)
