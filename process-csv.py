#!/usr/bin/env python3
# coding=utf-8

import os,sys,argparse,re,gzip
import pandas as pd
import numpy as np
from collections import *
from io import StringIO
from dateutil.tz import tzlocal

if __name__=='__main__':
	parser = argparse.ArgumentParser(usage='$0 "<python-code>" 1>output 2>progress', description='This program reads CSV data from STDIN, execute <python-code> to modify the DataFrame, \
then write modified output to STDOUT. The loaded DataFrame is named "df", output DataFrame is "out"; if "out" is missing, the <python-code> will be evaluated instead; \
if evaluation returns None (e.g., "df.value*=2"), df will be used as the output.')
	parser.add_argument('pycode', help='the python code to be executed')
	parser.add_argument('-index', default='False', help='to be passed to DataFrame.to_csv(index=), default=False')
	#nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt=parser.parse_args()
	globals().update(vars(opt))

	df = pd.read_csv(StringIO(sys.stdin.read()))

	if '=' in pycode:
		exec(pycode)
	else:
		out = eval(pycode)

	if 'out' not in locals():
		out = df

	print(out.to_csv(index=eval(index)), end='')
