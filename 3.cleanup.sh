#!/bin/bash

if [ ! "$STUDY_ID" ]; then
	echo "Error: STUDY_ID is not defined"
	exit 1
fi


INPATH=2.decrypted/$STUDY_ID
OUTPRE=3

need_to_update () {
  # Usage: $0 input_file [output_file1 output_file2 ...]
  # It returns 1 if and only if input_file is updated within 1 day and every output file exist and non-empty
  fin="$1"
  shift
  while [ $# -gt 0 ]; do
    if [ ! -s "$1" ]; then
      echo 1
      return
    fi
    shift
  done
  days_til_now=`python3 -c "import os,sys; import pandas as pd;\
    print((pd.Timestamp.now()-pd.Timestamp(os.stat('$fin').st_mtime, unit='s'))/pd.to_timedelta('1D'))"`
  echo "$days_til_now < 1" | bc
}

copy_last_mod_time() {
	# Usage: $0 src tgt [tgt2 ...]
	# It copies the last modification time from src to tgt
	fin="$1"
	shift
	while [ $# -gt 0 ]; do
		python3 -c "import os,sys;import pandas as pd;st=os.stat('$fin');os.utime('$1', (st.st_atime, st.st_mtime))"
		shift
	done
}

## Firstly, copy over all other files, then process and overwrite problematic files
unset DO_NOT_COPY
declare -A DO_NOT_COPY=( ["light.csv.gz"]=1 ["sociabilityLog.csv.gz"]=1 ["sociabilityCallLog.csv.gz"]=1 ["tapsLog.csv.gz"]=1 \
  ["accessibilityLog.csv.gz"]=1 ["steps.csv.gz"]=1)
find $INPATH -type f -iname '*' \
| while read file; do
  if [ ! ${DO_NOT_COPY[`basename "$file"`]} ]; then
    newfn=$OUTPRE.${file:2}
    mkdir -p `dirname "$newfn"`
    cp -f "$file" "$newfn"
  fi
done

# 1. ambient light, clamp between 0 and 65535
echo -n "Patching ambient light value limit "
for user in $INPATH/*; do
	if [ -s "$user/light.csv.gz" ] && [ `need_to_update "$user/light.csv.gz" "$OUTPRE.${user:2}/light.csv.gz"` == 1 ]; then
		zcat -f $user/light.csv.gz | ./process-csv.py 'df.value=df.value.clip(0,65535)' | gzip >$OUTPRE.${user:2}/light.csv.gz
		copy_last_mod_time "$user/light.csv.gz" "$OUTPRE.${user:2}/light.csv.gz"
		echo -n .
	fi
done
echo Done


# 2. remove duplicate WhatsApp message
pycode="import os,sys
fixed = {int(k):int(v) for k,v in zip(sys.argv[1::2], sys.argv[2::2])}
prev_arr = [-9999]*9999
N = 0
while True:
	try:
		L=input()
	except:
		break
	N+=1
	if N==1:
		print(L)
		continue
	arr=L.split(',')
	res=[(abs(int(v)-int(prev_arr[i]))<=fixed[i] if i in fixed else v==prev_arr[i]) for i,v in enumerate(arr)]
	if False in res:
		print(L)
	prev_arr = arr
"
echo -n "Filtering duplicate social messages/calls "
for user in $INPATH/*; do
	if [ -s "$user/sociabilityLog.csv.gz" ] && [ `need_to_update "$user/sociabilityLog.csv.gz" "$OUTPRE.${user:2}/sociabilityLog.csv.gz"` == 1 ]; then
		zcat -f $user/sociabilityLog.csv.gz     | sort -t , -n -k 1 | python3 -c "$pycode" 0 1000000 9 1000000 | gzip >$OUTPRE.${user:2}/sociabilityLog.csv.gz
		copy_last_mod_time "$user/sociabilityLog.csv.gz" "$OUTPRE.${user:2}/sociabilityLog.csv.gz"
		echo -n .
	fi
	if [ -s "$user/sociabilityCallLog.csv.gz" ] && [ `need_to_update "$user/sociabilityCallLog.csv.gz" "$OUTPRE.${user:2}/sociabilityCallLog.csv.gz"` == 1 ]; then
		zcat -f $user/sociabilityCallLog.csv.gz | sort -t , -n -k 1 | python3 -c "$pycode" 0 1000000 6 1000000 | gzip >$OUTPRE.${user:2}/sociabilityCallLog.csv.gz
		copy_last_mod_time "$user/sociabilityCallLog.csv.gz" "$OUTPRE.${user:2}/sociabilityCallLog.csv.gz"
		echo -ne "\b:"
	fi
done
echo Done


# 3. apply app-grouper
echo -n "Applying app-grouper "
for user in $INPATH/*; do
	if [ -s "$user/tapsLog.csv.gz" ] && [ `need_to_update "$user/tapsLog.csv.gz" "$OUTPRE.${user:2}/tapsLog.csv.gz"` == 1 ]; then
		echo "in_app_class" >tmp-$$
		zcat -f $user/tapsLog.csv.gz | awk 'BEGIN{FS=","}{if(NR>1)print $2}' | ./app-grouper/app-grouper.py >>tmp-$$
		zcat -f $user/tapsLog.csv.gz | paste -d , - tmp-$$ | gzip >$OUTPRE.${user:2}/tapsLog.csv.gz
		copy_last_mod_time "$user/tapsLog.csv.gz" "$OUTPRE.${user:2}/tapsLog.csv.gz"
		echo -n .
	fi

	if [ -s "$user/accessibilityLog.csv.gz" ] && [ `need_to_update "$user/accessibilityLog.csv.gz" "$OUTPRE.${user:2}/accessibilityLog.csv.gz"` == 1 ]; then
		echo "in_app_class" >tmp-$$
		zcat -f $user/accessibilityLog.csv.gz | awk 'BEGIN{FS=","}{if(NR>1)print $2}' | ./app-grouper/app-grouper.py >>tmp-$$
		zcat -f $user/accessibilityLog.csv.gz | paste -d , - tmp-$$ | gzip >$OUTPRE.${user:2}/accessibilityLog.csv.gz
		copy_last_mod_time "$user/accessibilityLog.csv.gz" "$OUTPRE.${user:2}/accessibilityLog.csv.gz"
		echo -ne "\b:"
	fi
done
echo Done
rm -f tmp-$$


# 4. fix steps data starting/end date, duplicate timestamp, NaN values
echo -n "Patching steps data "
pycode="
import sys,math
import pandas as pd

steps = pd.read_csv(sys.argv[1])

# duplicate timestamp take average of StepsCount
steps = steps.groupby('timestamp', as_index=False).mean()

# clamps steps' datetime range
heart = pd.read_csv(sys.argv[2])
steps_nz = steps[steps.StepsCount>0]
t_max = max(heart.timestamp.max(), steps_nz.timestamp.max())
t_min = min(heart.timestamp.min(), steps_nz.timestamp.min())
steps = steps[(steps.timestamp>=t_min)&(steps.timestamp<=t_max)]

# conform heart's index to be the same as steps' index
steps = steps.set_index(pd.to_datetime(steps.timestamp, unit='ms'))
heart = heart[['HR']].set_index(pd.to_datetime(heart.timestamp, unit='ms'))
heart_cnt = heart.groupby(pd.Grouper(freq='1min')).count()
heart_cnt = heart_cnt.merge(pd.DataFrame(index=steps.index), how='right', left_index=True, right_index=True).fillna(0)

# set steps at invalid minutes to NaN
steps.loc[(steps.StepsCount==0)&(heart_cnt.HR==0), 'StepsCount'] = math.nan

print(steps.to_csv(index=False))
"
for user in $INPATH/*; do
	if [ -s "$user/steps.csv.gz" ] && [ -s "$user/heart.csv.gz" ] \
	  && [ `need_to_update "$user/steps.csv.gz" "$OUTPRE.${user:2}/steps.csv.gz"` == 1 ] \
	  && [ `need_to_update "$user/heart.csv.gz" "$OUTPRE.${user:2}/steps.csv.gz"` == 1 ]; then
		python3 -c "$pycode" "$user/steps.csv.gz" "$user/heart.csv.gz" | gzip >$OUTPRE.${user:2}/steps.csv.gz
		copy_last_mod_time "$user/steps.csv.gz" "$OUTPRE.${user:2}/steps.csv.gz"
		echo -n .
	fi
done
echo Done


### Rename some files to match down-stream processing script name
echo -n "Renaming files for down-stream feature processing "
srcs=(textsLog.csv.gz light.csv.gz sociabilityLog.csv.gz)
tgts=(smsLog.csv.gz ambientLight.csv.gz sociabilityMsgLog.csv.gz)
for user in $INPATH/*; do
	for i in `seq 0 $[${#srcs[*]}-1]`; do
		if [ -s "$OUTPRE.${user:2}/${srcs[i]}" ]; then
			mv -vf "$OUTPRE.${user:2}/${srcs[i]}" "$OUTPRE.${user:2}/${tgts[i]}"
		fi
	done
done

