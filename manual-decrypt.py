#!/usr/bin/env python3

import os, sys, argparse, gzip

from datetime import datetime, timedelta, time
from contextlib import redirect_stdout

import boto3, jinja2, psycopg2
import pandas as pd
from flask import Flask, render_template, redirect, request, session, abort
from raven.contrib.flask import Sentry
from werkzeug.middleware.proxy_fix import ProxyFix
from config import load_django
from api import *
from config.constants import CHECKABLE_FILES
from config.settings import *
from libs.s3 import *
from libs.encryption import *
from database.models import Researcher, Study, Participant


def Open(fn, mode='r', **kwargs):
	if fn == '-':
		return sys.stdin if mode.startswith('r') else sys.stdout
	fn = os.path.expanduser(fn)
	return gzip.open(fn, mode, **kwargs) if fn.lower().endswith('.gz') else open(fn, mode, **kwargs)


def decrypt_file(study_name_id, patient_id, in_filename, out_filename):
	with Open(in_filename, 'rb') as fp:
		file_data = fp.read()

	decrypted_data = decrypt(study_name_id, patient_id, file_data, '.gz' in in_filename.lower())

	with Open(out_filename, 'w') as fp:
		print(decrypted_data, end = '', file = fp)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(usage = '$0 <input-lines 1>output-lines\nOR: $0 study_name patient_id input_filename output_filename',
             description = 'manually decrypt user data files, each line is in the form of "study_name(study_id) patient_id input_filename output_filename"')
	parser.add_argument('args', help = 'the 4 arguments: study_name patient_id input_filename output_filename', nargs='*')
	opt = parser.parse_args()

	if len(opt.args) == 4:
		decrypt_file(*opt.args)
	else:
		while True:
			try:
				study_id, patient_id, in_filename, out_filename = input().split()[:4]
			except:
				break
			decrypt_file(study_id, patient_id, in_filename, out_filename)
