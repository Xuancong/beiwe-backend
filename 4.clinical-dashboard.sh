#!/bin/bash

if [ ! "$STUDY_ID" ]; then
	echo 'Error: STUDY_ID is not defined'
	exit 1
fi

OUTPATH=4.clinical-dashboard

mkdir -p $OUTPATH/$STUDY_ID

fn=`date "+%FT%T%z"`.json

echo -n "Generating clinical dashboard data: " >&2

./clinical-dashboard/generate-clinical-table.py --last-date `date '+%Y-%m-%d'` 3.decrypted/$STUDY_ID >$OUTPATH/$STUDY_ID/$fn

ln -sf $fn $OUTPATH/$STUDY_ID/last-clinical-table.json

# keep the latest 100 files
n=0
N=100
ls -alt $OUTPATH/$STUDY_ID | grep ^- | awk '{print $(NF)}' \
| while read file; do
    n=$[n+1]
    if [ $n -gt $N ]; then
      rm -f "$OUTPATH/$STUDY_ID/$file"
    fi
  done

