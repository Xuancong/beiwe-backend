# -*- coding: utf-8 -*-
import hashlib
import json

from django.db import models
from django.db.models import F, Func
from django.utils import timezone

from database.user_models import Researcher
from database.models import AbstractModel, JSONTextField
from config.constants import (ALL_DEVICE_SETTINGS, ALL_STUDY_SETTINGS)
from config.study_constants import (AUDIO_SURVEY_SETTINGS, DEFAULT_CONSENT_SECTIONS_JSON)
from database.validators import LengthValidator, hex_validator


class Study(AbstractModel):
	# When a Study object is created, a default DeviceSettings object is automatically
	# created alongside it. If the Study is created via the researcher interface (as it
	# usually is) the researcher is immediately shown the DeviceSettings to edit. The code
	# to create the DeviceSettings object is in database.signals.populate_study_device_settings.
	name = models.TextField(unique = True, help_text = 'Name of the study; can be of any length')
	encryption_key = models.CharField(max_length = 32, validators = [LengthValidator(32)],
	                                  help_text = 'Key used for encrypting the study data')
	object_id = models.CharField(max_length = 24, unique = True, validators = [LengthValidator(24)],
	                             help_text = 'ID used for naming S3 files')
	is_test = models.BooleanField(default = True)

	# Whether various device options are turned on
	type_map = {bool: 'Boolean', int: 'Integer', float: 'Float', str: 'Text'}
	for key, value in ALL_STUDY_SETTINGS:
		value_type = type_map[type(value)]
		kwargs = {'default': value, 'blank': True} if value_type == 'Text' else {'default': value}
		exec('%s = models.%sField(**kwargs)' % (key, value_type))


	@classmethod
	def create_with_object_id(cls, **kwargs):
		""" Creates a new study with a populated object_id field """

		study = cls(object_id = cls.generate_objectid_string("object_id"), **kwargs)
		study.save()
		return study

	@classmethod
	def get_all_studies_by_name(cls):
		""" Sort the un-deleted Studies a-z by name, ignoring case. """
		return (cls.objects
		        .filter(deleted = False)
		        .annotate(name_lower = Func(F('name'), function = 'LOWER'))
		        .order_by('name_lower'))

	def get_surveys_for_study(self):
		survey_json_list = []
		for survey in self.surveys.all():
			survey_dict = survey.as_native_python()
			# Make the dict look like the old Mongolia-style dict that the frontend is expecting
			survey_dict.pop('id')
			if survey_dict.pop('deleted'):
				continue
			survey_dict['_id'] = survey_dict.pop('object_id')
			survey_json_list.append(survey_dict)
		return survey_json_list

	def get_survey_ids_for_study(self, survey_type = 'tracking_survey'):
		return self.surveys.filter(survey_type = survey_type, deleted = False).values_list('id', flat = True)

	def get_survey_basic_info(self, survey_type = 'tracking_survey'):
		ret = self.surveys.filter(survey_type = survey_type, deleted = False).exclude(target_users__startswith='@')\
			.order_by('id').values_list('id', 'name', 'object_id', 'settings')
		return [i[0:-1]+(json.loads(i[-1]),) for i in ret]

	def get_device_settings(self):
		return self.device_settings

	def get_study_and_device_settings(self):
		ret = self.device_settings.as_native_python()
		ret.update({k: self.as_dict()[k] for k, v in ALL_STUDY_SETTINGS})
		return ret

	def get_researchers(self):
		return Researcher.objects.filter(studies = self)

	def reg_allow_list_get_userpass(self, username):
		res = [L.split()[1] for L in self.registration_allow_list.splitlines() if L.startswith(username+' ')]
		return res[0] if res else ''

	def reg_allow_list_set_userpass(self, username, password):
		res = [(username+' '+password if L.startswith(username+' ') else L) for L in self.registration_allow_list.splitlines()]
		self.registration_allow_list = '\n'.join(res)
		self.save()

class AbstractSurvey(AbstractModel):
	""" AbstractSurvey contains all fields that we want to have copied into a survey backup whenever it is updated. """

	AUDIO_SURVEY = 'audio_survey'
	TRACKING_SURVEY = 'tracking_survey'
	DUMMY_SURVEY = 'dummy'
	IMAGE_SURVEY = 'image_survey'
	SURVEY_TYPE_CHOICES = (
		(AUDIO_SURVEY, AUDIO_SURVEY),
		(TRACKING_SURVEY, TRACKING_SURVEY),
		(DUMMY_SURVEY, DUMMY_SURVEY),
		(IMAGE_SURVEY, IMAGE_SURVEY)
	)

	name = models.TextField(default = '', blank = True, help_text = 'Name of the survey')
	content = JSONTextField(default = '[]', help_text = 'JSON blob containing information about the survey questions.')
	survey_type = models.CharField(max_length = 16, choices = SURVEY_TYPE_CHOICES, help_text = 'What type of survey this is.')
	settings = JSONTextField(default = '{}', help_text = 'JSON blob containing settings for the survey.')
	timings = JSONTextField(default = json.dumps([[], [], [], [], [], [], []]),
	                        help_text = 'JSON blob containing the times at which the survey is sent.')
	target_users = models.TextField(default = '', blank = True, help_text = 'The list of users (separated by space) that it applies to, *: all users')

	# (hh:mm:ss : absolute expiry time in 24-hour format; ### : expire ### seconds after notification appears; empty : no expiry)
	expiry = models.TextField(default = '', blank = True, help_text = 'Survey expire time')

	# MD5 hash value of the content of the survey
	checksum = models.CharField(max_length=32, null = True, blank = True, unique=False, validators=[LengthValidator(32), hex_validator])

	class Meta:
		abstract = True


class Survey(AbstractSurvey):
	"""
	Surveys contain all information the app needs to display the survey correctly to a participant,
	and when it should push the notifications to take the survey.

	Surveys must have a 'survey_type', which is a string declaring the type of survey it
	contains, which the app uses to display the correct interface.

	Surveys contain 'content', which is a JSON blob that is unpacked on the app and displayed
	to the participant in the form indicated by the survey_type.

	Timings schema: a survey must indicate the day of week and time of day on which to trigger;
	by default it contains no values. The timings schema mimics the Java.util.Calendar.DayOfWeek
	specification: it is zero-indexed with day 0 as Sunday. 'timings' is a list of 7 lists, each
	inner list containing any number of times of the day. Times of day are integer values
	indicating the number of seconds past midnight.

	Inherits the following fields from AbstractSurvey
	content
	survey_type
	settings
	timings
	"""

	# This is required for file name and path generation
	object_id = models.CharField(max_length = 24, unique = True, validators = [LengthValidator(24)])
	# the study field is not inherited because we need to change its related name
	study = models.ForeignKey('Study', on_delete = models.PROTECT, related_name = 'surveys')

	@classmethod
	def create_with_object_id(cls, **kwargs):
		object_id = cls.generate_objectid_string("object_id")
		txt = f'[]{json.dumps([[], [], [], [], [], [], []])}{"{}"}'
		checksum = hashlib.md5(txt.encode('utf-8')).hexdigest()
		survey = cls.objects.create(object_id=object_id, checksum=checksum, **kwargs)
		return survey

	@classmethod
	def create_with_settings(cls, survey_type, **kwargs):
		"""
		Create a new Survey with the provided survey type and attached to the given Study,
		as well as any other given keyword arguments. If the Survey is audio and no other
		settings are given, give it the default audio survey settings.
		"""

		if survey_type == cls.AUDIO_SURVEY and 'settings' not in kwargs:
			kwargs['settings'] = json.dumps(AUDIO_SURVEY_SETTINGS)

		survey = cls.create_with_object_id(survey_type = survey_type, **kwargs)
		return survey


class SurveyArchive(AbstractSurvey):
	""" All felds declared in abstract survey are copied whenever a change is made to a survey """
	archive_start = models.DateTimeField()
	archive_end = models.DateTimeField(default = timezone.now)
	# two new foreign key references
	survey = models.ForeignKey('Survey', on_delete = models.PROTECT, related_name = 'archives')
	study = models.ForeignKey('Study', on_delete = models.PROTECT, related_name = 'surveys_archive')


class DeviceSettings(AbstractModel):
	"""
	The DeviceSettings database contains the structure that defines
	settings pushed to devices of users in of a study.
	"""

	# Whether various device options are turned on
	type_map = {bool: 'Boolean', int: 'Integer', float: 'Float', str: 'Text'}
	for params in ALL_DEVICE_SETTINGS:
		for key, value in params:
			value_type = type_map[type(value)]
			kwargs = {'default': value, 'blank': True} if value_type=='Text' else {'default': value}
			exec('%s = models.%sField(**kwargs)' % (key, value_type))

	# Special sections
	consent_sections = JSONTextField(default = DEFAULT_CONSENT_SECTIONS_JSON)

	study = models.OneToOneField('Study', on_delete = models.PROTECT, related_name = 'device_settings')
