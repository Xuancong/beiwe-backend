# This file needs to populate all the other models in order for django to identify that it has all the models
from collections import Counter

from .common_models import *
from .study_models import *
from .user_models import *
from .profiling_models import *
from .data_access_models import *

from datetime import datetime, timedelta

class OTPtracking(models.Model):
	study_user_id = models.CharField(max_length = 256)
	otp_value = models.CharField(max_length = 8)
	create_on = models.DateTimeField(auto_now = True)

	@classmethod
	def is_OTP_expired(cls, study_user_id, otp_expire_in_seconds):
		obj = cls.objects.filter(study_user_id = study_user_id)
		if not obj.exists():
			return True
		return (datetime.now() - obj.get().create_on).total_seconds() > otp_expire_in_seconds

	@classmethod
	def set_OTP(cls, study_user_id, OTP):
		obj = cls.objects.filter(study_user_id = study_user_id)
		if obj.exists():
			obj = obj.get()
			obj.otp_value = OTP
		else:
			obj = cls(study_user_id = study_user_id, otp_value=OTP)
		obj.save()

	@classmethod
	def get_OTP(cls, study_user_id):
		try:
			return cls.objects.get(study_user_id = study_user_id).otp_value
		except:
			return ''

	@classmethod
	def delete(cls, study_user_id):
		cls.objects.filter(study_user_id = study_user_id).delete()

	@classmethod
	def shrink(cls, expire_seconds):
		start_date = datetime.now()-timedelta(seconds = expire_seconds)
		cls.objects.filter(create_on__lt = start_date).delete()

# def debug_OTP():
# 	# must be called from __main__ in app.py
# 	import sys
# 	key = 'test01 xuancong84@gmail.com'
# 	res = OTPtracking.is_OTP_expired(key)
# 	if res:
# 		OTPtracking.set_OTP(key, 'asd23r54')
# 	else:
# 		OTPtracking.delete(key)
# 	sys.exit(0)