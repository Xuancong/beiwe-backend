#!/bin/bash

if [ ! "$STUDY_ID" ]; then
	echo "Error: STUDY_ID is not defined"
	exit 1
fi

INPATH=1.decrypted/$STUDY_ID
OUTPRE=2

# ADDED: filter duplicate main sleep for Fitbit
pycode="
import os,sys
basepath=sys.argv[1]
progbar='|/-\\\\'
sleephash=set()
for ii,fn in enumerate(sys.stdin):
	fulln = basepath+'/'+fn.strip()
	txt = open(fulln).read()
	its = txt.splitlines()
	if 'sleep' in basepath:
		import hashlib
		hash = str(len(txt))+':'+hashlib.md5(txt.encode('utf8', 'ignore')).hexdigest()
		if hash in sleephash:
			continue
		import pandas as pd
		df = pd.read_csv(fulln)
		S = set(df[df.Level!='main'].timestamp)
		s = set(df[df.Level=='main'].timestamp)
		if not S&s:
			continue
		sleephash.add(hash)
	if ii==0 or len(its)>1:
		print('\n'.join(its[(0 if ii==0 else 1):]))
	print(progbar[ii%4], file=sys.stderr, end='\\b', flush=True)
"

py_limit_time_gap="
import os,sys
i=0
last_tms=0
th=int(sys.argv[1])
for L in sys.stdin:
	if i==0:
		i+=1
		print(L.strip())
		continue
	tms = int(L.strip().split(',')[0])
	if tms-last_tms<th:
		continue
	print(L.strip())
	last_tms = tms
"

body() {
	IFS= read -r header
	printf '%s\n' "$header"
	"$@"
}

need_to_update() {
	# Usage: $0 input_file [output_file1 output_file2 ...]
	# It returns 1 if and only if input_file is updated within 1 day and every output file exist and non-empty
	fin="$1"
	shift
	while [ $# -gt 0 ]; do
		if [ ! -s "$1" ]; then
			echo 1
			return
		fi
		shift
	done
	days_til_now=$(python3 -c "import os,sys; import pandas as pd;\
    print((pd.Timestamp.now()-pd.Timestamp(os.stat('$fin').st_mtime, unit='s'))/pd.to_timedelta('1D'))")
	echo "$days_til_now < 1" | bc
}

copy_last_mod_time() {
	# Usage: $0 src tgt [tgt2 ...]
	# It copies the last modification time from src to tgt
	fin="$1"
	shift
	while [ $# -gt 0 ]; do
		python3 -c "import os,sys;import pandas as pd;st=os.stat('$fin');os.utime('$1', (st.st_atime, st.st_mtime))"
		shift
	done
}

# aggregate data files for every participant
locs=()
for patient in $INPATH/*; do
	echo -n "Aggregating for $(basename $patient) "
	mkdir -p $OUTPRE.${patient:2}
	{
		echo "{"
		suffix=
		for fea in $patient/*; do
			echo -ne "$suffix\"$(basename $fea)\":["

			# skip if no change
			if [ $(need_to_update "$fea" "$OUTPRE.${fea:2}.csv.gz") == 1 ]; then
				anyfn="$fea/$(ls $fea | head -1)"
				col=$(cat $anyfn | head -1 | python3 -c "print(input().split(',').index('timestamp')+1)")
				if [[ "$fea" =~ /accel$ ]]; then
					# limit number of accel readings
					ls $fea | sort | python3 -c "$pycode" $fea | body sort -t , -n -k $col | uniq |
						python3 -c "$py_limit_time_gap" 400 | gzip >$OUTPRE.${fea:2}.csv.gz
				else
					ls $fea | sort | python3 -c "$pycode" $fea | body sort -t , -n -k $col | uniq | gzip >$OUTPRE.${fea:2}.csv.gz
				fi
				copy_last_mod_time "$fea" "$OUTPRE.${fea:2}.csv.gz"
			fi
			if [[ "$fea" =~ /gps$ ]] && [ $(need_to_update "$fea" "$OUTPRE.${fea:2}-patch2.csv.gz" "$OUTPRE.${fea:2}-mobility.csv.gz") == 1 ]; then
				locs+=("$fea")
				zcat -f $OUTPRE.${fea:2}.csv.gz | sed "s:^timestamp:recv-timestamp:g; s:locationTime:timestamp:1" |
					gzip >$OUTPRE.${fea:2}-patch1.csv.gz
				./patch-gps.py $OUTPRE.${fea:2}-patch1.csv.gz $OUTPRE.${fea:2}-patch2.csv.gz
				copy_last_mod_time "$fea" "$OUTPRE.${fea:2}-patch2.csv.gz"
			fi

			ls $fea | sort | sed "s:\..*::g" | tr '\n' ' ' | awk '{$1=$1;printf $0"]"}' | sed 's: :,:g'
			echo -n . >&2
			suffix=",\n"
		done
		echo -e "\n}"
	} >$OUTPRE.${patient:2}/meta.json
	gzip -f $OUTPRE.${patient:2}/meta.json
	echo OK
done

# compute Beiwe mobility features for every participant, multi-thread
for fea in ${locs[*]}; do
	echo "Computing mobility feature for $(dirname $fea)"
	Rscript gps2mobility.R $OUTPRE.${fea:2}-patch2.csv.gz $OUTPRE.${fea:2}-mobility.csv &
	while [ "$(ps aux | grep gps2mobility.R | wc -l)" -ge 7 ]; do
		sleep 1
	done
done
wait

# imbue the timestamp column for aggregated mobility feature files
RED='\033[1;33m'
NC='\033[0m'
for fea in ${locs[*]}; do
	if [ -s "$OUTPRE.${fea:2}-mobility.csv" ]; then
		echo "Adding timestamp column for $fea"
		./add-timestamp-from-date.py $OUTPRE.${fea:2}-mobility.csv $OUTPRE.${fea:2}-mobility.csv.gz
		copy_last_mod_time "$fea" "$OUTPRE.${fea:2}-mobility.csv.gz"
	else
		echo -e "${RED}Warning: Mobility feature for $fea is missing!${NC}"
	fi
done
