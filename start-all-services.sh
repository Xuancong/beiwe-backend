
session_name=HOPES
cmds=(
"top"
"mkdir -p db.s3-bucket && if [ $(which node) ]; then node ./third-party/s3rver/bin/s3rver.js -d db.s3-bucket; else ./third-party/s3rver/bin/node ./third-party/s3rver/bin/s3rver.js -d db.s3-bucket; fi"
"if [ ! -d ./db.psql ]; then ./scripts/reset-postgresql-db.sh; fi; ./start-postgresql-server.sh"
"#PATH=$HOME/anaconda3/bin:$PATH ./run.sh"
)

if [ "`tmux ls | grep $session_name`" ]; then
	echo "The service already started!" >&2
	exit 1
fi

tmux new-session -s $session_name -d -x 240 -y 60

for i in `seq 0 $[${#cmds[*]}-1]`; do
	sleep 0.5
	tmux select-layout tile
	sleep 0.5
	tmux send-keys -l "${cmds[i]}"
	sleep 0.5
	tmux send-keys Enter
	if [ $i != $[${#cmds[*]}-1] ]; then
		sleep 0.5
		tmux split-window
	fi
done


