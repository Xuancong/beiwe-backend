import base64
import boto3
import os
import json
from Crypto.Cipher import AES
from Crypto.PublicKey import RSA

study = os.getenv('STUDY_ID')
s3 = boto3.client('s3',
                  aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
                  aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'))
sm = boto3.client('secretsmanager',
                  region_name='ap-southeast-1',
                  aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
                  aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'))

s3_bucket = 'nikola-test-prefix-beiwe'
s3_prefix = study + '/'
s3_suffix = '.raw'


def get_private_key(secret_name):
    response = sm.get_secret_value(SecretId=secret_name)
    return json.loads(response['SecretString'])['private']


def remove_PKCS5_padding(data_string):
    return data_string[0: -ord(data_string[-1])]


def fetch_key(bucket=s3_bucket, prefix=s3_prefix, suffix=s3_suffix):
    kwargs = {'Bucket': bucket}

    if isinstance(prefix, str):
        kwargs['Prefix'] = prefix

    while True:
        resp = s3.list_objects_v2(**kwargs)
        for objContents in resp['Contents']:
            s3_key = objContents['Key']
            if not s3_key.startswith(prefix) or not s3_key.endswith(suffix):
                continue
            study_name, patient, feature_name, time_stamp = s3_key.split('/')
            time_stamp = time_stamp.split('.')[0]
            directory = 'decrypted/' + study_name + '/' + patient + '/' + feature_name + '/'
            name = time_stamp + '.csv'
            file_exists = False
            if os.path.exists(directory + name):
                file_exists = True
            if not file_exists:
                yield objContents
        try:
            kwargs['ContinuationToken'] = resp['NextContinuationToken']
        except KeyError:
            break


for key in fetch_key():
    study, patient_id, feature, timestamp = key['Key'].split('/')
    if "009" not in patient_id:
        continue
    # if "bluetooth" not in feature:
    #     continue
    if "wifi" not in feature:
        continue
    timestamp = timestamp.split('.')[0]
    file_dir = 'decrypted/' + study + '/' + patient_id + '/' + feature + '/'
    file_name = timestamp + '.csv'
    sm_cache = {}
    if not os.path.isdir(file_dir):
        os.makedirs(file_dir)

    write_here = open(file_dir + file_name, 'w+')

    if patient_id not in sm_cache.keys() or sm_cache[patient_id] is None:
        sm_cache[patient_id] = get_private_key('private/' + study + '/' + patient_id)

    original_data = s3.get_object(Bucket=s3_bucket, Key=key['Key'])
    private_key = RSA.importKey(sm_cache[patient_id])

    file_data = [line for line in original_data['Body'].read().split('\n') if line != '']

    decoded_key = base64.urlsafe_b64decode(file_data[0].encode('utf-8'))
    decrypted_key = base64.urlsafe_b64decode(private_key.decrypt(decoded_key))

    for i, data in enumerate(file_data):
        if i == 0 or i == 1:
            continue

        if data is None:
            continue

        iv, data = data.split(':')
        iv = base64.urlsafe_b64decode(iv.encode('utf-8'))
        data = base64.urlsafe_b64decode(data.encode('utf-8'))
        decrypted = AES.new(decrypted_key, mode=AES.MODE_CBC, IV=iv).decrypt(data)
        returning = remove_PKCS5_padding(decrypted)
        if returning == '':
            continue

        write_here.write(returning)

    write_here.close()
