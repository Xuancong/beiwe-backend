#!/usr/bin/env python3
# coding=utf-8

import os, sys, argparse, re, gzip
import pandas as pd
from dateutil.tz import tzlocal

def proc(df):
	df['timestamp'] = df.Date.apply(lambda t:int(pd.Timestamp(t, tzinfo=tzlocal()).timestamp())*1000)
	return df[['timestamp']+[col for col in df.columns if col!='timestamp']]

def Open(fn, mode='r'):
	if fn=='-':
		return sys.stdin if mode.startswith('r') else sys.stdout
	return gzip.open(fn, mode) if fn.lower().endswith('.gz') else open(fn, mode)

if __name__=='__main__':
	parser = argparse.ArgumentParser(usage='$0 input-file output-file 1>progress 2>warning_msg',
									 description='This program processes features, use "-" for STDIN/STDOUT')
	parser.add_argument('input_fn', help='positional argument')
	parser.add_argument('output_fn', help='positional argument')
	parser.add_argument('-optional', help='optional argument')
	#nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt=parser.parse_args()
	globals().update(vars(opt))

	df = proc(pd.read_csv(Open(input_fn)))
	Open(output_fn, 'w').write(df.to_csv(index=False)) if output_fn == '-' else df.to_csv(output_fn, index=False)
