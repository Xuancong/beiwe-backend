# How to decrypt the encrypted files uploaded to S3 bucket by the client

### Step 1: Setting up the repo
- Clone the `HOPES backend project`

### Step 2: Create temporary IAM user
- Sign-in to the AWS console
	> Make sure you have the `administrator access` to create the IAM user
- Go to `IAM` service
- Create `IAM User` that has read access to the following HOPES
	> `S3 bucket` where the encrypted data files are being uploaded
	> `Secrets Manager` where the encryption private keys are available
- Note down the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_KEY_ACCESS`

### Step 3: Run the decryption script
- Open `decryptor.py` file from the `project`
- Set the `s3_bucket` variable to the specific `S3 HOPES upload data bucket`
- Run the following command on `bash` shell
	> `cd <project>`
	> `AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY_ID> AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY> STUDY_ID=<STUDY_ID> python decryptor.py`
- Wait for the program to finish
	> It might take some time if there are lot of files
- Once finished, check the `decrypted` folder created for the decrypted data


### Step 4: Delete temporary IAM user
- Once the decryption is complete, sign in to `AWS console` using the same credentials used for IAM user creation
- Go to `IAM` service
- Click `Users`
- Delete the `temporary user` created for the purpose fo decryption
