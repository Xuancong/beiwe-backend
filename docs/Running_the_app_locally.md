# Running the backend `ADMIN UI` server locally

### Step 1: Docker installation and local Postgres setup
- Install `Docker for Desktop`
- Install `Kitematic`
- Search for `postgres` container
- Click `Create` on the latest `Official postgres` container
- Go to `Settings -> General` and add the following to the `Environment Variables`
	> `POSTGRES_USER <POSTGRES_USER>`
	> `POSTGRES_PASSWORD <POSTGRES_PASSWORD>`
- Go to `Settings -> Hostname/Ports` and chnage the default `PUBLISHED IP:PORT` to use `5432`
- Go to `Home` and click `Restart`
	> Verify Access URL is `localhost:5432`

### Step 2: Create temporary IAM user
- Sign-in to the AWS console
	> Make sure you have the `administrator access` to create the IAM user
- Go to `IAM` service
- Create `IAM User` that has `read/write` access to the following HOPES
	> `S3 bucket` where the encrypted data files should be uploaded
	> `Secrets Manager` where the encryption public and private keys are created
- Note down the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_KEY_ACCESS`

### Step 3: Create S3 Bucket
- Sign-in to the AWS console
	> Make sure you have the `administrator access` to create the S3 bucket
- Go to `S3` storage service
- Create a new bucket
	> Bucket name referred as `<S3_BUCKET_NAME>`
	> Bucket endpoint URL referred as `<S3_BUCKET_ENDPOINT_URL>`
 
### Step 4: Change config file to updated values
- Go to the `project` folder
- Open `config/remote_db_env.py` file
- Change the following lines
	> `os.environ['DOMAIN_NAME'] = <Backend URL to be populated on the QR code>`
	> `os.environ['RDS_PASSWORD'] = <POSTGRES_PASSWORD>`
	> `os.environ['RDS_USERNAME'] = <POSTGRES_USER>`
	> `os.environ['RDS_DB_NAME'] = <DATABASE_NAME>`
	> `os.environ['S3_BUCKET'] = <S3_BUCKET_NAME>`
	> `os.environ['S3_ENDPOINT_URL'] = <S3_BUCKET_ENDPOINT_URL>`
	> `os.environ['S3_ACCESS_CREDENTIALS_USER'] = <AWS_ACCESS_KEY_ID>`
	> `os.environ['S3_ACCESS_CREDENTIALS_KEY'] = <AWS_SECRET_KEY_ACCESS>`
	> `os.environ['SECRETS_MANAGER_ACCESS_CREDENTIALS_USER'] = <AWS_ACCESS_KEY_ID>`
	> `os.environ['SECRETS_MANAGER_ACCESS_CREDENTIALS_KEY'] = <AWS_SECRET_KEY_ACCESS>`

### Step 5: Run the app
> Note: Make sure we have Python3 and pip installed
- Go to `bash` shell
- Go to the `project` folder
- Run the following command
	> `pip install -r requirements.txt`
	> `python app.py`
- Now the app is up and running
	> Access the app through the URL provided on the console
	> Initial login for the app running locally will be `default_admin` and `abc123`

### Step 6: Delete temporary IAM user
- Once the development work is complete, sign in to `AWS console` using the same credentials used for IAM user creation
- Go to `IAM` service
- Click `Users`
- Delete the `temporary user` created for the purpose fo decryption
