const crypto = require("crypto");
const AWS = require("aws-sdk");
const QRCode = require("qrcode");
const hbs = require("handlebars");
const request = require("request-promise-native");
const fs = require("fs");
const pool = require("./pool.json");
const used = require("./used.json");

const smCreds = new AWS.Credentials({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});
const cmCreds = new AWS.Credentials({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});

const sm = new AWS.SecretsManager({
  credentials: smCreds,
  region: "ap-southeast-1"
});
const acm = new AWS.ACM({ credentials: cmCreds, region: "ap-southeast-1" });

function generateRandom(length = 8) {
  const buffer = crypto.randomBytes(256);
  return buffer
    .toString("hex")
    .substr(0, length)
    .toUpperCase()
    .match(/.{1,4}/g)
    .join(" ");
}

(async () => {
  const candidates = pool.filter(c => !used.includes(c));
  for (let candidate of candidates) {
    used.push(candidate);
    fs.writeFileSync("./used.json", JSON.stringify(used));

    const CA_DOMAIN = ".mohtgroup.com";
    const CA_ARN =
      "arn:aws:acm-pca:ap-southeast-1:309179128206:certificate-authority/70caabae-a28e-4630-a6b8-e3d52c0e895d";
    const STUDY_ID = 4;
    const STUDY_ID_HASH = "J1ZttI48Y3YR41NZhBikK6C6";
    const HOPES_URL = "https://mxeicoqioghzxhrkdenwpmba.study.mohtgroup.com";
    const HOPES_UI_URL = "https://admin-ui.study.mohtgroup.com";
    const DAYS_TO_END = 300;
    const DATE_NOW = new Date();
    const DATE_END = new Date();

    DATE_END.setDate(DATE_END.getDate() + DAYS_TO_END);

    const meta = {
      startDate: DATE_NOW.toISOString(),
      endDate: DATE_END.toISOString(),
      studyId: STUDY_ID_HASH,
      hopesURL: HOPES_URL,
      fitbitId: candidate,
      lastThree: candidate.split("@")[0].split(".")[2],
      hopesId:
        candidate +
        "_" +
        generateRandom(12)
          .toLowerCase()
          .replace(/ /g, ""),
      fitbitPassword: generateRandom(16),
      hopesPassword: generateRandom(12)
    };
    console.log(`creating user ${meta.hopesId}`);

    const hopesPasswordNext = generateRandom(12).replace(/ /g, "");
    const hopesPasswordReregister = generateRandom(12).replace(/ /g, "");

    const qrMeta = {
      hashIteration: Math.floor(
        Math.random() * (60000 - 40000) + 40000
      ).toFixed(0),
      hashKey: crypto.randomBytes(64).toString("base64"),
      latitudeOffset: Math.random() * 1000.0 - 500.0,
      longitudeOffset: Math.random() * 1000.0 - 500.0,
      uid: meta.hopesId,
      utp: meta.hopesPassword.replace(/ /g, ""),
      step: 0,
      url: HOPES_URL.replace("https://", "")
    };

    try {
      await request.post({
        url: `${HOPES_UI_URL}/validate_login`,
        form: {
          username: "default_admin",
          password: "abc123"
        },
        jar: true,
        followAllRedirects: true
      });
      await request.post({
        url: `${HOPES_UI_URL}/create_defined_patient`,
        formData: {
          participant_id: meta.hopesId,
          set_password: meta.hopesPassword.replace(/ /g, ""),
          study_id: STUDY_ID
        },
        jar: true
      });
      const { SecretString } = await sm
        .getSecretValue({ SecretId: `public/${STUDY_ID_HASH}/${meta.hopesId}` })
        .promise();
      const secret = JSON.parse(SecretString);
      secret.fitbitId = meta.fitbitId;
      secret.fitbitPassword = meta.fitbitPassword.replace(/ /g, "");
      await sm
        .updateSecret({
          SecretId: `public/${STUDY_ID_HASH}/${meta.hopesId}`,
          SecretString: JSON.stringify(secret)
        })
        .promise();

      const DomainName = `${meta.lastThree}.${meta.studyId}.study${CA_DOMAIN}`;
      const { CertificateArn } = await acm
        .requestCertificate({ DomainName, CertificateAuthorityArn: CA_ARN })
        .promise();
      await new Promise(resolve => {
        setTimeout(resolve, 7000);
      });
      const { Certificate, PrivateKey } = await acm
        .exportCertificate({ CertificateArn, Passphrase: meta.hopesId })
        .promise();

      const UnencryptedPrivateKey = crypto.createPrivateKey({
        key: PrivateKey,
        passphrase: meta.hopesId
      });

      meta.qrCode = await QRCode.toDataURL(
        JSON.stringify({ ...qrMeta, unp: hopesPasswordNext }),
        {
          scale: 5,
          errorCorrectionLevel: "L"
        }
      );

      meta.qrReregister = await QRCode.toDataURL(
        JSON.stringify({
          ...qrMeta,
          step: 1,
          utp: hopesPasswordNext,
          unp: hopesPasswordReregister
        }),
        {
          scale: 5,
          errorCorrectionLevel: "L"
        }
      );
      meta.cert = await QRCode.toDataURL(
        JSON.stringify({
          step: 3,
          body: Certificate
        }),
        {
          scale: 5,
          errorCorrectionLevel: "L"
        }
      );
      // TODO: important to get this from the exported NGINX truststore chain
      meta.chain = await QRCode.toDataURL(
        JSON.stringify({
          step: 2,
          body: fs.readFileSync("./external.pem", "utf8")
        }),
        {
          scale: 5,
          errorCorrectionLevel: "L"
        }
      );
      meta.key = await QRCode.toDataURL(
        JSON.stringify({
          step: 4,
          body: UnencryptedPrivateKey.export({ type: "pkcs1", format: "pem" })
        }),
        {
          scale: 5,
          errorCorrectionLevel: "L"
        }
      );
      meta.public = await QRCode.toDataURL(
        JSON.stringify({ step: 5, body: secret.public }),
        {
          scale: 5,
          errorCorrectionLevel: "L"
        }
      );

      const template = hbs.compile(fs.readFileSync("./with-keys.html", "utf8"));
      fs.writeFileSync(`./templates/${meta.hopesId}.html`, template(meta));
    } catch (e) {
      console.error(e);
    }
  }
})();
