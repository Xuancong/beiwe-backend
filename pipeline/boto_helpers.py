import os
import json
import boto3

# This component of pipeline is part of the Beiwe server, the correct import is 'from pipeline.xyz...'
from pipeline.configuration_getters import get_current_region


def get_aws_object_names():
    configs_folder = get_configs_folder()
    with open(os.path.join(configs_folder, 'aws-object-names.json')) as fn:
        return json.load(fn)


def get_boto_client(client_type, pipeline_region=None):
    from config.settings import BEIWE_SERVER_AWS_ACCESS_KEY_ID, BEIWE_SERVER_AWS_SECRET_ACCESS_KEY
    region_name = pipeline_region or get_current_region()

    return boto3.client(
        client_type,
        aws_access_key_id=BEIWE_SERVER_AWS_ACCESS_KEY_ID,
        aws_secret_access_key=BEIWE_SERVER_AWS_SECRET_ACCESS_KEY,
        region_name=region_name,
    )


def get_pipeline_folder():
    return os.path.abspath(__file__).rsplit('/', 1)[0]


def get_configs_folder():
    return os.path.join(get_pipeline_folder(), 'configs')
