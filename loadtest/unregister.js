const request = require("request-promise-native");
const fs = require("fs");

console.log("Unregistering Test Participants");

if (!process.env.STUDY_ID) {
  console.error(
    "Please provide a STUDY_ID from the Beiwe Backend. e.g, STUDY_ID=7 yarn loadtest"
  );
  process.exit(1);
}

const studyId = process.env.STUDY_ID;

(async () => {
  try {
    await request.post({
      url: "https://moht-beta-ui.mohtgroup.com/validate_login",
      form: {
        username: "default_admin",
        password: "abc123"
      },
      jar: true,
      followAllRedirects: true
    });
    const participants = fs
      .readFileSync("./participants.csv", "utf8")
      .split("\r\n")
      .splice(1)
      .filter(participant => participant !== "")
      .map(participant => {
        const d = participant.split(",");
        request
          .post({
            url: "https://moht-beta-ui.mohtgroup.com/reset_device",
            form: {
              patient_id: d[0],
              study_id: studyId
            },
            jar: true,
            followAllRedirects: true
          })
          .then(() => console.log(`${d[0]} is unregistered`))
          .catch(err => console.error(`${d[0]} failed`));
      });
  } catch (err) {
    console.error(err);
  }
})();
