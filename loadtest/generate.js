const request = require("request-promise-native");
const fs = require("fs");

console.log("Creating Test Participants");

if (!process.env.STUDY_ID) {
  console.error(
    "Please provide a STUDY_ID from the Beiwe Backend. e.g, STUDY_ID=7 yarn loadtest"
  );
  process.exit(1);
}

const studyId = process.env.STUDY_ID;

(async () => {
  try {
    await request.post({
      url:
        "http://hopes-beta-ui.ap-southeast-1.elasticbeanstalk.com/validate_login",
      form: {
        username: "default_admin",
        password: "abc123"
      },
      jar: true,
      followAllRedirects: true
    });
    const r = request.post({
      url: `http://hopes-beta-ui.ap-southeast-1.elasticbeanstalk.com/create_many_patients/${studyId}`,
      formData: {
        number_of_new_patients: 20,
        desired_filename: "load_test.csv"
      },
      jar: true
    });
    r.on("response", function(res) {
      res.pipe(fs.createWriteStream("participants.csv"));
    });
  } catch (err) {
    console.error(err);
  }
})();
