const crypto = require("crypto");
const fs = require("fs");
const files = fs.readdirSync(__dirname + "/files");

const participants = fs
  .readFileSync("./participants.csv", "utf8")
  .split("\r\n")
  .splice(1)
  .filter(participant => participant !== "")
  .map(participant => {
    const d = participant.split(",");
    return {
      username: d[0],
      password: d[1]
    };
  });

function createHash(password) {
  return crypto
    .createHash("sha256")
    .update(password, "utf8")
    .digest("base64")
    .replace(/\//g, "_")
    .replace(/\+/g, "-");
}

function generateDevice(uid) {
  return {
    bluetooth_id: createHash(uid + "BLE"),
    phone_number: createHash(uid + "PN"),
    device_id: createHash(uid),
    device_os: "Android",
    os_version: 9,
    hardware_id: "samsungexynos8895",
    brand: "samsung",
    manufacturer: "samsung",
    model: "SM-N950F",
    product: "greatltexx",
    beiwe_version: "34-development"
  };
}

module.exports = {
  pickParticipant: (context, ee, next) => {
    context.vars["participant"] =
      participants[Math.floor(Math.random() * participants.length)];
    return next();
  },
  registerUser: (req, context, ee, next) => {
    const device = generateDevice(context.vars["participant"].username);
    context.vars["participant"].device_id = device.device_id;
    req.form = {
      patient_id: context.vars["participant"].username,
      new_password: context.vars["participant"].password,
      password: createHash(context.vars["participant"].password),
      ...device
    };
    return next();
  },
  uploadRandomLogFiles: (req, context, ee, next) => {
    const file_name = files[Math.floor(Math.random() * files.length)];
    const fname = file_name.split("_");
    const name = [
      context.vars["participant"].username,
      fname[1],
      new Date().getTime()
    ].join("_");
    req.formData = {
      device_id: context.vars["participant"].device_id,
      file_name: name,
      patient_id: context.vars["participant"].username,
      password: createHash(context.vars["participant"].password),
      file: fs.createReadStream(__dirname + "/files/" + file_name)
    };
    return next();
  },
  response: (req, res, context, ee, next) => {
    if (res.statusCode !== 200) {
      console.log(
        context.vars["participant"].username,
        context.vars["participant"].device_id,
        req.url,
        res.statusCode,
        req.formData ? req.formData.file_name : null
      );
    }
    return next();
  }
};
