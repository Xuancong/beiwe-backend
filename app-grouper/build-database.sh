#!/bin/bash

cat apps.list | while read app; do
	if [ "$app" == in_app_name ]; then
		continue
	fi
	cls="`echo $app | ./app-grouper.py`"
	echo "$app => $cls"
done

