#!/usr/bin/env python3
# coding=utf-8

import os, sys, argparse, re, time, random, html
import urllib.parse
import urllib.request


classes = ['android system', 'social messenger', 'social media', 'entertainment',
		   'map navigation', 'utility tools', 'games']

PS_class_map = {
# Exceptional Apps
	"com.google.android.gm" : "social messenger",
	"com.android.chrome" : "social media",
	"org.mozilla.firefox" : "social media",
	"com.cloudmosa.puffinTV" : "social media",
	"mobi.mgeek.TunnyBrowser" : "social media",

# Sub-category remap
	"finance" : "utility tools",


# Google's category to our category
	"Art & Design" : "utility tools",
	"Augmented Reality" : "utility tools",
	"Auto & Vehicles" : "map navigation",
	"Beauty" : "utility tools",
	"Books & Reference" : "utility tools",
	"Business" : "finance",
	"Comics" : "games",
	"Communication" : "social messenger",
	"Dating" : "social messenger",
	"Daydream" : "utility tools",
	"Education" : "entertainment",
	"Entertainment" : "entertainment",
	"Events" : "entertainment",
	"Finance" : "finance",
	"Food & Drink" : "map navigation",
	"Health & Fitness" : "utility tools",
	"House & Home" : "utility tools",
	"Libraries & Demo" : "android system",
	"Lifestyle" : "utility tools",
	"Maps & Navigation" : "map navigation",
	"Medical" : "utility tools",
	"Music & Audio" : "entertainment",
	"News & Magazines" : "social media",
	"Parenting" : "entertainment",
	"Personalization" : "android system",
	"Photography" : "entertainment",
	"Productivity" : "utility tools",
	"Shopping" : "finance",
	"Social" : "social media",
	"Sports" : "social media",
	"Tools" : "utility tools",
	"Travel & Local" : "map navigation",
	"Video Players & Editors" : "entertainment",
	"Wear OS by Google" : "utility tools",
	"Weather" : "utility tools",

# Google's game categories
	"Action" : "games",
	"Adventure" : "games",
	"Arcade" : "games",
	"Board" : "games",
	"Card" : "games",
	"Casino" : "games",
	"Casual" : "games",
	"Educational" : "games",
	"Music" : "games",
	"Puzzle" : "games",
	"Racing" : "games",
	"Role Playing" : "games",
	"Simulation" : "games",
	"Sports" : "games",
	"Strategy" : "games",
	"Trivia" : "games",
	"Word" : "games"
}

# do self-check
M = lambda c: (M(PS_class_map[c]) if c in PS_class_map else c)
assert {M(c) for c in PS_class_map.values()} == set(classes)


def get_webpage(url):
	error_cnt = 0
	while True:
		try:
			with urllib.request.urlopen(url) as resp:
				ret = resp.read()
			if error_cnt:
				print(file=sys.stderr)
			return ret
		except Exception as e:
			if '404' in str(e):
				return b''
			if error_cnt == 0:
				print('HTTP error when getting', url, end='', file=sys.stderr)
			error_cnt += 1
			print('.', end='', file=sys.stderr)
			time.sleep(1+random.random()*5)

def get_next_textfield(s, start_posi):
	ret = ''
	while ret == '':
		p = s.find('>', start_posi)
		if p < 0:
			return ''
		p1 = s.find('<', p+1)
		ret = s[p+1:p1].strip()
		start_posi = p1+1
	return html.unescape(ret)

def get_playstore_cls(pkgName):
	url = 'https://play.google.com/store/apps/details?id='+urllib.parse.quote(pkgName)
	txt0 = get_webpage(url)
	txt = txt0.decode('utf8', 'ignore')

	# get App category name
	res = [get_next_textfield(txt, m.end()) for m in re.finditer('"hrTbp R8zArc"', txt)]
	try:
		cat_name = res[1]
		assert cat_name
	except:
		print('Warning: cannot get APP category from Google Play Store for', pkgName, file=sys.stderr)
		cat_name = 'android system'

	# get App full name
	app_name = ''
	try:
		posi = txt.find('"AHFaub"')
		app_name = get_next_textfield(txt, posi)
	except:
		print('Warning: cannot get APP name from Google Play Store for', pkgName, file=sys.stderr)

	return cat_name, app_name

def check(s, S):
	for s1 in S:
		if s1 in s:
			return True
	return False

def get_cls(pkgName):
	if pkgName in PS_class_map:
		cls = PS_class_map[pkgName]
	elif pkgName.startswith('com.android.'):
		cls = classes[0]
	else:
		cat_name, app_name = get_playstore_cls(pkgName)
		if cat_name == 'Communication' and check(app_name.lower(), ['browser', 'explorer']):
			cls = 'social media'
		else:
			cls = cat_name

	while cls in PS_class_map:
		cls = PS_class_map[cls]
	return cls

def load_lib(fn=None):
	if fn is None:
		fn = re.sub('.[a-zA-Z]+$', '', sys.argv[0]) + '.json'
	if os.path.isfile(fn):
		try:
			return eval(open(fn).read())
		except:
			pass
	return {}

def save_lib(fn=None):
	if fn is None:
		fn = re.sub('.[a-zA-Z]+$', '', sys.argv[0]) + '.json'
	fp = open(fn, 'w')
	print(str(M), file=fp)


if __name__=='__main__':
	parser = argparse.ArgumentParser(usage='$0 <input 1>output 2>progress', description='Get the class for each app')
	parser.add_argument('-file', help='The package_name-to-class mapping file in JSON format', type=str, default=None)
	#nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt=parser.parse_args()
	globals().update(vars(opt))

	M = load_lib(file)
	# res1 = get_cls('com.orange.kidspiano.music.songs')
	# res2 = get_cls('com.huawei.android.launcher')

	while True:
		try:
			L = input()
		except:
			break
		pkgName = L.strip()
		if pkgName not in M:
			M[pkgName] = get_cls(pkgName)
			save_lib(file)
		print(M[pkgName])
