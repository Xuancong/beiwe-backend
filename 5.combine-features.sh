#!/bin/bash

if [ ! "$STUDY_ID" ]; then
    echo "Error: STUDY_ID is not defined"
    exit 1
fi

mkdir -p 5.decrypted/$STUDY_ID
./feature-processor/combine-all.py $* 4.decrypted/$STUDY_ID -o 5.decrypted/$STUDY_ID -op 5.decrypted/$STUDY_ID/all-data.pson.gz | gzip>5.decrypted/$STUDY_ID/all-data.csv.gz


