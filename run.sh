#!/usr/bin/env bash


cd `dirname $0`

if [ -s secret.sh ]; then
	if [ ! "`grep export secret.sh`" ]; then
		echo "Error: in secret.sh, environment variables must be defined using 'export'"
		exit 1
	fi
	echo -n "Sourcing secret.sh for setting up credentials ... "
	source secret.sh
	echo "Done"
fi

python3 app.py 2>&1 | python3 -c "
import os,sys
while True:
	L=sys.stdin.readline()
	if not L:
		break
	print(L.strip(), file=sys.stdout, flush=True)
	print(L.strip(), file=sys.stderr, flush=True)
" | gzip >>log.gz

