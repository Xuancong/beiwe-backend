import boto3
from cluster_management.deployment_helpers.constants import (get_aws_credentials,get_global_config)
from cluster_management.deployment_helpers.aws.boto_helpers import (create_ec2_resource,create_ec2_client)

ec2 = create_ec2_resource()
ec2Client = create_ec2_client()

def prompt_for_new_vpc_name(with_prompt=True):
    name = input()
    return name
print("Enter the vpc name")
name = prompt_for_new_vpc_name(with_prompt=False)

vpc = ec2.create_vpc(CidrBlock='10.1.0.0/16')

vpc.create_tags(Tags=[{"Key": "Name", "Value": name}])
vpc.wait_until_available()


# # # enable public dns hostname so that we can SSH into it later

ec2Client.modify_vpc_attribute( VpcId = vpc.id , EnableDnsSupport = { 'Value': True } )
ec2Client.modify_vpc_attribute( VpcId = vpc.id , EnableDnsHostnames = { 'Value': True } )

# # # create an internet gateway and attach it to VPC
internetgateway = ec2.create_internet_gateway()
vpc.attach_internet_gateway(InternetGatewayId=internetgateway.id)

# # #Allocate an Elastic IP for NAT Gateway
eip = ec2Client.allocate_address( Domain='vpc' )
eip_alloc = eip["AllocationId"]


# # # create main route table and a public route
mainroutetable = vpc.create_route_table()
route = mainroutetable.create_route(DestinationCidrBlock='0.0.0.0/0', GatewayId=internetgateway.id)

# # # create public subnet-1a and associate it with main route table
subnet = ec2.create_subnet(CidrBlock='10.1.2.0/24', VpcId=vpc.id, AvailabilityZone='ap-southeast-1a')
mainroutetable.associate_with_subnet(SubnetId=subnet.id)


# # # create public subnet-1b and associate it with main route table
subnet = ec2.create_subnet(CidrBlock='10.1.20.0/24', VpcId=vpc.id, AvailabilityZone='ap-southeast-1b')
mainroutetable.associate_with_subnet(SubnetId=subnet.id)

# # create a NAT gateway
natgateway = ec2Client.create_nat_gateway(SubnetId=subnet.id,AllocationId=eip_alloc)

## Need to wait until NAT Gateway is provisioned ##


# create nat route table and a route to nat gateway
natroutetable = vpc.create_route_table()
route = natroutetable.create_route(DestinationCidrBlock='0.0.0.0/0', GatewayId=natgateway.id)

# # create private subnet-1a and associate it with nat route table
subnet = ec2.create_subnet(CidrBlock='10.1.1.0/24', VpcId=vpc.id, AvailabilityZone='ap-southeast-1a')
natroutetable.associate_with_subnet(SubnetId=subnet.id)

# # create private subnet-1b and associate it with nat route table
subnet = ec2.create_subnet(CidrBlock='10.1.10.0/24', VpcId=vpc.id, AvailabilityZone='ap-southeast-1b')
natroutetable.associate_with_subnet(SubnetId=subnet.id)

# create vpn subnet and associate it with main route table
# subnet = ec2.create_subnet(CidrBlock='10.1.0.0/24', VpcId=vpc.id, AvailabilityZone='ap-southeast-1a')
# mainroutetable.associate_with_subnet(SubnetId=subnet.id)

# Need to create DB Subnet Group





