#!/usr/bin/env bash


cd `dirname $0`

if [ -s secret.sh ]; then
	if [ ! "`grep export secret.sh`" ]; then
		echo "Error: in secret.sh, environment variables must be defined using 'export'"
		exit 1
	fi
	echo -n "Sourcing secret.sh for setting up credentials ... "
	source secret.sh
	echo "Done"
fi

gunicorn --timeout 60 -w 4 --certfile ssl/ssl.crt --keyfile ssl/ssl.key --bind 0.0.0.0:${PORT-443} app:app \
	--log-level info --access-logfile - --error-logfile - 

