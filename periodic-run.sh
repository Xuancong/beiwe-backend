#!/bin/bash

cd "`dirname $0`"
while [ 1 ]; do
	wifi=`nmcli radio wifi`

	if [ "$wifi" == disabled ]; then
		which nmcli
		sudo nmcli radio wifi on
		sleep 20
	fi

  # load access credentials
	source secret.sh

	# compulsory pipelines
	./secure-manual-decrypt-all.sh && ./1.patch-data.sh && ./2.aggregate-data.sh && ./3.cleanup.sh \
		&& ./4.extract-features.sh && ./5.combine-features.sh

	# optional pipelines
  ./4.data-completion-dashboard.sh
  ./4.clinical-dashboard.sh

	if [ -s push-tester.sh ]; then
		./push-tester.sh
	fi

	if [ "$wifi" == disabled ]; then
		sudo nmcli radio wifi off
	fi

	echo 'Last updated on:'
	date

	# sleep period
	sleep $[6*3600]
done

