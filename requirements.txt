#Crypto==1.4.1
boto==2.49.0
celery==4.3.0
boto3==1.17.53
botocore==1.20.112
cronutils>=0.2.1
Django==2.2.24
django-extensions==2.2.1
Flask==2.0.2
nose==1.3.7
psycopg2==2.9.6
psycopg2-binary==2.9.6
Pillow==8.2.0
pycryptodome>=3.9.9
python-dateutil==2.8.1
pytz==2020.1
qrcode==6.1
raven==6.10.0
s3transfer==0.3.3
six==1.15.0
sqlparse==0.3.1
urllib3>=1.25.9
Werkzeug==2.0.2
WTForms==2.2.1
Jinja2==3.0.2
gunicorn>=20.1.0
firebase-admin
ipython
blinker
tinyec

# Keep these ones up-to-date
termcolor
pynacl

