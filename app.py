import os, sys, re, uuid, io, traceback
from datetime import datetime, timedelta, time
from contextlib import redirect_stdout

import boto3, jinja2, psycopg2
import pandas as pd
import firebase_admin
from firebase_admin import credentials, messaging
from flask import Flask, render_template, redirect, request, session, abort
from raven.contrib.flask import Sentry
from werkzeug.middleware.proxy_fix import ProxyFix
from config import load_django
from api import *
from config.constants import CHECKABLE_FILES
from config.settings import *
from libs.s3 import *
from libs.admin_authentication import is_logged_in
from libs.security import set_secret_key
from database.models import Researcher, Study, Participant, Survey

# # if running locally we want to use a sqlite database
# if __name__ == '__main__':
#     os.environ['DJANGO_DB_ENV'] = "local"
#
# # if running through WSGI we want
# if not __name__ == '__main__':
#     os.environ['DJANGO_DB_ENV'] = "remote"
# Load and set up Django


def subdomain(directory):
	app = Flask("Beiwe", static_folder = directory + "/static")
	set_secret_key(app)
	loader = [app.jinja_loader, jinja2.FileSystemLoader(directory + "/templates")]
	app.jinja_loader = jinja2.ChoiceLoader(loader)
	app.wsgi_app = ProxyFix(app.wsgi_app)
	sys.fcm_app = firebase_admin.initialize_app()
	return app


# Register pages here
app = subdomain("frontend")
app.jinja_env.globals['current_year'] = datetime.now().strftime('%Y')
app.register_blueprint(mobile_api.mobile_api)
app.register_blueprint(admin_pages.admin_pages)
app.register_blueprint(mobile_pages.mobile_pages)
app.register_blueprint(system_admin_pages.system_admin_pages)
app.register_blueprint(admin_api.admin_api)
app.register_blueprint(participant_administration.participant_administration)
app.register_blueprint(survey_api.survey_api)
app.register_blueprint(data_access_api.data_access_api)
app.register_blueprint(data_access_web_form.data_access_web_form)
app.register_blueprint(copy_study_api.copy_study_api)
app.register_blueprint(data_pipeline_api.data_pipeline_api)

app.config.update(
	SESSION_COOKIE_SECURE = True,
	SESSION_COOKIE_HTTPONLY = True,
	SESSION_COOKIE_SAMESITE = 'Strict',
)

# Don't set up Sentry for local development
if os.environ['DJANGO_DB_ENV'] != 'local':
	sentry = Sentry(app, dsn = SENTRY_ELASTIC_BEANSTALK_DSN)


@app.route("/<page>.html")
def strip_dot_html(page):
	# Strips away the dot html from pages
	return redirect("/%s" % page)

def get_push_stacks(patient_id):
	try:
		patient = Participant.objects.get(patient_id=patient_id)
		survey_pks = patient.study.get_survey_ids_for_study()
		list = [survey_pk for survey_pk in survey_pks if Survey.objects.get(pk = survey_pk).target_users=='@'+patient_id]
		return list
	except:
		return []

@app.context_processor
def inject_dict_for_all_templates():
	return {"SENTRY_JAVASCRIPT_DSN": SENTRY_JAVASCRIPT_DSN, 'get_push_stacks': get_push_stacks}


@app.template_filter('get_date_color')
def get_date_color(user: Participant):
	try:
		elapse = (pd.Timestamp.now(tz='utc')-pd.Timestamp(user.last_upload_time, tz='utc')).total_seconds()
		color = eval(user.study.date_elapse_color)
		return color
	except:
		pass
	return 'black'


@app.template_filter('print_date')
def print_date(s_date):
	try:
		return str(pd.Timestamp(s_date, tz = 'utc').tz_convert('tzlocal()').floor('s'))
	except:
		return 'ERROR'


@app.template_filter('print_data_completion')
def print_data_completion(patient, study):
	df = patient.get_upload_info()
	if df.empty: return ''
	formula = DEFAULT_DAILY_CHECK_FORMULA if study.daily_check_formula == 'default' else study.daily_check_formula
	try:
		out = io.StringIO()
		with redirect_stdout(out):
			exec(formula)
		out = out.getvalue()
	except Exception as e:
		out = f'Error: {e}'
	return out


@app.template_filter('escape_quote')
def escape_quote(text):
	return text.replace('"', "&quot;").replace("'", "&apos;") if text else ''


@app.template_filter('unescape_quote')
def unescape_quote(text):
	return text.replace("&quot;", '"').replace("&apos;", "'") if text else ''


@app.after_request
def apply_caching(response):
	response.headers["Access-Control-Allow-Origin"] = "https://" + DOMAIN_NAME
	return response


csrf_exemption = set(['self_register', 'register_user', 'request_otp', 'upload', 'download_surveys', 'completion_report'])

@app.before_request
def csrf_protect():
	if request.method == "POST" and not (csrf_exemption & set(request.url.split('/'))):
		token = session.pop('_csrf_token', None)
		if not token or token != request.form.get('_csrf_token'):
			return abort(403)


def generate_csrf_token():
	if '_csrf_token' not in session:
		session['_csrf_token'] = uuid.uuid4().hex
	return session['_csrf_token']

app.jinja_env.globals['csrf_token'] = generate_csrf_token


def handle_exceptions(e):
	print('Error at time:', pd.Timestamp.now(), e)
	etype, value, tb = sys.exc_info()
	print(traceback.print_exception(etype, value, tb))


# MAIN START
if __name__ == '__main__':
	# Test PostgreSQL connection
	try:
		conn = psycopg2.connect(host = os.environ['RDS_HOSTNAME'],
		                        database = os.environ['RDS_DB_NAME'],
		                        user = os.environ['RDS_USERNAME'],
		                        password = os.environ['RDS_PASSWORD'])
		conn.close()
	except:
		confirm = input("PostgreSQL server connection failed, continue? (y/n)").strip().lower()
		if confirm != 'y':
			sys.exit(1)

	# Test S3 bucket connection
	try:
		conn = boto3.client('s3',
		                    endpoint_url = os.environ['S3_ENDPOINT_URL'],
		                    region_name = os.environ.get('S3_ACCESS_CREDENTIALS_REGION', None),
		                    aws_access_key_id = os.environ['S3_ACCESS_CREDENTIALS_USER'],
		                    aws_secret_access_key = os.environ['S3_ACCESS_CREDENTIALS_KEY'])
		buckets = conn.list_buckets()
		if 'Buckets' not in buckets or os.environ['S3_BUCKET'] not in [d['Name'] for d in buckets['Buckets']]:
			conn.create_bucket(Bucket = os.environ['S3_BUCKET'])
			conn.put_object(Bucket = os.environ['S3_BUCKET'], Key = 'test-key', Body = 'test-body')
			obj = conn.get_object(Bucket = os.environ['S3_BUCKET'], Key = 'test-key')
			assert (obj['Body']._raw_stream.data.decode() == 'test-body')
		else:
			obj = conn.get_object(Bucket = os.environ['S3_BUCKET'], Key = 'test-key')
			assert (obj['Body']._raw_stream.data.decode() == 'test-body')
	except:
		confirm = input("S3 bucket access failed, continue? (y/n)").strip().lower()
		if confirm != 'y':
			sys.exit(1)

	# might be necessary if running on windows/linux subsystem on windows.
	# from gevent.wsgi import WSGIServer
	# http_server = WSGIServer(('', 8080), app)
	# http_server.serve_forever()
	use_HTTP = os.getenv('USE_HTTP') == '1'

	if HOST == '0.0.0.0':
		print('Please use %s://localhost:%s/ if you want to launch browser from localhost' % (
			'http' if use_HTTP else 'https', PORT), flush = True, file = sys.stderr)

	app.register_error_handler(Exception, handle_exceptions)

	app.run(host = HOST, port = PORT, threaded = True, debug = os.getenv('DEBUG', False),
	        ssl_context = None if use_HTTP else ('./ssl/ssl.crt', './ssl/ssl.key'))



else:
	# Points our custom 404 page (in /frontend/templates) to display on a 404 error
	@app.errorhandler(404)
	def e404(e):
		return render_template("404.html", is_logged_in = is_logged_in()), 404
