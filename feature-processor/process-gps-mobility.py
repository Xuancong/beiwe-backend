#!/usr/bin/env python3
# coding=utf-8

import os, sys, argparse
import pandas as pd
from utils import *


def proc(df):
	return df.drop(columns='timestamp').rename(columns={'Date':'datetime'}).set_index('datetime')


if __name__=='__main__':
	parser = argparse.ArgumentParser(usage='$0 input-file output-file 1>progress 2>warning_msg',
									 description='This program processes features, use "-" for STDIN/STDOUT')
	parser.add_argument('input_fn', help='positional argument')
	parser.add_argument('output_fn', help='positional argument')
	parser.add_argument('--verbose', '-v', default=1, type=int, help='verbose level')
	#nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt=parser.parse_args()
	globals().update(vars(opt))

	df = proc(pd.read_csv(Open(input_fn)))
	Open(output_fn, 'w').write(df.to_csv()) if output_fn == '-' else df.to_csv(output_fn)
