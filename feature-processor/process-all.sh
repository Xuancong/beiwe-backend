#!/usr/bin/env bash

if [ $# -lt 2 ]; then
	echo "Usage: $0 <in_folder> <out_folder>"
	echo "<in_folder> and <out_folder> contains each user's data folders"
	exit 1
fi

mkdir -p "$2"
INPATH="`realpath \"$1\"`"
OUTPATH="`realpath \"$2\"`"

data_exist() {
	while [ "$1" ]; do
		if [ $(zcat -f "$1" 2>/dev/null | wc -l) -lt 2 ]; then
			return
		fi
		shift
	done
	echo yes
}

need_to_update() {
	# Usage: $0 input_file [output_file1 output_file2 ...]
	# It returns 1 if and only if input_file is updated within 1 day and every output file exist and non-empty
	fin="$1"
	shift
	while [ $# -gt 0 ]; do
		if [ ! -s "$1" ]; then
			echo 1
			return
		fi
		shift
	done
	days_til_now=$(python3 -c "import os,sys; import pandas as pd;\
    print((pd.Timestamp.now()-pd.Timestamp(os.stat('$fin').st_mtime, unit='s'))/pd.to_timedelta('1D'))")
	echo "$days_til_now < 1" | bc
}

copy_last_mod_time() {
	# Usage: $0 src tgt [tgt2 ...]
	# It copies the last modification time from src to tgt
	fin="$1"
	shift
	while [ $# -gt 0 ]; do
		python3 -c "import os,sys;import pandas as pd;st=os.stat('$fin');os.utime('$1', (st.st_atime, st.st_mtime))"
		shift
	done
}

echo -e 'Running feature extraction (\x1b[92mY\x1b[0m=Yes, \x1b[93mN\x1b[0m=No data, \x1b[92mS\x1b[0m=Skip, \x1b[1m\x1b[91mE\x1b[0m=Error):'

cd $(dirname $0)
for script in process-*.py; do
	feature_name=${script:8:$((${#script} - 11))}
	echo -n "Extracting features for $feature_name "

	for user in $INPATH/*; do
		username="$(basename "$user")"
		mkdir -p "$OUTPATH/$username"
		if [ $(data_exist "$user/$feature_name.csv.gz") ]; then
			if [ $(need_to_update "$user/$feature_name.csv.gz" "$OUTPATH/$username/$feature_name.csv.gz") == 1 ]; then
				./$script -v 0 "$user/$feature_name.csv.gz" "$OUTPATH/$username/$feature_name.csv.gz"
				if [ $? == 0 ] && [ "$(data_exist $OUTPATH/$username/$feature_name.*csv.gz)" ]; then
					echo -ne '\x1b[92mY\x1b[0m'
					copy_last_mod_time "$user/$feature_name.csv.gz" $OUTPATH/$username/$feature_name.*csv.gz
				else
					echo -ne '\x1b[1m\x1b[91mE\x1b[0m'
					echo -e "\x1b[1m\x1b[91mError in processing $user/$feature_name.csv.gz\x1b[0m" >&2
				fi
			else
				echo -ne '\x1b[92mS\x1b[0m'
			fi
		else
			echo -ne '\x1b[93mN\x1b[0m'
		fi
	done
	echo
done
