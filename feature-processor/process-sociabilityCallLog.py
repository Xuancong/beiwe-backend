#!/usr/bin/env python3
# coding=utf-8

import os, sys, argparse, re, gzip, json
from utils import *


def proc(df, meta):
	# convert timestamp to datetime and set as index
	dt = pd.to_datetime(df['timestamp'], unit='ms', origin='unix', utc=True)
	df['datetime'] = pd.DatetimeIndex(dt).tz_convert('tzlocal()')
	df = df.set_index('datetime').sort_index()

	# convert to phone-call's data format
	if len(df) == 0:
		sys.exit(0)

	df = pd.concat([pd.DataFrame({'app': v.app.iloc[0], 'hashed phone number': v.contact.iloc[0],
		 'call type': ('Incoming Call' if v.eventType.iloc[0] == 1 else 'Outgoing Call'),
		 'duration in seconds': v.recordedDuration.iloc[-1]}, index=[v.index[0]]) for k, v in df.groupby('sessionId') if len(v)])

	# group according to each day
	dfg = df.groupby(pd.Grouper(freq='1D'))

	# number of incoming/outgoing/missed calls
	ret = create_frame_from_groupby_value_counts(dfg, 'call type', cls=['Incoming Call', 'Outgoing Call'])

	# total duration of call
	ret['totalDur'] = dfg[['duration in seconds']].sum()

	# number of people talked (only those with call duration > 0)
	# dfg1 = df[df['duration in seconds'] > 0].groupby(pd.Grouper(freq='1D'))
	# ret['n_ppl_talked'] = create_frame_from_groupby_value_types(dfg1, 'hashed phone number')
	# BUG: groupby after duration_in_seconds>0 selection will cause inconsistent empty slots
	ret['n_ppl_talked'] = [len(j[j['duration in seconds']>0]['hashed phone number'].value_counts()) for i,j in dfg]

	ret.index.name = 'datetime'

	# resolve between true zero vs NaN
	ret = resolve_zero_vs_nan(ret, meta)

	return ret


if __name__ == '__main__':
	parser = argparse.ArgumentParser(usage='$0 input-file output-file 1>progress 2>warning_msg',
									 description='This program processes features, use "-" for STDIN/STDOUT')
	parser.add_argument('input_fn', help='positional argument')
	parser.add_argument('output_fn', help='positional argument')
	parser.add_argument('--verbose', '-v', default=1, type=int, help='verbose level')
	# nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt = parser.parse_args()
	globals().update(vars(opt))

	meta = json.load(Open(os.path.dirname(input_fn)+'/meta.json.gz'))
	df = proc(pd.read_csv(input_fn), meta['sociabilityCallLog'])
	Open(output_fn, 'w').write(df.to_csv()) if output_fn == '-' else df.to_csv(output_fn)
