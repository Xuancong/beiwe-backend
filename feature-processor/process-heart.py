#!/usr/bin/env python3
# coding=utf-8

import os, sys, argparse, re, gzip
import pandas as pd
import numpy as np
from utils import *

sleep_levels = ['deep', 'light', 'rem', 'wake']
asleep_levels = ['deep', 'light', 'rem']

def proc(fn, sleep_fn=None, steps_fn=None):
	df = load_and_preprocess(fn).ffill()

	# load sleep file if available
	try:
		sleep_df = load_and_preprocess(sleep_fn)
		sleep_df = sleep_df[sleep_df.Level.isin(sleep_levels)][['Level', 'Seconds']]    # since we need resting HR, must include non-main sleep
		# sleep_df = sleep_df[(sleep_df.MainSleep == True) & (sleep_df.Level.isin(sleep_levels))][['Level', 'Seconds']]
	except:
		sleep_df = pd.DataFrame(columns = ['Level', 'Seconds'], index=pd.DatetimeIndex([], tz='tzlocal()'))
		
	# load steps file if available
	try:
		steps_df = load_and_preprocess(steps_fn)[['StepsCount']]
	except:
		steps_df = pd.DataFrame(columns = ['StepsCount'], index=pd.DatetimeIndex([], tz='tzlocal()'))


	# compute HRV
	df['TZ'] = df.HR.diff() * 1000 / df.timestamp.diff()
	df = df.rename(columns={'TZ': 'HRV'}).drop(columns='timestamp')

	# group according to each hour
	# df = df.groupby(pd.Grouper(freq='1s')).mean().ffill()   # to make it precise, this is ~4.5 times slower
	dfg = df.groupby(pd.Grouper(freq='1H'))

	# max/min/std/mean of HR
	ret_hourly = create_frame_from_groupby(dfg, 'HR', ['max', 'min', 'std', 'mean'])

	# compute HRV
	apply_frame_from_groupby(ret_hourly, dfg, 'HRV', ['max', 'min'])

	dfg = df.groupby(pd.Grouper(freq='1D'))
	ret_daily = create_frame_from_groupby(dfg, 'HR', ['max', 'min', 'std', 'mean', 'median'])
	apply_frame_from_groupby(ret_daily, dfg, 'HRV', ['max', 'min', 'std'])
	ret_daily['HR_Q.25'] = dfg['HR'].quantile(0.25)
	ret_daily['HR_Q.125'] = dfg['HR'].quantile(0.125)

	# added daily HRV-based
	abs_HRV = df[['HRV']].abs().rename(columns={'HRV': 'abs_HRV'})
	apply_frame_from_groupby(ret_daily, abs_HRV.groupby(pd.Grouper(freq='1D')), 'abs_HRV', ['mean', 'std'])

	# add HR and HRV within particular sleep levels
	end_points = sleep_df.index + pd.to_timedelta(sleep_df.Seconds.values, unit='s')
	sleep_df = pd.concat([sleep_df, pd.DataFrame('none', index=pd.DatetimeIndex(set(end_points)-set(sleep_df.index)), columns = sleep_df.columns)]).sort_index()
	df[['HRV']] = abs_HRV
	df2 = df.join(sleep_df[['Level']], how='outer').join(steps_df, how='outer')
	df2.Level = df2.Level.ffill().fillna('none')
	df2.StepsCount = df2.StepsCount.ffill(limit=12).fillna(0)
	for level in sleep_levels:
		dfg = df2[df2.Level == level].groupby(pd.Grouper(freq = '1D'))
		apply_frame_from_groupby(ret_daily, dfg, 'HR',  {'mean_HR_%s'%level: 'mean', 'std_HR_%s'%level: 'std', 'max_HR_%s'%level: 'max', 'min_HR_%s'%level: 'min'})
		apply_frame_from_groupby(ret_daily, dfg, 'HRV', {'mean_abs_HRV_%s'%level: 'mean', 'std_abs_HRV_%s'%level: 'std', 'max_abs_HRV_%s'%level: 'max', 'min_abs_HRV_%s'%level: 'min'})

	# add HR and HRV within all asleep levels
	dfg = df2[df2.Level.isin(asleep_levels)].groupby(pd.Grouper(freq = '1D'))
	apply_frame_from_groupby(ret_daily, dfg, 'HR',  {'mean_HR_asleep': 'mean', 'std_HR_asleep': 'std', 'max_HR_asleep':'max', 'min_HR_asleep':'min'})
	apply_frame_from_groupby(ret_daily, dfg, 'HRV', {'mean_abs_HRV_asleep': 'mean', 'std_abs_HRV_asleep': 'std', 'max_abs_HRV_asleep':'max', 'min_abs_HRV_asleep':'min'})

	# add HR and HRV within not-sleeping and zero-step time intervals
	dfg = df2[(df2.Level=='none') & (df2.StepsCount==0)].groupby(pd.Grouper(freq = '1D'))
	apply_frame_from_groupby(ret_daily, dfg, 'HR', {'mean_HR_resting': 'mean', 'std_HR_resting': 'std', 'max_HR_resting': 'max', 'min_HR_resting': 'min'})
	apply_frame_from_groupby(ret_daily, dfg, 'HRV', {'mean_abs_HRV_resting': 'mean', 'std_abs_HRV_resting': 'std', 'max_abs_HRV_resting': 'max', 'min_abs_HRV_resting': 'min'})

	return ret_daily, ret_hourly


if __name__ == '__main__':
	parser = argparse.ArgumentParser(usage='$0 input-file output-file 1>progress 2>warning_msg',
									 description='This program processes features, use "-" for STDIN/STDOUT')
	parser.add_argument('input_fn', help='positional argument')
	parser.add_argument('output_fn', help='positional argument')
	parser.add_argument('--verbose', '-v', default=1, type=int, help='verbose level')
	# nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt = parser.parse_args()
	globals().update(vars(opt))

	df_daily, df_hourly = proc(input_fn,
	                           os.path.dirname(input_fn) + '/sleep.csv.gz',
	                           os.path.dirname(input_fn) + '/steps.csv.gz')
	# Open(output_fn, 'w').write(df.to_csv()) if output_fn == '-' else df.to_csv(output_fn)

	if output_fn == '-':
		print(df_daily.to_csv(), flush=True)
		print(df_hourly.to_csv(), end='', flush=True)
	else:
		df_daily.to_csv(add_str_to_fn(output_fn, 'daily'))
		df_hourly.to_csv(add_str_to_fn(output_fn, 'hourly'))
