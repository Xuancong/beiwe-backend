#!/usr/bin/env python3
# coding=utf-8

import os, sys, argparse, json
import numpy as np
import pandas as pd
from utils import *
from app_grouper import classes

# def proc(df):
# 	"""
# 	'tapsLog.csv'
# 	:param df:
# 	:return:
# 	"""
#
# 	usecols = ['timestamp',
# 			   'in_app_name',
# 			   # 'orientation',
# 			   # 'tz',
# 			   ]
#
# 	df = df[usecols]
# 	if verbose>0:
# 		print("Null report for input data:\n", df.isnull().sum(axis=0), file=sys.stderr)
#
# 	# TODO: Value checking
#
# 	# create the structure of the transformed dataset
# 	hourly_feature_dtypes = \
# 		dict(n_taps=int,  # total number of taps in the hour
# 			 mean_intap_dur=float,  # mean of inter-tap duration while the screen is on
# 			 std_intap_dur=float,  # std dev -- " --
# 			 max_intap_dur=float,  # max -- " --
# 			 min_intap_dur=float,  # min -- " --
# 			 )
#
# 	daily_feature_dtypes = \
# 		dict(n_unique_app=int,  # number of distinct in-app names
# 			 mean_intap_dur=float,  # mean of inter-tap duration while the screen is on
# 			 std_intap_dur=float,  # std dev -- " --
# 			 max_intap_dur=float,  # max -- " --
# 			 min_intap_dur=float,  # min -- " --
# 			 taps_per_hour=float,  # rate of taps per hour
# 			 # 'frac_soc_med',       # REQUEST: proportion of taps made in social media apps (WhatsApp, Wechat, Line, etc.)
# 			 )
#
# 	###  Create hourly features  ###
#
# 	grouped_hour, timestamp_hour, _ = group_by_hour(df)
# 	unique_hours = grouped_hour.groups.keys()
# 	new_df_hourly = pd.DataFrame(np.nan, index=unique_hours, columns=hourly_feature_dtypes.keys())
#
# 	for hour, group_inds in grouped_hour.groups.items():
# 		# df_hour = df.loc[group_inds, :]
#
# 		###  n_taps  ###
# 		new_df_hourly.loc[hour, 'n_taps'] = len(group_inds)
#
# 		###  intertap durations  ###
# 		ts_ = timestamp_hour[group_inds]  # Series of datetime objects
# 		sorted_ts_ = ts_.sort_values()  # ascending
# 		diffs = sorted_ts_.diff()
# 		intertap_durations = diffs.iloc[1:]
#
# 		new_df_hourly.loc[hour, 'mean_intap_dur'] = intertap_durations.mean().value  # in nanoseconds
# 		new_df_hourly.loc[hour, 'std_intap_dur'] = intertap_durations.std().value
# 		new_df_hourly.loc[hour, 'min_intap_dur'] = intertap_durations.min().value
# 		new_df_hourly.loc[hour, 'max_intap_dur'] = intertap_durations.max().value
#
# 	###  Create daily features  ###
#
# 	grouped_date, timestamp_date, _ = group_by_date(df)
# 	unique_dates = grouped_date.groups.keys()
# 	new_df_daily = pd.DataFrame(np.nan, index=unique_dates, columns=daily_feature_dtypes.keys())
#
# 	for date, group_inds in grouped_date.groups.items():
# 		df_date = df.loc[group_inds, :]
#
# 		###  n_unique_app  ###
# 		new_df_daily.loc[date, 'n_unique_app'] = len(df_date['in_app_name'].unique())
#
# 		###  intertap durations  ###
# 		ts_ = timestamp_date[group_inds]  # Series of datetime objects
# 		sorted_ts_ = ts_.sort_values()  # ascending
# 		diffs = sorted_ts_.diff()
# 		intertap_durations = diffs.iloc[1:]
#
# 		new_df_daily.loc[date, 'mean_intap_dur'] = intertap_durations.mean().value  # in nanoseconds
# 		new_df_daily.loc[date, 'std_intap_dur'] = intertap_durations.std().value
# 		new_df_daily.loc[date, 'min_intap_dur'] = intertap_durations.min().value
# 		new_df_daily.loc[date, 'max_intap_dur'] = intertap_durations.max().value
#
# 		hours_in_date = np.array([hour.date() == date for hour in new_df_hourly.index])  # inefficient
# 		assert np.sum(hours_in_date) > 0
# 		new_df_daily.loc[date, 'taps_per_hour'] = np.mean(new_df_hourly[hours_in_date]['n_taps'])
#
# 	# enforce types
# 	new_df_hourly = new_df_hourly.astype(hourly_feature_dtypes)
# 	new_df_daily = new_df_daily.astype(daily_feature_dtypes)
#
# 	if verbose > 1:
# 		print("Hourly DF:\n", new_df_hourly.head(), file=sys.stderr)
# 	assert not new_df_hourly.isnull().any().any()
# 	if verbose > 0:
# 		print("Null report for transformed data:\n", new_df_hourly.isnull().sum(axis=0), file=sys.stderr)
#
# 	if verbose > 1:
# 		print("Daily DF:\n", new_df_daily.head(), file=sys.stderr)
# 	assert not new_df_daily.isnull().any().any()
# 	if verbose > 0:
# 		print("Null report for transformed data:\n", new_df_daily.isnull().sum(axis=0), file=sys.stderr)
#
# 	new_df_daily.index.name = new_df_hourly.index.name = 'datetime'
#
# 	return new_df_daily, new_df_hourly

getabbr = lambda t: ''.join([s[:3] for s in t.split()]).upper()
bin_center = lambda t: np.sqrt(t.left * t.right)
cut2XY = lambda c: ([bin_center(x) for x in c.index], [x for x in c])
lin_bin_center = lambda t: np.sqrt(max(1, t.left) * t.right)
lin_cut2XY = lambda c: ([lin_bin_center(x) for x in c.index], [x for x in c])

def get_PL_distrib_peak(df, cls=None):
	if type(cls)==list:
		return pd.concat([get_PL_distrib_peak(df, cls1) for cls1 in cls])

	s = (df[df.in_app_class!=cls[1:] if cls[0]=='!' else df.in_app_class==cls] if cls else df).iloc[:, -1]
	CLS = ('_no'+getabbr(cls[1:]) if cls[0]=='!' else '_'+getabbr(cls)) if cls else ''

	# 1. get peak position
	if s.size < 200:
		return pd.Series({'ITID_1D_peak_X%s'%CLS: nan, 'ITID_1D_peak_Y%s'%CLS: nan})

	bin_center = lambda t: np.sqrt(t.left * t.right)
	#     out_X_Q5pc, out_X_Q95pc = s.quantile([0.05, 0.95]).values  # already have?
	hi, lo = s.max(), s.min()
	bins = np.logspace(np.log10(lo), np.log10(hi), 100)
	cut = pd.cut(s, bins=bins, include_lowest=True).value_counts().sort_index()
	cut *= 1 / cut.sum()
	return pd.Series({'ITID_1D_peak_X%s'%CLS: bin_center(cut.idxmax()), 'ITID_1D_peak_Y%s'%CLS: cut.max()})


def compute_PL_features(df, cls=None):
	if type(cls)==list:
		return pd.concat([compute_PL_features(df, cls1) for cls1 in cls])

	s = (df[df.in_app_class!=cls[1:] if cls[0]=='!' else df.in_app_class==cls] if cls else df).iloc[:, -1]
	CLS = ('_no'+getabbr(cls[1:]) if cls[0]=='!' else '_'+getabbr(cls)) if cls else ''

	# extract power-law distribution features
	if s.size < 1000:
		return pd.Series({'ITID_7D_peak_X%s'%CLS: nan,        'ITID_7D_peak_Y%s'%CLS: nan,
		                  'ITID_7D_logUnif_slope%s'%CLS: nan, 'ITID_7D_logUnif_Yinter%s'%CLS: nan,
		                  'ITID_7D_linUnif_slope%s'%CLS: nan, 'ITID_7D_linUnif_Yinter%s'%CLS: nan})

	# 1. get peak position
	hi, lo = s.max(), s.min()
	bins = np.logspace(np.log10(lo), np.log10(hi), 100)
	cut = pd.cut(s, bins=bins, include_lowest=True).value_counts().sort_index()
	PL_logUnif_offset = np.log10(1 / cut.sum())
	cut *= 1 / cut.sum()
	PL_distrib_peak_X = bin_center(cut.idxmax())

	# 2. compute power-law distribution log-uniform-histogram gradient
	X, Y = cut2XY(cut)
	idxs = [i for i, v in enumerate(Y) if v > 0]
	log10X, log10Y = np.log10(np.array(X)[idxs]), np.log10(np.array(Y)[idxs])
	P = log10Y.argmax()
	logUnif_a, logUnif_b = np.polyfit(log10X[P:], log10Y[P:], 1)

	# 3. compute power-law distribution linear-uniform-histogram gradient
	lin_cut = pd.cut(s, bins=100, include_lowest=True).value_counts().sort_index()
	PL_linUnif_offset = np.log10(1 / lin_cut.sum())
	lin_cut *= 1 / lin_cut.sum()
	X, Y = lin_cut2XY(lin_cut)
	idxs = [i for i, v in enumerate(Y) if v > 0]
	log10X, log10Y = np.log10(np.array(X)[idxs]), np.log10(np.array(Y)[idxs])
	linUnif_a, linUnif_b = np.polyfit(log10X, log10Y, 1)

	ret = pd.Series({'ITID_7D_peak_X%s'%CLS: PL_distrib_peak_X,'ITID_7D_peak_Y%s'%CLS: cut.max(),
	                 'ITID_7D_logUnif_slope%s'%CLS: logUnif_a, 'ITID_7D_logUnif_Yinter%s'%CLS: logUnif_b,
	                 'ITID_7D_linUnif_slope%s'%CLS: linUnif_a, 'ITID_7D_linUnif_Yinter%s'%CLS: linUnif_b})
	return ret

def proc(df, powerstate_filename, meta):
	# convert timestamp to datetime and set as index
	dt = pd.to_datetime(df['timestamp'], unit='ms', origin='unix', utc=True)
	df = df.set_index(dt.dt.tz_convert(tzlocal())).sort_index()
	df = df.drop(columns='orientation').rename(columns={'tz': 'event'})
	df.event = 'tap'

	# remove rows with duplicate timestamp (failsafe, just in case)
	df = df.loc[~df.index.duplicated(keep='last')].copy()

	# load powerstate file if available
	try:
		ps_df = pd.read_csv(powerstate_filename)
		ps_dt = pd.to_datetime(ps_df['timestamp'], unit='ms', origin='unix', utc=True)
		ps_df = ps_df.set_index(ps_dt.dt.tz_convert(tzlocal()))[['event']].sort_index()
	except:
		ps_df = pd.DataFrame(columns=['event'])

	# only consider screen on/off events for now
	# ps_df = ps_df[(ps_df.event=='Screen turned on')|(ps_df.event=='Screen turned off')]
	ps_df = ps_df[ps_df.event == 'Screen turned off']

	# # make sure the 1st event is screen_on, the last event is screen_off
	# if len(ps_df) == 0 or ps_df.iloc[0, 0] != 'Screen turned on':
	# 	ps_df.loc[df.index.min() - pd.to_timedelta('1ms')] = 'Screen turned on'
	# 	ps_df = ps_df.sort_index()
	# if ps_df.iloc[-1, 0] != 'Screen turned off':
	# 	ps_df.loc[df.index.max() + pd.to_timedelta('1ms')] = 'Screen turned off'
	# 	ps_df = ps_df.sort_index()

	# # Drop consecutive screen on/off, due to phone/App crash, reboot, etc
	# # consecutive screen_on take the last
	# ps_df_on = ps_df[(ps_df.event=='Screen turned on') & (ps_df.shift(-1)!=ps_df).event]
	# # consecutive screen_off take the first
	# ps_df_off = ps_df[(ps_df.event == 'Screen turned off') & (ps_df.shift() != ps_df).event]
	# ps_df = pd.concat([ps_df_on, ps_df_off]).sort_index()


	# 1. compute hourly features regardless of APP
	df.timestamp = df.timestamp.shift(-1) - df.timestamp
	df_aug = pd.concat([df, ps_df]).sort_index()
	df_aug_all = df_aug[(df_aug.event=='tap')&(df_aug.event.shift(-1)!='Screen turned off')]	# discard the tap interval right before screen_off
	df_aug = df_aug_all[df_aug_all.timestamp < 3000]    # ignore long inter-tap durations
	dfg_aug = df_aug.groupby(pd.Grouper(freq='1H'))

	new_df_hourly = create_frame_from_groupby(dfg_aug, 'timestamp', {
		'mean_intap_dur': 'mean',
		'median_intap_dur': 'median',
		'std_intap_dur': 'std',
	})
	new_df_hourly['n_taps'] = df.groupby(pd.Grouper(freq='1H')).count().timestamp
	new_df_hourly['Q.25_intap_dur'] = quantile(dfg_aug['timestamp'], 0.25)
	new_df_hourly['Q.125_intap_dur'] = quantile(dfg_aug['timestamp'], 0.125)


	# 2. compute daily features, in-tap stats for every app class except game, and in-app time
	dfg = df.groupby(pd.Grouper(freq='1D'))
	new_df_daily = Try(lambda : dfg['in_app_class'].value_counts().to_frame().unstack(), pd.DataFrame())
	new_df_daily.columns = Try(lambda : ['n_taps_in_' + i.replace(' ', '_') for i in new_df_daily.columns.get_level_values(1)], [])
	expected_columns = ['n_taps_in_' + i.replace(' ', '_') for i in classes]

	# if some patients do not use APPs of certain classes at all, those columns should be 0; keep column order
	assert set(new_df_daily.columns)<=set(expected_columns), 'unexpected app class'
	for add_col in set(expected_columns)-set(new_df_daily.columns):
		new_df_daily[add_col] = 0
	new_df_daily = new_df_daily[expected_columns]

	# add in-app time
	in_app_time = Try(lambda: df_aug_all.groupby(pd.Grouper(freq='1D')).apply(lambda df: df.groupby('in_app_class')[['timestamp']].sum())
	                  .unstack().fillna(0)*0.001, pd.DataFrame())
	in_app_time.columns = in_app_time.columns.get_level_values(-1)
	for add_col in set(classes)-set(in_app_time.columns):
		in_app_time[add_col] = 0
	in_app_time.columns = [getabbr(s)+'_inapp_time' for s in in_app_time.columns]
	new_df_daily = new_df_daily.join(in_app_time[sorted(in_app_time.columns)], how='outer')

	# add 1-day inter-tap power-law distribution peak
	dfgITI = df_aug_all[df_aug_all.timestamp < 1800000][['in_app_class', 'timestamp']].groupby(pd.Grouper(freq='1D'))
	df_daily_ITID = dfgITI.apply(get_PL_distrib_peak)
	df_daily_ITID_SOCMES_noGAM = dfgITI.apply(get_PL_distrib_peak, ['social messenger', '!games'])

	# add 7-day-aggregate inter-tap power-law distribution line-fit parameters
	ITI_index, ITI_dfs = zip(*list(dfgITI))
	df_daily_ITID_7D = pd.DataFrame([compute_PL_features(pd.concat(ITI_dfs[max(0, ii-6):(ii+1)])) for ii, df in enumerate(ITI_dfs)], index=ITI_index)
	df_daily_ITID_7D_SOCMES_noGAM = pd.DataFrame([compute_PL_features(pd.concat(ITI_dfs[max(0, ii - 6):(ii + 1)]),
	                                            ['social messenger', '!games']) for ii, df in enumerate(ITI_dfs)], index=ITI_index)

	# force every day has a row, absent means 0
	new_df_daily = new_df_daily.join(pd.DataFrame(index=[i[0] for i in dfg]), how='outer').fillna(0)
	new_df_daily['n_unique_app'] = dfg['in_app_name'].nunique()
	for app_cls in classes:
		if app_cls == 'games': continue
		cls_abbr = getabbr(app_cls)
		dfSMg_aug = df_aug[df_aug.in_app_class==app_cls].groupby(pd.Grouper(freq='1D'))
		daily_inter_tap = create_frame_from_groupby(dfSMg_aug, 'timestamp', {
			cls_abbr+'_mean_intap_dur': 'mean',
			cls_abbr+'_median_intap_dur': 'median',
			cls_abbr+'_std_intap_dur': 'std',
		})
		daily_inter_tap[cls_abbr+'_Q.05_intap_dur'] = quantile(dfSMg_aug['timestamp'], 0.05)
		new_df_daily = new_df_daily.join(daily_inter_tap, how='outer')

	new_df_daily.index.name = new_df_hourly.index.name = 'datetime'

	# resolve between true zero vs NaN
	new_df_daily = resolve_zero_vs_nan(new_df_daily, meta)
	# Note: even when n_taps is truly 0 (no missing data), stats can still be NaN, so cannot simply set everything to 0
	new_df_hourly[['n_taps']] = resolve_zero_vs_nan(new_df_hourly[['n_taps']], meta, edge_overflow=0.4, freq='H')

	# inter-tap power-law distribution features does not follow the default NaN resolution
	for df in [df_daily_ITID, df_daily_ITID_SOCMES_noGAM, df_daily_ITID_7D, df_daily_ITID_7D_SOCMES_noGAM]:
		new_df_daily = new_df_daily.join(df, how='outer')

	return new_df_daily, new_df_hourly


if __name__ == '__main__':
	parser = argparse.ArgumentParser(usage='$0 input-file output-file 1>progress 2>warning_msg',
									 description='This program processes features, use "-" for STDIN/STDOUT')
	parser.add_argument('input_fn', help='positional argument')
	parser.add_argument('output_fn', help='positional argument')
	parser.add_argument('--verbose', '-v', default=1, type=int, help='verbose level')
	# nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt = parser.parse_args()
	globals().update(vars(opt))

	meta = json.load(Open(os.path.dirname(input_fn) + '/meta.json.gz'))
	new_df_daily, new_df_hourly = proc(pd.read_csv(Open(input_fn)), os.path.dirname(input_fn)+'/powerState.csv.gz', meta['tapsLog'])

	if output_fn == '-':
		print(new_df_daily.to_csv(), flush=True)
		print(new_df_hourly.to_csv(), end='', flush=True)
	else:
		new_df_daily.to_csv(add_str_to_fn(output_fn, 'daily'))
		new_df_hourly.to_csv(add_str_to_fn(output_fn, 'hourly'))
