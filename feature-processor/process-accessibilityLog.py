#!/usr/bin/env python3
# coding=utf-8

import os, sys, argparse, re, gzip, json
import pandas as pd
import numpy as np
from utils import *


def create_frame_from_groupby(dfg, col_name, func_names):
	ret = pd.DataFrame()
	if func_names:
		df = dfg[[col_name]]
		ret = eval('df.%s()'%func_names[0])
		ret = ret.rename(columns={col_name: col_name+'_'+func_names[0]})
		for func_name in func_names[1:]:
			ret[[col_name+'_'+func_name]] = eval('df.%s()'%func_name)
	return ret


def proc(df, meta, tz='tzlocal()'):
	# convert timestamp to datetime and set as index
	dt = pd.to_datetime(df['timestamp'], unit='ms', origin='unix', utc=True)
	df['datetime'] = pd.DatetimeIndex(dt).tz_convert(tz)

	# remove rows with duplicate timestamp
	df = df.loc[~df.timestamp.duplicated(keep='last')].ffill()

	# group according to each hour
	df = df.set_index('datetime')
	dfg = df.groupby(pd.Grouper(freq='1H'))

	# count total taps
	ret = create_frame_from_groupby(dfg, 'text', ['count'])

	# resolve between true zero vs NaN
	ret = resolve_zero_vs_nan(ret, meta, edge_overflow=0.4, freq='H')

	return ret


if __name__ == '__main__':
	parser = argparse.ArgumentParser(usage='$0 input-file output-file 1>progress 2>warning_msg',
									 description='This program processes features, use "-" for STDIN/STDOUT')
	parser.add_argument('input_fn', help='positional argument')
	parser.add_argument('output_fn', help='positional argument')
	parser.add_argument('--verbose', '-v', default=1, type=int, help='verbose level')
	# nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt = parser.parse_args()
	globals().update(vars(opt))

	meta = json.load(Open(os.path.dirname(input_fn) + '/meta.json.gz'))
	df = proc(pd.read_csv(Open(input_fn), low_memory=False), meta['accessibilityLog'])
	Open(output_fn, 'w').write(df.to_csv()) if output_fn == '-' else df.to_csv(output_fn)
