#!/usr/bin/env python3
# coding=utf-8

import os, sys, gzip, argparse
from utils import *


# based on real-world statistics, https://www.vox.com/2016/5/10/11639214/how-people-around-the-world-sleep
mean_sleep_start = pd.to_timedelta('23:15:00')
mean_sleep_stop = pd.to_timedelta('07:15:00')
day_break = (mean_sleep_start+mean_sleep_stop)/2/pd.to_timedelta('1D')
half_day = pd.to_timedelta('12H')


# maps sleep start time to -1 to +1
def sleep_start_to_value(df: pd.DataFrame):
	try:
		dt = df.index[0]-mean_sleep_start
		return (dt-dt.round('D'))/half_day
	except:
		return nan


# maps sleep stop time to -1 to +1
def sleep_stop_to_value(df: pd.DataFrame):
	try:
		dt = df.index[0]+pd.to_timedelta(df['Seconds'][0], 's')-mean_sleep_stop
		return (dt-dt.round('D'))/half_day
	except:
		return nan


def time_til_asleep(df):
	try:
		main_ind = df[df.Level=='main'].index[0]
		row = df.loc[main_ind].iloc[1]
		return row['Seconds'] if row['Level']=='wake' else 0
	except:
		return nan


def time_til_getup(df):
	# NOTE: the end-point of main sleep does not necessarily correspond to the end-point of the last sleep segment
	try:
		df_mains = df[df.Level=='main']
		getuptime = df_mains.index[0]+pd.to_timedelta(df_mains.Seconds[0], 's')
		diff = pd.Series(abs(df.index-getuptime), index=df.index)
		row = df.loc[diff.idxmin()]
		return row['Seconds'] if row['Level']=='wake' else 0
	except:
		return nan


nan_sum = lambda df : df.sum() if len(df.index) else nan

# revised by Wang Xuancong to fix the sleep day-split boundary and add in goto-bed time, wake-up time, time-til-fall-asleep, and time-til-get-up
def proc(df, verbose=1):
	dt = pd.to_datetime(df['timestamp'], unit='ms', origin='unix', utc=True)
	df = df.set_index(dt.dt.tz_convert('tzlocal()')).sort_index()[['Level', 'Seconds', 'Efficiency', 'MainSleep']]

	if verbose>0:
		print("Loaded data size:", df.shape, file=sys.stderr)
		print("Null report for input data:\n", df.isnull().sum(axis=0), file=sys.stderr)

	levels = ['wake', 'light', 'deep', 'rem']

	df_new = df.groupby(pd.Grouper(freq='D', base=day_break)).apply(lambda df: pd.Series({
		'mean_efficiency': df[df['Level'].isin(levels[1:])]['Efficiency'].mean(),  # mean sleep efficiency
		**{'tot_%s_hrs' % level: nan_sum(df[df.Level == level]['Seconds'])/3600 for level in levels},  # total duration of each sleep levels
		'tot_hrs': nan_sum(df[df['Level'].isin(levels[1:])]['Seconds'])/3600,  # total duration of all asleep levels
		'n_wake_in_main': (df[(df.Level == 'wake') & df.MainSleep].shape[0] if len(df.index) else nan),  # number of awake segments in main sleep period
		'n_wake_gt180s_in_main': (df[(df.Level == 'wake') & (df.Seconds > 180) & df.MainSleep].shape[0] if len(df.index) else nan), # number of awake segments greater than 180 secs during main sleep period
		'tot_main_hrs': nan_sum(df[df['Level'].isin(levels) & df.MainSleep]['Seconds'])/3600,    # total duration of "main sleep" segment (including awake periods... proxy for time in bed?)
		**{'frac_%s_main' % level: nan_sum(df[(df.Level == level) & df.MainSleep]['Seconds'])/3600 for level in levels},   # fraction of each sleep level in main sleep, (Stage1: not divided yet)
		'mainsleep_start': sleep_start_to_value(df[df.Level=='main']),
		'mainsleep_finish': sleep_stop_to_value(df[df.Level=='main']),
		'time_til_asleep': time_til_asleep(df),
		'time_til_getup': time_til_getup(df)
	}))

	# fraction of each sleep level in main sleep, (Stage2: do division allowing NaN/NaN=NaN)
	# Use Add-one smoothing because:
	#   Case 1: no sleep data => frac=NaN
	#   Case 2: did not sleep => frac=0
	np.seterr(divide='ignore')
	df_new[['frac_%s_main'%level for level in levels]] = df_new[['frac_%s_main'%level for level in levels]].div(df_new['tot_main_hrs']+1, axis=0)

	if verbose>1:
		print(df_new.head(), file=sys.stderr)
	if verbose>0:
		print("Null report for transformed data:\n", df_new.isnull().sum(axis=0), file=sys.stderr)

	df_new.index.name = 'datetime'

	return df_new


if __name__ == '__main__':
	parser = argparse.ArgumentParser(usage='$0 input-file output-file 1>progress 2>warning_msg',
									 description='This program processes features, use "-" for STDIN/STDOUT')
	parser.add_argument('input_fn', help='positional argument')
	parser.add_argument('output_fn', help='positional argument')
	parser.add_argument('--verbose', '-v', default=1, type=int, help='verbose level')
	# nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt = parser.parse_args()
	globals().update(vars(opt))

	df = proc(pd.read_csv(Open(input_fn)), verbose=verbose)

	# patch: Fitbit output NaN sleep when "not wearing" or "not sleeping", need to distinguish
	df_steps = pd.read_csv(os.path.dirname(input_fn) + '/steps.csv.gz')
	dt = pd.to_datetime(df_steps['timestamp'], unit='ms', origin='unix', utc=True)
	df_steps = df_steps.set_index(dt.dt.tz_convert('tzlocal()')).sort_index()[['StepsCount']]
	wear_mask = df_steps.groupby(pd.Grouper(freq='D', base=day_break)).apply(lambda df: df.StepsCount.notna().sum() > 1200)

	# set the corresponding columns of zero-sleep rows to 0
	df.loc[wear_mask, [col for col in df.columns if 'tot' in col]] = df.loc[wear_mask, [col for col in df.columns if 'tot' in col]].fillna(0)

	df.to_csv(sys.stdout if output_fn=='-' else output_fn)


# def proc(df):
# 	"""
# 	'sleep.csv'
#
# 	:param df:
# 	:return:
# 	"""
#
# 	usecols = ['timestamp',  #
# 			   'Level',  # ['main', 'wake', 'light', 'deep', 'rem', 'non-main']
# 			   'Seconds',  # duration of the segment (with 30s resolution)
# 			   # 'TZ',          # time zone
# 			   'Efficiency',  # total time asleep / total time in bed
# 			   'MainSleep',  # correctly loads as boolean
# 			   ]
#
# 	df = df[usecols]
#
# 	# TODO: Koa: As far as I see it, entries with df['Level'].isin(['main', 'non-main']) are not only redundant, but
# 	# also inconsistently recorded. Drop for now but check with XC why it doesn't consistently appear in data.
# 	stages = ['wake', 'light', 'deep', 'rem']
# 	df = df[df['Level'].isin(stages)]
#
# 	# combine duplicate values
# 	dups = df.duplicated('timestamp')
# 	if dups.any():
# 		new_df = df[dups].groupby('timestamp').aggregate(np.sum)
# 		new_df = {'timestamp': new_df.index}.update({key: new_df[key] for key in new_df.columns})
# 		df = pd.concat([df[~ dups], new_df], axis=0, )
#
# 	df = df.sort_values(by='timestamp')
# 	df = df.reset_index(drop=True)
#
# 	if verbose>0:
# 		print("Loaded data size:", df.shape, file=sys.stderr)
# 		print("Null report for input data:\n", df.isnull().sum(axis=0), file=sys.stderr)
#
# 	# TODO: Value checking
#
# 	# create the structure of the transformed dataset
# 	# Each of the following features are recorded on a daily basis from 12am-12pm
# 	feature_dtypes = \
# 		dict(mean_efficiency=float,  # mean of efficiency
# 			 tot_deep_hrs=int,  # total duration of deep sleep segments
# 			 tot_light_hrs=int,  # total duration of light sleep segments (from 12am-12pm)
# 			 tot_rem_hrs=int,  # total duration of rem sleep segments (from 12am-12pm)
# 			 tot_wake_hrs=int,  # total duration of awake sleep segments (from 12am-12pm)
# 			 tot_hrs=float,  # total duration of sleep in hours
# 			 n_wake_in_main=int,  # number of awake segments in main sleep period
# 			 n_wake_gt180s_in_main=int,  # number of awake segments greater than 180 secs during main sleep period
# 			 tot_main_hrs=int,  # total duration of "main sleep" segment (including awake periods... proxy for time in bed?)
# 			 frac_deep_main=float,  # (total deep sleep in main sleep) / (total duration of main sleep)
# 			 frac_light_main=float,  # (total light sleep in main sleep) / (total duration of main sleep)
# 			 frac_rem_main=float,
# 			 frac_wake_main=float,
# 			 # main_start=int,  # start of main sleep segment
# 			 # main_finish=int,  # end of main sleep segment
# 			 )
#
# 	grouped, _, _ = group_by_date(df)
# 	unique_dates = grouped.groups.keys()
# 	df_new = pd.DataFrame(nan, index=unique_dates, columns=feature_dtypes.keys())
#
# 	for date, group_inds in grouped.groups.items():
#
# 		df_date = df.iloc[group_inds, :]
#
# 		###  mean_efficiency  ###
# 		# TODO: Not sure about this
# 		df_new.loc[date, 'mean_efficiency'] = np.mean(df_date['Efficiency'])
#
# 		###  total durations  ###
# 		for level in stages:
# 			feature_name = 'tot_{}_hrs'.format(level)
# 			df_new.loc[date, feature_name] = np.sum(df_date[df_date['Level'] == level]['Seconds']) / 3600  # sec -> hrs
#
# 		###  total hours of sleep in the day  ###
# 		df_new.loc[date, 'tot_hrs'] = np.sum(df_date[df_date['Level'].isin(['light', 'rem', 'deep'])]['Seconds']) / 3600
#
# 		###  main sleep features  ###
# 		main_mask = df_date['MainSleep']
#
# 		###  n_awake_in_main  ###
# 		awake_main_mask = np.logical_and(df_date['Level'] == 'wake', main_mask)
# 		df_new.loc[date, 'n_wake_in_main'] = np.sum(awake_main_mask)
#
# 		###  n_awake_180_in_main  ###
# 		df_new.loc[date, 'n_wake_gt180s_in_main'] = np.sum(np.logical_and(df_date['Seconds'] > 180, awake_main_mask))
#
# 		###  tot_main  ###
# 		total_in_main = np.sum(df_date[main_mask]['Seconds'])
# 		df_new.loc[date, 'tot_main_hrs'] = total_in_main / 3600
#
# 		###  proportions in main sleep  ###
# 		if total_in_main > 0:  # no main sleep is possible
# 			for level in stages:
# 				level_main_mask = np.logical_and(df_date['Level'] == level, main_mask)
# 				df_new.loc[date, 'frac_{}_main'.format(level)] = \
# 					np.sum(df_date[level_main_mask]['Seconds']) / total_in_main
# 		else:
# 			for level in stages:
# 				df_new.loc[date, 'frac_{}_main'.format(level)] = 0
#
# 		###  main_start, main_finish  ###
# 		# ts_main = df_date['timestamp'][main_mask]  # in the original, seconds since epoch, format
# 		# ts_main = ts_main.sort_values()  # ascending order
# 		# if len(ts_main):
# 		# 	df_new.loc[date, 'main_start'] = ts_main.iloc[0]
# 		# 	df_new.loc[date, 'main_finish'] = ts_main.iloc[-1]
# 		# else:
# 		# 	df_new = df_new.drop(date)
#
# 	# enforce types
# 	# df_new = df_new.astype(feature_dtypes)
#
# 	if verbose>1:
# 		print(df_new.head(), file=sys.stderr)
# 	assert not df_new.isnull().any().any()
# 	if verbose>0:
# 		print("Null report for transformed data:\n", df_new.isnull().sum(axis=0), file=sys.stderr)
#
# 	df_new.index.name = 'datetime'
#
# 	return df_new