#!/usr/bin/env python3
# coding=utf-8

import os, sys, argparse, re, gzip, json
from utils import *


def proc(df, meta, tz='tzlocal()'):
	# convert timestamp to datetime and set as index
	dt = pd.to_datetime(df['timestamp'], unit='ms', origin='unix', utc=True)
	df['datetime'] = pd.DatetimeIndex(dt).tz_convert(tz)

	# remove rows with duplicate timestamp
	df = df.loc[~df.timestamp.duplicated(keep='last')].ffill()

	# group according to each day
	df = df.set_index('datetime')
	dfg = df.groupby(pd.Grouper(freq='1D'))

	# number of incoming/outgoing/missed calls
	ret = create_frame_from_groupby_value_counts(dfg, 'call type', cls=['Incoming Call', 'Outgoing Call', 'Missed Call'])

	# total duration of call
	ret['totalDur'] = dfg[['duration in seconds']].sum()

	# number of people talked (only those with call duration > 0)
	# dfg1 = df[df['duration in seconds'] > 0].groupby(pd.Grouper(freq='1D'))
	# ret['n_ppl_talked'] = create_frame_from_groupby_value_types(dfg1, 'hashed phone number')
	ret['n_ppl_talked'] = [len(j[j['duration in seconds'] > 0]['hashed phone number'].value_counts()) for i, j in dfg]

	ret.index.name = 'datetime'

	# resolve between true zero vs NaN
	df_new = resolve_zero_vs_nan(ret, meta)

	return df_new


if __name__ == '__main__':
	parser = argparse.ArgumentParser(usage='$0 input-file output-file 1>progress 2>warning_msg',
									 description='This program processes features, use "-" for STDIN/STDOUT')
	parser.add_argument('input_fn', help='positional argument')
	parser.add_argument('output_fn', help='positional argument')
	parser.add_argument('--verbose', '-v', default=1, type=int, help='verbose level')
	# nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt = parser.parse_args()
	globals().update(vars(opt))

	meta = json.load(Open(os.path.dirname(input_fn) + '/meta.json.gz'))
	df = proc(pd.read_csv(Open(input_fn)), meta['callLog'])
	Open(output_fn, 'w').write(df.to_csv()) if output_fn == '-' else df.to_csv(output_fn)
