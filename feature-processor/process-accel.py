#!/usr/bin/env python3
# coding=utf-8

import os, sys, argparse, re, gzip
import pandas as pd
import numpy as np

from utils import *

def create_frame_from_groupby(dfg, col_name, func_names):
	ret = pd.DataFrame()
	if func_names:
		df = dfg[[col_name]]
		ret = eval('df.%s()'%func_names[0])
		ret = ret.rename(columns={col_name: col_name+'_'+func_names[0]})
		for func_name in func_names[1:]:
			ret[[col_name+'_'+func_name]] = eval('df.%s()'%func_name)
	return ret

def proc(df, tz='tzlocal()'):
	# convert timestamp to datetime and set as index
	dt = pd.to_datetime(df['timestamp'], unit='ms', origin='unix', utc=True)
	df['datetime'] = pd.DatetimeIndex(dt).tz_convert('tzlocal()')

	# remove rows with duplicate timestamp
	df = df.loc[~df.timestamp.duplicated(keep='last')].copy()

	# add new column: sqrt(x^2+y^2+z^2)
	df['L'] = np.sqrt(np.square(df[['x', 'y', 'z']]).sum(axis=1))

	# add new column: |∆(x,y,z)/∆t|
	dT = df.timestamp.diff()
	ddt = np.sqrt(np.square(df.x.diff()/dT) + np.square(df.y.diff()/dT) + np.square(df.z.diff()/dT))
	df['ddt'] = pd.Series(0).append(ddt[1:])

	df = df.set_index('datetime')

	# 1. compute hourly features
	dfg = df.groupby(pd.Grouper(freq='1H'))

	# compute max/min/std/mean of L
	new_df_hourly = create_frame_from_groupby(dfg, 'L', ['max', 'min', 'std', 'mean'])

	# compute max of |∆(x,y,z)/∆t|
	new_df_hourly[['ddt_max']] = dfg[['ddt']].max()

	# 2. compute daily features
	dfg = df.groupby(pd.Grouper(freq='1D'))

	# compute max/min/std/mean of L
	new_df_daily = create_frame_from_groupby(dfg, 'L', ['max', 'min', 'std', 'mean'])

	# compute max of |∆(x,y,z)/∆t|
	new_df_daily[['ddt_max']] = dfg[['ddt']].max()

	# proportion of active minutes
	np.warnings.filterwarnings('ignore')
	new_df_daily['active_proportion'] = dfg['ddt'].apply(lambda df: df[df>0.00001].count()/df.count())

	return new_df_daily, new_df_hourly


if __name__ == '__main__':
	parser = argparse.ArgumentParser(usage='$0 input-file output-file 1>progress 2>warning_msg',
									 description='This program processes features, use "-" for STDIN/STDOUT')
	parser.add_argument('input_fn', help='positional argument')
	parser.add_argument('output_fn', help='positional argument')
	parser.add_argument('--verbose', '-v', default=1, type=int, help='verbose level')
	# nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt = parser.parse_args()
	globals().update(vars(opt))

	new_df_daily, new_df_hourly = proc(pd.read_csv(Open(input_fn)))

	if output_fn == '-':
		print(new_df_daily.to_csv(), flush=True)
		print(new_df_hourly.to_csv(), end='', flush=True)
	else:
		new_df_daily.to_csv(add_str_to_fn(output_fn, 'daily'))
		new_df_hourly.to_csv(add_str_to_fn(output_fn, 'hourly'))
