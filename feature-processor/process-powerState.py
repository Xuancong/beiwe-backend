#!/usr/bin/env python3
# coding=utf-8

import os, sys, gzip, argparse, json
import numpy as np
import pandas as pd

from utils import *

# """
# By Phang Ye Sheng (revised by Xuancong)
# """
# def proc(df):
# 	"""
# 		Power state (powerState.csv)
# INPUT: a log of every event such as screen turn on/off, power-down signal, device idle state change, etc.
# OUTPUT: 	every hour, number of times of screen-turn-on, number of power-down signals, max/min/std/mean duration of each screen-on session
#
# 		:param df:
# 		:return:
# 		"""
# 	usecols = ['timestamp',
# 			   'event',
# 			   # 'orientation',
# 			   # 'tz',
# 			   ]
#
# 	# create the structure of the transformed dataset
# 	hourly_feature_dtypes = \
# 		dict(screen_on_times=int,  # total number of taps in the hour
# 			 power_down_times=int,  # mean of inter-tap duration while the screen is on
# 			 max_screen_on_time=float,  # std dev -- " --
# 			 min_screen_on_time=float,
# 			 std_screen_on_time=float,
# 			 mean_screen_on_time=float,
# 			 )
# 	df = df[usecols]
#
# 	if verbose>0:
# 		print("Null report for input data:\n", df.isnull().sum(axis=0), file=sys.stderr)
#
# 	###removing data that we dont use for calculation
#
# 	# df_subset = df
# 	# df_subset = df[~(df.event == ('Device Idle (Doze) state change signal received; device in idle state.'))]
# 	# df_subset = df[~(df.event == ('Device Idle (Doze) state change signal received; device not in idle state.'))]
# 	# df_subset = df[~(df.event == ('Power Save Mode state change signal received; device in power save state.'))]
# 	# df_subset.reset_index(inplace=True, drop=True)
# 	# revised by Xuancong
# 	df_subset = df[df.event.isin(['Screen turned on', 'Screen turned off'])].reset_index(drop=True)
#
# 	### Calulating screen on time
# 	### Screen on time is defined as if event is 'Screen turned on' find the difference in time between 'Screen turned on' and
# 	### 'Screen turned off' or 'Device shut down signal received'
# 	# diff_list = []
# 	# for i in range(df_subset.shape[0]):
# 	# 	category = df_subset.event[i]
# 	# 	if category == 'Screen turned on':
# 	# 		time_diff = df_subset.timestamp[i + 1] - df_subset.timestamp[i]
# 	# 	else:
# 	# 		time_diff = np.nan
# 	# 	diff_list.append(time_diff)
# 	# df_subset['diff'] = diff_list
# 	# revised by Xuancong
# 	df_subset['diff'] = df_subset.timestamp[1:].append(pd.Series(np.nan)).reset_index(drop=True)-df_subset.timestamp[0:]
# 	df_subset = df_subset[~(df_subset['diff'].isna())]  # removing all nan value
# 	df_subset.reset_index(inplace=True, drop=True)  # reindex
#
# 	grouped_hour, timestamp_hour, _ = group_by_hour(df_subset)
# 	unique_hours = grouped_hour.groups.keys()
# 	new_df_hourly = pd.DataFrame(np.nan, index=unique_hours, columns=hourly_feature_dtypes.keys())
#
# 	for hour, group_inds in grouped_hour.groups.items():
# 		df_hour = df_subset.loc[group_inds, :]
# 		###  screen on times and power down times per hour ###
# 		new_df_hourly.loc[hour, 'screen_on_times'] = int(np.sum(df_hour['event'] == 'Screen turned on'))
# 		new_df_hourly.loc[hour, 'power_down_times'] = int(np.sum(df_hour['event'] == 'Device shut down signal received'))
#
# 		### Computing mean, max, min, std of screen on time
# 		new_df_hourly.loc[hour, 'mean_screen_on_time'] = df_hour['diff'].mean()  # in minutes divide by 60000
# 		new_df_hourly.loc[hour, 'std_screen_on_time'] = df_hour['diff'].std()  # data now in milisecond
# 		new_df_hourly.loc[hour, 'min_screen_on_time'] = df_hour['diff'].min()
# 		new_df_hourly.loc[hour, 'max_screen_on_time'] = df_hour['diff'].max()
#
# 	new_df_hourly = new_df_hourly.astype(hourly_feature_dtypes)
# 	if verbose>1:
# 		print("Hourly DF:\n", new_df_hourly.head(), file=sys.stderr)
# 	# assert not new_df_hourly.isnull().any().any()
# 	if verbose>0:
# 		print("Null report for transformed data:\n", new_df_hourly.isnull().sum(axis=0), file=sys.stderr)
#
# 	new_df_hourly.index.name = 'datetime'
#
# 	return new_df_hourly


def proc(df, meta, tz=tzlocal()):
	# convert timestamp to datetime and set as index
	dt = pd.to_datetime(df['timestamp'], unit='ms', origin='unix', utc=True)
	df = df.set_index(dt.dt.tz_convert(tzlocal())).drop(columns='tz').sort_index()
	df = df[df.event.isin(['Screen turned on', 'Screen turned off', 'Device shut down signal received'])]

	# compute screen on duration for each screen_on event
	df['dur'] = ((df.timestamp.shift(-1)-df.timestamp)*0.001).clip(upper=3600*24)
	df.loc[~((df.event=='Screen turned on')&(df.event.shift(-1)!='Screen turned on')), 'dur'] = 0

	# compute the screen_on duration overflow into the next hour
	df['overflow'] = (df.index+pd.to_timedelta(df.dur, unit='s')-df.index.ceil('H')).dt.total_seconds().clip(lower=0)

	# 1. hourly feature
	ret_hourly = df.groupby(pd.Grouper(freq='1H'))[['dur', 'overflow']].sum()
	ret_hourly.dur += (ret_hourly.overflow.shift().fillna(0)-ret_hourly.overflow)
	while ret_hourly.dur.max()>3600:
		overflow = ret_hourly.dur-ret_hourly.dur.clip(upper=3600)
		ret_hourly.dur += (overflow.shift().fillna(0)-overflow)
	n_event = df[['event']].groupby(pd.Grouper(freq='1H')).count()
	ret_hourly = ret_hourly.drop(columns='overflow').join(n_event, how='outer').rename(columns={'event':'n_screen_on'})

	# 2. daily feature
	ret_daily = create_frame_from_groupby(df[df.dur>0].groupby(pd.Grouper(freq='D')), 'dur', ['max', 'min', 'std', 'mean', 'median'])
	dfg = df[['event']].groupby(pd.Grouper(freq='D'))
	n_poweroff = pd.DataFrame([[k,v[v.event=='Device shut down signal received'].size] for k,v in dfg], columns=['datetime', 'n_poweroff'])
	ret_daily = ret_daily.join(n_poweroff.set_index('datetime'), how='outer')

	ret_daily.index.name = ret_hourly.index.name = 'datetime'

	# resolve between true zero vs NaN
	ret_daily = resolve_zero_vs_nan(ret_daily, meta)
	ret_hourly = resolve_zero_vs_nan(ret_hourly, meta, edge_overflow=0.4, freq='H')

	return ret_daily, ret_hourly


if __name__ == '__main__':
	parser = argparse.ArgumentParser(usage='$0 input-file output-file 1>progress 2>warning_msg',
									 description='This program processes features, use "-" for STDIN/STDOUT')
	parser.add_argument('input_fn', help='positional argument')
	parser.add_argument('output_fn', help='positional argument')
	parser.add_argument('--verbose', '-v', default=1, type=int, help='verbose level')
	# nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt = parser.parse_args()
	globals().update(vars(opt))

	meta = json.load(Open(os.path.dirname(input_fn) + '/meta.json.gz'))
	df_daily, df_hourly = proc(pd.read_csv(Open(input_fn)), meta['powerState'])

	if output_fn == '-':
		print(df_daily.to_csv(), flush=True)
		print(df_hourly.to_csv(), end='', flush=True)
	else:
		df_daily.to_csv(add_str_to_fn(output_fn, 'daily'))
		df_hourly.to_csv(add_str_to_fn(output_fn, 'hourly'))
