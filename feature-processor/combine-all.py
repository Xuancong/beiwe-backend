#!/usr/bin/env python3
# coding=utf-8

import os, sys, argparse, fnmatch, traceback
from collections import *
from utils import *


def load_df(filename):
	try:
		df = pd.read_csv(filename)
		df = df.set_index('datetime' if 'datetime' in df.columns else df.columns[0])
		df.index = df.index.map(pd.Timestamp)
		assert len(df)
		if df.index[0].tz is None:
			df.index = df.index.tz_localize(tzlocal())
		return df
	except:
		return None


def get_feature_dimension(fn):
	with Open(fn, mode='rt', encoding='utf-8', errors='ignore') as fp:
		cols = fp.readline().strip().split(',')[1:]
	return cols


def is_feature_hourly(fn):
	try:
		hour = load_df(fn).index.hour
	except:
		# returning 0 means not sure
		return 0
	# note it is possible to have a daily feature timed at non-zero o'clock everyday
	# it is possible to encounter many files with either only 1 row or a few rows all at non-zero o'clock
	return (1 if len(set(hour))>1 else -1)*(0.01*min(100, len(hour)))


def stemname(fn):
	while True:
		cnt = 0
		for suffix in ['.csv', '.gz']:
			if fn.endswith(suffix):
				fn = fn[:-len(suffix)]
				cnt += 1
		if cnt == 0:
			break
	return fn


def modify_col_names(cols, hourly=False, prefix=None, suffix=None, stem=True):
	if hourly:
		cols = ['%s_%02dh'%(c,i) for c in cols for i in range(24)]
	if prefix is not None:
		if stem: prefix = stemname(prefix)
		cols = [prefix+'_'+c for c in cols]
	if suffix is not None:
		if stem: suffix = stemname(suffix)
		cols = [c+'_'+suffix for c in cols]
	return cols


def build_feature_for_one_patient(user_path, feature_dims, is_hourly):
	df = pd.DataFrame()
	for fea_name, col_names in feature_dims.items():
		filename = user_path+'/'+fea_name
		rhs = load_df(filename)
		if rhs is None:
			columns = modify_col_names(col_names, hourly=is_hourly[fea_name], prefix=fea_name)
			df = df.join(pd.DataFrame(columns=columns), how='outer')
		else:
			if is_hourly[fea_name]:
				rhs['HOUR'] = rhs.index.hour
				rhs.index = rhs.index.floor('D')
				rhs = rhs.reset_index().set_index(['datetime', 'HOUR']).unstack()
				rhs.columns = ['%s_%02dh'%(i,int(j)) for i,j in zip(rhs.columns.get_level_values(0), rhs.columns.get_level_values(1))]
				col_names = modify_col_names(col_names, hourly=True)
				# put back missing hour columns
				if set(rhs.columns) != set(col_names):
					for col in set(col_names)-set(rhs.columns):
						rhs[col] = nan
			rhs.index = rhs.index.floor('D')
			df = df.join(rhs[col_names].rename(columns={s:t for s,t in zip(col_names, modify_col_names(col_names, prefix=fea_name))}), how='outer')

	# Merging tzlocal() with FixedOffset+8 gives UTC-0
	if len(df.index) > 0:
		df.index = df.index.tz_convert('tzlocal()')

	return df


# scan all feature files to ensure their dimensions conform
def scan_feature_files(root_path, pattern):
	# if pattern is a string, it implies filename pattern
	# if pattern is a list, it implies the list of feature files
	ret_cols_map = OrderedDict({})
	ret_userlist = []
	ret_is_hourly = {}
	colname_set = set()
	for user in os.listdir(root_path):
		if not os.path.isdir(root_path+'/'+user): continue
		ret_userlist += [user]
		file_list = fnmatch.filter(os.listdir(root_path+'/'+user), pattern) if type(pattern)==str else pattern
		for file in file_list:
			if not os.path.isfile(root_path+'/'+user+'/'+file): continue
			cols = get_feature_dimension(root_path + '/' + user + '/' + file)
			if file in ret_cols_map:
				if set(ret_cols_map[file]) != set(cols):
					raise Exception('feature file with inconsistent columns: %s\nColumns of this file: %s\nColumns of other file: %s'
						  %(root_path + '/' + user + '/' + file, cols, ret_cols_map[file]))
				if type(ret_is_hourly[file]) != bool:
					val = ret_is_hourly[file] + is_feature_hourly(root_path + '/' + user + '/' + file)
					ret_is_hourly[file] = True if val>=1 else (False if val<=-1 else val)
			else:
				ret_cols_map[file] = cols
				ret_is_hourly[file] = is_feature_hourly(root_path + '/' + user + '/' + file)
				new_col_set = set(modify_col_names(cols, prefix=file))
				if new_col_set & colname_set:
					raise Exception('duplicate column name %s'%(new_col_set&colname_set))
				colname_set |= new_col_set

	# make sure all are boolean
	ret_is_hourly = {k:(v if type(v) is bool else v>0) for k,v in ret_is_hourly.items()}

	return ret_cols_map, ret_is_hourly, ret_userlist


if __name__=='__main__':
	parser = argparse.ArgumentParser(usage='$0 [options] input-folder 1>output 2>warning_and_progress',
									 description='This program processes features, use "-" for STDIN/STDOUT')
	parser.add_argument('root_path', help="the root path containing every user's data folders")
	parser.add_argument('--output-folder', '-o', type=str, default=None, help="output folder to store each user's data file")
	parser.add_argument('--feature-files', '-f', help='manually specify the list of feature files to be considered, (by default, consider all feature files in the directory)', nargs='*')
	parser.add_argument('--filename-pattern', '-p', type=str, default='*.csv.gz', help='what filenames (string pattern) do we consider?')
	parser.add_argument('--output-pson', '-op', type=str, default='', help='output combined file in PSON format')
	parser.add_argument('--verbose', '-v', type=int, default=0, help='verbosity level')
	#nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt=parser.parse_args()
	globals().update(vars(opt))

	# scan feature dimensions
	feature_dims, is_hourly, user_list = scan_feature_files(root_path, feature_files if feature_files else filename_pattern)

	# print feature dimension information
	print('Feature dimensions:\n%s'%('\n'.join(['%-8s = %s%s%s = %s'%('%s%d%s'%(TC.BLG,len(v),TC.END)+(TC.BLY+'x24'+TC.END if is_hourly[k] else ''), TC.BLR, k, TC.END, v) for k,v in feature_dims.items()])), file=sys.stderr)
	print('Total dimension size = %s%d%s\n' % (TC.BLG, sum([len(v)*(24 if is_hourly[f] else 1) for f,v in feature_dims.items()]), TC.END), file=sys.stderr)

	# build output features
	Y = TC.LG + 'Y' + TC.END
	N = TC.LY + 'N' + TC.END
	E = TC.BLR + 'E' + TC.END
	print('Building feature files for every patient (%s=Yes; %s=No data; %s=Error):'%(Y, N, E), file=sys.stderr)
	out = {}
	for user in user_list:
		try:
			if verbose>0:
				print('building feature for User %s'%user, file=sys.stderr, flush=True)
			df = build_feature_for_one_patient(root_path+'/'+user, feature_dims, is_hourly)
			if output_folder:
				makedirs(output_folder)
				df.to_csv(output_folder+'/'+user+'.csv.gz')
			out[user] = df
			print(Y if len(df) else N, end='', file=sys.stderr, flush=True)
		except:
			print(E, end='', file=sys.stderr)
			traceback.print_exc()
	print(file=sys.stderr)

	if output_pson:
		from pandas_serializer import *
		pandas_save(out, output_pson)

	print(str({k:v.to_csv() for k,v in out.items()}).replace("', ", "',\n"))
