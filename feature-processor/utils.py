import datetime, os, sys, gzip
import pandas as pd
import numpy as np
from dateutil.tz import tzlocal

nan = np.nan

## This set the timezone to local, which is incorrect coz Unix timestamp is always defined to be at UTC-0
# def ms_epoch_to_ts(ms_epoch_ts):
# 	ms_epoch = ms_epoch_ts / 1000  # seconds since epoch time
# 	return ms_epoch.apply(lambda x_: datetime.datetime.fromtimestamp(x_, tz=tzlocal()))

def ms_epoch_to_ts(ms_epoch_ts):
	dt = pd.to_datetime(ms_epoch_ts, unit='ms', origin='unix', utc=True)
	ret = dt.dt.tz_convert(tzlocal())
	return ret


def group_by_hour(df):
	"""
	:param df: DataFrame with a column 'timestamp' of dtype int containing milliseconds from epoch time
	:return:
	"""
	assert 'timestamp' in df.columns
	assert df['timestamp'].dtype == int

	# group timestamps by hour
	ts_ = ms_epoch_to_ts(df['timestamp'])
	end_hour = ts_.dt.ceil(freq='H')  # the ending hour of the bin into which the timestamp falls
	# end_hour = ts_.ceil('H')

	grouped = end_hour.to_frame(name='end_hour').groupby('end_hour')

	return grouped, ts_, end_hour


def group_by_date(df):
	"""
	:param df: DataFrame with a column 'timestamp' of dtype int containing milliseconds from epoch time
	:return:
	"""
	assert 'timestamp' in df.columns
	assert df['timestamp'].dtype == int

	# group timestamps by day
	ts_ = ms_epoch_to_ts(df['timestamp'])

	# pd.groupby(df['timestamp'].dt.normalize())  # normalize() sets to midnight
	dates = pd.Series([x_.date() for x_ in ts_], name='date')
	grouped = dates.to_frame().groupby(by='date')

	return grouped, ts_, dates


def quantile(obj, Q):
	try:
		return obj.quantile(Q)
	except:
		return []


def Try(L, alt):
	try:
		return L()
	except:
		return alt


def apply_frame_from_groupby(df, dfg, col_name, func_names):
	func_names = {col_name+'_'+i:i for i in func_names} if type(func_names)==list else func_names
	if dfg.size().to_list() == []:
		df1 = pd.DataFrame([], columns=[s for s in func_names])
	else:
		df1 = dfg[col_name]
	for out_name, func_name in func_names.items():
		df[out_name] = eval(('df1%s' if func_name.startswith('[') else 'df1.%s()') % func_name)
	return df


def create_frame_from_groupby(dfg, col_name, func_names):
	ret = pd.DataFrame()
	func_names = {col_name+'_'+i:i for i in func_names} if type(func_names)==list else func_names
	if dfg.size().to_list() == []:
		ret = pd.DataFrame([], columns=[s for s in func_names])
	else:
		df = dfg[col_name]
		for out_name, func_name in func_names.items():
			ret[out_name] = eval(('df%s' if func_name.startswith('[') else 'df.%s()') % func_name)
	return ret


def create_frame_from_groupby_value_counts(dfg, col_name, cls=None):
	if type(cls) == list:
		selected_cls = cls
	else:
		data0 = dfg.filter(lambda t: True)
		stats = data0[col_name].value_counts()
		N_cls_present = stats.size
		N_cls = N_cls_present if cls is None else min(cls, N_cls_present)
		selected_cls = stats.index.tolist()[:N_cls]
	map_null = lambda t: t if len(t) else {k: 0 for k in selected_cls}
	ret = pd.DataFrame.from_dict({g[0]: map_null(g[1][col_name].value_counts()) for g in dfg}, orient='index')
	ret = ret.fillna(0).sort_index()
	for k in selected_cls:
		if k not in ret.columns:
			ret[k] = 0
	return ret


def create_frame_from_groupby_value_types(dfg, col_name):
	ret = pd.DataFrame.from_dict({g[0]: len(g[1][col_name].value_counts()) for g in dfg}, orient='index')
	ret = ret.fillna(0).sort_index()
	return ret


def makedirs(path):
	try:
		os.makedirs(path)
	except:
		pass


def Open(fn, mode='r', **kwargs):
	if fn == '-':
		return sys.stdin if mode.startswith('r') else sys.stdout
	fn = os.path.expanduser(fn)
	return gzip.open(fn, mode, **kwargs) if fn.lower().endswith('.gz') else open(fn, mode, **kwargs)


def preprocess_df(df):
	# convert timestamp to datetime and set as index
	dt = pd.to_datetime(df['timestamp'], unit = 'ms', origin = 'unix', utc = True)
	df = df.set_index(pd.DatetimeIndex(dt).tz_convert('tzlocal()')).sort_index()

	# remove rows with duplicate timestamp
	df = df.loc[~df.index.duplicated(keep = 'last')]
	df.index.name = 'datetime'
	return df


def load_and_preprocess(fn):
	df = pd.read_csv(Open(fn))
	return preprocess_df(df)


def add_str_to_fn(fn, s):
	p = fn.rfind('.csv')
	return fn+'.'+s if p<0 else fn[:p+1]+s+fn[p:]


def resolve_zero_vs_nan(df, meta, edge_overflow=0, freq='D', cut_off='auto'):
	key_col = '_COUNT_'
	while key_col in df.columns:
		key_col += '_'
	cut_off = (1 if freq[-1]=='D' else 0) if cut_off=='auto' else cut_off
	f_stamps = pd.to_datetime(sorted(meta), unit='ms', utc=True).tz_convert('tzlocal()')
	df_ = pd.DataFrame([0] * f_stamps.size, index=f_stamps, columns=[key_col])
	df_zero = df_.groupby(pd.Grouper(freq=freq)).count()
	if edge_overflow > 0:
		assert edge_overflow < 0.5, 'setting edge_overflow>=0.5 is not supported'
		dfL = df_.groupby(pd.Grouper(freq=freq, base=-edge_overflow)).count()
		dfR = df_.groupby(pd.Grouper(freq=freq, base=edge_overflow)).count()
		dfL = dfL.set_index(dfL.index.round(freq))
		dfR = dfR.set_index(dfR.index.round(freq))
		df_zero = pd.concat([dfL, df_zero, dfR], axis=1).fillna(0).max(axis=1).to_frame(key_col)
	ret = df.join(df_zero, how='outer').apply(lambda df1: df1.fillna(0) if df1[key_col]>cut_off else df1+np.nan, axis=1).drop(columns=key_col)
	ret.index.name = df.index.name
	return ret


class TC:
	HEADER = '\033[95m'
	OKBLUE = '\033[94m'
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'
	END = '\033[0m'
	BLR = '\033[1m\033[91m'
	BLG = '\033[1m\033[92m'
	BLY = '\033[1m\033[93m'
	LR = '\033[91m'
	LG = '\033[92m'
	LY = '\033[93m'
