#!/usr/bin/env python3
# coding=utf-8

import numpy as np
import pandas as pd
import os, sys, math, gzip, argparse

from utils import *


def proc(fn, powerState_fn=None):
	df = load_and_preprocess(fn)[['value']]

	# load powerState file if available
	try:
		ps_df = load_and_preprocess(powerState_fn)[['event']]
	except:
		ps_df = pd.DataFrame(columns = ['event'], index=pd.DatetimeIndex([], tz='tzlocal()'))

	df = np.log1p(df)

	# hourly features
	new_df_hourly = create_frame_from_groupby(df.groupby(pd.Grouper(freq='H')), 'value', {'mean_log1p_lum': 'mean', 'max_log1p_lum': 'max'})

	# daily features
	dfg = df.groupby(pd.Grouper(freq = 'D'))
	new_df_daily = create_frame_from_groupby(dfg, 'value', {'n_readings': 'count', 'mean_log1p_lum': 'mean', 'max_log1p_lum': 'max'})
	new_df_daily['n_pos_readings'] = dfg.apply(lambda df: (df>0).sum())

	df2 = df.join(ps_df, how='outer')
	df2.event = df2.event.ffill().fillna('Screen turned on')
	apply_frame_from_groupby(new_df_daily, df2[df2.event=='Screen turned on'].groupby(pd.Grouper(freq = 'D')),
	                         'value', {'mean_log1p_lum_screen_on': 'mean', 'max_log1p_lum_screen_on': 'max'})

	return new_df_daily, new_df_hourly


if __name__ == '__main__':
	parser = argparse.ArgumentParser(usage='$0 input-file output-file 1>progress 2>warning_msg',
									 description='This program processes features, use "-" for STDIN/STDOUT')
	parser.add_argument('input_fn', help='positional argument')
	parser.add_argument('output_fn', help='positional argument')
	parser.add_argument('--verbose', '-v', default=1, type=int, help='verbose level')
	# nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt = parser.parse_args()
	globals().update(vars(opt))

	new_df_daily, new_df_hourly = proc(input_fn, os.path.dirname(input_fn) + '/powerState.csv.gz')

	if output_fn == '-':
		print(new_df_daily.to_csv(), flush=True)
		print(new_df_hourly.to_csv(), end='', flush=True)
	else:
		new_df_daily.to_csv(add_str_to_fn(output_fn, 'daily'))
		new_df_hourly.to_csv(add_str_to_fn(output_fn, 'hourly'))


# def proc(fn, powerState_fn):
# 	"""
# 	'light.csv' Ambient light (light.csv)
# 	INPUT: every 5 minutes (or longer), a value in units of lux ranging from 0 to ~50k
# 	OUTPUT: every hour, max/mean/min of log(1+value)
#
# 	:param df:
# 	:return:
# 	"""
# 	df = pd.read_csv(Open(fn))
# 	usecols = ['timestamp',
# 			   'value',
# 			   ]
#
# 	df = df[usecols]
# 	df = df.sort_values(by='timestamp')
#
# 	if verbose>0:
# 		print("Loaded data size:", df.shape)
# 		print("Null report for input data:\n{}".format(df.isnull().sum(axis=0)))
#
# 	assert not df.duplicated('timestamp').any()
#
# 	# create the structure of the transformed dataset
# 	hourly_feature_dtypes = \
# 		dict(mean_log1p_lum=float,      # Mean lumens in the hour
# 			 max_log1p_lum=float,       # max lumens in the hour
# 			 )
#
# 	daily_feature_dtypes = \
# 		dict(n_readings=int,                # number of readings taken in the day
# 			 n_pos_readings=int,            # number of readings greater than zero
# 			 mean_log1p_lum=float,          # mean lumens across the whole day
# 			 # mean_hourmax_log1p_lum=float,  # mean of the "max lumens in an hour" across all hours in the day
# 			 )
#
# 	################################
# 	###  Create hourly features  ###
# 	################################
#
# 	grouped_hour, timestamp_hour, _ = group_by_hour(df)
# 	unique_hours = grouped_hour.groups.keys()
# 	new_df_hourly = pd.DataFrame(np.nan, index=unique_hours, columns=hourly_feature_dtypes.keys())
#
# 	for hour, group_inds in grouped_hour.groups.items():
# 		df_hour = df.loc[group_inds, :]
# 		new_df_hourly.loc[hour, 'mean_log1p_lum'] = np.mean(np.log1p(df_hour['value']))
# 		new_df_hourly.loc[hour, 'max_log1p_lum'] = np.max(np.log1p(df_hour['value']))
#
# 	# enforce types
# 	new_df_hourly = new_df_hourly.astype(hourly_feature_dtypes)
#
# 	if verbose>1:
# 		print("Hourly DF:\n{}".format(new_df_hourly.head()))
# 	assert not new_df_hourly.isnull().any().any()
# 	if verbose>0:
# 		print("Null report for transformed data:\n", new_df_hourly.isnull().sum(axis=0))
#
# 	###############################
# 	###  Create daily features  ###
# 	###############################
#
# 	grouped_date, timestamp_date, _ = group_by_date(df)
# 	unique_dates = grouped_date.groups.keys()
# 	new_df_daily = pd.DataFrame(np.nan, index=unique_dates, columns=daily_feature_dtypes.keys())
#
# 	for date, group_inds in grouped_date.groups.items():
#
# 		df_date = df.iloc[group_inds, :]
#
# 		new_df_daily.loc[date, 'n_readings'] = len(df_date)  # number of measurements
# 		new_df_daily.loc[date, 'n_pos_readings'] = np.sum(df_date['value'] > 0)  # number of measurements
# 		new_df_daily.loc[date, 'mean_log1p_lum'] = np.mean(np.log1p(df_date['value']))  # mean of the measurements
#
# 		# hours_in_day = new_df_hourly.index.intersection(timestamp_date[group_inds])
# 		# new_df_daily.loc[date, 'mean_hourmax_log1p_lum'] = np.mean(new_df_hourly.loc[hours_in_day]['max_log1p_lum'])
#
# 	# enforce dtypes
# 	new_df_daily = new_df_daily.astype(daily_feature_dtypes)
#
# 	if verbose > 1:
# 		print("Daily DF:\n", new_df_daily.head())
# 	assert not new_df_daily.isnull().any().any()
# 	if verbose > 0:
# 		print("Null report for transformed data:\n", new_df_daily.isnull().sum(axis=0))
#
# 	new_df_daily.index.name = new_df_hourly.index.name = 'datetime'
#
# 	return new_df_daily, new_df_hourly
