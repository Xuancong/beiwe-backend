#!/usr/bin/env python3
# coding=utf-8

import os, sys, gzip, argparse, json
import numpy as np
import pandas as pd
from utils import *


def proc(df, meta):
	"""
	'sociabilityLog.csv'

	:param df:
	:param root_savedir:
	:return:
	"""

	usecols = ['timestamp',  # timestamp of the event processed in milliseconds (epoch time)
			   # 'app',           # package name of the app, e.g.: com.whatsapp
			   'contact',  # hashed contact name of the other person
			   'orientation',  # Sent -> 0, Received -> 1
			   'length',  # message length -> integer, attachments -> -1
			   # 'minute',        # whatsapp chat screen timestamp beside the messages hh:mm format converted to hhmm applicable only for messages from chat screen
			   'type',
			   # type of message captured, Voice Recording - 7, File - 6, Location - 5, Contact - 4, Audio - 3, Video - 2, Image - 1, Text - 0
			   # 'groupName',     # hashed group name of the chat if available
			   # 'useCaseType',   # Where the message is recorded from, Popup - 2, Foreground - 1, Notification - 0
			   # 'received',      # timestamp of the event received in milliseconds (epoch time)
			   # 'tz',            # timezone difference in minutes 480(+8:00 * 60 minutes) for Singapore
			   ]

	# df = pd.read_csv(gzip.open(data_filepath, 'r'), usecols=usecols)
	df = df[usecols]
	if verbose>0:
		print("Null report for input data:\n", df.isnull().sum(axis=0), file=sys.stderr)

	# TODO: Value checking

	# create the structure of the transformed dataset
	feature_dtypes = \
		dict(n_msg_recv=int,
			 n_msg_sent=int,
			 tot_len_txt_recv=int,
			 tot_len_txt_sent=int,
			 n_img_recv=int,
			 n_img_sent=int,
			 n_contact_only_recv=int,
			 n_contact_recv_and_sent=int,
			 n_contact_only_sent=int,
			 )

	grouped, _, _ = group_by_date(df)

	unique_dates = grouped.groups.keys()
	df_new = pd.DataFrame(np.nan, index=unique_dates, columns=feature_dtypes.keys())

	for date, group_inds in grouped.groups.items():

		df_date = df.loc[group_inds, :]

		###  n_msg_received  ###
		df_new.loc[date, 'n_msg_recv'] = np.sum(df_date['orientation'] == 1)

		###  n_msg_sent  ###
		df_new.loc[date, 'n_msg_sent'] = np.sum(df_date['orientation'] == 0)

		###  total_len_txt_received  ###
		mask = np.logical_and(df_date['type'] == 0, df_date['orientation'] == 1)  # received texts
		df_new.loc[date, 'tot_len_txt_recv'] = np.sum(df_date['length'][mask])

		###  total_len_txt_sent  ###
		mask = np.logical_and(df_date['type'] == 0, df_date['orientation'] == 0)  # sent texts
		df_new.loc[date, 'tot_len_txt_sent'] = np.sum(df_date['length'][mask])

		###  n_images_received  ###
		mask = np.logical_and(df_date['type'] == 1, df_date['orientation'] == 1)  # received images
		df_new.loc[date, 'n_img_recv'] = np.sum(mask)  # some errors arising here

		###  n_images_sent  ###
		mask = np.logical_and(df_date['type'] == 1, df_date['orientation'] == 0)  # sent images
		df_new.loc[date, 'n_img_sent'] = np.sum(mask)

		###  n_contact_only_recv, n_contact_only_sent, n_contact_recv_and_sent  ###
		date_grouped = df_date.groupby(['contact', 'orientation'])

		# hard code sent/recv, since there could only be one for a given day
		contact_counts = pd.DataFrame(np.nan, index=df_date['contact'].unique(), columns=[0, 1])  # [received, sent]
		for (contact, orientation), inds in date_grouped.groups.items():
			contact_counts.loc[contact, orientation] = len(inds)
		contacted = ~ contact_counts.isnull()  # True means there is contact in the direction of the column

		df_new.loc[date, 'n_contact_only_recv'] = np.sum(np.logical_and(contacted.loc[:, 0], ~ contacted.loc[:, 1]))
		df_new.loc[date, 'n_contact_only_sent'] = np.sum(np.logical_and(~ contacted.loc[:, 0], contacted.loc[:, 1]))
		df_new.loc[date, 'n_contact_recv_and_sent'] = np.sum(np.logical_and(contacted.loc[:, 0], contacted.loc[:, 1]))

	# enfore types
	df_new = df_new.astype(feature_dtypes)

	if verbose>1:
		print(df_new.head(), file=sys.stderr)
	assert not df_new.isnull().any().any()
	if verbose>0:
		print("Null report for transformed data:\n", df_new.isnull().sum(axis=0), file=sys.stderr)

	df_new.index.name = 'datetime'

	# resolve between true zero vs NaN
	df_new = df_new.set_index(pd.to_datetime(df_new.index).tz_localize('tzlocal()'))
	df_new = resolve_zero_vs_nan(df_new, meta)

	return df_new

	############################################
	###  Save features as stand-alone files  ###
	############################################
	# overwrite features currently in directory
	# for key in df_new.keys():
	#     feature = df_new[key]
	#     feature.index.name = 'date'
	#     feature.to_csv(os.path.join(root_savedir, 'sociabilityLog-{}.csv'.format(key)),
	#                    index=True, header=True)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(usage='$0 input-file output-file 1>progress 2>warning_msg',
									 description='This program processes features, use "-" for STDIN/STDOUT')
	parser.add_argument('input_fn', help='positional argument')
	parser.add_argument('output_fn', help='positional argument')
	parser.add_argument('--verbose', '-v', default=1, type=int, help='verbose level')
	# nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt = parser.parse_args()
	globals().update(vars(opt))

	meta = json.load(Open(os.path.dirname(input_fn) + '/meta.json.gz'))
	df = proc(pd.read_csv(Open(input_fn)), meta['sociabilityLog'])
	Open(output_fn, 'w').write(df.to_csv()) if output_fn == '-' else df.to_csv(output_fn)
