#!/usr/bin/env python3
# coding=utf-8

import os, sys, gzip, argparse, json
import numpy as np
import pandas as pd
from utils import *


def proc(df, meta):
	"""
	'textsLog.csv'
	:param df:
	:return:
	"""

	usecols = ['timestamp',
			   'hashed phone number',
			   'sent vs received',
			   'message length',
			   # 'time sent',
			   # 'tz',
			   ]

	# df = pd.read_csv(gzip.open(data_filepath, 'r'), usecols=usecols)
	df = df[usecols]
	if verbose>0:
		print("Null report for input data:\n", df.isnull().sum(axis=0), file=sys.stderr)

	# TODO: Value checking

	# create the structure of the transformed dataset
	feature_dtypes = \
		dict(n_msg_recv=int,
			 n_msg_sent=int,
			 tot_len_recv=int,
			 tot_len_sent=int,
			 n_contact_only_recv=int,
			 n_contact_recv_and_sent=int,
			 n_contact_only_sent=int,
			 )

	grouped, _, _ = group_by_date(df)

	unique_dates = grouped.groups.keys()
	df_new = pd.DataFrame(np.nan, index=unique_dates, columns=feature_dtypes.keys())

	for date, group_inds in grouped.groups.items():

		df_date = df.loc[group_inds, :]

		###  n_msg_recv  ###
		df_new.loc[date, 'n_msg_recv'] = int(np.sum(df_date['sent vs received'] == 'received SMS'))

		###  n_msg_sent  ###
		df_new.loc[date, 'n_msg_sent'] = int(np.sum(df_date['sent vs received'] == 'sent SMS'))

		###  total_len_recv  ###
		mask = df_date['sent vs received'] == 'received SMS'
		df_new.loc[date, 'tot_len_recv'] = int(np.sum(df_date['message length'][mask]))

		###  total_len_sent  ###
		mask = df_date['sent vs received'] == 'sent SMS'
		df_new.loc[date, 'tot_len_sent'] = int(np.sum(df_date['message length'][mask]))

		###  n_contact_only_recv, n_contact_only_sent, n_contact_recv_and_sent  ###
		date_grouped = df_date.groupby(['hashed phone number', 'sent vs received'])

		# hard code sent/recv, since there could only be one (or even none) for a given day
		contact_counts = pd.DataFrame(np.nan,
									  index=df_date['hashed phone number'].unique(),
									  columns=['received SMS', 'sent SMS'])
		for (contact, direction), inds in date_grouped.groups.items():
			contact_counts.loc[contact, direction] = len(inds)
		contacted = ~ contact_counts.isnull()  # True means there is contact in the direction of the column

		df_new.loc[date, 'n_contact_only_recv'] = np.sum(np.logical_and(contacted['received SMS'],
																		~ contacted['sent SMS']))
		df_new.loc[date, 'n_contact_only_sent'] = np.sum(np.logical_and(~ contacted['received SMS'],
																		contacted['sent SMS']))
		df_new.loc[date, 'n_contact_recv_and_sent'] = np.sum(np.logical_and(contacted['received SMS'],
																			contacted['sent SMS']))

	# enforce types
	df_new = df_new.astype(feature_dtypes)

	if verbose>1:
		print(df_new.head(), file=sys.stderr)
	assert not df_new.isnull().any().any()
	if verbose>0:
		print("Null report for transformed data:\n", df_new.isnull().sum(axis=0), file=sys.stderr)

	df_new.index.name = 'datetime'

	# resolve between true zero vs NaN
	df_new = df_new.set_index(pd.to_datetime(df_new.index).tz_localize('tzlocal()'))
	df_new = resolve_zero_vs_nan(df_new, meta)

	return df_new

	############################################
	###  Save features as stand-alone files  ###
	############################################

	# overwrite features currently in directory
	# for key in df_new.keys():
	# 	feature = df_new[key]
	# 	feature.index.name = 'date'
	# 	feature.to_csv(os.path.join(root_savedir, 'textsLog-{}.csv'.format(key)),
	# 				   index=True, header=True)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(usage='$0 input-file output-file 1>progress 2>warning_msg',
									 description='This program processes features, use "-" for STDIN/STDOUT')
	parser.add_argument('input_fn', help='positional argument')
	parser.add_argument('output_fn', help='positional argument')
	parser.add_argument('--verbose', '-v', default=1, type=int, help='verbose level')
	# nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt = parser.parse_args()
	globals().update(vars(opt))

	meta = json.load(Open(os.path.dirname(input_fn) + '/meta.json.gz'))
	df = proc(pd.read_csv(Open(input_fn)), meta['textsLog'])
	Open(output_fn, 'w').write(df.to_csv()) if output_fn == '-' else df.to_csv(output_fn)
