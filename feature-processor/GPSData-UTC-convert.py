#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 19:33:14 2019
@author: Dell
"""

import pandas as pd
import os, sys
from os.path import join
from tzlocal import get_localzone

if len(sys.argv)<3:
    print("Usage: $0 input-path output-path")
    sys.exit(1)

path = sys.argv[1]
output_path = sys.argv[2]
all_files = os.listdir(path)

os.makedirs(output_path, exist_ok=True)

for f in all_files:
    data = pd.read_csv(join(path, f))
    if (len(data) != 0):
        filename = str(pd.to_datetime(float(f[:-4]) * 1e6) + pd.to_timedelta('8:00:00'))
        filename = filename[:13] + '_00_00.csv'
        data['UTC time'] = pd.to_datetime(data['timestamp']*1e6) + pd.to_timedelta('8:00:00') ## converting milliseconds to nanoseconds + 8:00:00 for SG time
        data['UTC time'] = data['UTC time'].dt.strftime('%Y-%m-%d%T%h:%M:%s')
        data['UTC time'] = data['UTC time'].str.pad(29, side='right', fillchar='0')
        data['UTC time'] = data['UTC time'].str.slice(0, -6).str.replace(' ', 'T')
        data = data[['timestamp', 'UTC time', 'latitude', 'longitude', 'altitude', 'accuracy']]
        data.to_csv(join(output_path, filename), index=False)
