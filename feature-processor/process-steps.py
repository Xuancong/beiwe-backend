#!/usr/bin/env python3
# coding=utf-8

import os, sys, argparse, re, json
from utils import *


def proc(fn, meta=None):
	# convert timestamp to datetime and set as index
	df = load_and_preprocess(fn)

	# 1. hourly features
	dfg_hour = df.groupby(pd.Grouper(freq='1H'))
	new_df_hourly = create_frame_from_groupby(dfg_hour, 'StepsCount', {'n_steps':'sum', 'steps_per_minute':'mean', 'wearing_mins':'count', 'max_steps_minute':'max'})
	new_df_hourly['n_steps'] = new_df_hourly.apply(lambda r: r['n_steps'] if r['wearing_mins']>0 else nan, axis=1)

	if verbose>1:
		print("Hourly DF:\n", new_df_hourly.head(), file=sys.stderr)
	if verbose>0:
		print("Null report for transformed hourly data:\n", new_df_hourly.isnull().sum(axis=0), file=sys.stderr)


	# 2. daily features
	dfg_daily = df.groupby(pd.Grouper(freq='1D'))
	new_df_daily = create_frame_from_groupby(dfg_daily, 'StepsCount', {'n_steps': 'sum', 'steps_per_minute': 'mean', 'wear_minutes': 'count', 'max_steps_minute': 'max'})
	new_df_daily[['n_pos_readings', 'n_walks', 'max_walk_steps', 'mean_walk_steps', 'max_walk_mins', 'mean_walk_mins', 'steps_per_minute_walk',
				  'max_consec_minutes_gt_3_steps', 'max_consec_minutes_gt_30_steps']] = dfg_daily.apply(compute_walks)

	# set NaNs properly
	new_df_daily = new_df_daily.apply(lambda r: r if r['wear_minutes'] else nan, axis=1).assign(wear_minutes=new_df_daily['wear_minutes'])

	if verbose>1:
		print("Daily DF:\n", new_df_daily.head(), file=sys.stderr)
	if verbose>0:
		print("Null report for transformed daily data:\n", new_df_daily.isnull().sum(axis=0), file=sys.stderr)

	return new_df_daily, new_df_hourly


def start_stop_posi(s):
	S = '0' + s.replace({True:'1', False:'0'}).str.cat() + '0'
	p_begin = pd.Series([m.start() for m in re.finditer('01', S)], dtype=np.int) - 1
	p_end = pd.Series([m.start() for m in re.finditer('10', S)], dtype=np.int) - 1
	return p_begin, p_end, p_end-p_begin


def compute_walks(df):
	if df.empty:
		return pd.Series([nan] * 9, dtype = float)
	mask0 = df['StepsCount']>0
	p_begin, p_end, minutes = start_stop_posi(mask0)
	# a walk must take at least 3 consecutive minutes
	mask3min = minutes>=3
	p_begin, p_end, minutes = p_begin[mask3min], p_end[mask3min], minutes[mask3min]
	_, _, minutes3	= start_stop_posi(df['StepsCount']>3)
	_, _, minutes30	= start_stop_posi(df['StepsCount']>30)
	cumsum = df['StepsCount'].fillna(0).cumsum().to_numpy()
	walks = cumsum[p_end] - cumsum[p_begin]
	return pd.Series([mask0.sum(),		# n_pos_readings
					  walks.size,		# n_walks
					  walks.max() if walks.size else 0,			# max_walk_steps
					  walks.mean() if walks.size else 0,		# mean_walk_steps
					  minutes.max() if minutes.size else 0,		# max_walk_mins
					  minutes.mean() if minutes.size else 0,	# mean_walk_mins
					  walks.sum()/minutes.sum() if minutes.size else 0,	# steps_per_minute_walk
					  minutes3.max() if minutes3.size else 0,	# max_consec_minutes_gt_3_steps
					  minutes30.max() if minutes30.size else 0,	# max_consec_minutes_gt_30_steps
					  ], dtype=float)



if __name__ == '__main__':
	parser = argparse.ArgumentParser(usage='$0 input-file output-file 1>progress 2>warning_msg',
									 description='This program processes features, use "-" for STDIN/STDOUT')
	parser.add_argument('input_fn', help='positional argument')
	parser.add_argument('output_fn', help='positional argument')
	parser.add_argument('--verbose', '-v', default=1, type=int, help='verbose level')
	# nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt = parser.parse_args()
	globals().update(vars(opt))

	# meta = json.load(Open(os.path.dirname(input_fn) + '/meta.json.gz'))
	new_df_daily, new_df_hourly = proc(input_fn)

	if output_fn == '-':
		print(new_df_daily.to_csv(), flush=True)
		print(new_df_hourly.to_csv(), end='', flush=True)
	else:
		new_df_daily.to_csv(add_str_to_fn(output_fn, 'daily'))
		new_df_hourly.to_csv(add_str_to_fn(output_fn, 'hourly'))
