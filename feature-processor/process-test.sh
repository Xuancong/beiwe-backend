#!/usr/bin/env bash


if [ $# -lt 2 ]; then
	echo "Usage: $0 <in_folder> <out_folder> [<feature_name>/tapsLog/sleep/steps/... ...]"
	echo "<in_folder> and <out_folder> contains each user's data folders"
	exit 1
fi

mkdir -p "$2"
INPATH="`realpath \"$1\"`"
OUTPATH="`realpath \"$2\"`"

function data_exist {
	while [ "$1" ]; do
		if [ `zcat -f "$1" 2>/dev/null | wc -l` -lt 2 ]; then
			return
		fi
		shift
	done
	echo yes
}

echo -e 'Running feature extraction (\x1b[92mY\x1b[0m=Yes, \x1b[93mN\x1b[0m=No data, \x1b[1m\x1b[91mE\x1b[0m=Error):'

cd `dirname $0`
for feature_name in ${@:3}; do
	echo -n "Extracting features for $feature_name "

	for user in $INPATH/*; do
		username="`basename \"$user\"`"
		mkdir -p "$OUTPATH/$username"
		if [ `data_exist "$user/$feature_name.csv.gz"` ]; then
			./process-$feature_name.py -v 0 "$user/$feature_name.csv.gz" "$OUTPATH/$username/$feature_name.csv.gz"
			if [ "`data_exist $OUTPATH/$username/$feature_name.*csv.gz`" ]; then
				echo -ne '\x1b[92mY\x1b[0m'
			else
				echo -ne '\x1b[1m\x1b[91mE\x1b[0m'
				echo "Error in processing $user/$feature_name.csv.gz"
			fi
		else
			echo -ne '\x1b[93mN\x1b[0m'
		fi
	done
	echo
done

