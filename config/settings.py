import socket
from os import getenv

"""
To customize any of these values, append a line to config/remote_db_env.py such as:
os.environ['S3_BUCKET'] = 'bucket_name'
"""

# This is the secret key for the website. Mostly it is used to sign cookies. You should provide a
#  cryptographically secure string to this value.
FLASK_SECRET_KEY = getenv("FLASK_SECRET_KEY")

# the name of the s3 bucket that will be used to store user generated data, and backups of local
# database information.
S3_BUCKET = getenv("S3_BUCKET")
S3_ENDPOINT_URL = getenv("S3_ENDPOINT_URL")

def get_ip_address():
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	try:
		s.connect(("8.8.8.8", 80))
	except:
		pass
	return s.getsockname()[0]

# Domain name: the domain name for the Internet to reach this server, used for QR generation
DOMAIN_NAME = getenv("DOMAIN_NAME", get_ip_address())
PORT = int(getenv("PORT", "80" if getenv('USE_HTTP', '') == '1' else '443'))
SERVER_URL = getenv("SERVER_URL", get_ip_address() + ':%d'%PORT)

# The interface that HTTP(S) should be hosted on
HOST = getenv("HOST", get_ip_address())

# A list of email addresses that will receive error emails. This value must be a
# comma separated list; whitespace before and after addresses will be stripped.
SYSADMIN_EMAILS = getenv("SYSADMIN_EMAILS")

# Sentry DSNs
SENTRY_ANDROID_DSN = getenv("SENTRY_ANDROID_DSN")
SENTRY_DATA_PROCESSING_DSN = getenv("SENTRY_DATA_PROCESSING_DSN")
SENTRY_ELASTIC_BEANSTALK_DSN = getenv("SENTRY_ELASTIC_BEANSTALK_DSN")
SENTRY_JAVASCRIPT_DSN = getenv("SENTRY_JAVASCRIPT_DSN")

# Production/Staging: set to "TRUE" if staging
IS_STAGING = getenv("IS_STAGING") or "PRODUCTION"

# S3 bucket access
S3_ACCESS_CREDENTIALS_USER = getenv("S3_ACCESS_CREDENTIALS_USER")
S3_ACCESS_CREDENTIALS_KEY = getenv("S3_ACCESS_CREDENTIALS_KEY")

S3_REGION_NAME = getenv("S3_REGION_NAME", "us-east-1")

# Secrets Manager access
SECRETS_MANAGER_ACCESS_CREDENTIALS_USER = getenv("SECRETS_MANAGER_ACCESS_CREDENTIALS_USER")
SECRETS_MANAGER_ACCESS_CREDENTIALS_KEY = getenv("SECRETS_MANAGER_ACCESS_CREDENTIALS_KEY")

SECRETS_MANAGER_REGION_NAME = getenv("SECRETS_MANAGER_REGION_NAME", "ap-southeast-1")

PCA_ARN = getenv("PCA_ARN")

SESSION_EXPIRE_IN_SECONDS = 99999
OTP_PRUNE_INTERVAL = 24*3600
OTP_PRUNE_EXPIRY = 24*3600	# must be >= the otp_expire_in_sec of every study

# background color indicator for date elapse in seconds
DEFAULT_DATE_ELAPSE_COLOR = '"lime" if elapse<30*3600 else ("orange" if elapse<72*3600 else "red")'

DEFAULT_DAILY_CHECK_FORMULA = """
N_expect = round(24*3600/study.device_settings.create_new_data_files_frequency_seconds)
df1 = (df.groupby(pd.Grouper(freq='1D')).sum().fillna(0).iloc[-study.study_cycle_days:, :]/N_expect).clip(0, 1).mean(axis=1)
today = pd.Timestamp.today('tzlocal()').floor('D')
out = ''
for date, val in df1.items():
	level = round(val*255)
	color = '#%X%X%X'%(255-level, level, 0)
	char = 'T' if date==today else 'X'
	out += f'<font color={color}>{char}</font>'
print(out)
"""

DEFAULT_DATA_COMPLETION_EVALUATION = """
# To be put into study settings
df.index.name = 'Date\\Feature'
df_report = pd.DataFrame(index = df.index)
for fea in ['accessibility', 'gps', 'calls', 'sms', 'taps', 'usage', 'pedometer', 'power_state', 'sociability_call', 'sociability_msg']:
	if settings[fea]:
		if fea not in df.columns:
			df[fea] = 0
		th_min = round(3600*24/settings['create_new_data_files_frequency_seconds']-1)
		df_report[fea] = np.minimum(df[fea]/th_min, 1.0)

for fea in ['accelerometer', 'gyro', 'magnetometer']:
	if settings[fea]:
		if fea not in df.columns:
			df[fea] = 0
		th_min = round(3600*24/(settings[f'{fea}_on_duration_seconds']+settings[f'{fea}_off_duration_seconds'])-1)
		df_report[fea] = np.minimum(df[fea]/th_min, 1.0)

if settings['ambientlight']:
	if 'ambientlight' not in df.columns:
		df['ambientlight'] = 0
	th_min = round(3600*24/(settings['ambientlight_interval_seconds'])-1)
	df_report['ambientlight'] = np.minimum(df['ambientlight']/th_min, 1.0)

df_report['Average'] = df_report.mean(axis=1)
df_report = df_report[['Average']+df_report.columns.to_list()[:-1]]
df_summary = df_report.mean()
"""

DEFAULT_DATA_COMPLETION_REPORT = """
<style>
#canvas{
	background: #ffffff;
	box-shadow:5px 5px 5px #ccc;
	border:5px solid #eee;
	margin-bottom:10px;
}
th, td {
	padding-left: 5px;
	padding-right: 5px;
}
</style>	

<body>
<canvas id="canvas" height="400" width="650" > </canvas><br>
<a onclick="toggle_details()"><u>Toggle display detailed completion report</u></a>
<div id="detailed_table" style="display:none">
{{ print_dataframe(df_report)|safe }}<br>
{{ print_dataframe(df)|safe }}
</div>
</body>

<script type="text/javascript">
var df1 = {{ df_summary.to_dict().__str__()|safe }};
var dc1 = {{ daily_chart.__str__()|safe }};

function color_code(val) {
	if(isNaN(val)) return '#000';
	return 'rgb('+Math.round(255*(1-val))+','+Math.round(255*val)+',0)';
}

function draw_completion_chart() {
	var canvas = document.getElementById("canvas");
	var ctx = canvas.getContext("2d");
	ctx.fillStyle = "#000;"
	
	var rowSize = 20;
	var margin = 10;
	var sep = 10;
	var per = 60;
	var fea_right_align = margin+120;
	var header = "Data Completion Rate";
	var barSize = canvas.width-fea_right_align-sep-margin-per

	// adjust canvas height
	canvas.height = margin*4 + rowSize*(Object.keys(df1).length+2+Object.keys(dc1).length);

	// START: draw chart header
	ctx.textBaseline = "top"
	ctx.textAlign = "center";
	ctx.font = "bold 16pt Arial";
	ctx.fillText(header, canvas.width/2, margin);
	
	// draw the 1st row for overall rate
	ctx.font = "bold 14pt Helvetica";
	ctx.textAlign = "end";
	var y = margin*4-2;
	var overall = Object.values(df1);
	overall = overall.reduce((a,b) => (a+b)) / overall.length;
	ctx.fillStyle = "#000";
	ctx.fillText("Overall", fea_right_align, y);
	ctx.fillStyle = "#7F7F7F";
	ctx.lineWidth = 3;
	ctx.strokeRect(fea_right_align+sep, y, barSize, rowSize*0.8);
	ctx.fillRect(fea_right_align+sep, y, barSize, rowSize*0.8);
	ctx.fillStyle = (overall<0.6?"#FF0000":(overall<0.8?"#FFA500":"#00FF00"));
	ctx.fillRect(fea_right_align+sep, y, barSize*overall, rowSize*0.8);
	ctx.fillText((overall*100).toFixed(1)+'%', canvas.width, y);
	y += rowSize+2;
	
	// draw daily chart
	ctx.font = "12pt Helvetica";
	for(var month in dc1){
		var x = 0;
		ctx.fillStyle = "#000";
		ctx.fillText(month, fea_right_align, y);
		for(var day_val of dc1[month]){
			ctx.fillStyle = color_code(day_val);
			// alert(ctx.fillStyle);
			ctx.fillRect(fea_right_align+sep+barSize*x/31, y, barSize*0.8/31, rowSize*0.8);
			x += 1;
		}
		y += rowSize;
	}
	y += rowSize
	
	// iterate over every feature
	for(var fea in df1){
		ctx.fillStyle = "#000";
		ctx.fillText(fea, fea_right_align, y);
		ctx.fillStyle = "#7F7F7F";
		ctx.fillRect(fea_right_align+sep, y, barSize, rowSize*0.8);
		ctx.fillStyle = (df1[fea]<0.6?"#FF0000":(df1[fea]<0.8?"#FFA500":"#00FF00"));
		ctx.fillRect(fea_right_align+sep, y, barSize*df1[fea], rowSize*0.8);
		ctx.fillText((df1[fea]*100).toFixed(1)+'%', canvas.width, y);
		y += rowSize;
	}
}

var detailed_table = document.getElementById("detailed_table");
function toggle_details(){
	detailed_table.style.display = (detailed_table.style.display=="none"?"block":"none");
}

draw_completion_chart();
</script>
"""
