from os import getenv
from config.study_constants import *
from config.settings import *

### Environment settings ###
# All settings here can be configured by setting an environment variable, or by editing the default value

# To customize any of these values, use the following pattern.
# DEFAULT_S3_RETRIES = getenv("DEFAULT_S3_RETRIES") or 10
# Note that this file is _not_ in the gitignore.

# Networking
# This value is used in libs.s3, does what it says.
DEFAULT_S3_RETRIES = getenv("DEFAULT_S3_RETRIES") or 1

# File processing directives
# NOTE: these numbers were determined through trial and error on a C4 Large AWS instance.
# Used in data download and data processing, base this on CPU core count.
CONCURRENT_NETWORK_OPS = getenv("CONCURRENT_NETWORK_OPS") or 10
# Used in file processing, number of files to be pulled in and processed simultaneously.
# Higher values reduce s3 usage, reduce processing time, but increase ram requirements.
FILE_PROCESS_PAGE_SIZE = getenv("FILE_PROCESS_PAGE_SIZE") or 250

# This string will be printed into non-error hourly reports to improve error filtering.
DATA_PROCESSING_NO_ERROR_STRING = getenv(
	"DATA_PROCESSING_NO_ERROR_STRING") or "2HEnBwlawY"

# The number of minutes after which a queued celery task will be invalidated.
# (this is not a timeout, it only invalidates tasks that have not yet run.)
CELERY_EXPIRY_MINUTES = getenv("CELERY_EXPIRY_MINUTES") or 4
CELERY_ERROR_REPORT_TIMEOUT_SECONDS = getenv(
	"CELERY_ERROR_REPORT_TIMEOUT_SECONDS") or 60 * 15

## Data streams and survey types ##create_new_patient_hopes
ALLOWED_EXTENSIONS = {'csv', 'json', 'mp4', "wav", 'txt', 'jpg'}
PROCESSABLE_FILE_EXTENSIONS = [".csv", ".mp4", ".wav"]

# Constants for for the keys in data_stream_to_s3_file_name_string
ACCELEROMETER = "accelerometer"
ACCESSIBILITY = "accessibility"
AMBIENTLIGHT = "ambientlight"
BLUETOOTH = "bluetooth"
BLUETOOTH_STATE = "bluetooth_state"
CALL_LOG = "calls"
GPS = "gps"
GYROMETER = "gyro"
MAGNETOMETER = "magnetometer"
PEDOMETER = "pedometer"
POWER_STATE = "power_state"
SMS_LOG = "sms"
SOCIABILITY_CALL = "sociability_call"
SOCIABILITY_MSG = "sociability_msg"
TAPS = "taps"
USAGE = "usage"
VOICE_RECORDING = "audio_recordings"
IMAGE_FILE = "image_survey"
WIFI = "wifi"
PROXIMITY = "proximity"
DEVICEMOTION = "devicemotion"
REACHABILITY = "reachability"
IDENTIFIERS = "identifiers"
ANDROID_LOG_FILE = "app_log"
IOS_LOG_FILE = "ios_log"
SURVEY_ANSWERS = "survey_answers"
SURVEY_TIMINGS = "survey_timings"

# File names that will be checked for data presence
CHECKABLE_FILES = ['accel', 'accessibilityLog', 'callLog', 'gyro', 'gps', 'magnetometer', 'pedometer',
                   'light', 'powerState', 'tapsLog', 'sociabilityCallLog', 'sociabilityLog', 'textsLog', 'usage']
ALLOW_EMPTY_FILES = {'callLog', 'textsLog', 'sociabilityCallLog', 'sociabilityLog'}

# All study parameters that will not be pushed to the participants' phones
ALL_STUDY_SETTINGS = [
	["study_cycle_days", 30],
	["date_elapse_color", DEFAULT_DATE_ELAPSE_COLOR],
	["daily_check_formula", DEFAULT_DAILY_CHECK_FORMULA],
	["external_dashboards", ''],
	["otp_expire_in_sec", 86400],
	["otp_email_title", 'Authentication for Study Registration'],
	["otp_email_content",
	 '<html>Thank you for registration!<br>Your OTP token is: <font size="+2" style="color:red"><b><OTP></b></font><br>It will last for <DURATION></html>'],
	["registration_allow_list", ''],
	["allow_same_qr_same_phone_rereg", True],
	["allow_same_qr_diff_phone_rereg", True],
	["data_completion_eval_formulae", DEFAULT_DATA_COMPLETION_EVALUATION],
	["data_completion_report_template", DEFAULT_DATA_COMPLETION_REPORT],
]

# All device parameters that will be pushed to the participants' phones
ALL_DEVICE_SETTINGS = [
	[[CALL_LOG, True], [SMS_LOG, True]],
	[[ACCELEROMETER, True], ["accelerometer_off_duration_seconds", 590], ["accelerometer_on_duration_seconds", 10]],
	[[AMBIENTLIGHT, True], ["ambientlight_interval_seconds", 300]],
	[[GPS, True], ["use_gps_fuzzing", True], ["gps_off_duration_seconds", 0], ["gps_on_duration_seconds", 0]],
	[[BLUETOOTH, False], ["bluetooth_on_duration_seconds", 60], ["bluetooth_total_duration_seconds", 300], ["bluetooth_global_offset_seconds", 0]],
	[[GYROMETER, True], ["gyro_off_duration_seconds", 590], ["gyro_on_duration_seconds", 10]],
	[[MAGNETOMETER, True], ["magnetometer_off_duration_seconds", 590], ["magnetometer_on_duration_seconds", 10]],
	[[PEDOMETER, True], ["pedometer_off_duration_seconds", 0], ["pedometer_on_duration_seconds", 30 * 60]],
	[[DEVICEMOTION, True], ["devicemotion_off_duration_seconds", 590], ["devicemotion_on_duration_seconds", 10]],
	[[USAGE, True], ["usage_update_interval_seconds", 3600]],
	[[WIFI, False], ["wifi_log_frequency_seconds", 3600]],
	[[POWER_STATE, True]],
	[[BLUETOOTH_STATE, False]],
	[[TAPS, True]],
	[[ACCESSIBILITY, False]],
	[[SOCIABILITY_CALL, False]],
	[[SOCIABILITY_MSG, False]],
	[[PROXIMITY, True]],
	[[REACHABILITY, True]],
	[["allow_upload_over_cellular_data", True], ["use_anonymized_hashing", True], ["use_compression", True]],
	[["phone_number_length", 8], ["primary_care", PRIMARY_CARE_PHONE]],
	[["check_for_new_surveys_frequency_seconds", 7200], ["create_new_data_files_frequency_seconds", 3600], ["seconds_before_auto_logout", 0],
	 ["upload_data_files_frequency_seconds", 3600], ["voice_recording_max_time_length_seconds", 240]],
	[["about_page_text", ABOUT_PAGE_TEXT], ["mainpage_title", MAINPAGE_TITLE],
	 ["consent_form_text", CONSENT_FORM_TEXT], ["survey_submit_success_toast_text", SURVEY_SUBMIT_SUCCESS_TOAST_TEXT]]
]

# The format that dates should be in throughout the codebase
API_TIME_FORMAT = "%Y-%m-%dT%H:%M:%S"
"""1990-01-31T07:30:04 gets you jan 31 1990 at 7:30:04am
   human string is YYYY-MM-DDThh:mm:ss """

# Chunks
# This value is in seconds, it sets the time period that chunked files will be sliced into.
CHUNK_TIMESLICE_QUANTUM = 3600
# the name of the s3 folder that contains chunked data
CHUNKS_FOLDER = "CHUNKED_DATA"
PIPELINE_FOLDER = "PIPELINE_DATA"

SHORT_NAME_MAPPING = {
	ACCELEROMETER: 'acc',
	ACCESSIBILITY: 'acb',
	AMBIENTLIGHT: 'amb',
	BLUETOOTH: 'blu',
	BLUETOOTH_STATE: 'bls',
	CALL_LOG: 'cal',
	GPS: 'gps',
	GYROMETER: 'gyr',
	MAGNETOMETER: 'mag',
	PEDOMETER: 'ped',
	POWER_STATE: 'pow',
	SMS_LOG: 'sms',
	SOCIABILITY_CALL: 'soc',
	SOCIABILITY_MSG: 'som',
	TAPS: 'tap',
	USAGE: 'use',
	WIFI: 'wif',
	PROXIMITY: 'prx'
}
SHORT_NAME_INV_MAPPING = {v: k for k, v in SHORT_NAME_MAPPING.items()}
assert len(SHORT_NAME_MAPPING.values())==len(SHORT_NAME_INV_MAPPING.values()), 'SHORT_NAME_MAPPING is not unique'

ALL_DATA_STREAMS = [ACCELEROMETER,
                    AMBIENTLIGHT,
                    ACCESSIBILITY,
                    BLUETOOTH,
                    BLUETOOTH_STATE,
                    CALL_LOG,
                    GPS,
                    IDENTIFIERS,
                    ANDROID_LOG_FILE,
                    POWER_STATE,
                    SURVEY_ANSWERS,
                    SURVEY_TIMINGS,
                    TAPS,
                    SOCIABILITY_CALL,
                    SMS_LOG,
                    USAGE,
                    VOICE_RECORDING,
                    WIFI,
                    PROXIMITY,
                    GYROMETER,
                    PEDOMETER,
                    MAGNETOMETER,
                    DEVICEMOTION,
                    REACHABILITY,
                    IOS_LOG_FILE,
                    IMAGE_FILE]

SURVEY_DATA_FILES = [SURVEY_ANSWERS, SURVEY_TIMINGS]

UPLOAD_FILE_TYPES = {
	ACCELEROMETER,
	ACCESSIBILITY,
	AMBIENTLIGHT,
	BLUETOOTH,
	BLUETOOTH_STATE,
	CALL_LOG,
	DEVICEMOTION,
	GPS,
	GYROMETER,
	PEDOMETER,
	TAPS,
	SOCIABILITY_CALL,
	ANDROID_LOG_FILE,
	MAGNETOMETER,
	POWER_STATE,
	REACHABILITY,
	SURVEY_ANSWERS,
	SURVEY_TIMINGS,
	SMS_LOG,
	USAGE,
	VOICE_RECORDING,
	WIFI,
	PROXIMITY,
	IOS_LOG_FILE,
	IMAGE_FILE
}


def data_stream_to_s3_file_name_string(data_type):
	"""Maps a data type to the internal string representation used throughout the codebase.
		(could be a dict mapping, but it is fine) """
	if data_type == IDENTIFIERS:
		return "identifiers"
	if data_type not in UPLOAD_FILE_TYPES:
		raise Exception("unknown data type: %s" % data_type)
	return data_type


CHUNKABLE_FILES = {
	ACCELEROMETER,
	AMBIENTLIGHT,
	ACCESSIBILITY,
	BLUETOOTH,
	BLUETOOTH_STATE,
	CALL_LOG,
	GPS,
	TAPS,
	SOCIABILITY_CALL,
	IDENTIFIERS,
	ANDROID_LOG_FILE,
	POWER_STATE,
	SURVEY_TIMINGS,
	SMS_LOG,
	USAGE,
	WIFI,
	PROXIMITY,
	GYROMETER,
	PEDOMETER,
	MAGNETOMETER,
	DEVICEMOTION,
	REACHABILITY,
	IOS_LOG_FILE
}

# Survey Question Types
FREE_RESPONSE = "free_response"
CHECKBOX = "checkbox"
RADIO_BUTTON = "radio_button"
SLIDER = "slider"
INFO_TEXT_BOX = "info_text_box"

ALL_QUESTION_TYPES = {FREE_RESPONSE,
                      CHECKBOX,
                      RADIO_BUTTON,
                      SLIDER,
                      INFO_TEXT_BOX}

NUMERIC_QUESTIONS = {RADIO_BUTTON,
                     SLIDER,
                     FREE_RESPONSE}

# Free Response text field types (answer types)
FREE_RESPONSE_NUMERIC = "NUMERIC"
FREE_RESPONSE_SINGLE_LINE_TEXT = "SINGLE_LINE_TEXT"
FREE_RESPONSE_MULTI_LINE_TEXT = "MULTI_LINE_TEXT"

TEXT_FIELD_TYPES = {FREE_RESPONSE_NUMERIC,
                    FREE_RESPONSE_SINGLE_LINE_TEXT,
                    FREE_RESPONSE_MULTI_LINE_TEXT}

# Comparators
COMPARATORS = {"<",
               ">",
               "<=",
               ">=",
               "==",
               "!="}

NUMERIC_COMPARATORS = {"<",
                       ">",
                       "<=",
                       ">="}

SET_OPERATORS = {"∋", "∌"}

# Password Check Regexes
SYMBOL_REGEX = "[^a-zA-Z0-9]"
LOWERCASE_REGEX = "[a-z]"
UPPERCASE_REGEX = "[A-Z]"
NUMBER_REGEX = "[0-9]"
PASSWORD_REQUIREMENT_REGEX_LIST = [
	SYMBOL_REGEX, LOWERCASE_REGEX, UPPERCASE_REGEX, NUMBER_REGEX]

DEVICE_IDENTIFIERS_HEADER = "patient_id,MAC,phone_number,device_id,device_os,os_version,product,brand,hardware_id,manufacturer,model,beiwe_version\n"

# Encryption constants
ASYMMETRIC_KEY_LENGTH = 2048  # length of private/public keys
ITERATIONS = 1000  # number of SHA iterations in password hashing
RSA_METHOD = "RSA"
ECC_METHOD = "ECC"
ED25519_METHOD = "ED25519"
ENCRYPT_METHOD = getenv('ENCRYPT_METHOD', ECC_METHOD)

# Error reporting send-from emails
E500_EMAIL_ADDRESS = 'e500_error@{}'.format(DOMAIN_NAME)
OTHER_EMAIL_ADDRESS = 'telegram_service@{}'.format(DOMAIN_NAME)
