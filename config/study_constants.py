import json
import string

ABOUT_PAGE_TEXT = (
    'The HOPES application runs on your phone and helps researchers collect information about your '
    'behaviors. HOPES may ask you to fill out short surveys or to record your voice. It may collect '
    'information about your location (using phone GPS) and how much you move (using phone '
    'accelerometer). HOPES may also monitor how much you use your phone for calling and texting and '
    'keep track of the people you communicate with. Importantly, HOPES never records the names or '
    'phone numbers of anyone you communicate with. While it can tell if you call the same person '
    'more than once, it does not know who that person is. HOPES also does not record the content of '
    'your text messages or phone calls. HOPES may keep track of the different Wi-Fi networks and '
    'Bluetooth devices around your phone, but the names of those networks are replaced with random '
    'codes.\n\nAlthough HOPES collects large amounts of data, the data is processed to protect your '
    'privacy. This means that it does not know your name, your phone number, or anything else that '
    'could identify you. HOPES only knows you by an identification number. Because HOPES does not '
    'know who you are, it cannot communicate with your study team if you are ill or in danger. '
    'Researchers will not review the data HOPES collects until the end of the study. To make it '
    'easier for you to connect with your study team, the \'Call my Study Team\' button appears at the '
    'bottom of every page.'
    # '\n\n was conceived and designed by Dr. Jukka-Pekka \'JP\' Onnela at '
    # 'the Harvard T.H. Chan School of Public Health. Development of the Beiwe smartphone application '
    # 'and data analysis software is funded by NIH grant 1DP2MH103909-01 to Dr. Onnela. The smartphone '
    # 'application was built by Zagaran, Inc., in Cambridge, Massachusetts.'
)

MAINPAGE_TITLE = '"Welcome to %s"%study.name'

PRIMARY_CARE_PHONE = '92341641'

CONSENT_FORM_TEXT = ''

SURVEY_SUBMIT_SUCCESS_TOAST_TEXT = (
    'Thank you for completing the survey. The study team will not see your answers immediately, so '
    'if you need help or are thinking about harming yourself, please contact your study team. You '
    'can also press the \'Call My Study Team\' button.'
)

DEFAULT_CONSENT_SECTIONS = {
    "welcome": {"text": "", "more": ""},
    "data_gathering": {"text": "", "more": ""},
    "privacy": {"text": "", "more": ""},
    "data_use": {"text": "", "more": ""},
    "time_commitment": {"text": "", "more": ""},
    "study_survey": {"text": "", "more": ""},
    "study_tasks": {"text": "", "more": ""},
    "withdrawing": {"text": "", "more": ""}
}
DEFAULT_CONSENT_SECTIONS_JSON = json.dumps(DEFAULT_CONSENT_SECTIONS)

AUDIO_SURVEY_SETTINGS = {
    'audio_survey_type': 'compressed',
    'bit_rate': 64000,
    'sample_rate': 44100,
}

OBJECT_ID_ALLOWED_CHARS = string.ascii_uppercase + string.ascii_lowercase + string.digits
EASY_ALPHANUMERIC_CHARS = string.ascii_lowercase + '0123456789'
DATA_COMPLETION_REPORT ="""<style type="text/css">
th, td {
  padding-left: 5px;
  padding-right: 5px;
}
</style>
{{ df.to_html()|safe }}"""